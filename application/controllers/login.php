<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index($msg = NULL) {


        $data['user_data'] = $this->session->userdata('user');
        $department = $data['user_data']["department"];

        if (isset($department)) {
            redirect('home');
        } else {

            $data['msg'] = $msg;
            $this->load->view('login', $data);
        }
    }

    public function process() {


        $username = $this->input->post('username');
        $password = $this->input->post('password');
        // Validate the user can login

        $result = $this->login_model->validate($username, $password);
        // Now we verify the result
        if (!$result) {

            if (empty($password)) {
                $msg = '<font color=red>Invalid username and/or password.</font><br />';
                $this->index($msg);
            } else {

                $msg = '<font color=red>Invalid username and/or password.</font><br />';
                $this->index($msg);
            }
        } else {
            // If user did validate, 
            // Send them to members area
            redirect('home');
        }
    }

    public function reset_password() {
        $email = $this->input->post('email');
        if (!empty($email)) {
            $this->password_reset($email);
        } else if (empty($email)) {
            $msg = '<div class="alert alert-danger text-center"> Please provice your email address or  try again later</div>';

            $this->index($msg);
        }
    }

    public function check_mail($email) {
        $sql_1 = "Select * from employee where email='$email'";
        $query_1 = $this->db->query($sql_1);
        foreach ($query_1->result() as $value) {
            $e_mail = $value->email;
            if (empty($email)) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    public function password_reset($email) {
        $this->db->trans_start();
        $check_mail = $this->check_mail($email);
        if (!$check_mail) {
            $msg = '<div class="alert alert-danger text-center"> The email does not exist in the  system </div>';

            $this->index($msg);
        } else {



            $sql_1 = "Select * from employee where email='$email'";
            $query_1 = $this->db->query($sql_1);
            foreach ($query_1->result() as $value) {
                $title = $value->title;
                $f_name = $value->f_name;
                $s_name = $value->s_name;
                $o_name = $value->o_name;
                $e_mail = $value->email;
                $employee_id = $value->id;

                $employee_name = $title . ': ' . $f_name . ' ' . $s_name . ' ' . $o_name;
                $base_url = $this->config->item('base_url');
                //configure email settings
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = 'ssl://smtp.gmail.com';
                $config['smtp_port'] = '465';
                $config['smtp_user'] = 'harrisdindisamuel@gmail.com'; // email id
                $config['smtp_pass'] = 'ANDROIDFROYO8750'; // email password
                $config['mailtype'] = 'html';
                $config['wordwrap'] = TRUE;
                $config['charset'] = 'iso-8859-1';
                $config['newline'] = "\r\n"; //use double quotes here
                $this->email->initialize($config);
                $from_email = "noreply@uniqueloo.co.ke";
                $name = "Unique Loo System";
                $subject = "Account Password Reset";
                $random_key = $this->generate_random_key($employee_id);

                $mail_body = '
                                                                           ---------------------
                                                                                Hey :' . $employee_name . '!
                                                                                    <fieldset>
                                                                                We currently received a request for resetting your UNIQUE LOO  ACCOUNT Password. You can reset your UNIQUE LOO  Personal Account Password 
                                                                                through the link below:
                                                                                <hr>
                                                                                ------------------------
                                                                                Please click this link to activate your account:<a href="' . $base_url . 'login/reset_p/' . $random_key . '">Reset</a>
                                                                                

                                                                                ------------------------ ';



                //send mail
                $this->email->from($from_email, $name);
                $this->email->to($e_mail);
                $this->email->subject($subject);
                $this->email->message($mail_body);
                if ($this->email->send()) {
                    // mail sent



                    $msg = '<div class="alert alert-success text-center">Your mail has been sent successfully!</div>';

                    $this->index($msg);
                } else {
                    //error

                    $msg = '<div class="alert alert-danger text-center">There is error in sending mail! Please try again later</div>';

                    $this->index($msg);
                }
            }
        }


        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    function generate_random_key($employee_id) {
        $random = '';

        $random_1 = chr(rand(ord('0'), ord('999')));
        $random_2 = chr(rand(ord('A'), ord('Z')));
        $stamp = date("Ymdhis");
        $randomised_stamp = rand($stamp, 0123456789);
        $randomised_stamp = str_replace("-", "", $randomised_stamp);
        $randomised_stamp = strrev(str_replace("!@#$%^&*()|:}{<>?|/+=", "_", $randomised_stamp));
        $random .= $random_1 . $randomised_stamp . $random_2;

        $this->db->trans_start();
        $data_update = array(
            'random_key' => $random
        );
        $this->db->where('id', $employee_id);
        $this->db->update('employee', $data_update);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {

            return $random;
        }
    }

    public function reset_p($msg = NULL) {

        $data['msg'] = $msg;
        $this->load->view('reset_password', $data);
    }

    public function new_password() {
        $password_1 = $this->input->post('password_1');
        $password_2 = $this->input->post('password_2');
        $random_key = $this->input->post("raondom_key");

        if ($password_1 === $password_2) {

            $new_password = $this->login_model->new_password($password_1, $random_key);
            if (!$new_password) {
                $msg = '<font color=red>Password reset unsuccessfull Please try agian later or Contact SysAdmin.</font><br />';
                $check_response = $msg;
                $response = array(
                    'response' => $check_response
                );
                echo json_encode([$response]);
                //$this->reset_p($msg);
            } else {
                $msg = '<font color=red>Password reset successfull .</font><br />';
                $check_response = $msg;
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
            }
        }
    }

}
