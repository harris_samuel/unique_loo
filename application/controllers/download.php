<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $this->load->view('welcome');
    }

    public function client_invoice() {
        $r_key = $this->uri->segment(3);
         $data['invoice_types'] = $this->get_client_details($r_key);
         $data['booked_by'] = $this->get_booked_by($r_key);
        $this->load->view('client_invoice',$data);
        
       
    }
    function get_client_details($r_key) {
        $query = $this->db->query("Select * from clnt_invoice where random_key='$r_key'");
       return $query->result();
    }
    function get_booked_by($r_key){
        $query = $this->db->query("Select concat(employee.f_name,' ', employee.s_name,' ',employee.o_name ) as booked_by , employee.phone_no from clnt_invoice inner join job_card on job_card.job_card_no = clnt_invoice.job_card_no inner join employee on employee.id = job_card.user_id where clnt_invoice.random_key='$r_key'");
       return $query->result();
    }

    public function get_user_configurations() {
        $client_configurations = $this->operations_model->get_user_configurations();
        if (empty($client_configurations)) {
            
        } else {
            echo json_encode($client_configurations);
        }
    }

    function client_info() {
        $job_card_id = $this->uri->segment(3);
        $get_job_card_id = $this->db->query("Select * from clnt_invoice where random_key='$job_card_id'");
        foreach ($get_job_card_id->result() as $value) {
            $job_card_id = $value->job_card_id;

            $client_info = $this->operations_model->client_info($job_card_id);
            if (empty($client_info)) {
                $no_data = "No Data";
                echo json_encode($no_data);
            } else {
                echo json_encode($client_info);
            }
        }
    }

    public function get_invoice_job_details() {
        $job_card_id = $this->uri->segment(3);
        $get_job_card_id = $this->db->query("Select * from clnt_invoice where random_key='$job_card_id'");
        foreach ($get_job_card_id->result() as $value) {
            $job_card_id = $value->job_card_id;
            $get_job_details = $this->operations_model->get_invoice_job_details($job_card_id);
            if (empty($get_job_details)) {
                
            } else {
                echo json_encode($get_job_details);
            }
        }
    }

    function get_job_sub_total() {
        $job_card_id = $this->uri->segment(3);

        $get_job_card_id = $this->db->query("Select * from clnt_invoice where random_key='$job_card_id'");
        foreach ($get_job_card_id->result() as $value) {
            $job_card_id = $value->job_card_id;
            $get_job_sub_total = $this->operations_model->get_job_sub_total($job_card_id);
            if (empty($get_job_sub_total)) {
                $no_data = "No Data";
                echo json_encode($no_data);
            } else {
                echo json_encode($get_job_sub_total);
            }
        }
    }

}
