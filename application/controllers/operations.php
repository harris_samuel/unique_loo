<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Operations extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();
        $data['employees_list'] = $this->get_employees();

        $data['clients'] = $this->get_clients();
        $data['parameters'] = $this->get_standards();
        // $data['asset_status_list'] = $this->operations_model->asset_status();
        $this->load->view('job_card_v', $data);
    }

    public function asset_calculator() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();
        $data['employees_list'] = $this->get_employees();

        $data['clients'] = $this->get_clients();
        $data['parameters'] = $this->get_standards();
        $data['booked_jobs'] = $this->unqouted_booked_jobs();
        // $data['asset_status_list'] = $this->operations_model->asset_status();
        $this->load->view('asset_calc_v', $data);
    }

    public function client_job_invoice() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();
        $data['employees_list'] = $this->get_employees();

        $data['clients'] = $this->get_clients();
        $data['parameters'] = $this->get_standards();
        $data['invoice_job_list'] = $this->client_job_invoice_list();
        $this->load->view('client_job_invoice_v', $data);
    }

    function job_invoice_list() {
        $job_invoice_list = $this->client_job_invoice_list();
        if (empty($job_invoice_list)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($job_invoice_list);
        }
    }

    function get_booked_jobs() {
        $get_booked_jobs = $this->unqouted_booked_jobs();
        if (empty($get_booked_jobs)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($get_booked_jobs);
        }
    }

    public function cashier() {

        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $data['asset_types'] = $this->get_asset_type();

        $data['booked_clients'] = $this->operations_model->view_booked_clients();
        $this->load->view('cashier_v', $data);
    }

    function view_booked_clients() {
        $client_list = $this->operations_model->view_booked_clients();
        if (empty($client_list)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($client_list);
        }
    }

    function view_completed_jobs() {
        $client_list = $this->operations_model->view_completed_jobs();
        if (empty($client_list)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($client_list);
        }
    }

    function my_future_booked_jobs() {
        $id = $this->session->userdata('user_id');
        $client_list = $this->operations_model->my_future_booked_jobs($id);
        if (empty($client_list)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($client_list);
        }
    }

    function other_future_booked_jobs() {
        $id = $this->session->userdata('user_id');

        $client_list = $this->operations_model->other_future_booked_jobs($id);
        if (empty($client_list)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($client_list);
        }
    }

    function my_booked_jobs() {
        $id = $this->session->userdata('user_id');
        $client_list = $this->operations_model->my_booked_jobs($id);
        if (empty($client_list)) {

            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($client_list);
        }
    }

    function other_booked_jobs() {
        $id = $this->session->userdata('user_id');

        $client_list = $this->operations_model->other_booked_jobs($id);
        if (empty($client_list)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($client_list);
        }
    }

    function my_booked_clients() {
        $id = $this->session->userdata('user_id');
        $client_list = $this->operations_model->my_booked_clients($id);
        if (empty($client_list)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($client_list);
        }
    }

    function other_booked_clients() {
        $id = $this->session->userdata('user_id');

        $client_list = $this->operations_model->other_booked_clients($id);
        if (empty($client_list)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($client_list);
        }
    }

    public function get_asset_details() {
        $this->db->trans_start();
        $asset_name = $this->uri->segment(3);
        $asset_name = urldecode($asset_name);
        $query = $this->db->query("select * from standards where parameter = '$asset_name'");
        $result = $query->result_array();
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $no_data = "Oops there was an error :( ";
            echo json_encode($no_data);
        } else {
            echo json_encode($result);
        }
    }

    public function book_client() {
        $client_id = $this->input->post('client_name');
        $delivery_date = $this->input->post('delivery_date');
        $event_date = $this->input->post('event_date');
        $location = $this->input->post('location');
        $est_location_dstnce = $this->input->post('est_location_dstnce');
        $cnfrmd_location_dstnce = $this->input->post('cnfrmd_location_dstnce');
        $time_from = $this->input->post('time_from');
        $time_to = $this->input->post('time_to');
        $edit_job_card = $this->input->post('edit_job_card');
        $edit_job_card_no = $this->input->post('edit_job_card_no');
        $booked_by_id = $this->input->post('booked_by');
        $notes = $this->input->post('notes');
        $venue = $this->input->post('venue');

        $check_client_existense = $this->operations_model->check_client_existense($client_id);
        if (empty($check_client_existense)) {
            $check_client = "Client picked does not exist in the  system, Please add cllient before booking a job card under the Client.";

            $response = array(
                'response' => $check_client
            );
            echo json_encode([$response]);
        } else
        if (!empty($check_client_existense)) {

            $job_card_checker = $this->operations_model->check_job_card_existense($client_id, $delivery_date, $event_date, $location, $est_location_dstnce, $cnfrmd_location_dstnce, $time_from, $time_to, $edit_job_card, $edit_job_card_no, $booked_by_id, $notes, $venue);
          
            if (empty($job_card_checker)) {
                $job_card_id = $this->operations_model->book_client($client_id, $delivery_date, $event_date, $location, $est_location_dstnce, $cnfrmd_location_dstnce, $time_from, $time_to, $edit_job_card, $edit_job_card_no, $booked_by_id, $notes, $venue);
                if (empty($job_card_id)) {
                    
                } else {
                    // $created_job_card_no = $this->operations_model->created_job_card_no($job_card_id);
                    $job_card_id = "$job_card_id";
                    $id = array(
                        'id' => $job_card_id
                    );
                    echo json_encode([$id]);
                }
            } else {

                $check_client = "Job Details Exist in the  System.";

                $response = array(
                    'response' => $check_client
                );
                echo json_encode([$response]);
            }
        }
    }

    public function update_job_card() {
        $client_id = $this->input->post('client_name');
        $delivery_date = $this->input->post('delivery_date');
        $event_date = $this->input->post('event_date');
        $location = $this->input->post('location');
        $est_location_dstnce = $this->input->post('est_location_dstnce');
        $cnfrmd_location_dstnce = $this->input->post('cnfrmd_location_dstnce');
        $time_from = $this->input->post('time_from');
        $time_to = $this->input->post('time_to');
        $edit_job_card = $this->input->post('edit_job_card');
        $edit_job_card_no = $this->input->post('edit_job_card_no');
        $booked_by_id = $this->input->post('booked_by_id');
        $notes = $this->input->post('notes');
        $venue = $this->input->post('venue');

        $check_client_existense = $this->operations_model->check_client_existense($client_id);
        if (empty($check_client_existense)) {
            $check_client = "Client picked does not exist in the  system, Please add cllient before booking a job card under the Client.";

            $response = array(
                'response' => $check_client
            );
            echo json_encode([$response]);
        } else
        if (!empty($check_client_existense)) {
            $job_card_id = $this->operations_model->update_job_card($client_id, $delivery_date, $event_date, $location, $est_location_dstnce, $cnfrmd_location_dstnce, $time_from, $time_to, $edit_job_card, $edit_job_card_no, $booked_by_id, $notes, $venue);
            if (empty($job_card_id)) {
                
            } else {
                // $created_job_card_no = $this->operations_model->created_job_card_no($job_card_id);
                $job_card_id = "$job_card_id";
                $id = array(
                    'id' => $job_card_id
                );
                echo json_encode([$id]);
            }
        }
    }

    function created_job_card_no() {
        $job_card_id = $this->uri->segment(3);
        $created_job_card_no = $this->operations_model->created_job_card_no($job_card_id);
        if (empty($created_job_card_no)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($created_job_card_no);
        }
    }

    function get_job_card_detail() {
        $job_card_id = $this->uri->segment(3);
        $get_job_card_detail = $this->operations_model->get_job_card_detail($job_card_id);
        if (empty($get_job_card_detail)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($get_job_card_detail);
        }
    }

    public function get_booked_assets() {
        $job_card_id = $this->uri->segment(3);
        $get_booked_assets = $this->operations_model->get_booked_assets($job_card_id);
        if (empty($get_booked_assets)) {
            
        } else {
            echo json_encode($get_booked_assets);
        }
    }

    public function save_asset_value() {
        $job_card_id = $this->input->post('job_card_id');
        $asset_name = $this->input->post('asset_name');
        $asset_price = $this->input->post('asset_value');
        $asset_quantity = $this->input->post('asset_quantity');
        $discount = $this->input->post('discount');
        $asset_no_days = $this->input->post('asset_no_days');
        $other_job_card_id = $this->input->post('other_job_card_id');
        $asset_description = $this->input->post('asset_description');
        $other_amount = $this->input->post('other_amount');
        $other_days = $this->input->post('other_days');
        $other_qty = $this->input->post('other_qty');
        $other_discount = $this->input->post('other_discount');
        $save_asset_value = $this->operations_model->save_asset_value($job_card_id, $asset_name, $asset_price, $asset_quantity, $discount, $asset_no_days, $other_job_card_id, $asset_description, $other_amount, $other_days, $other_qty, $other_discount);
    }

    public function get_job_details() {
        $job_card_id = $this->uri->segment(3);

        $get_job_details = $this->operations_model->get_job_details($job_card_id);
        if (empty($get_job_details)) {
            
        } else {
            echo json_encode($get_job_details);
        }
    }

    function get_assgnd_assets() {
        $job_card_id = $this->uri->segment(3);
        $get_assgnd_assets = $this->operations_model->get_assgnd_assets($job_card_id);
        if (empty($get_assgnd_assets)) {
            
        } else {
            echo json_encode($get_assgnd_assets);
        }
    }

    public function get_invoice_job_details() {
        $job_card_id = $this->uri->segment(3);
        $get_job_details = $this->operations_model->get_invoice_job_details($job_card_id);
        if (empty($get_job_details)) {
            
        } else {
            echo json_encode($get_job_details);
        }
    }

    public function more_job_details() {
        $job_card_id = $this->uri->segment(3);
        $get_job_details = $this->operations_model->more_job_details($job_card_id);
        if (empty($get_job_details)) {
            
        } else {
            echo json_encode($get_job_details);
        }
    }

    public function get_assigned_assets() {
        $job_card_id = $this->uri->segment(3);
        $get_assigned_assets = $this->operations_model->get_assigned_assets($job_card_id);
        if (empty($get_assigned_assets)) {
            $check_response = "No Assets Assigned";
            $get_assigned_assets = array(
                'name' => $check_response
            );
            echo json_encode([$get_assigned_assets]);
        } else {
            echo json_encode($get_assigned_assets);
        }
    }

    public function get_remaining_assets() {
        $asset_type = $this->uri->segment(3);
        $asset_type = urldecode($asset_type);
        $get_remaining_assets = $this->operations_model->get_remaining_assets($asset_type);
        if (empty($get_remaining_assets)) {


            $check_response = "No Assets Found";
            $get_remaining_assets = array(
                'name' => $check_response
            );
            echo json_encode([$get_remaining_assets]);
        } else {
            echo json_encode($get_remaining_assets);
        }
    }

    public function assign_asset() {
        $job_card_id = $this->input->post('job_card_id_v');
        $job_card_id_1 = $this->input->post('job_card_id_v_1');
        $asset_type = $this->input->post('asset_type');
        $asset_name = $this->input->post('asset_name');
        $asset_value = $this->input->post('asset_value');
        $asset_type_1 = $this->input->post('asset_type_1');
        $asset_name_1 = $this->input->post('asset_name_1');

        //  $asign_asset = $this->operations_model->assign_asset($job_card_id, $job_card_id_1, $asset_value, $asset_name_1, $asset_name, $asset_type, $asset_type_1);

        $check_assgnd_same_job = $this->operations_model->check_assgnd_same_job($asset_name, $job_card_id);
        if ($check_assgnd_same_job) {
            $check_response = "Asset exists";
            $asset_check = array(
                'asset_check' => $check_response
            );
            echo json_encode([$asset_check]);
        } else {
            $asign_asset = $this->operations_model->assign_asset($job_card_id, $job_card_id_1, $asset_value, $asset_name_1, $asset_name, $asset_type, $asset_type_1);

            $check_response = "Asset not assigned";
            $asset_check = array(
                'asset_check' => $check_response
            );
            echo json_encode([$asset_check]);
        }
    }

    //JOB CARD GENERATOR FUCNTION BEGIN
    public function last_job_card_no() {
        $this->db->trans_start();
        $query = $this->db->query("select max(id) as last_inserted_id from job_card_no");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            foreach ($query->result() as $value) {
                $last_inserted_id = $value->last_inserted_id;
                return $last_inserted_id;
            }
        }
    }

    public function job_card_no_generator() {



        $this->db->trans_start();



        $last_inserted_id = $this->last_job_card_no();
        if (empty($last_inserted_id)) {
            $new_job_card_no = "JC-1-1";
            $data_insert = array(
                'no' => $new_job_card_no
            );
            $this->db->insert('job_card_no', $data_insert);
        } else {


            $query = $this->db->query("select no as last_job_card_no from job_card_no where id = '$last_inserted_id'");
            foreach ($query->result() as $value) {
                $get_last_job_card_no = $value->last_job_card_no;

                $break_job_card_data = explode("-", $get_last_job_card_no);

                $chunk_one = $break_job_card_data[0];
                $chunk_two = $break_job_card_data[1];
                $chunk_three = $break_job_card_data[2];
                $high_value = 9999;
                $low_value = 1;
                $base_ltr = "JC";
                $seperator = "-";
                if (empty($get_last_job_card_no)) {
                    $new_job_card_no = "JC-1-1";
                } else {
                    if ($chunk_three === "999") {
                        $new_chunk_two = $chunk_two + $low_value;
                        $new_job_card_no = $base_ltr . $seperator . $new_chunk_two . $seperator . $low_value;
                    } else {
                        $new_chunk_three = $chunk_three + $low_value;
                        $new_job_card_no = $base_ltr . $seperator . $chunk_two . $seperator . $new_chunk_three;
                    }
                }

                $data_insert = array(
                    'no' => $new_job_card_no
                );
                $this->db->insert('job_card_no', $data_insert);
            }
        }


        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    //JOB CARD GENERATOR FUNCTION END






    public function get_booked_job_details() {
        $job_card_id = $this->uri->segment(3);
        $get_job_detials = $this->operations_model->get_booked_job_details($job_card_id);
        if (empty($get_job_detials)) {
            $data = "Empty";
            echo json_encode($data);
        } else {
            echo json_encode($get_job_detials);
        }
    }

    function invoice_client() {
        $job_card_no = $this->uri->segment(3);
        $generate_invoice = $this->operations_model->invoice_client($job_card_no);

        if (empty($generate_invoice)) {
            $generate_invoice = "Failed ...Please consult the  Help Desk.";
            echo json_encode($generate_invoice);
        } else {
            $this->generate_random_key($job_card_no);
            $generate_invoice = "$generate_invoice";
            $generate_invoice = array(
                'invoice_no' => $generate_invoice
            );
            echo json_encode([$generate_invoice]);
        }
    }

    function generate_invoice() {
        $job_card_id = $this->uri->segment(3);
        $generate_invoice = $this->operations_model->generate_invoice($job_card_id);
        if (empty($generate_invoice)) {
            $check_response = "Failed! Please contact help desk";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            $check_response = $generate_invoice;
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        }
    }

    function generate_random_key($job_card_no) {
        $this->db->trans_start();
        $query = $this->db->query("Select * from job_card where id = '$job_card_no' LIMIT 1");
        foreach ($query->result() as $value) {
            $id = $value->id;
            $job_card_no = $value->job_card_no;
            $invoice_no = $value->invoice_no;
            $client_id = $value->clnt_id;


            $query_clnt_details = $this->db->query("Select * from client where id = '$client_id'");
            foreach ($query_clnt_details->result() as $value) {
                $client_name = $value->name;
                $client_email = $value->email;






                $random = '';

                $random_1 = chr(rand(ord('0'), ord('999')));
                $random_2 = chr(rand(ord('A'), ord('Z')));
                $stamp = date("Ymdhis");
                $randomised_stamp = rand($stamp, 0123456789);
                $randomised_stamp = str_replace("-", "", $randomised_stamp);
                $randomised_stamp = strrev(str_replace("!@#$%^&*()|:}{<>?|/+=", "_", $randomised_stamp));
                $random .= $random_1 . $randomised_stamp . $random_2;
                $today = date("Y-m-d H:i:s");
                $data_insert = array(
                    'job_card_id' => $id,
                    'job_card_no' => $job_card_no,
                    'invoice_no' => $invoice_no,
                    'random_key' => $random,
                    'date_added' => $today
                );
                $this->db->insert('clnt_invoice', $data_insert);

                $random_key = $random;
                $base_url = $this->config->item('base_url');
                //configure email settings

                $config['mailtype'] = 'html';
                $config['wordwrap'] = TRUE;
                $config['charset'] = 'utf-8';
                $config['priority'] = '1';
                $config['newline'] = "\r\n"; //use double quotes here
                $this->email->initialize($config);
                $from_email = "info@uniqueloo.com";
                $name = "Unique Loo Mobile Toilets";
                $subject = "Client Job Invoice ";

                $mail_body = '
                                                                           ---------------------
                                                                           
      

                                                                           <br>
                                                                            
                                                                                Good Day  :' . $client_name . '!
                                                                                    <fieldset>
                                                                                An invoice was generated for your Job , Below are the  details of your Job : 
                                                                                <hr>
                                                                                <table>
                                                                                <tbody>
                                                                                <tr><td>Job Card No :  ' . $job_card_no . '</td><td></td></tr>
                                                                                <tr><td>Invoice No : ' . $invoice_no . '</td><td></td></tr>
                                                                                </tbody><table>
                                                                                <br>
                                                                                ------------------------
                                                                                Please follow link to view the  Invoice and print it :<a href="' . $base_url . 'download/client_invoice/' . $random_key . '">DownLoad Invoice</a>
                                                                                

                                                                                ------------------------ ';



                //send mail
                $this->email->from($from_email, $name);
                $this->email->reply_to('info@uniqueloo.com', 'Unique Loo Mobile Toilets');
                $this->email->to('info@uniqueloo.com', 'Unique Loo Mobile Toilets');
                $this->email->cc($client_email, $client_name);
                $this->email->subject($subject);
                $this->email->message($mail_body);
                $message_type = "Proforma Invoice";
                if ($this->email->send()) {
                    // mail sent
                    $output = $this->email->print_debugger();
                    $data_insert = array(
                        'output' => $output,
                        'job_card_no' => $job_card_no,
                        'type' => $message_type
                    );
                    $this->db->insert('mail_debugger', $data_insert);
                } else {
                    //error
                    $output = $this->email->print_debugger();
                    $data_insert = array(
                        'output' => $output,
                        'job_card_no' => $job_card_no,
                        'type' => $message_type
                    );
                    $this->db->insert('mail_debugger', $data_insert);
                }
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    function delete_job_card() {
        $delete_job_card = $this->uri->segment(3);
        $delete_job_card = $this->operations_model->delete_job_card($delete_job_card);
        if (empty($delete_job_card)) {
            
        } else {
            echo json_encode($delete_job_card);
        }
    }

    public function client_payment_list() {
        $client_payment_list = $this->operations_model->client_payment_list();
        if (empty($client_payment_list)) {
            
        } else {
            echo json_encode($client_payment_list);
        }
    }

    public function my_client_payment_list() {
        $user_id = $this->session->userdata('user_id');
        $my_client_payment_list = $this->operations_model->my_client_payment_list($user_id);
        if (empty($my_client_payment_list)) {
            
        } else {
            echo json_encode($my_client_payment_list);
        }
    }

    public function get_user_configurations() {
        $client_configurations = $this->operations_model->get_user_configurations();
        if (empty($client_configurations)) {
            
        } else {
            echo json_encode($client_configurations);
        }
    }

    function client_info() {
        $job_card_id = $this->uri->segment(3);
        $client_info = $this->operations_model->client_info($job_card_id);
        if (empty($client_info)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($client_info);
        }
    }

    function get_job_sub_total() {
        $job_card_id = $this->uri->segment(3);
        $get_job_sub_total = $this->operations_model->get_job_sub_total($job_card_id);
        if (empty($get_job_sub_total)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($get_job_sub_total);
        }
    }

    function get_total_job_value() {
        $job_card_id = $this->uri->segment(3);
        $get_total_job_cost = $this->operations_model->get_total_job_value($job_card_id);
        if (empty($get_total_job_cost)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($get_total_job_cost);
        }
    }

    function submit_payment() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $job_card_id_payment = $this->input->post('job_card_id_payment');
        $data['submit_payments'] = $this->operations_model->submit_payment($job_card_id_payment);
        $this->load->view('submit_payments', $data);
    }

    function payments() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();
        $data['payment_clients'] = $this->operations_model->my_booked_clients();

        $this->load->view('payments_v', $data);
    }

    function general() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->view('admin_template/form', $data);
    }

    function get_payment_details() {
        $job_card_id = $this->uri->segment(3);
        $get_payment_details = $this->operations_model->get_payment_details($job_card_id);
        if (empty($get_payment_details)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($get_payment_details);
        }
    }

    function get_blld_clnt_info() {
        $job_card_id = $this->uri->segment(3);
        $get_blld_clnt_info = $this->operations_model->get_blld_clnt_info($job_card_id);
        if (empty($get_blld_clnt_info)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($get_blld_clnt_info);
        }
    }

    function submit_payment_form() {
        $asset_name = $this->input->post('asset_name');
        $quantity = $this->input->post('quantity');
        $amount_charged = $this->input->post('amount_charged');
        $amount_paid = $this->input->post('amount_paid');
        $balance = $this->input->post('balance');
        $payment_method = $this->input->post('payment_method');
        $payment_code = $this->input->post('payment_code');
        $job_card_no = $this->input->post('job_card_id');
        $invoice_no = $this->input->post('invoice_no');
        $statement_id = $this->input->post('statement_id');
        $submit_payment_form = $this->operations_model->submit_payment_form();
    }

    function update_job_quotation() {
        $update_job_quotation = $this->operations_model->update_job_quotation();
    }

    public function waiting_clients() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->view('waiting_clients_v', $data);
    }

    public function assign_resources() {
        $job_card_id = $this->uri->segment(3);
        $data['job_details'] = $this->operations_model->get_job($job_card_id);
        $data['client_info'] = $this->operations_model->get_blld_clnt_info($job_card_id);
        //$data['available_assets'] = $this->operations_model->get_available_assets();
        $this->load->view('assign_resources_v', $data);
    }

    public function available_assets() {
        $type = $this->uri->segment(3);
        $asset_type = $this->operations_model->get_available_assets($type);
        if (empty($asset_type)) {
            
        } else {
            echo json_encode($asset_type);
        }
    }

    public function assign() {
        $job_card_id = $this->input->post('job_card_id');
        $asset_name = $this->input->post('asset_name');
        $asset_value = $this->input->post('asset_value');

        $assign = $this->operations_model->assign($job_card_id, $asset_name, $asset_value);
    }

    public function job_cards_per_client() {
        $job_cards_per_client = $this->operations_model->job_cards_per_client();

        if (empty($job_cards_per_client)) {
            
        } else {
            echo json_encode($job_cards_per_client);
        }
    }

    public function revenue_collected() {
        $revenue_collected = $this->operations_model->revenue_collected();
        if (empty($revenue_collected)) {
            
        } else {
            echo json_encode($revenue_collected);
        }
    }

    public function expenses_incurred() {
        $expenses_incurred = $this->operations_model->expenses_incurred();
        if (empty($expenses_incurred)) {
            
        } else {
            echo json_encode($expenses_incurred);
        }
    }

    public function revenue_per_client() {
        $revenue_per_client = $this->operations_model->revenue_per_client();
        if (empty($revenue_per_client)) {
            
        } else {
            echo json_encode($revenue_per_client);
        }
    }

    public function job_per_client() {
        $job_per_client = $this->operations_model->job_per_client();
        if (empty($job_per_client)) {
            
        } else {
            echo json_encode($job_per_client);
        }
    }

    public function expenses_per_job_card() {
        $expenses_per_job_card = $this->operations_model->expenses_job_card();
        if (empty($expenses_per_job_card)) {
            
        } else {
            echo json_encode($expenses_per_job_card);
        }
    }

    public function revenue_per_job_card() {
        $revenue_per_job_card = $this->operations_model->revenue_per_job_card();
        if (empty($revenue_per_job_card)) {
            
        } else {
            echo json_encode($revenue_per_job_card);
        }
    }

    public function total_clients() {
        $total_clients = $this->operations_model->total_clients();
        if (empty($total_clients)) {
            $check_response = "No Clients!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($total_clients);
        }
    }

    public function total_jobs() {
        $total_jobs = $this->operations_model->total_jobs();
        if (empty($total_jobs)) {
            $check_response = "No Jobs!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($total_jobs);
        }
    }

    public function total_revenue() {
        $total_revenue = $this->total_revenue_qry();
        if (empty($total_revenue)) {
            $check_response = "No Revenue!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($total_revenue);
        }
    }

    public function total_jobs_per_day() {
        $total_revenue = $this->total_jobs_per_day_qry();
        if (empty($total_revenue)) {
            $check_response = "No Jobs per Day!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($total_revenue);
        }
    }

    public function total_expenses() {
        $total_expenses = $this->operations_model->total_expenses();
        if (empty($total_expenses)) {
            $check_response = "No Expense!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($total_expenses);
        }
    }

    public function active_users() {
        $active_users = $this->operations_model->active_users();
        if (empty($active_users)) {
            
        } else {
            echo json_encode($active_users);
        }
    }

    public function daily_revenue() {
        $daily_revenue = $this->operations_model->daily_revenue();
        if (empty($daily_revenue)) {
            $check_response = "No Revenue!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($daily_revenue);
        }
    }

    public function daily_expense() {
        $daily_expense = $this->operations_model->daily_expense();
        if (empty($daily_expense)) {
            $check_response = "No Daily Expense!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($daily_expense);
        }
    }

    function no_jobs_mtd() {
        $no_jobs_mtd = $this->no_jobs_mtd_qry();
        if (empty($no_jobs_mtd)) {
            $check_response = "No Jobs MTD!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($no_jobs_mtd);
        }
    }

    function no_of_jobs_actve_mtd() {
        $no_jobs_mtd = $this->no_of_jobs_actve_mtd_qry();
        if (empty($no_jobs_mtd)) {
            $check_response = "No Active Jobs MTD!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($no_jobs_mtd);
        }
    }

    function no_of_jobs_clrd_mtd() {
        $no_jobs_mtd = $this->no_of_jobs_clrd_mtd_qry();
        if (empty($no_jobs_mtd)) {
            $check_response = "No Cleared Jobs MTD!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($no_jobs_mtd);
        }
    }

    function xpctd_rvne_mtd() {
        $no_jobs_mtd = $this->xpctd_rvne_mtd_qry();
        if (empty($no_jobs_mtd)) {
            $check_response = "No Expected Revenue MTD!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($no_jobs_mtd);
        }
    }
    
    
    function xpctd_rvne_ytd() {
        $no_jobs_ytd = $this->xpctd_rvne_ytd_qry();
        if (empty($no_jobs_ytd)) {
            $check_response = "No Expected Revenue YTD!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($no_jobs_ytd);
        }
    }
    
    

    function actl_rvne_mtd() {
        $no_jobs_mtd = $this->actl_rvne_mtd_qry();
        if (empty($no_jobs_mtd)) {
            $check_response = "No Actual Revenue MTD!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($no_jobs_mtd);
        }
    }

    public function dashboard() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();
        $data['revenue_per_client'] = $this->operations_model->revenue_per_client();
        $data['total_no_jobs'] = $this->operations_model->job_per_client();


        $this->load->view('dashboard', $data);
    }

    public function myLabels() {
        $get_labels = $this->operations_model->get_date();
        foreach ($get_labels as $info) {
            echo $info['month'] . ',';
        }
    }

    public function ack_payments() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->view('ack_payments_v', $data);
    }

    public function un_approved_pymnts() {
        $un_approved_pymnts = $this->operations_model->un_approved_pymnts();
        if (empty($un_approved_pymnts)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($un_approved_pymnts);
        }
    }

    function approve_transaction() {
        $job_card_id = $this->uri->segment(3);
        $job_card_no = $this->uri->segment(4);
        $approve_transaction = $this->operations_model->approve_transaction($job_card_id);
        if ($approve_transaction) {
            $check_response = "Transaction approved successfully!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            $check_response = "Transaction was not approved!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        }
    }

    public function get_dataset() {
        $dataset = "[";
        $data_set = $this->operations_model->get_date();
        foreach ($data_set as $value) {
            $date = $value['date'];
            $date = strtotime($date);
            $dataset .= '[' . $date . ',' . $value['revenue'] . '],';
        }
        $dataset = substr($dataset, 0, -1);
        $dataset .= "]";

        echo $dataset;
    }

    function get_labels() {
        $labels = "[";
        $label = $this->operations_model->getdstnct_date();
        foreach ($label as $value) {
            $labels .= '"' . $value['date'] . '",';
        }
        $labels = substr($labels, 0, -1);
        $labels .= "]";

        echo $labels;
    }

    function getdatasets() {
        $revenues = "[";
        $revenue = $this->operations_model->get_date();
        foreach ($revenue as $value) {
            $revenues .= '"' . $value['revenue'] . '",';
        }
        $revenues = substr($revenues, 0, -1);
        $revenues .= "]";

        echo $revenues;
    }

    public function get_dataset2() {
        $dataset = "[";
        $data_set = $this->operations_model->get_date();
        foreach ($data_set as $value) {
            $date = $value['date'];
            $date = strtotime($date);
            $dataset .= '[' . $date . ',' . $value['expense'] . '],';
        }
        $dataset = substr($dataset, 0, -1);
        $dataset .= "]";

        echo $dataset;
    }

    function get_vardata() {
        $this->db->trans_start();
        $data_set = "";
        $label = "";
        $bardata = "";
        $query_1 = $this->db->query("select distinct date_added from statement");
        foreach ($query_1->result_array() as $value) {
            $date = $value['date_added'];
            $label .= $date;
            $query_2 = $this->db->query("Select count(amnt_cr) as revenue from statement where date_added like '%$date%'");
            foreach ($query_2->result_array() as $value) {
                $revenue = $value['revenue'];
                $data_set .= $revenue;

                $bardata .= "{ " . $label . "," . $data_set . "}";

                echo $bardata;
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    public function daily_report() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $data['report'] = $this->operations_model->daily_report();
        $this->load->view('daily_report_v', $data);
    }

    public function job_status() {
        $job_status = $this->operations_model->job_status();
        if (empty($job_status)) {
            
        } else {
            echo json_encode($job_status);
        }
    }

    public function client_payment_statement() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $data['clnt_pymnt_stmnt'] = $this->operations_model->client_payment_statement();
        $this->load->view('clnt_pymnt_stmnt_v', $data);
    }

    public function get_client_lists() {
        $client_list = $this->operations_model->get_client_lists();
        if (empty($client_list)) {
            
        } else {
            echo json_encode($client_list);
        }
    }

    function release_job_assets() {

        $release_job_card_id = $this->uri->segment(3);
        $release_job_assets = $this->operations_model->release_job_assets($release_job_card_id);


        if ($release_job_assets) {
            $check_response = "Job Closed successfully!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            $check_response = "Job  was not Closed!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        }
    }

    function check_user_existence() {
        $employee_no = $this->uri->segment(3);
        $check_user_existence = $this->operations_model->check_user_existence($employee_no);
        if ($check_user_existence) {
            $check_response = "User Exists in the System";
            $email_check = array(
                'user_check' => $check_response
            );
            echo json_encode([$email_check]);
        } else {
            $check_response = "User Does not exist in the System";
            $email_check = array(
                'user_check' => $check_response
            );
            echo json_encode([$email_check]);
        }
    }

    public function view_job_status() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->view('job_status_v', $data);
    }

    public function high_charts() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->view('high_charts_v');
    }

    public function edit_job_card() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $data['job_cards'] = $this->get_job_cards();
        $this->load->view('edit_job_card_v', $data);
    }

    function get_job_card_details() {
        $job_card_id = $this->uri->segment(3);
        $get_job_card_details = $this->operations_model->get_job_card_details($job_card_id);
        if (empty($get_job_card_details)) {
            $check_response = "Job Card Does not exist in the System";
            $get_job_card_details = array(
                'job_card_no' => $check_response
            );
            echo json_encode([$get_job_card_details]);
        } else {
            echo json_encode($get_job_card_details);
        }
    }

    function future_jobs_report() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $data['future_jobs_data'] = $this->operations_model->future_jobs_report();
        $data['summarised_future_jobs_data'] = $this->operations_model->summarised_future_jobs_data();

        $this->load->view('future_jobs_report_v', $data);
    }

    function event_job_details() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $data['job_details'] = $this->operations_model->event_job_details();
        $this->load->view('event_job_details_v', $data);
    }

    function payment_report() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $data['asset_types'] = $this->get_asset_type();

        $data['booked_clients'] = $this->operations_model->view_booked_clients();

        $data['payment_report'] = $this->operations_model->payment_report();
        $this->load->view('payment_report_v', $data);
    }

    function chart_js() {
        $month = date('F');
        header('Content-Type: application/json');
        $rows = '';
        $query = "SELECT sum(amount_dr) as revenue , date_format(time_from,'%d-%b-%Y') as date FROM `total_job_card_cost` inner join job_card on job_card.id = total_job_card_cost.job_card_id where date_format(time_from,'%M')='$month' and total_job_card_cost.status='Active'  group by date_format(time_from,'%d-%b-%Y')";
        $result = $this->db->query($query);
        $total_rows = $result->num_rows;
        if ($result) {
            $rows = $result->result_array();
        }

        print json_encode($rows, JSON_NUMERIC_CHECK);
    }

    function daily_expense_bar() {
        $year = date('Y');
        header('Content-Type: application/json');
        $rows = '';
        $query = "SELECT sum(amount_dr) as revenue , date_format(date_added,'%b-%Y') as date FROM `total_job_card_cost` where date_format(date_added,'%Y')='$year' and total_job_card_cost.status='Active'  group by date_format(date_added,'%b-%Y')";
        $result = $this->db->query($query);
        $total_rows = $result->num_rows;
        if ($result) {
            $rows = $result->result_array();
        }

        print json_encode($rows, JSON_NUMERIC_CHECK);
    }

    function job_status_ytd() {
        header('Content-Type: application/json');
        $rows = '';
        $query = "select count(job_card.id) as value, client.name as label , client.id as client_id,  year(job_card.time_from) as event_year from job_card inner join client on client.id = job_card.clnt_id where year(job_card.time_from) >= year(NOW()) group by  client.id, year(job_card.time_from)";
        $result = $this->db->query($query);
        $total_rows = $result->num_rows;
        if ($result) {
            $rows = $result->result_array();
        }

        print json_encode($rows, JSON_NUMERIC_CHECK);
    }

    function jobs_per_month() {
        header('Content-Type: application/json');
        $rows = '';
        $query = "select count(job_card.id) as value, year(time_from) as event_year, date_format(time_from,' %b-%Y') as label from job_card where year(time_from) >= year(now()) group by month(time_from)";
        $result = $this->db->query($query);
        $total_rows = $result->num_rows;
        if ($result) {
            $rows = $result->result_array();
        }

        print json_encode($rows, JSON_NUMERIC_CHECK);
    }

    function top_clients_ytd() {
        header('Content-Type: application/json');
        $rows = '';
        $query = "select client.name as client_name , sum(total_job_card_cost.amount_dr) as amount_dr , year(time_from) as event_year, month(time_from) as event_month from client inner join job_card on job_card.clnt_id = client.id inner join total_job_card_cost on total_job_card_cost.job_card_id = job_card.id where year(time_from) >= year(NOW()) and total_job_card_cost.status='Active' group by client.id order by amount_dr desc LIMIT 0,10";
        $result = $this->db->query($query);
        $total_rows = $result->num_rows;
        if ($result) {
            $rows = $result->result_array();
        }

        print json_encode($rows, JSON_NUMERIC_CHECK);
    }

    function view_line_chart() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->view('chart_js', $data);
    }

    function asset_status() {
        $query = $this->db->query("SELECT count(job_card_no) as job_card_no,number,asset.type,asset.status,asset.id as asset_id FROM  asset inner join assgnd_rsces on assgnd_rsces.name = asset.id inner join job_card on job_card.id = assgnd_rsces.job_card_id");
        $result = $query->result_array();
        echo json_encode($result);
    }

    function get_asset_status_details() {
        $asset_id = $this->uri->segment(3);
        $asset_id = urldecode($asset_id);
        $get_asset_status_details = $this->operations_model->get_asset_status_details($asset_id);
        echo json_encode($get_asset_status_details);
    }

    function view_payments() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $data['payment_report'] = $this->operations_model->payment_report();
        $this->load->view('view_payments', $data);
    }

    function delete_job_card_details() {
        $job_card_id = $this->uri->segment(3);
        $delete_job_card_details = $this->operations_model->delete_job_card_details($job_card_id);
        if ($delete_job_card_details) {
            $check_response = "Job Deleted successfully!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            $check_response = "Job was not deleted!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        }
    }

    function asset_status_list() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->view('asset_status_v', $data);
    }

    public function get_active_clients() {

        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->operations_model->get_active_clients($q);
        }
    }

    function sales_report() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();
        $data['sales_report_data'] = $this->operations_model->sales_report();
        $this->load->view('sales_report_v', $data);
    }

    function client_accnt_statement() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();
        $data['clnt_accnt_sttmnt'] = $this->operations_model->client_accnt_statement();
        $this->load->view('client_accnt_statement_v', $data);
    }

    function check_client_existence() {
        $name = $this->uri->segment(3);
        $name = urldecode($name);



        $check_client = $this->admin_model->check_client($name);
        if (!empty($check_client)) {
            $check_response = "Client Already Exist in the System!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else if (empty($check_client)) {
            $check_response = "Client Does Not Exist in the System!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        }
    }

    function job_card_report() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();
        $data['employees_list'] = $this->get_employees();

        $data['clients'] = $this->get_clients();

        $data['job_details'] = $this->operations_model->job_card_report();
        $this->load->view('job_card_report_v', $data);
    }

    function get_client_information() {
        $client_name = $this->uri->segment(3);
        $get_client_information = $this->operations_model->get_client_information($client_name);
        if (empty($get_client_information)) {
            
        } else {
            echo json_encode($get_client_information);
        }
    }

    function get_job_information() {
        $job_card_id = $this->uri->segment(3);
        $get_job_information = $this->operations_model->get_job_information($job_card_id);
        if (empty($get_job_information)) {
            
        } else {
            echo json_encode($get_job_information);
        }
    }

    function get_job_quotation_details() {
        $job_card_id = $this->uri->segment(3);
        $get_job_quotation_details = $this->operations_model->get_job_quotation_details($job_card_id);
        if (empty($get_job_quotation_details)) {
            $check_response = "No Data!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            echo json_encode($get_job_quotation_details);
        }
    }

    function cancel_job_card() {
        $job_card_id = $this->uri->segment(3);
        $this->db->trans_start();
        $cancel = "Cancelled";
        $data_update = array(
            'status' => $cancel
        );
        $this->db->where('id', $job_card_id);
        $this->db->update('job_card', $data_update);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $check_response = "Failed!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else {
            $check_response = "Success!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        }
    }

}

?>