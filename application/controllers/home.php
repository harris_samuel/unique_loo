<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->library('form_validation', 'session');
        $this->load->helper('url');

        $this->session->userdata('username');
        $this->session->userdata('type');

        $username = $this->session->userdata('username');
        $type = $this->session->userdata('type');
        $this->load->view('home',$data);
    }

    public function profile() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->view('profile', $data);
    }

    public function do_logout() {
        $login_logs_id = $this->session->userdata('login_logs_id');

        $username = $this->session->userdata('username');
        $in_active = 'In_Active';
        $time_end = date("H:i:s");
        $data = array(
            'is_active' => $in_active,
            'time_end' => $time_end
        );

        $this->db->where('login_logs_id', $login_logs_id);
        $this->db->update('login_logs', $data);


        $update_logout = "SELECT login_logs_id, is_active FROM `login_logs` where login_logs_id < '$login_logs_id' and user_name='$username' and is_active = 'Active'";
        $query = $this->db->query($update_logout);
        foreach ($query->result() as $value) {
            $login_logs_id = $value->login_logs_id;

            $in_active = "In Active";
            $login_logs_update = array(
                'is_active' => $in_active,
                'time_end' => $time_end
            );
            $this->db->where('login_logs_id', $login_logs_id);
            $this->db->update('login_logs', $login_logs_update);
        }



        $this->session->sess_destroy();
        redirect('login');
    }

    public function page_not_found() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->view('page_not_found_v', $data);
    }

    public function intrnl_srvr_error() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->view('intrnl_srvr_error_v', $data);
    }

    public function dashboard() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->view('admin_template/index', $data);
    }

    public function dashboard2() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->view('admin_template/index2', $data);
    }

    public function dashboard3() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->view('admin_template/index3', $data);
    }

    public function tables() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->view('admin_template/tables', $data);
    }

    public function tables2() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->view('admin_template/tables_dynamic', $data);
    }

}

?>