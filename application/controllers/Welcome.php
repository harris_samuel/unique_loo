<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function buttons(){
		$this->load->view('buttons');
	}

	public function calendar(){
		$this->load->view('admin_template/calender');
	}

	public function editors(){
		$this->load->view('editors');
	}

	public function forms(){
		$this->load->view('form');
	}

	public function face(){
		$this->load->view('interface');
	}

	public function login(){
		$this->load->view('login');
	}


	public function stats(){
		$this->load->view('stats');
	}

	public function tables(){
		$this->load->view('tables');
	}

}
