<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $this->load->library('form_validation', 'session');
        $this->load->helper('url');

        $this->session->userdata('username');
        $this->session->userdata('type');

        $username = $this->session->userdata('username');
        $type = $this->session->userdata('type');
        $this->load->view('home', $data);
    }

    public function users() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();
        $data['users'] = $this->get_users();
        $this->load->view('users', $data);
    }

    function check_user_email() {
        $email = $this->uri->segment(3);
        $check_mail = $this->admin_model->check_user_email($email);
        if (!$check_mail) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function users_data() {
        $start = $this->uri->segment(3);
        $end = $this->uri->segment(4);
        if (empty($start) and empty($end)) {
            $sql = "SELECT * FROM `users` ";
        } else {
            $sql = "SELECT * FROM `users` where timestamp between '$start' and '$end' ";
        }

        $query = $this->db->query($sql);
        $result = $query->result();

        foreach ($result AS $data):
            $return[] = $data;
        endforeach;
        if (!empty($return)) {
            echo json_encode($return);
        } else {
            echo '[]';
        }
    }

    public function add_new_user() {
        $user_name = $this->input->post('add_user_name');
        $user_status = $this->input->post('add_user_status');
        $user_email = $this->input->post('add_user_email');

        $get_user_national_id = $this->get_employee_national_id($user_email);
        echo 'National ID'.$get_user_national_id;

        $this->admin_model->add_new_user($user_name, $user_status, $user_email,$get_user_national_id);
    }

    function get_employee_national_id($user_email) {
        //$this->db->trans_start();
        $id_no = "";
        $sql = "Select id_no from employee where email='$user_email'";
        $query = $this->db->query($sql);
        foreach ($query->result() as $value) {
            $id_no .= $value->id_no;
        }
        return $id_no;
//        $this->db->trans_complete();
//        if ($this->db->trans_status() === FALSE) {
//            
//        }
    }

    public function department() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $data['department'] = $this->get_department();
        $this->load->view('department', $data);
    }

    public function roles() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $data['roles'] = $this->get_roles();
        $this->load->view('roles', $data);
    }

    public function roles_data() {
        $start = $this->uri->segment(3);
        $end = $this->uri->segment(4);
        if (empty($start) and empty($end)) {
            $sql = "SELECT id,name,status,timestamp FROM `roles` order by name ";
        } else {
            $sql = "SELECT id,name,status,timestamp FROM `roles` where timestamp between '$start' and '$end' order by name ";
        }

        $query = $this->db->query($sql);
        $result = $query->result();

        foreach ($result AS $data):
            $return[] = $data;
        endforeach;
        if (!empty($return)) {
            echo json_encode($return);
        } else {
            echo '[]';
        }
    }

    public function add_new_role() {
        $role_name = $this->input->post('role_name');
        $role_status = $this->input->post('role_status');
        $this->admin_model->add_new_role($role_name, $role_status);
    }

    public function assets() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();
        $data['asset_types'] = $this->get_asset_type();
        $data['assets'] = $this->get_assets();
        $this->load->view('assets', $data);
    }

    function employees() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $data['employees'] = $this->admin_model->get_employees();
        $data['titles'] = $this->admin_model->get_titles();
        $data['roles'] = $this->get_roles();
        $data['active_roles'] = $this->get_active_user_roles();
        $data['department'] = $this->get_department();
        $data['countries'] = $this->get_countries();
        $this->load->view('employees', $data);
    }

    public function get_employee_details() {
        $employee_id = $this->uri->segment(3);
        $employee_details = $this->admin_model->get_employee_details($employee_id);
        if (empty($employee_details)) {
            echo 'No Data';
        } else {
            echo json_encode($employee_details);
        }
    }

    public function get_edit_employee_details() {
        $employee_id = $this->uri->segment(3);
        $employee_detials = $this->admin_model->get_edit_employee_details($employee_id);
        echo json_encode($employee_detials);
    }

    public function edit_employee() {
        $this->admin_model->edit_employee();
    }

    public function delete_employee() {
        $employee_id = $this->input->post('delete_employee_id');
        $this->admin_model->delete_employee($employee_id);
    }

    public function employees_data() {
        $start = $this->uri->segment(3);
        $end = $this->uri->segment(4);
        if (empty($start) and empty($end)) {
            $sql = "SELECT * FROM `employees` ";
        } else {
            $sql = "SELECT * FROM `employees` where timestamp between '$start' and '$end' ";
        }

        $query = $this->db->query($sql);
        $result = $query->result();

        foreach ($result AS $data):
            $return[] = $data;
        endforeach;
        if (!empty($return)) {
            echo json_encode($return);
        } else {
            echo '[]';
        }
    }

    public function getCountries($keyword) {
        $this->db->select('countries_id, countries_name', 'countries_id');
        $this->db->from('country');
        $this->db->like('countries_name', $keyword, 'after');
        $this->db->order_by("countries_name", "asc");

        $query = $this->db->get();
        foreach ($query->result_array() as $row) {
            //$data[$row['friendly_name']];
            $data[] = $row;
        }
        //return $data;
        return $query;
    }

    function list_countries() {
        $keyword = $this->input->post('keyword');
        $list = $this->get_countries($keyword);
        echo json_encode($list);
    }

    public function add_new_employee() {

        $this->admin_model->add_new_employee();
    }

    public function get_countries() {

        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->admin_model->get_countries($q);
        }
    }

    #Roles Function starts from here ......

    public function get_role_details() {
        $role_id = $this->uri->segment(3);
        $role_detials = $this->admin_model->get_role_details($role_id);
        echo json_encode($role_detials);
    }

    public function get_edit_role_details() {
        $role_id = $this->uri->segment(3);
        $role_detials = $this->admin_model->get_edit_role_details($role_id);
        echo json_encode($role_detials);
    }

    public function edit_role() {
        $this->admin_model->edit_role();
    }

    public function delete_role() {
        $role_id = $this->input->post('delete_role_id');
        $this->admin_model->delete_role($role_id);
    }

    #Departments Function starts from here ......

    public function get_department_details() {
        $department_id = $this->uri->segment(3);
        $department_detials = $this->admin_model->get_department_details($department_id);
        echo json_encode($department_detials);
    }

    public function get_edit_department_details() {
        $department_id = $this->uri->segment(3);
        $department_detials = $this->admin_model->get_edit_department_details($department_id);
        echo json_encode($department_detials);
    }

    public function edit_department() {
        $this->admin_model->edit_department();
    }

    public function delete_department() {
        $department_id = $this->input->post('delete_department_id');
        $this->admin_model->delete_department($department_id);
    }

    public function add_new_department() {
        $department_name = $this->input->post('department_name');
        $department_status = $this->input->post('department_status');
        $this->admin_model->add_new_department($department_name, $department_status);
    }

    //User Functions start here...
    public function get_user_emails() {
        $get_user_emails = $this->admin_model->get_user_emails();
        if (empty($get_user_emails)) {
            
        } else {
            echo json_encode($get_user_emails);
        }
    }

    public function get_edit_user_details() {
        $user_id = $this->uri->segment(3);
        $get_edit_user_details = $this->admin_model->get_edit_user_details($user_id);
        if (empty($get_edit_user_details)) {
            
        } else {
            echo json_encode($get_edit_user_details);
        }
    }

    public function delete_user() {
        $this->admin_model->delete_user();
    }

    //Asset CRUD Functions start here...

    public function add_asset() {
        $asset_name = $this->input->post('asset_name');
        $asset_type = $this->input->post('asset_type');
        $asset_price = $this->input->post('asset_price');
        $purchase_date = $this->input->post('purchase_date');
        $asset_uuid = $this->input->post('asset_id');
        $check_uuid = $this->admin_model->check_uuid($asset_uuid);
        if (!empty($check_uuid)) {
            $check_response = "Asset UUID Already Exist!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else if (empty($check_uuid)) {
            $add_assets = $this->admin_model->add_asset($asset_type, $asset_price, $purchase_date, $asset_uuid);
            $check_response = "Asset Added Successfully!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        }
    }

    public function get_asset() {
        $id = $this->uri->segment(3);
        $asset = $this->admin_model->get_asset($id);
        if (empty($asset)) {
            
        } else {
            echo json_encode($asset);
        }
    }

    public function delete_asset() {
        $id = $this->input->post('delete_asset_id');
        $asset = $this->admin_model->delete_asset($id);
        if (empty($asset)) {
            
        } else {
            
        }
    }

    public function edit_asset() {
        $asset_name = $this->input->post('edit_asset_name');
        $asset_type = $this->input->post('edit_asset_type');
        $asset_price = $this->input->post('edit_asset_price');
        $purchase_date = $this->input->post('edit_purchase_date');
        $asset_uuid = $this->input->post('edit_number');
        $id = $this->input->post('edit_asset_id');

        $add_assets = $this->admin_model->edit_asset($id, $asset_name, $asset_type, $asset_price, $purchase_date, $asset_uuid);
    }

    public function clients() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $data['clients'] = $this->get_clients();
        $this->load->view('clients', $data);
    }

    public function add_new_client() {
        $name = $this->input->post('add_name');
        $address = $this->input->post('add_address');
        $website = $this->input->post('add_website');
        $industry = $this->input->post('add_industry');
        $phone_no = $this->input->post('add_phone');
        $email = $this->input->post('add_email');


        $check_uuid = $this->admin_model->check_client($name);
        if (!empty($check_uuid)) {
            $check_response = "Client Already Exist in the System!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        } else if (empty($check_uuid)) {
            $this->admin_model->add_new_client($name, $address, $website, $industry, $phone_no, $email);
            $check_response = "Client Added Successfully!";
            $response = array(
                'response' => $check_response
            );
            echo json_encode([$response]);
        }
    }

    public function get_client_details() {
        $client_id = $this->uri->segment(3);
        $get_client = $this->admin_model->get_client_details($client_id);
        if (empty($get_client)) {
            
        } else {
            echo json_encode($get_client);
        }
    }

    public function edit_client() {


        $name = $this->input->post('edit_name');
        $address = $this->input->post('edit_address');
        $website = $this->input->post('edit_website');
        $industry = $this->input->post('edit_industry');
        $phone = $this->input->post('edit_phone');
        $email = $this->input->post('edit_email');
        $id = $this->input->post('edit_client_id');

        $edit_client = $this->admin_model->edit_client($name, $address, $website, $industry, $phone, $email, $id);
    }

    public function delete_client() {
        $id = $this->input->post('delete_client_id');
        $delete_client = $this->admin_model->delete_cleint($id);
    }

    public function standards() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $data['standards'] = $this->get_standards();
        $this->load->view('standards', $data);
    }

    public function add_new_standard() {
        $standard_name = $this->input->post('add_standard_name');
        $standard_value = $this->input->post('add_standard_value');
        $status = $this->input->post('add_standard_status');
        $this->admin_model->add_new_standard($standard_name, $standard_value, $status);
    }

    public function edit_standard() {
        $standard_name = $this->input->post('edit_standard_name');
        $standard_value = $this->input->post('edit_standard_value');
        $status = $this->input->post('edit_standard_status');
        $id = $this->input->post('edit_standard_id');
        $this->admin_model->edit_standards($standard_name, $standard_value, $status, $id);
    }

    public function delete_standard() {
        $id = $this->input->post('delete_standard_id');
        $this->admin_model->delete_standards($id);
    }

    public function get_edit_standard_details() {
        $id = $this->uri->segment(3);
        $standard_detail = $this->admin_model->get_edit_standard_details($id);
        if (empty($standard_detail)) {
            echo json_encode($standard_detail);
        } else {
            echo json_encode($standard_detail);
        }
    }

    public function access_control() {
        $data['system_users'] = $this->admin_model->system_users();
        $data['admin_rights_functions'] = $this->admin_model->admin_functions();
        $data['daily_rights_functions'] = $this->admin_model->daily_functions();
        $data['reports_rights_functions'] = $this->admin_model->reports_functions();
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();


        $this->load->view('access_control_v', $data);
    }

    public function get_user_permissions() {
        $user_id = $this->uri->segment(3);
        $get_user_permissions = $this->admin_model->get_user_permissions($user_id);
        if (empty($get_user_permissions)) {
            $get_user_permissions = "No Rights Assigned";
            $id = array(
                'id' => $get_user_permissions
            );
            echo json_encode([$id]);
        } else {
            echo json_encode($get_user_permissions);
        }
    }

    function assign_new_access() {
        $this->db->trans_start();
        $functions = $this->input->post('functions');
        $temp = $this->input->post('functions');
        $user_id = $this->input->post('user_name');
        echo 'User id' . $user_id . '</br>';

        $today = date("Y-m-d H:i:s");
        $status = "Active";
        $delete_access = $this->delete_access($user_id);
        for ($i = 0; $i < count($temp); $i++) {

            $data_insert = array(
                'function_id' => $functions[$i],
                'user_id' => $user_id,
                'date_added' => $today,
                'status' => $status
            );
            $this->db->insert('permissions', $data_insert);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function delete_access($user_id) {
        $this->db->trans_start();
        $query = $this->db->query("select * from permissions where user_id='$user_id'");
        foreach ($query->result() as $value) {
            $id = $value->id;
            $this->db->delete('permissions', array('id' => $id));
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            $success = "SUCCESS";
            return $success;
        }
    }

    function check_email_existence() {
        $email = $this->uri->segment(3);
        $check_email_existence = $this->admin_model->check_email_existence($email);
        if ($check_email_existence) {
            $check_response = "Email Exists in the System";
            $email_check = array(
                'email_check' => $check_response
            );
            echo json_encode([$email_check]);
        } else {
            $check_response = "Email Does not exist in the System";
            $email_check = array(
                'email_check' => $check_response
            );
            echo json_encode([$email_check]);
        }
    }

    function check_employee_no_existence() {
        $employee_no = $this->uri->segment(3);
        $check_employee_no_existence = $this->admin_model->check_employee_no_existence($employee_no);
        if ($check_employee_no_existence) {
            $check_response = "Employee No Exists in the System";
            $employee_no_check = array(
                'employee_no_check' => $check_response
            );
            echo json_encode([$employee_no_check]);
        } else {
            $check_response = "Employee No Does not exist in the System";
            $employee_no_check = array(
                'employee_no_check' => $check_response
            );
            echo json_encode([$employee_no_check]);
        }
    }

    function check_email_id() {
        $email = $this->uri->segment(3);
        $check_email_existence = $this->admin_model->check_email_id($email);
        if ($check_email_existence) {
            $check_response = "Email Exists in the System";
            $email_check = array(
                'email_check' => $check_response
            );
            echo json_encode([$email_check]);
        } else {
            $check_response = "Email Does not exist in the System";
            $email_check = array(
                'email_check' => $check_response
            );
            echo json_encode([$email_check]);
        }
    }

    public function asset_type() {
        $data['admin_functions'] = $this->user_admin_functions();
        $data['daily_functions'] = $this->user_daily_functions();
        $data['reports_functions'] = $this->user_reports_functions();

        $data['asset_types'] = $this->get_asset_type();
        $this->load->view('asset_type', $data);
    }

    public function add_new_asset_type() {
        $name = $this->input->post('asset_type_name');
        $status = $this->input->post('asset_type_status');
        $add_new_asset_type = $this->admin_model->add_new_asset_type($name, $status);
        if ($add_new_asset_type) {
            
        } else {
            
        }
    }

    public function edit_asset_type() {
        $name = $this->input->post('edit_asset_type_name');
        $status = $this->input->post('edit_asset_type_status');
        $id = $this->input->post('edit_asset_type_id');
        $add_new_asset_type = $this->admin_model->edit_asset_type($name, $status, $id);
        if ($add_new_asset_type) {
            
        } else {
            
        }
    }

    public function delete_asset_type() {
        $id = $this->input->post('delete_asset_type_id');
        $delete_asset_type = $this->admin_model->delete_asset_type($id);
    }

    public function get_edit_asset_type_details() {
        $asset_type_id = $this->uri->segment(3);
        $get_asset_type_details = $this->admin_model->get_asset_type_details($asset_type_id);
        if (empty($get_asset_type_details)) {
            
        } else {
            echo json_encode($get_asset_type_details);
        }
    }

}
