x<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Unique Loo! | </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">

        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    </head>


    <body class="nav-md">

        <div class="container body">


            <div class="main_container">


                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">

                        <div class="navbar nav_title" style="border: 0;">

                            <a href="<?php echo base_url(); ?>"> <img  style="margin-left: 60px; margin-right: 0px; margin-bottom: 0px; margin-top: 0px;" src="<?php echo base_url(); ?>images/logo.png" alt="logo"  /></a>

                        </div>
                        <div class="clearfix"></div>



                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">
                                    <li><a href="<?php echo base_url(); ?>" ><i class="fa fa-home"></i> HOME <span class="fa fa-chevron-right"></span></a> </li>




                                    <li><a><i class="fa fa-edit"></i> ADMINISTRATION <span class="fa fa-chevron-down"></span></a>

                                        <ul class="nav child_menu" style="display: none">

                                            <li>


                                                <?php
                                                $base_url = base_url();
                                                foreach ($admin_functions as $value) {
                                                    $controller_name = $value['controller_name'];
                                                    $function_name = $value['functions_name'];
                                                    $i_tag = $value['i_tag'];
                                                    $description = $value['description'];
                                                    $span_tag = $value['span_tag'];
                                                    ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>



                                        </ul> 

                                    </li>

                                    <li><a><i class="fa fa-edit"></i> DAILY OPERATIONS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($daily_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>






                                        </ul>
                                    </li>



                                    <li><a><i class="fa fa-edit"></i> REPORTS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($reports_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>







                                        </ul>
                                    </li>



                                </ul>
                            </div>

                        </div>
                        <!-- /sidebar menu -->


                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" href="<?php echo base_url(); ?>home/do_logout" title="Logout">
                                <span class="glyphicon glyphicon-off"  aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->




                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">

                    <div class="nav_menu">
                        <nav class="" user="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="<?php echo base_url(); ?>assets/images/img.jpg" alt=""><?php
                                        $username = $this->session->userdata('user_name');
                                        echo $username;
                                        ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <li><a href="<?php echo base_url(); ?>admin/profile">  Profile</a>
                                        </li>


                                        <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                        </li>
                                    </ul>
                                </li>


                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->





                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="page-title">
                            <div class="title_left">
                                <h3>
                                    Unique Loo
                                    <small>
                                        Daily Operations Report 
                                    </small>
                                </h3>
                            </div>

                        </div>
                        <div class="clearfix"></div>

                        <div class="row">

                            <div id="payment_statement_list" class="payment_statement_list">

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Daily Operations <small>Revenues and Expense per Job Card per client YTD</small></h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <table id="payment_statement_table" class="payment_statement_table dataTables_scroll table table-bordered">
                                                <thead>
                                                    <tr class="headings">
                                                        <th>Date : </th>
                                                        <th>Client Name : </th>
                                                        <th>Phone No : </th>
                                                        <th>Job Card No : </th>
                                                        <th>Invoice No : </th>
                                                        <th>Month : </th>
                                                        <th>Year : </th>
                                                        <th>Amount : </th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>

                                                <tfoot>
                                                    <tr class="headings">
                                                        <th>Date : </th>
                                                        <th>Client Name : </th>
                                                        <th>Phone No : </th>
                                                        <th>Job Card No : </th>
                                                        <th>Invoice No : </th>
                                                        <th>Month : </th>
                                                        <th>Year : </th>
                                                        <th>Amount : </th>
                                                        <th>Action</th>
                                                    </tr>
                                                </tfoot>

                                                <tbody>
                                                    <?php foreach ($clnt_pymnt_stmnt as $value) {
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $value['date_added']; ?></td>
                                                            <td><?php echo $value['client_name']; ?></td>
                                                            <td><?php echo $value['phone_no']; ?></td>
                                                            <td><?php echo $value['job_card_no']; ?></td>
                                                            <td><?php echo $value['invoice_no']; ?></td>
                                                            <td><?php echo $value['month']; ?></td>
                                                            <td><?php echo $value['year']; ?></td>
                                                            <td><?php echo $value['amount_charged']; ?></td>

                                                            <td class="center">
                                                                <input type="hidden" name="view_statement_details_id" class="view_statement_details_id" id="view_statement_details_id" value="<?php echo $value['job_card_id']; ?>"/>
                                                                <button id="edit_statement_link" class="edit_statement_link">
                                                                    <i class="glyphicon glyphicon-edit icon-white"></i>
                                                                </button> 



                                                            </td>    
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>


                            </div>






                            <br />
                            <br />
                            <br />

                        </div>
                    </div>




                    <!-- Submit Payments Details Div Start -->
                    <div class="submit_payments_div" id="submit_payments_div" style="display: none;">

                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Make Payments <small></small></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close_submit_payments_div"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <br />
                                        <button id="go_back_invoice" class="go_back_invoice btn btn-default"><i class="fa fa-backward"></i>View Invoice</button>
                                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left submit_payment_form">



                                            <table class="table table-responsive table-striped table-bordered dataTable">
                                                <thead>
                                                <th>Asset Name</th>
                                                <th>Quantity</th>
                                                <th>@</th>
                                                <th>Discount</th>
                                                <th>Amount Charged</th>
                                                </thead>

                                                <tbody id="client_payment_div" class="client_payment_div">

                                                </tbody>

                                            </table>

                                            <table class="table">
                                                <tbody style="width: 50%">
                                                    <tr class="submit_payment_subtotal_tr">
                                                        <th style="width:50%">Subtotal:</th>
                                                        <td></td>
                                                    </tr>
                                                    <tr class="submit_payment_tax_tr">
                                                    </tr>

                                                    <tr class="submit_payment_total_tr">

                                                    </tr>
                                                </tbody>
                                            </table>


                                            <table class="table table-bordered">
                                                <thead>
                                                <th>Amount Paid To Date</th>
                                                <th>Amount to be Paid</th>
                                                <th>Balance Remaining</th>
                                                <th>Payment Method</th>
                                                <th>Payment Code</th>
                                                <th>Payment Date</th>
                                                </thead>
                                                <tbody>
                                                    <tr class="submit_payments_amount_paid">
                                                        <td>
                                                            <input type="hidden" name="total_job_card_cost_id" required="" id="total_job_card_cost_id" class="total_job_card_cost_id form-control"/>
                                                            <input type="hidden" required="" readonly="" id="submit_payments_job_card_id" name="submit_payments_job_card_id" class="submit_payments_job_card_id form-control"/>
                                                            <input type="text" required="" readonly="" id="deposit_paid" class="deposit_paid form-control" placeholder="Amount Paid To Date : " name="deposit_paid"/>
                                                            <input type="hidden" required="" readonly="" id="hidden_total_cost" class="hidden_total_cost form-control" placeholder="Total Cost" name="hidden_total_cost" />
                                                            <input type="hidden" required="" readonly="" id="hidden_discount" class="hidden_discount form-control" placeholder="Discount" name="hidden_discount"/></td>


                                                        <td>

                                                            <input type="number" min="1"  required="" name="submit_amount_paid" id="submit_amount_paid" placeholder="Amount Paid : " class="submit_amount_paid form-control"/>

                                                        </td>
                                                        <td> <input type="text" required="" readonly="" name="balance_remaining" id="balance_remaining" placeholder="Balance Remaining : "  class="form-control balance_remaining"/>  </td>  
                                                        <td><select required="" name="payment_method" id="payment_method" class="payment_method form-control">
                                                                <option value="">Please select : </option>
                                                                <option value="Cash" >Cash</option>
                                                                <option value="M-Pesa" >M-Pesa</option>
                                                                <option value="Airtel Money">Airtel Money</option>
                                                                <option value="Orange Money">Orange Money</option>
                                                                <option value="PDQ">PDQ</option>
                                                                <option value="Cheque">Cheque</option>
                                                                <option value="EFT">EFT</option>
                                                                <option value="RTGS">RTGS</option>
                                                            </select></td>
                                                        <td><textarea id="payment_code" required="" class="payment_code form-control" name="payment_code" placeholder="Payment Code and Statement"></textarea></td>
                                                        <td><input type="text" name="payment_date" id="payment_date" class="payment_date form-control" placeholder="Payment Date :"/></td>
                                                    </tr>

                                                </tbody>
                                            </table>




                                            <div class="ln_solid"></div>

                                            <div class="div_client_make_payment" id="div_client_make_payment" >

                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                        <button type="submit" class="btn btn-primary btn_cancel_payment" id="btn_cancel_payment">Cancel</button>
                                                        <button type="submit" class="btn btn-success"><i class="fa fa-credit-card"></i>Make Payment</button>
                                                    </div>
                                                </div>  
                                            </div>


                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- Submit Payments details Div End -->


                    <!--Ajax Loader Div Start -->
                    <div id="ajax_loader_div" class="ajax_loader_div" style="display: none;">

                        <img src="<?php echo base_url(); ?>assets/images/ajax_loader/ajax-loader.gif" alt="Ajax Loader" height="42" width="42"> 
                        <hr>
                        <div class="alert alert-info">Please Wait While Information is Loaded</div>
                    </div>

                    <!-- Ajax Loader Div End -->




                    <!-- footer content -->
                    <footer>
                        <div class="">
                            <p class="pull-right">Unique Loo ! by <a>Harris Dindi</a>. |
                                <span class="lead"> <i class="fa fa-paw"></i> Unique Loo!</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </footer>
                    <!-- /footer content -->


                </div>
                <!-- /page content -->
            </div>

        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>

        <!-- Sweet Alert Library -->
        <script src="<?php echo base_url(); ?>assets/dist/sweetalert-dev.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
        <!--.......................-->


        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- chart js -->
        <script src="<?php echo base_url(); ?>assets/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url(); ?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>


        <!-- Datatables -->
        <script src="<?php echo base_url(); ?>assets/js/datatables/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>
        <script>
            $(document).ready(function () {
                $('input.tableflat').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });
            });

            var asInitVals = new Array();
            $(document).ready(function () {
                var oTable = $('#example').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0]
                        } //disables sorting for column one
                    ],
                    'iDisplayLength': 12,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "<?php echo base_url('assets2/js/datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
                    }
                });
                $("tfoot input").keyup(function () {
                    /* Filter on the column based on the index of this element's parent <th> */
                    oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
                });
                $("tfoot input").each(function (i) {
                    asInitVals[i] = this.value;
                });
                $("tfoot input").focus(function () {
                    if (this.className == "search_init") {
                        this.className = "";
                        this.value = "";
                    }
                });
                $("tfoot input").blur(function (i) {
                    if (this.value == "") {
                        this.className = "search_init";
                        this.value = asInitVals[$("tfoot input").index(this)];
                    }
                });
            });
        </script>


        <script>
            $(document).ready(function () {

                $(".close_submit_payments_div").click(function () {
                    $(".ajax_loader_div").show();
                    $(".payment_statement_list").show();
                    $(".submit_payments_div").hide();
                    $(".ajax_loader_div").hide();

                });

                $(".go_back_invoice").click(function () {

                    $(".ajax_loader_div").show();
                    $(".payment_statement_list").show();
                    $(".submit_payments_div").hide();
                    $(".ajax_loader_div").hide();

                });




                $('.submit_payment_form').submit(function (event) {
                    dataString = $(".submit_payment_form").serialize();
                    $(".ajax_loader_div").show();

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/operations/submit_payment_form",
                        data: dataString,
                        success: function (data) {
                            sweetAlert("Great!...", "Paymnet made Successfully!", "success");
                            $(".payment_statement_list").show();
                            $(".submit_payments_div").hide();
                            $(".ajax_loader_div").hide();

                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }

                    });
                    event.preventDefault();
                    return false;
                });






                $(".submit_amount_paid").keyup(function () {
                    var total_cost = $(".hidden_total_cost").val();
                    var total_cost = parseInt(total_cost);
                    var amount_paid_currenty = $(".submit_amount_paid").val();

                    var amount_paid_currently = parseInt(amount_paid_currenty);
                    var deposit_paid = $(".deposit_paid").val();
                    var deposit_paid = parseInt(deposit_paid);
                    var total_paid = amount_paid_currently + deposit_paid;
                    var balance_remaining = total_cost - total_paid;

                    console.log(amount_paid_currently);
                    console.log(deposit_paid);
                    console.log(total_cost);
                    console.log(total_paid);
                    if (total_paid > total_cost) {
                        sweetAlert("Sorry!", "You cannot pay more than you owe", "warning");
                        $(".submit_amount_paid").val("");
                        $(".div_client_make_payment").hide();
                    } else {
                        $(".div_client_make_payment").show();

                    }
                    $(".balance_remaining").val(balance_remaining);
                });



              
                    
                     $(document).on('click', ".edit_statement_link", function () {
                    $(".ajax_loader_div").show();


                    var job_card_id = $(this).closest('tr').find('input[name="view_statement_details_id"]').val();

                    html1 = '';
                    htmlhead1 = '';


                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_payment_details/" + job_card_id,
                        dataType: "JSON",
                        success: function (data) {
                            $(".payment_statement_list").hide();
                            for (i = 0; i < data.length; i++) {


                                html1 += '<tr>\n\
                        <td class = "form-group">\n\
            \n\
                        <input type = "text" required=""  class = "form-control asset_name col-md-6 col-sm-6 col-xs-12 "  readonly="" name="asset_name[]" id = "asset_name" value="' + data[i].asset_name + '" placeholder = "Asset Name">\n\
            </td>\n\
            <td class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control quantity col-md-6 col-sm-6 col-xs-12 "  readonly="" name="quantity[]" id = "quantity" value="' + data[i].qty + '" placeholder = "Quantity">\n\
            </td>\n\\n\
            <td class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control value col-md-6 col-sm-6 col-xs-12 "  readonly="" name="value[]" id = "value" value="' + data[i].value + '" placeholder = "Value">\n\
            </td>\n\\n\
            <td class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control discount col-md-6 col-sm-6 col-xs-12 "  readonly="" name="discount[]" id = "discount" value="' + data[i].discount + '" placeholder = "Discount">\n\
            </td>\n\
            <td class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control col-md-6 col-sm-6 col-xs-12  amount_charged' + data[i].amount_charged + '" name="amount_charged[]" readonly=""  id = "amount_charged' + data[i].amount_charged + '" value="' + data[i].amount_charged + '" placeholder = "Amount Charged">\n\
            <input type="hidden" required="" readonly="" class="form-control hidden hidden_balance' + data[i].statement_id + ' " name="hidden_balance[]" id="hidden_balance' + data[i].statement_id + '" value="' + data[i].balance + '">\n\
            <input type="hidden" required="" readonly="" class="form-control" name="statement_id[]" id="statement_id" value="' + data[i].statement_id + '">\n\
            <input type="hidden" required="" readonly="" class="form-control" name="client_id[]" id="client_id" value="' + data[i].client_id + '">\n\
            <input type="hidden" required="" readonly="" class="form-control" name="job_card_id[]" id="job_card_id" value="' + data[i].job_card_id + '">\n\
            <input type="hidden" required="" readonly="" class="form-control" name="invoice_no[]" id="invoice_no" value="' + data[i].invoice_no + '">\n\
            <input type="hidden" required="" readonly="" class="form-control" name="amount_paid_td[]" id="amount_paid_td" value="' + data[i].amount_paid + '">\n\
</td>\n\
                   </tr>';
                                $("#client_payment_div").on("keyup", ".amount_paid" + data[i].statement_id, function () {

                                    var amount_paid = this.value;
                                    var amount_paid = parseFloat(amount_paid);
                                    var amount_owed = $("#" + this.id.replace("amount_paid", "hidden_balance")).val();
                                    var amount_owed = parseFloat(amount_owed);

                                    if (amount_paid > amount_owed) {
                                        sweetAlert("Oops...", "You cannot Pay more than you owe!", "error");

                                        $(".div_client_make_payment").hide("slow");
                                    } else {
                                        var new_balance = amount_owed - amount_paid;

                                        $("#" + this.id.replace("amount_paid", "balance")).val(new_balance);
                                        $(".div_client_make_payment").show("slow");
                                    }
                                });
                            }

                            $.ajax({
                                type: "GET",
                                url: "<?php echo base_url(); ?>operations/get_total_job_value/" + job_card_id,
                                dataType: 'JSON',
                                success: function (data) {
                                    var job_card_id = data[0].job_card_id;
                                    var statement_id = data[0].id;
                                    var total_job_price = data[0].amount_cr;
                                    var discount = data[0].discount;
                                    var total_job_price = total_job_price - discount;
                                    var tax = total_job_price * 0.16;
                                    var sub_total = total_job_price * 0.84;
                                    var amount_cr = data[0].amount_cr;
                                    var amount_dr = data[0].amount_dr;
                                    var deposit_paid = amount_dr - discount;
                                    var balance_remaining = amount_cr - amount_dr - discount;
                                    var pymnt_code = data[0].pymnt_code;
                                    var pymnt_mthd = data[0].pymnt_mthd;
                                    var payment_date = data[0].payment_date;

                                    $(".submit_payments_job_card_id").val(job_card_id);
                                    $(".total_job_card_cost_id").val(statement_id);
                                    $('.job_card_id_payment').val(job_card_id);
                                    $(".submit_payment_subtotal_tr").empty();
                                    $(".submit_payment_subtotal_tr").append("  <th>Subtotal:</th> <td>" + sub_total + "</td>");
                                    $(".submit_payment_tax_tr").empty();
                                    $(".submit_payment_tax_tr").append(" <th>Tax (16.0%)</th><td>" + tax + "</td>");
                                    $(".submit_payment_total_tr").empty();
                                    $(".submit_payment_total_tr").append("<th>Total:</th> <td>" + total_job_price + "</td>");
                                    if (deposit_paid < 0) {
                                        var deposit_paid = 0;
                                        $(".deposit_paid").val(deposit_paid);
                                    } else {
                                        $(".deposit_paid").val(deposit_paid);
                                    }

                                    $(".balance_remaining").val(balance_remaining);
                                    $(".hidden_total_cost").val(amount_cr);
                                    $(".hidden_discount").val(discount);
                                    $('.payment_code').val(pymnt_code);
                                    $('.payment_date').val(payment_date);
                                    $('.payment_method option[value=' + pymnt_mthd + ']').attr("selected", "selected");

                                }, error: function (data) {
                                    sweetAlert("Oops...", "Something went wrong!", "error");

                                }
                            });




                            $('#client_payment_div').empty();
                            $('#client_payment_div').append(html1);


                            $(".invoice_form_details_div").hide();
                            $(".submit_payments_div").show();
                            $(".ajax_loader_div").hide();


                        },
                        error: function (data) {

                        }
                    });




                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_blld_clnt_info/" + job_card_id,
                        dataType: "JSON",
                        success: function (data) {
                            var client_id = data[0].client_id;
                            var client_name = data[0].client_name;
                            var address = data[0].address;
                            var website = data[0].website;
                            var phone_no = data[0].phone_no;
                            var email = data[0].email;
                            var job_card_no = data[0].job_card_no;
                            var invoice_no = data[0].invoice_no;
                            $(".billed_client").empty();
                            $(".billed_client").append(" To <address> <strong>" + client_name + "</strong> <br> " + address + " <br>Phone: " + phone_no + " <br>Email: " + email + "  </address>");

                            $(".billed_client_invoice_details").empty();
                            $(".billed_client_invoice_details").append(" <b>Invoice # " + invoice_no + "</b> <br>  <b>Order ID:</b> " + job_card_no + " <br> <b>Payment Due:</b> 2/22/2014 <br>  ");
                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }
                    });




                });





            });




            $(document).ready(function () {
                $('#payment_statement_table').DataTable({
                    "scrollY": "200px",
                    "scrollCollapse": true,
                    "paging": false
                });
            });
        </script>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script>
            var x = jQuery.noConflict();
            x(function () {
                x("#payment_date").datepicker(
                        {dateFormat: 'yy-mm-dd'}
                );



            });
        </script>


    </body>

</html>