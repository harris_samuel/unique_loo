<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Unique Loo! | </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">

        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    </head>


    <body class="nav-md">

        <div class="container body">


            <div class="main_container">


                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">

                        <div class="navbar nav_title" style="border: 0;">

                            <a href="<?php echo base_url(); ?>"> <img  style="margin-left: 60px; margin-right: 0px; margin-bottom: 0px; margin-top: 0px;" src="<?php echo base_url(); ?>images/logo.png" alt="logo"  /></a>

                        </div>
                        <div class="clearfix"></div>



                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">
                                    <li><a href="<?php echo base_url(); ?>" ><i class="fa fa-home"></i> HOME <span class="fa fa-chevron-right"></span></a> </li>




                                    <li><a><i class="fa fa-edit"></i> ADMINISTRATION <span class="fa fa-chevron-down"></span></a>

                                        <ul class="nav child_menu" style="display: none">

                                            <li>


                                                <?php
                                                $base_url = base_url();
                                                foreach ($admin_functions as $value) {
                                                    $controller_name = $value['controller_name'];
                                                    $function_name = $value['functions_name'];
                                                    $i_tag = $value['i_tag'];
                                                    $description = $value['description'];
                                                    $span_tag = $value['span_tag'];
                                                    ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>



                                        </ul> 

                                    </li>

                                    <li><a><i class="fa fa-edit"></i> DAILY OPERATIONS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($daily_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>






                                        </ul>
                                    </li>



                                    <li><a><i class="fa fa-edit"></i> REPORTS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($reports_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>







                                        </ul>
                                    </li>



                                </ul>
                            </div>

                        </div>
                        <!-- /sidebar menu -->


                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" href="<?php echo base_url(); ?>home/do_logout" title="Logout">
                                <span class="glyphicon glyphicon-off"  aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->




                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">

                    <div class="nav_menu">
                        <nav class="" user="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="<?php echo base_url(); ?>assets/images/img.jpg" alt=""><?php
                                        $username = $this->session->userdata('user_name');
                                        echo $username;
                                        ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <li><a href="<?php echo base_url(); ?>admin/profile">  Profile</a>
                                        </li>


                                        <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                        </li>
                                    </ul>
                                </li>


                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->





                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="page-title">
                            <div class="title_left">
                                <h3>
                                    Unique Loo
                                    <small>
                                        Daily Operations Report 
                                    </small>
                                </h3>
                            </div>

                        </div>
                        <div class="clearfix"></div>

                        <div class="row">

                            <div id="payment_report_div" class="payment_report_div">



                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Daily Operations <small>Revenues and Expense per Job Card per client YTD</small></h2>
                                            <br>

                                            <div id="info_box_reload" class=" info_box_reload" style="display: none; margin-left: 187px;" >
                                                <h5>Information has been Updated, Please reload the  page to view up to date information. </h5>
                                                <a class="btn btn-primary btn-xs reload_information " id="reload_information" href="<?php echo base_url(); ?>operations/view_payments" >
                                                    <i class="fa fa-refresh"></i>
                                                    Reload Asset
                                                </a>   

                                            </div>

                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content daily_report_div" id="daily_report_div">
                                            <table id="example" class="table table-striped table-bordered responsive-utilities jambo_table">
                                                <thead>
                                                    <tr class="headings">
                                                        <th>No</th>
                                                        <th>Client Name </th>
                                                        <th>Job Card No </th>
                                                        <th>Invoice No </th>
                                                        <th>Amount Billed</th>
                                                        <th>Amount Paid</th>
                                                        <th>Balance</th>
                                                        <th>Payment Date</th>
                                                        <th>Event Date</th>
                                                        <th>Date Sourced</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>

                                                <tfoot>
                                                    <tr class="headings">
                                                        <th>No</th>
                                                        <th>Client Name </th>
                                                        <th>Job Card No </th>
                                                        <th>Invoice No </th>
                                                        <th>Amount Billed</th>
                                                        <th>Amount Paid</th>
                                                        <th>Balance</th>
                                                        <th>Payment Date</th>
                                                        <th>Event Date</th>
                                                        <th>Date Sourced</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </tfoot>

                                                <tbody>
                                                    <?php
                                                    $i = 1;
                                                    foreach ($payment_report as $value) {
                                                        ?>
                                                        <tr class="daily_report_tr" id="daily_report_tr" >
                                                            <td class="center"><?php echo $i; ?></td>
                                                            <td><?php echo $value['client_name']; ?></td>
                                                            <td><?php echo $value['job_card_no']; ?></td>
                                                            <td><?php echo $value['invoice_no']; ?></td>
                                                            <td><?php echo $value['amount_billed']; ?></td>
                                                            <td><?php echo $value['amount_paid']; ?></td>
                                                            <td><?php echo $value['balance']; ?></td>
                                                            <td><?php echo $value['payment_date']; ?></td>
                                                            <td><?php echo $value['event_date']; ?></td>
                                                            <td><?php echo $value['date_srcd']; ?></td>

                                                            <td class="center">
                                                                <input type="hidden" name="edit_job_card_id" class="edit_job_card_id" id="edit_job_card_id" value="<?php echo $value['job_card_id']; ?>"/>
                                                                <input type="hidden" name="edit_job_card_no" class="edit_job_card_no" id="edit_job_card_no" value="<?php echo $value['job_card_no']; ?>"/>

                                                                <button  id="assign_resource_link" data-toggle="tooltip" data-placement="top" data-original-title="Assign Resources " class="assign_resource_link" >
                                                                    <i class="glyphicon glyphicon-edit icon-white"></i>

                                                                </button>|
                                                                <button  id="delete_job_card_link" data-toggle="tooltip" data-placement="top" data-original-title="Delete Job Card " class="delete_job_card_link" >
                                                                    <i class="glyphicon glyphicon-trash icon-white"></i>

                                                                </button>
                                                            </td>

                                                        </tr>
                                                        <?php
                                                        $i++;
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>



                                       

                                    </div>
                                </div>




                            </div>

                            <br />
                            <br />
                            <br />

                        </div>
                    </div>






                    <!-- Assign resources to  Job List Start -->

                    <div class="col-md-6 col-sm-12 col-xs-12 cleint_job_list_div" id="cleint_job_list_div" style="display: none;">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2> Assign Assets  <small></small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li><a class="close_assign_asset"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">



                                <div class="form-group">

                                    <div class="col-sm-12">





                                        <div id="booking_info_div" class="booking_info_div">

                                            <button id="view_payments_btn" class="btn btn-default view_payments_btn">View Payments   <i class="glyphicon glyphicon-backward icon-white"></i></button>

                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <th></th>
                                                <th></th>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input type="hidden" name="client_id_v" id="client_id_v" class="client_id_v form-control" readonly="readonly" placeholder="Cleint Id : "/>
                                                            <!--<input type="hidden" name="job_card_id_v" class="job_card_id_v" id="job_card_id_v" />-->
                                                            <input type="hidden" name="asset_tracker_id_v" class="asset_tracker_id_v" id="asset_tracker_id_v"/>
                                                            <br>
                                                            <label class="control-label col-md-6 col-sm-6 col-xs-12">Client Name : </label>

                                                            <input type="text" name="client_name_v" id="client_name_v" class="client_name_v form-control" readonly="readonly" placeholder="Client Name : "/>

                                                        </td>
                                                        <td>
                                                            <label class="control-label col-md-6 col-sm-6 col-xs-12">Job Card No : </label>

                                                            <input type="text" name="job_card_no_v" id="job_card_no_v" class="job_card_no_v form-control" readonly="readonly" placeholder="Job Card No : "/>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label col-md-6 col-sm-6 col-xs-12">Total Price : </label>

                                                            <input type="text" name="total_price_v" id="total_price_v" class="total_price_v form-control " readonly="readonly" placeholder="Total Price : "/>

                                                        </td>

                                                        <td>
                                                            <label class="control-label col-md-6 col-sm-6 col-xs-12">Invoice No : </label>

                                                            <input type="text" name="invoice_no_v" id="invoice_no_v" class="invoice_no_v form-control" readonly="readonly" placeholder="Invoice No : "/>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label col-md-6 col-sm-6 col-xs-12">Estimated Location Distance : </label>

                                                            <input type="text" name="estmtd_lctn_dstnce_v" id="estmtd_lctn_dstnce_v" class="estmtd_lctn_dstnce_v form-control" readonly="" placeholder="Estimated Location Distance : "/>

                                                        </td>
                                                        <td>
                                                            <label class="control-label col-md-6 col-sm-6 col-xs-12">Confirmed Location Distance : </label>

                                                            <input type="text" name="cnfrmd_lctn_dstnce_v" id="cnfrmd_lctn_dstnce_v" readonly="" class="cnfrmd_lctn_dstnce_v form-control" placeholder="Confirmed Location Distance : "/>

                                                        </td>

                                                    </tr>
                                                    <tr>

                                                        <td>
                                                            <label class="control-label col-md-6 col-sm-6 col-xs-12">Delivery Date : </label>

                                                            <input readonly="readonly" required="required" name="delivery_date_v" id="delivery_date_v" class="delivery_date_v form-control" type="text" placeholder="Delivery Date : "/>

                                                        </td>
                                                        <td>
                                                            <label class="control-label col-md-6 col-sm-6 col-xs-12">Event Date : </label>

                                                            <input readonly="readonly" required="required" name="event_date_v" id="event_date_v" class="event_date_v form-control" type="text" placeholder="Event Date : "/>

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>

                                        <div class="dashed-h">
                                            <hr>
                                        </div>



                                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Assets</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false">Support Service</a>
                                                </li>

                                            </ul>
                                            <div id="myTabContent" class="tab-content">


                                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                                    <form class="assign_asset_form" id="assign_asset_form" >
                                                        <input type="hidden" name="job_card_id_v" class="job_card_id_v" id="job_card_id_v" />

                                                        <div id="assign_asset_div" class="assign_asset_div">
                                                            <select name="asset_type" id="asset_type" class="asset_type  form-control">
                                                                <option>Please Select Asset Type : </option>
                                                                <?php
                                                                foreach ($asset_types as $value) {
                                                                    ?>
                                                                    <option value="<?php echo $value['name']; ?>" ><?php echo $value['name']; ?></option>
                                                                    <?php
                                                                }
                                                                ?>    
                                                            </select> 
                                                            <br/>

                                                            <div>

                                                                <select name="asset_name" id="asset_name" class="asset_name  form-control">
                                                                    <option>Please select  : </option>
                                                                </select>
                                                            </div>
                                                            <br/>
                                                            <div>
                                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                                    <button type="submit" class="btn btn-sm btn-default assign_asset_btn" id="assign_asset_btn" name="assign_asset_btn">Assign Asset : </button>    
                                                                </div>

                                                            </div>
                                                            <br/>
                                                            <div>
                                                            </div>
                                                            <br/>

                                                        </div>

                                                    </form>

                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                                    <form id="assign_asset_form_1" class="assign_asset_form_1 form">

                                                        <input type="hidden" name="asset_type_1" id="asset_type_1" class="asset_type_1" value="Support Service"/>
                                                        <input type="hidden" name="job_card_id_v_1" class="job_card_id_v_1" id="job_card_id_v_1" />
                                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                            <input type="text" name="asset_name_1" class="form-control asset_name_1"/>

                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                            <input type="number" min="1"  name="asset_value" class="asset_value form-control" id="asset_value" placeholder="Value (KES : )"/>
                                                        </div>

                                                        <div class="ln_solid"></div>
                                                        <div class="form-group">
                                                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                                <button type="button" class="btn btn-primary cancel_assign_asset_btn" id="cancel_assign_asset_btn">Cancel</button>
                                                                <button type="submit" class="btn btn-success book_job_btn" id="book_job_btn">Assign </button>
                                                            </div>
                                                        </div>

                                                    </form>

                                                </div>

                                            </div>
                                        </div>









                                    </div>
                                </div>





                            </div>
                        </div>
                    </div>



                    <!-- Assign resources to  Job List  End -->

















                    <!-- footer content -->
                    <footer>
                        <div class="">
                            <p class="pull-right">Unique Loo ! by <a>Harris Dindi</a>. |
                                <span class="lead"> <i class="fa fa-paw"></i> Unique Loo!</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </footer>
                    <!-- /footer content -->


                </div>
                <!-- /page content -->
            </div>

        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>

        <!-- Sweet Alert Library -->
        <script src="<?php echo base_url(); ?>assets/dist/sweetalert-dev.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
        <!--.......................-->


        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- chart js -->
        <script src="<?php echo base_url(); ?>assets/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url(); ?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>


        <!-- Datatables -->
        <script src="<?php echo base_url(); ?>assets/js/datatables/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>
        <script>
            $(document).ready(function () {

                $('.delete_job_card_link').on('click', function () {

                    //get
                    var job_card_id = $(this).closest('tr').find('input[name="edit_job_card_id"]').val();
                    var job_card_no = $(this).closest('tr').find('input[name="edit_job_card_no"]').val();


                    swal({
                        title: "Do you want to delete  Job Card No : " + job_card_no + " ?",
                        text: "Please Note You will not be able to re-do this action!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Delete it!",
                        closeOnConfirm: false
                    },
                    function () {


                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>operations/delete_job_card_details/" + job_card_id + "/" + job_card_no,
                            dataType: "JSON",
                            success: function (data) {
                                var response = data[0].response;
                                if (response === "Job Deleted successfully!") {
                                    swal("Success!", "Job Deleted Successfully.", "success");

                                    $('.info_box_reload').show("slow");

                                } else if (response === "Job was not deleted!") {
                                    swal("Oops...", "Something went wrong!", "error");

                                }
                            }, error: function (data) {
                                var response = data[0].response;
                                if (response === "Job Deleted successfully!") {
                                    swal("Success!", "Job Deleted Successfully.", "success");
                                    $('.info_box_reload').show("slow");
                                } else if (response === "Job was not deleted!") {
                                    swal("Oops...", "Something went wrong!", "error");

                                }
                            }
                        });


                    });


                });




                $(".view_payments_btn").click(function () {

                    $(".ajax_loader_div").show('slow');
                    $(".payment_report_div").show("slow");
                    $(".ajax_loader_div").hide('slow');
                    $(".cleint_job_list_div").hide('slow');
                    $(".assigned_assets_list_div").hide('slow');

                });

                $(".close_assign_asset").click(function () {

                    $(".ajax_loader_div").show('slow');
                    $(".payment_report_div").show("slow");
                    $(".ajax_loader_div").hide('slow');
                    $(".cleint_job_list_div").hide('slow');
                    $(".assigned_assets_list_div").hide('slow');

                });



                $(".cancel_assign_asset_btn").click(function () {

                    $(".ajax_loader_div").show('slow');
                    $(".payment_report_div").show("slow");
                    $(".ajax_loader_div").hide('slow');
                    $(".cleint_job_list_div").hide('slow');
                    $(".assigned_assets_list_div").hide('slow');

                });




                $('.assign_resource_link').on('click', function () {



                    //get
                    var job_card_id = $(this).closest('tr').find('input[name="edit_job_card_id"]').val();

                    $('.ajax_loader_div').show();
                    $('.payment_report_div').hide("slow");
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>operations/get_job_details/" + job_card_id,
                        dataType: "JSON",
                        success: function (data) {
                            $('.ajax_loader_div').hide();
                            $('.cleint_job_list_div').show();

                            $('.client_id_v').val(data[0].client_id);
                            $('.client_name_v').val(data[0].client_name);
                            $('.job_card_no_v').val(data[0].job_card_no);
                            $('.total_price_v').val(data[0].total_price);
                            $('.invoice_no_v').val(data[0].invoice_no);
                            $('.estmtd_lctn_dstnce_v').val(data[0].estmtd_lctn_dstnce);
                            $('.cnfrmd_lctn_dstnce_v').val(data[0].cnfrmd_lctn_dstnce);
                            $('.event_date_v').val(data[0].dte_evnt);
                            $('.delivery_date_v').val(data[0].dte_srcd);
                            $('.job_card_id_v').val(data[0].job_card_id);
                            $('.job_card_id_v_1').val(data[0].job_card_id);
                            $('.asset_tracker_id_v').val(data[0].asset_tracker_id);
                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }
                    });

                });





                $('#assign_asset_form_1').submit(function (event) {
                    dataString = $("#assign_asset_form_1").serialize();
                    $(".ajax_loader_div").show('slow');

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>operations/assign_asset",
                        data: dataString,
                        success: function (data) {


                            $(".ajax_loader_div").hide('slow');
                            $(".cleint_job_list_div").show('slow');
                            $(".assigned_assets_list_div").show('slow');


                            $(".ajax_loader_div").hide('slow');
                            sweetAlert("Success", "Asset Assigned Successfully", "success")
                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }

                    });
                    event.preventDefault();
                    return false;
                });


                $('#assign_asset_form').submit(function (event) {
                    dataString = $("#assign_asset_form").serialize();
                    $(".ajax_loader_div").show('slow');

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>operations/assign_asset",
                        data: dataString,
                        success: function (data) {

                            $(".ajax_loader_div").hide('slow');
                            $(".cleint_job_list_div").show('slow');
                            $(".assigned_assets_list_div").show('slow');
                            $(".ajax_loader_div").hide('slow');

                            sweetAlert("Success", "Asset Assigned Successfully", "success")

                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }

                    });
                    event.preventDefault();
                    return false;
                });




                $('.asset_type').change(function () {
                    var asset_type = $('.asset_type').val();
                    remaining_assets_list = '';
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_remaining_assets/" + asset_type,
                        dataType: "JSON",
                        success: function (response) {
                            remaining_assets_list = $('#asset_name').empty();
                            for (i = 0; i < response.length; i++) {

                                var value = response[i].name;

                                if (value === "No Assets Found") {

                                    $("#asset_name").hide();

                                } else {
                                    remaining_assets_list = '<option value="' + response[i].name + '">' + response[i].name + '</option>';
                                    $('#asset_name').append(remaining_assets_list);
                                    $("#asset_name").show();


                                }


                            }


                        }, error: function (repsonse) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }
                    });

                });









































                $(".filter_report_btn").click(function () {
                    $(".daily_report_div").hide();
                    $(".filter_report_div").show();
                });

                $(".go_back_btn").click(function () {
                    $(".daily_report_div").show();
                    $(".filter_report_div").hide();
                });




                $('input.tableflat').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });
            });

            var asInitVals = new Array();
            $(document).ready(function () {
                var oTable = $('#example').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0]
                        } //disables sorting for column one
                    ],
                    'iDisplayLength': 12,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "<?php echo base_url('assets2/js/datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
                    }
                });
                $("tfoot input").keyup(function () {
                    /* Filter on the column based on the index of this element's parent <th> */
                    oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
                });
                $("tfoot input").each(function (i) {
                    asInitVals[i] = this.value;
                });
                $("tfoot input").focus(function () {
                    if (this.className == "search_init") {
                        this.className = "";
                        this.value = "";
                    }
                });
                $("tfoot input").blur(function (i) {
                    if (this.value == "") {
                        this.className = "search_init";
                        this.value = asInitVals[$("tfoot input").index(this)];
                    }
                });
            });
        </script>
    </body>

</html>