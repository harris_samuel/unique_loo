<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Unique Loo |Employees </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">

        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    </head>


    <body class="nav-md">

        <div class="container body">


            <div class="main_container">





                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">

                        <div class="navbar nav_title" style="border: 0;">

                            <a href="<?php echo base_url(); ?>"> <img  style="margin-left: 60px; margin-right: 0px; margin-bottom: 0px; margin-top: 0px;" src="<?php echo base_url(); ?>images/logo.png" alt="logo"  /></a>


                        </div>
                        <div class="clearfix"></div>



                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">
                                    <li><a href="<?php echo base_url(); ?>" ><i class="fa fa-home"></i> HOME <span class="fa fa-chevron-right"></span></a> </li>




                                    <li><a><i class="fa fa-edit"></i> ADMINISTRATION <span class="fa fa-chevron-down"></span></a>

                                        <ul class="nav child_menu" style="display: none">

                                            <li>


                                                <?php
                                                $base_url = base_url();
                                                foreach ($admin_functions as $value) {
                                                    $controller_name = $value['controller_name'];
                                                    $function_name = $value['functions_name'];
                                                    $i_tag = $value['i_tag'];
                                                    $description = $value['description'];
                                                    $span_tag = $value['span_tag'];
                                                    ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>



                                        </ul> 

                                    </li>

                                    <li><a><i class="fa fa-edit"></i> DAILY OPERATIONS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($daily_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>






                                        </ul>
                                    </li>



                                    <li><a><i class="fa fa-edit"></i> REPORTS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($reports_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>







                                        </ul>
                                    </li>



                                </ul>
                            </div>

                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" href="<?php echo base_url(); ?>home/do_logout" title="Logout">
                                <span class="glyphicon glyphicon-off"  aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">

                    <div class="nav_menu">
                        <nav class="" user="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="<?php echo base_url(); ?>assets/images/img.jpg" alt=""><?php
                                        $username = $this->session->userdata('user_name');
                                        echo $username;
                                        ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <li><a href="<?php echo base_url(); ?>admin/profile">  Profile</a>
                                        </li>


                                        <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                        </li>
                                    </ul>
                                </li>


                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->






                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="page-title">
                            <div class="title_left">
                                <h3>
                                    Employees

                                </h3>
                            </div>


                        </div>
                        <div class="clearfix"></div>


                        <!-- Add New Employee Form start -->


                        <div class="row add_new_employee_div" id="add_new_employee_div" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Unique Loo  Add Employee </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                            </li>

                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left add_new_employee_form input_mask" id="add_new_employee_form" method="post">

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left employee_no"required="required" id="inputSuccess2" name="employee_no" placeholder="Employee No : ">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                <button type="button" name="check_employee_no_existence" class="check_employee_no_existence btn btn-primary btn-sm" id="check_employee_no_existence">Next</button>
                                                <p class="info"> Please note : The Employee Number has to be unique to all Employees. [ Hint...:  The format for employee no is : UL_  e.g UL_001,2,3... ] </p>

                                            </div>

                                            <div id="add_employee_other_info" class="add_employee_other_info" style="display: none;">
                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                    <input type="text" class="form-control has-feedback-left f_name"required="required" id="inputSuccess2" name="f_name" placeholder="First Name">
                                                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                </div>

                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                    <input type="text" class="form-control has-feedback-left s_name" required="required" id="inputSuccess2" name="s_name" placeholder="Sur Name">
                                                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                </div>

                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                    <input type="text" class="form-control has-feedback-left o_name" id="inputSuccess3" name="o_name" placeholder="Other Name">
                                                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                </div>

                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                    <select class="form-control gender has-feedback-left gender" required="" id="selectSuccess5" name="gender">
                                                        <option value="">Please select Gender : </option>
                                                        <option value="Female">Female</option>
                                                        <option value="Male">Male</option>
                                                    </select>

                                                </div>

                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                    <input type="text" class="form-control has-feedback-left id_no" required="required" id="inputSuccess2" name="id_no" placeholder="ID / Military/Passport No">
                                                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                </div>

                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                    <input type="text" class="form-control has-feedback-left email" id="inputSuccess4" name="email" placeholder="Email">
                                                    <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                                                </div>



                                                <div id="check_email_div" class="check_email_div" style="display: inline;">



                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                        <input type="text" class="form-control has-feedback-left phone_no" id="inputSuccess5" name="phone_no" placeholder="Phone">
                                                        <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                                                    </div>

                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">

                                                        <select class="form-control has-feedback-left marital_status" id="inputSuccess5" name="marital_status" placeholder="Marital Status" required="">
                                                            <option value="">Please select : </option>
                                                            <option value="Single" >Single</option>
                                                            <option value="Engaged" >Engaged</option>
                                                            <option value="Married" >Married</option>
                                                            <option value="Divorced" >Divorced</option>
                                                            <option value="Widowed" >Widowed</option>
                                                        </select>
                                                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                    </div>


                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                        <input type="text" class="form-control has-feedback-left nationality" id="inputSuccess5" name="nationality" placeholder="Country">
                                                        <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                                                    </div>



                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input class="date-picker form-control  has-feedback-left DoB"  id="DoB" name="dob"  type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Role : </label>
                                                        <select required="" class=" form-control  has-feedback-left role_id" name="role_id">
                                                            <option value="" >Please select Role : </option>                                                   
                                                            <?php foreach ($roles as $value) {
                                                                ?>
                                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                            <?php }
                                                            ?>
                                                        </select>

                                                    </div>

                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Department : </label>
                                                        <select required="" class=" form-control  has-feedback-left department_id" name="department_id">
                                                            <option value="">Please select Department : </option>
                                                            <?php foreach ($department as $value) {
                                                                ?>
                                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                            <?php }
                                                            ?>
                                                        </select>

                                                    </div>



                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Employee Status : </label>
                                                        <select required="" class=" form-control  has-feedback-left employee_status" name="employee_status">
                                                            <option value="">Please select Employee Status : </option>
                                                            <option value="Active">Active</option>
                                                            <option value="In Active">In Active</option>
                                                        </select>

                                                    </div>






                                                    <div class="ln_solid"></div>
                                                    <div class="form-group">
                                                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                            <button type="reset" id="reset_add_employee_form" class="btn btn-primary">Cancel</button>
                                                            <button type="submit" id="add_employee_form_btn" class=" add_employee_form_btn btn btn-success">Submit</button>
                                                        </div>
                                                    </div>



                                                </div>

                                            </div>





                                        </form>
                                    </div>





                                </div>
                            </div>
                        </div>

                        <!-- Add New Employee Form End --> 



                        <!-- View Employee Form Start -->

                        <div class="row view_employee_div" id="view_employee_div" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Unique Loo  View Employee </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                            </li>

                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left view_employee_form input_mask" id="view_employee_form" method="post">


                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control  has-feedback-left employee_no_v" required="required"  readonly="" id="employee_no_v" name="employee_no_v" placeholder="Employee No">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>


                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control  has-feedback-left employee_name_v" required="required"  readonly="" id="employee_name_v" name="employee_name_v" placeholder="First Name">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left id_no_v " required="required" readonly="" id="id_no_v" name="id_no_v" placeholder="Sur Name">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left dob_v " id="dob_v" readonly="" name="dob_v" placeholder="Other Name">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>


                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left gender_v " id="gender_v" readonly="" name="gender_v" placeholder="Gender">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left marital_status_v " readonly="" required="required" id="marital_status_v" name="marital_status_v " placeholder="ID / Military/Passport No">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left nationality_v" readonly="" id="nationality_v" name="nationality_v" placeholder="Nationality ">
                                                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left department_name_v" readonly="" id="department_name_v" name="department_name_v" placeholder="Deapartment">
                                                <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left role_name_v" readonly="" id="role_name_v" name="role_name_v" placeholder="Role : ">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>


                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left time_stamp_v" readonly="" required="required" id="time_stamp_v" name="time_stamp_v" placeholder="Last Transaction Stamp ">
                                                <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left phone_no_v" readonly="" required="required" id="phone_no_v" name="phone_no_v" placeholder="Phone No:  ">
                                                <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                                            </div>


                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <a id="go_back_view" class="btn btn-info btn-xs go_back_view"><i class="fa fa-backward"></i> Go Back  <span class="fa fa-chevron-left"></span></a>

                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">



                                            </div>






                                        </form>
                                    </div>






                                </div>
                            </div>
                        </div>

                        <!-- View Employee Form End -->



                        <!--  Edit Employee Form Start-->

                        <div class="row edit_employee_div" id="edit_employee_div" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Unique Loo  Edit Employee </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                            </li>

                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left edit_employee_form input_mask" id="edit_employee_form" method="post">

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left edit_employee_no"required="required" id="edit_employee_no" name="edit_employee_no" placeholder="Employee No">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                <p class="warning">Note : This field is unique and is required!</p>
                                            </div>


                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left edit_f_name"required="required" id="edit_f_name" name="edit_f_name" placeholder="First Name">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left edit_s_name" required="required" id="edit_s_name" name="edit_s_name" placeholder="Sur Name">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left edit_o_name" id="edit_o_name" name="edit_o_name" placeholder="Other Name">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <select required="required" class="form-control edit_gender has-feedback-left"  id="edit_gender" name="edit_gender">
                                                    <option value="">Please select Gender : </option>
                                                    <option value="Female">Female</option>
                                                    <option value="Male">Male</option>
                                                </select>

                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left edit_id_no" required="required" id="edit_id_no" name="edit_id_no" placeholder="ID / Military/Passport No">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left edit_email" id="edit_email" name="edit_email" placeholder="Email">
                                                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left edit_phone_no" id="edit_phone_no" name="edit_phone_no" placeholder="Phone">
                                                <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <select required="required" class="form-control has-feedback-left edit_marital_status"  id="edit_marital_status" name="edit_marital_status" placeholder="Marital Status" >
                                                    <option value="">Please select : </option>
                                                    <option value="Single">Single</option>
                                                    <option value="Engaged">Engaged</option>
                                                    <option value="Married">Married</option>
                                                    <option value="Divorced">Divorced</option>
                                                    <option value="Widowed">Widowed</option>
                                                </select>

                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>


                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left edit_nationality" required="required" id="edit_nationality" name="edit_nationality" placeholder="Country">
                                                <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input class="date-picker form-control  has-feedback-left edit_dob "  id="edit_dob" name="edit_dob" required="required" type="text">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Role : </label>
                                                <select class=" form-control  has-feedback-left edit_role_id" id="edit_role_id" name="edit_role_id">
                                                    <?php foreach ($roles as $value) {
                                                        ?>
                                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>

                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Department : </label>
                                                <select class=" form-control  has-feedback-left edit_department_id" id="edit_department_id" name="edit_department_id">
                                                    <?php foreach ($department as $value) {
                                                        ?>
                                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>

                                            </div>



                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Status : </label>
                                                <select required="" class=" form-control  has-feedback-left edit_status" id="edit_status" name="edit_status">
                                                    <option value="" >Please select : </option>
                                                    <option value="Active">Active</option>   
                                                    <option value="In Active">In Active</option>
                                                </select>

                                            </div>


                                            <input type="hidden" name="edit_employee_id" id="edit_employee_id" class="edit_employee_id "/>



                                            <div class="ln_solid"></div>
                                            <div class="form-group">
                                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                    <button type="reset" id="go_back_edit" class="btn btn-primary go_back_edit">Cancel</button>
                                                    <button type="submit" id="edit_employee_form_btn" class=" edit_employee_form_btn btn btn-success">Update </button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>





                                </div>
                            </div>
                        </div>





                        <!--  Edit Employee Form End-->

                        <!--Delete Employee Form Start -->

                        <div class="row delete_employee_div" id="delete_employee_div" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Unique Loo  Edit Employee </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                            </li>

                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left delete_employee_form input_mask" id="delete_employee_form" method="post">




                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <blockquote class="blockquote-reverse">
                                                    <h6>Are you sure you want to Delete Employee : </h6> </blockquote> 
                                                <br>
                                                <input id="delete_employee_name" readonly="readonly" class="delete_employee_name form-control has-feedback-left"/>
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                <input type="hidden" name="delete_employee_id" class=" hidden delete_employee_id" id="delete_employee_id"/>

                                                <div class="ln_solid"></div>
                                                <div class="form-group">
                                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                        <button type="reset" id="cancel_delete_button" class="btn btn-primary cancel_delete_button">Cancel</button>
                                                        <button type="submit" id="delete_employee_form_btn" class=" delete_employee_form_btn btn btn-success">Delete </button>
                                                    </div>
                                                </div>

                                            </div>



                                        </form>
                                    </div>






                                </div>
                            </div>
                        </div>

                        <!-- Delete Employee Form End  -->
                        <!--Ajax Loader Div Start -->
                        <div id="ajax_loader_div" class="ajax_loader_div" style="display: none;">

                            <img src="<?php echo base_url(); ?>assets/images/ajax_loader/ajax-loader.gif" alt="Ajax Loader" height="42" width="42"> 
                            <hr>
                            <div class="alert alert-info">Please Wait While Information is Loaded</div>
                        </div>

                        <!-- Ajax Loader Div End -->





                        <div class="row employees_table_div" id="employees_table_div">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Unique Loo  Employees </h2>
                                        <br>
                                        <div id="info_box_reload" class=" info_box_reload" style="display: none; margin-left: 187px;" >
                                            <h5>Information has been Updated, Please reload the  page to view up to date information. </h5>
                                            <a class="btn btn-primary btn-xs reload_information " id="reload_information" href="<?php echo base_url(); ?>admin/employees" >
                                                <i class="fa fa-refresh"></i>
                                                Reload Employee
                                            </a>   

                                        </div>

                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                            </li>

                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>


                                    <div class="x_content">

                                        <a class="btn btn-primary btn-xs add_new_employee " id="add_new_employee" >
                                            <i class="fa fa-plus"></i>
                                            Add Employee
                                        </a>



                                        <table id="employee_table" class="table table-striped table-bordered table-condensed table-hover table-responsive responsive-utilities jambo_table employee_table">
                                            <thead>
                                                <tr class="headings">

                                                    <th>No </th>
                                                    <th>Employee No : </th>
                                                    <th>Name </th>
                                                    <th>ID No </th>
                                                    <th>Gender</th>
                                                    <th>D.o.B </th>
                                                    <th>Marital Status </th>
                                                    <th>Nationality </th>
                                                    <th>Department</th>
                                                    <th>Role</th>
                                                    <th>Status</th>
                                                    <th class=" no-link last"><span class="nobr">Action</span>
                                                    </th>
                                                </tr>
                                            </thead>



                                            <tfoot>
                                                <tr class="footer">

                                                    <th>No </th>
                                                    <th>Employee No : </th>
                                                    <th>Name </th>
                                                    <th>ID No </th>
                                                    <th>Gender</th>
                                                    <th>D.o.B </th>
                                                    <th>Marital Status </th>
                                                    <th>Nationality </th>
                                                    <th>Department</th>
                                                    <th>Role</th>
                                                    <th>Status</th>
                                                    <th class=" no-link last"><span class="nobr">Action</span>
                                                    </th>
                                                </tr>
                                            </tfoot>

                                            <tbody>

                                                <?php
                                                $i = 1;
                                                foreach ($employees as $value) {
                                                    ?>
                                                    <tr class="even pointer">
                                                        <td class="a-center"><?php echo $i; ?></td>
                                                        <td><?php echo $value['employee_no']; ?></td>
                                                        <td><?php echo $value['employee_name']; ?></td>
                                                        <td><?php echo $value['id_no']; ?></td>
                                                        <td><?php echo $value['gender']; ?></td>
                                                        <td><?php echo $value['dob']; ?></td>
                                                        <td><?php echo $value['marital_status']; ?></td>
                                                        <td><?php echo $value['nationality']; ?></td>
                                                        <td><?php echo $value['department_name']; ?></td>
                                                        <td><?php echo $value['roles_name']; ?></td>
                                                        <td><?php echo $value['status']; ?></td>
                                                        <td class="a-center">
                                                            <input class="hidden td_employee_id" type="hidden" value="<?php echo $value['employee_id']; ?>" name="td_employee_id" id="td_employee_id"/>


                                                            <a class="btn btn-primary btn-xs view_employee_info" id="view_employee_info" >
                                                                <i class="fa fa-folder"></i>
                                                                View
                                                            </a>
                                                            <a class="btn btn-info btn-xs edit_employee_info" id="edit_employee_info" >
                                                                <i class="fa fa-pencil"></i>
                                                                Edit
                                                            </a>
                                                            <a class="btn btn-danger btn-xs delete_employee_info" id="delete_employee_info" >
                                                                <i class="fa fa-trash-o"></i>
                                                                Delete
                                                            </a>


                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                                ?>



                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>

                            <br />
                            <br />
                            <br />

                        </div>
                    </div>
                    <!-- footer content -->
                    <footer>
                        <div class="">
                            <p class="pull-right">Unique Loo ! by <a>Harris Dindi</a>. |
                                <span class="lead"> <i class="fa fa-paw"></i> Unique Loo!</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </footer>
                    <!-- /footer content -->

                </div>
                <!-- /page content -->
            </div>

        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>


        <!-- Sweet Alert Library -->
        <script src="<?php echo base_url(); ?>assets/dist/sweetalert-dev.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
        <!--.......................-->


        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- chart js -->
        <script src="<?php echo base_url(); ?>assets/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url(); ?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>



        <!-- daterangepicker -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js"></script>



        <!-- Datatables -->
        <script src="<?php echo base_url(); ?>assets/js/datatables/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>



        <!-- Spin JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/spin.js/2.3.2/spin.min.js"></script>





        <script>
            $(document).ready(function () {
                $('input.tableflat').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });
            });

            var asInitVals = new Array();
            $(document).ready(function () {
                var oTable = $('#employee_table').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0]
                        } //disables sorting for column one
                    ],
                    'iDisplayLength': 12,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "<?php echo base_url('assets/js/datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
                    }
                });
                $("tfoot input").keyup(function () {
                    /* Filter on the column based on the index of this element's parent <th> */
                    oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
                });
                $("tfoot input").each(function (i) {
                    asInitVals[i] = this.value;
                });
                $("tfoot input").focus(function () {
                    if (this.className == "search_init") {
                        this.className = "";
                        this.value = "";
                    }
                });
                $("tfoot input").blur(function (i) {
                    if (this.value == "") {
                        this.className = "search_init";
                        this.value = asInitVals[$("tfoot input").index(this)];
                    }
                });




                $(".add_new_employee").click(function () {

                    $(".add_new_employee_div").show('slow');
                    $(".employees_table_div").hide('slow');

                });

                $(".email").click(function () {

                    $(".check_email_div").show('slow');

                });


                $(".employee_no").keypress(function () {
                    $('.add_employee_other_info').hide("slow");
                });


                $(".check_employee_no_existence").click(function () {
                    var employee_no = $(".employee_no").val();

                    if (employee_no.indexOf('UL_') === -1) {
                        sweetAlert("Sorry...", "Employee No is required! Please key it in  :( ", "warning");

                    } else {

                       $(".modal_loading").show();

                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>admin/check_employee_no_existence/" + employee_no,
                            dataType: "json",
                            success: function (response) {
                                var employee_no_check = response[0].employee_no_check;

                                if (employee_no_check === "Employee No Exists in the System") {
                                       $(".modal_loading").show();
                                    sweetAlert("Sorry...", "Employee No has already been used in the System! :( ", "error");
                                    $('.add_employee_other_info').hide("slow");
                                       $(".modal_loading").hide();
                                } else if (employee_no_check === "Employee No Does not exist in the System") {
                                    // sweetAlert("Success", "Email not found, you can proceed with  employee registrations! :)", "success");
                                    $('.add_employee_other_info').show("slow");
                                       $(".modal_loading").hide();
                                }

                            }, error: function (data) {
                                sweetAlert("Oops...", "Something went wrong!", "error");

                            }
                        });

                    }

                });



                $(".check_email_existence").click(function () {
                    var email = $(".email").val();

                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>admin/check_email_existence/" + email,
                        dataType: "json",
                        success: function (response) {
                            var email_check = response[0].email_check;

                            if (email_check === "Email Exists in the System") {
                                sweetAlert("Sorry...", "Email Exists in the System! :( ", "error");

                            } else if (email_check === "Email Does not exist in the System") {
                                sweetAlert("Success", "Email not found, you can proceed with  employee registrations! :)", "success");
                                $('.check_email_div').show("slow");
                            }

                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }
                    });

                });







                $('#reset_add_employee_form').click(function () {
                    $(".add_new_employee_div").hide('slow');
                    $(".employees_table_div").show('slow');
                });





                $('#add_new_employee_form').submit(function (event) {
                    dataString = $("#add_new_employee_form").serialize();

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/admin/add_new_employee",
                        data: dataString,
                        success: function (data) {

                            sweetAlert("Success!", "New Employee Added Successfully", "success");
                            $(".add_new_employee_div").hide('slow');
                            $(".employees_table_div").show('slow');
                            $(".info_box_reload").show('slow');


                            $('.f_name').val("");
                            $('.s_name').val("");
                            $('.o_name').val("");
                            $('.gender').val("");
                            $('.phone_no').val("");
                            $('.id_no').val("");
                            $('.role_id').val("");
                            $('.department_id').val("");
                            $('.email').val("");
                            $('.employee_status').val("");
                            $('.DoB').val("");


                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }

                    });
                    event.preventDefault();
                    return false;
                });





                $('#edit_employee_form').submit(function (event) {
                    dataString = $("#edit_employee_form").serialize();
                    $(".ajax_loader_div").show('slow');
                    $(".edit_employee_div").hide('slow');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/admin/edit_employee",
                        data: dataString,
                        success: function (data) {
                            sweetAlert("Success!", "Data updated successfully", "success");
                            $(".employees_table_div").show('slow');
                            $(".ajax_loader_div").hide('slow');


                            $(".info_box_reload").show('slow');
                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }

                    });
                    event.preventDefault();
                    return false;
                });



                $('#delete_employee_form').submit(function (event) {
                    dataString = $("#delete_employee_form").serialize();
                    $(".ajax_loader_div").show('slow');
                    $(".delete_employee_div").hide('slow');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/admin/delete_employee",
                        data: dataString,
                        success: function (data) {
                            sweetAlert("Success", "Data updated successfully", "success");

                            $(".employees_table_div").show('slow');
                            $(".ajax_loader_div").hide('slow');
                            $(".info_box_reload").show('slow');
                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }

                    });
                    event.preventDefault();
                    return false;
                });


                $('#go_back_edit').click(function () {
                    $(".edit_employee_div").hide('slow');
                    $(".ajax_loader_div").show('slow');
                    $(".employees_table_div").show('slow');
                    $(".ajax_loader_div").hide('slow');
                });



                $(document).on('click', ".edit_employee_info", function () {



                    //get data
                    var employee_id = $(this).closest('tr').find('input[name="td_employee_id"]').val();


                    $(".ajax_loader_div").show('slow');
                    $(".employees_table_div").hide('slow');
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>admin/get_edit_employee_details/" + employee_id,
                        dataType: "json",
                        success: function (response) {
                            $('#edit_employee_no').val(response[0].employee_no);
                            $('#edit_employee_id').val(response[0].employee_id);
                            $('#edit_f_name').val(response[0].f_name);
                            $('#edit_s_name').val(response[0].s_name);
                            $('#edit_o_name').val(response[0].o_name);
                            $('#edit_id_no').val(response[0].id_no);
                            $('#edit_dob').val(response[0].dob);
                            $('#edit_nationality').val(response[0].nationality);
                            $('#edit_gender option[value=' + response[0].gender + ']').attr("selected", "selected");
                            $('#edit_role_id option[value=' + response[0].role_id + ']').attr("selected", "selected");
                            $('#edit_department_id option[value=' + response[0].department_id + ']').attr("selected", "selected");
                            //$('#edit_status option[value=' + response[0].employee_status + ']').attr("selected", "selected");
                            $('#edit_marital_status option[value=' + response[0].marital_status + ']').attr("selected", "selected");
                            $('#edit_status').val(response[0].employee_status);

                            $('#edit_phone_no').val(response[0].phone_no);
                            $('#edit_email').val(response[0].email);
                            $(".edit_employee_div").show('slow');
                            $(".ajax_loader_div").hide('slow');

                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }
                    });
                });


                $('#go_back_view').click(function () {
                    $(".view_employee_div").hide('slow');
                    $(".ajax_loader_div").show('slow');
                    $(".employees_table_div").show('slow');
                    $(".ajax_loader_div").hide('slow');
                });



                $(document).on('click', ".view_employee_info", function () {

                    //get data
                    var employee_id = $(this).closest('tr').find('input[name="td_employee_id"]').val();


                    $(".ajax_loader_div").show('slow');
                    $(".employees_table_div").hide('slow');
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>admin/get_employee_details/" + employee_id,
                        dataType: "json",
                        success: function (response) {

                            $('#employee_id_v').val(response[0].id);
                            $('#employee_no_v').val(response[0].employee_no);
                            $('#employee_name_v').val(response[0].employee_name);
                            $('#id_no_v').val(response[0].id_no);
                            $('#dob_v').val(response[0].dob);
                            $('#gender_v').val(response[0].gender);
                            $('#marital_status_v').val(response[0].marital_status);
                            $('#nationality_v').val(response[0].nationality);
                            $('#department_name_v').val(response[0].department_name);
                            $('#role_name_v').val(response[0].roles_name);
                            $('#time_stamp_v').val(response[0].time_stamp);
                            $('#phone_no_v').val(response[0].phone_no);
                            $(".view_employee_div").show('slow');
                            $(".ajax_loader_div").hide('slow');

                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }
                    });
                });





                $('#cancel_delete_button').click(function () {
                    $(".delete_employee_div").hide('slow');
                    $(".ajax_loader_div").show('slow');
                    $(".employees_table_div").show('slow');
                    $(".ajax_loader_div").hide('slow');
                });



                $(document).on('click', ".delete_employee_info", function () {
                    //get data
                    var employee_id = $(this).closest('tr').find('input[name="td_employee_id"]').val();


                    $(".ajax_loader_div").show('slow');
                    $(".employees_table_div").hide('slow');
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>admin/get_employee_details/" + employee_id,
                        dataType: "json",
                        success: function (response) {

                            $('#delete_employee_id').val(response[0].employee_id);
                            $('#delete_employee_name').val(response[0].employee_name);
                            $(".delete_employee_div").show('slow');
                            $(".ajax_loader_div").hide('slow');

                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }
                    });
                });




























                function Draw() {

                    oTable = $("#employees_tables").DataTable({
                        "dom": 'T<"clear">lfrtip',
                        "tableTools": {
                            // "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
                        },
                        stateSave: true,
                        "bautoWidth": false,
                        "aoColumns": [
                            {"sTitle": "User No", "mData": "employee_name"},
                            {"sTitle": "User Name.", "mData": "id_no"},
                            {"sTitle": "Email", "mData": "dob"},
                            {"sTitle": "Status", "mData": "marital_status"},
                            {"sTitle": "User Name.", "mData": "nationality"},
                            {"sTitle": "Email", "mData": "department_name"},
                            {"sTitle": "Status", "mData": "roles_name"},
                            {"sTitle": "Salary", "mData": "id_no", "sDefaultContent": '<input type="button" value="Some Action">'}
                        ],
                        "bDeferRender": true,
                        "bProcessing": true,
                        "bDestroy": true,
                        "bLengthChange": true,
                        "iDisplayLength": 200,
                        "sA$axDataProp": "",
                        "sA$axSource": '<?php echo base_url() . "admin/employee_data"; ?>',
                        "aaSorting": [[2, "desc"]]
                    });

                }


            });
        </script>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

        <script>
            var j = jQuery.noConflict();
            j(document).ready(function () {






                j(function () {
                    j(".nationality").autocomplete({
                        source: "<?php echo site_url(); ?>admin/get_countries" // path to the get_countries method
                    });
                });






            });
        </script>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script>
            var x = jQuery.noConflict();
            x(function () {
                x("#DoB").datepicker(
                        {dateFormat: 'yy-mm-dd'}
                );


                x("#edit_dob").datepicker(
                        {dateFormat: 'yy-mm-dd'}
                );

            });
        </script>



        <div class="modal_loading" id="modal_loading"><!-- Place at bottom of page --></div>
        <style type="text/css">
            /* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
            .modal_loading {
                display:    none;
                position:   fixed;
                z-index:    1000;
                top:        0;
                left:       0;
                height:     100%;
                width:      100%;
                background: rgba( 255, 255, 255, .8 ) 
                    50% 50% 
                    no-repeat;
            }
        </style>

        <script type="text/javascript">
            $(document).ready(function () {
                var opts = {
                    lines: 12             // The number of lines to draw
                    , length: 7             // The length of each line
                    , width: 5              // The line thickness
                    , radius: 10            // The radius of the inner circle
                    , scale: 1.0            // Scales overall size of the spinner
                    , corners: 1            // Roundness (0..1)
                    , color: '#000'         // #rgb or #rrggbb
                    , opacity: 1 / 4          // Opacity of the lines
                    , rotate: 0             // Rotation offset
                    , direction: 1          // 1: clockwise, -1: counterclockwise
                    , speed: 1              // Rounds per second
                    , trail: 100            // Afterglow percentage
                    , fps: 20               // Frames per second when using setTimeout()
                    , zIndex: 2e9           // Use a high z-index by default
                    , className: 'spinner'  // CSS class to assign to the element
                    , top: '50%'            // center vertically
                    , left: '50%'           // center horizontally
                    , shadow: false         // Whether to render a shadow
                    , hwaccel: false        // Whether to use hardware acceleration (might be buggy)
                    , position: 'absolute'  // Element positioning
                }
                var target = document.getElementById('modal_loading');
                var spinner = new Spinner(opts).spin(target);
            });
        </script>







    </body>

</html>