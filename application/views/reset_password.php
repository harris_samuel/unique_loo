<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Unique Loo! | </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">


        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>


        <!-- Sweet Alert Library -->
        <script src="<?php echo base_url(); ?>assets/dist/sweetalert-dev.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
        <!--.......................-->

        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    </head>

    <body style="background:#F7F7F7;">

        <div class="">
            <a class="hiddenanchor" id="toregister"></a>
            <a class="hiddenanchor" id="tologin"></a>

            <a href="<?php echo base_url(); ?>"> <img  style="margin-left: 630px; margin-right: 0px; margin-bottom: -93px; margin-top: 40px;" src="<?php echo base_url(); ?>images/logo.png" alt="logo"  /></a>

            <div id="wrapper">
                <?php
//                if (!empty($msg)) {
//                    echo $msg;
//                }
                ?>
                <div id="login" class="animate form">
                    <section class="login_content">
                        <form class="form-signin new_password_form"  id="new_password_form" method="post">
                            <input type="hidden" value="<?php echo $this->uri->segment(3); ?>" name="random_key" id="random_key" class="  random_key form-control "/>
                            <h1>Login Form</h1>
                            <div>
                                <input type="password" class="form-control " placeholder="Password" name="password_1" required="" />
                            </div>
                            <div>
                                <input type="password" class="form-control" placeholder="Re-type Password" name="password_2" required="" />
                            </div>
                            <div>
                                <button class="btn btn-default submit" type="submit">Reset Password</button>


                            </div>
                            <div class="clearfix"></div>
                            <div class="separator">


                                <div class="clearfix"></div>
                                <br />
                                <div>
                                    <h1><i class="fa fa-users" style="font-size: 26px;"></i> Unique Loo!</h1>

                                    <p>©2015 All Rights Reserved. Unique Loo! </p>
                                </div>
                            </div>
                        </form>
                        <!-- form -->
                    </section>
                    <!-- content -->
                </div>










            </div>
        </div>

    </body>


    <script type="text/javascript">
        $(document).ready(function () {

            $('#new_password_form').submit(function (event) {
                dataString = $("#new_password_form").serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/login/new_password",
                    data: dataString,
                    success: function (data) {
                        data = JSON.parse(data);
                        var response = data[0].response;
                        
                        swal({
                            title: "<small>Success</small>!",
                            text: "" + response + " You will be redirected to Login Page in a Few seconds...",
                            html: true
                        });

                        setInterval(function () {
                            var home = "<?php echo base_url();?>";
                            window.location.href = home;
                        }, 3000);
                    }


                });
                event.preventDefault();
                return false;
            });
        });
    </script>

</html>