<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Unique Loo! | </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">
        <!-- editor -->
        <link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/editor/index.css" rel="stylesheet">
        <!-- select2 -->
        <link href="<?php echo base_url(); ?>assets/css/select/select2.min.css" rel="stylesheet">
        <!-- switchery -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/switchery/switchery.min.css" />

        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    </head>


    <body class="nav-md">

        <div class="container body">


            <div class="main_container">





                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">

                        <div class="navbar nav_title" style="border: 0;">

                            <a href="<?php echo base_url(); ?>"> <img  style="margin-left: 60px; margin-right: 0px; margin-bottom: 0px; margin-top: 0px;" src="<?php echo base_url(); ?>images/logo.png" alt="logo"  /></a>

                        </div>
                        <div class="clearfix"></div>



                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">
                                    <li><a href="<?php echo base_url(); ?>" ><i class="fa fa-home"></i> HOME <span class="fa fa-chevron-right"></span></a> </li>




                                    <li><a><i class="fa fa-edit"></i> ADMINISTRATION <span class="fa fa-chevron-down"></span></a>

                                        <ul class="nav child_menu" style="display: none">

                                            <li>


                                                <?php
                                                $base_url = base_url();
                                                foreach ($admin_functions as $value) {
                                                    $controller_name = $value['controller_name'];
                                                    $function_name = $value['functions_name'];
                                                    $i_tag = $value['i_tag'];
                                                    $description = $value['description'];
                                                    $span_tag = $value['span_tag'];
                                                    ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>



                                        </ul> 

                                    </li>

                                    <li><a><i class="fa fa-edit"></i> DAILY OPERATIONS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($daily_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>






                                        </ul>
                                    </li>



                                    <li><a><i class="fa fa-edit"></i> REPORTS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($reports_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>







                                        </ul>
                                    </li>



                                </ul>
                            </div>

                        </div>
                        <!-- /sidebar menu -->


                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" href="<?php echo base_url(); ?>home/do_logout" title="Logout">
                                <span class="glyphicon glyphicon-off"  aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">

                    <div class="nav_menu">
                        <nav class="" user="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="<?php echo base_url(); ?>assets/images/img.jpg" alt=""><?php
                                        $username = $this->session->userdata('user_name');
                                        echo $username;
                                        ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <li><a href="<?php echo base_url(); ?>admin/profile">  Profile</a>
                                        </li>


                                        <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                        </li>
                                    </ul>
                                </li>


                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->










                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">

                        <div class="page-title">
                            <div class="title_left">
                                <h3>Unique Loo</h3>
                            </div>
                            <div class="title_right">
                                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                    <div class="input-group">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>


                        <table>
                            <tr>
                                <td>
                                    <div class="btn_view_client_payment_list_div" id="btn_view_client_payment_list_div" style="display: none;">
                                        <button id="btn_view_client_payment_list" class="btn_view_client_payment_list btn btn-primary">View Client Payments List</button>
                                    </div>

                                </td>
                                <td>
                                    <div class="btn_view_open_jobs_div" id="btn_view_open_jobs_div" style="display: none;">
                                        <button id="btn_view_open_jobs" class="btn_view_open_jobs btn btn-danger">
                                            View Open Jobs
                                        </button>
                                    </div>

                                </td>
                            </tr>
                        </table>



                        <div class="row">



                            <div  id="job_status_list" class="job_status_list">


                                <div class="col-md-6 col-xs-12">

                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2><i class="fa fa-bars"></i> Unique Loo <small>Pending Jobs Payments</small></h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li><a class="close_open_jobs_div"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">


                                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#my_jobs_tab" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">My Jobs</a>
                                                    </li>
                                                    <li role="presentation" class=""><a href="#other_jobs_tab" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false">Other Jobs</a>
                                                    </li>

                                                </ul>
                                                <div id="myTabContent" class="tab-content">
                                                    <div role="tabpanel" class="tab-pane fade active in" id="my_jobs_tab" aria-labelledby="home-tab">
                                                        <!--<ul class="my_job_list_ul to_do" id="my_job_list_ul"></ul>-->


                                                        <table class="table table-responsive">
                                                            <thead>
                                                            <th>Client name</th>
                                                            <th>Job Card No</th>
                                                            <th>Invoice No</th>
                                                            <th>Amount </th>
                                                            <th>Job Status</th>

                                                            </thead>

                                                            <tbody id="my_job_list_ul" class="my_job_list_ul to_do">

                                                            </tbody>

                                                        </table>





                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="other_jobs_tab" aria-labelledby="profile-tab">

                                                        <!--<ul class="all_job_list_ul to_do" id="all_job_list_ul"></ul>-->




                                                        <table class="table table-responsive">
                                                            <thead>
                                                            <th>Client name</th>
                                                            <th>Job Card No</th>
                                                            <th>Invoice No</th>
                                                            <th>Amount </th>
                                                            <th>Job Status</th>

                                                            </thead>

                                                            <tbody id="all_job_list_ul" class="all_job_list_ul to_do">

                                                            </tbody>

                                                        </table>

                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>



                                </div>

                                <div class="col-md-6 col-xs-12">

                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2><i class="fa fa-bars"></i> Unique Loo <small>Future Date Jobs</small></h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li><a class="close_open_jobs_div"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">


                                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#my_future_jobs_tab" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">My Jobs</a>
                                                    </li>
                                                    <li role="presentation" class=""><a href="#other_future_jobs_tab" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false">Other Jobs</a>
                                                    </li>

                                                </ul>
                                                <div id="myTabContent" class="tab-content">
                                                    <div role="tabpanel" class="tab-pane fade active in" id="my_future_jobs_tab" aria-labelledby="home-tab">



                                                        <table class="table table-responsive">
                                                            <thead>
                                                            <th>Client name</th>
                                                            <th>Job Card No</th>
                                                            <th>Invoice No</th>
                                                            <th>Amount </th>
                                                            <th>Job Status</th>
                                                            </thead>

                                                            <tbody id="my_future_job_list_ul" class="my_future_job_list_ul to_do">

                                                            </tbody>

                                                        </table>

                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="other_future_jobs_tab" aria-labelledby="profile-tab">


                                                        <table class="table table-responsive">
                                                            <thead>
                                                            <th>Client name</th>
                                                            <th>Job Card No</th>
                                                            <th>Invoice No</th>
                                                            <th>Amount </th>
                                                            <th>Job Status</th>

                                                            </thead>

                                                            <tbody id="all_future_job_list_ul" class="all_future_job_list_ul to_do">

                                                            </tbody>

                                                        </table>

                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>




                                </div>



                            </div>





                        </div>





                        <div class="client_invoice_details" id="client_invoice_detaikls" style="display: none;" >





                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Client Payments <small></small></h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                               
                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <br />
                                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" id="last-name" name="last-name" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Middle Name / Initial</label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="middle-name">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div id="gender" class="btn-group" data-toggle="buttons">
                                                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                                <input type="radio" name="gender" value="male"> &nbsp; Male &nbsp;
                                                            </label>
                                                            <label class="btn btn-primary active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                                <input type="radio" name="gender" value="female" checked=""> Female
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input id="birthday" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text">
                                                    </div>
                                                </div>
                                                <div class="ln_solid"></div>
                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                        <button type="submit" class="btn btn-primary">Cancel</button>
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <!-- Invoice Form Div Start -->


                        <div id="invoice_form_details_div" class="invoice_form_details_div" style="display: none;" >






                            <div class="row">
                                <div class="col-md-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Unique Loo  <small>Client Invoice</small></h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li>
                                                    <!--<a class="close-link close_invoice_link"><i class="fa fa-close"></i></a>-->
                                                    <button class=" close_invoice_link" id="close_invoice_link"><i class="fa fa-close"></i></button>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">

                                            <section class="content invoice">
                                                <!-- title row -->
                                                <div class="row">
                                                    <div class="col-xs-12 invoice-header">
                                                        <h1>

                                                            <small class="pull-right"><?php
                                                                $today = date("F j, Y, g:i a");
                                                                echo $today;
                                                                ?></small>
                                                        </h1>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- info row -->
                                                <div class="row invoice-info">
                                                    <div class="col-sm-4 invoice-col client_conf">

                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col-sm-4 invoice-col invoiced_client">

                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col-sm-4 invoice-col invoice_no_details">

                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.row -->

                                                <!-- Table row -->
                                                <div class="row">
                                                    <div class="col-xs-12 table">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Qty</th>
                                                                    <th>Product</th>
                                                                    <th>Subtotal(KES)</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="client_ivoice_stmnt" class="client_ivoice_stmnt">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.row -->

                                                <div class="row">
                                                    <!-- accepted payments column -->
                                                    <div class="col-xs-6">
                                                        <p class="lead">Payment Methods:</p>
                                                        <img src="<?php echo base_url(); ?>assets/images/visa.png" alt="Visa">
                                                        <img src="<?php echo base_url(); ?>assets/images/mastercard.png" alt="Mastercard">
                                                        <img src="<?php echo base_url(); ?>assets/images/orange-money.png" alt="Orange Money">
                                                        <img src="<?php echo base_url(); ?>assets/images/airtel-money.jpg" alt="Airtel Money">
                                                        <img src="<?php echo base_url(); ?>assets/images/mpesa.jpg" alt="M-PESA">
                                                        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                                            More Instructions will be provided by the  Cashier </p>
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col-xs-6">
                                                        <p class="lead">Amount Due : On or Before the  Service Day</p>
                                                        <div class="table-responsive">
                                                            <table class="table">
                                                                <tbody>
                                                                    <tr class="subtotal_tr">
                                                                        <th style="width:50%">Subtotal:</th>
                                                                        <td>$250.30</td>
                                                                    </tr>
                                                                    <tr class="tax_tr">
                                                                    </tr>

                                                                    <tr class="total_tr">

                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.row -->

                                                <!-- this row will not appear when printing -->
                                                <div class="row no-print">
                                                    <div class="col-xs-12">
                                                        <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                                                        <input type="hidden" name="job_card_id_payment" id="job_card_id_payment" class="job_card_id_payment form-control"/>
                                                        <input type="submit" id="submit_payment_link" class="submit_payment_link btn btn-success pull-right fa-credit-card " value="Submit Payment"  >


<!--                                                        <form method="post" action="<?php echo base_url(); ?>operations/submit_payment">
   <input type="hidden" name="job_card_id_payment" id="job_card_id_payment" class="job_card_id_payment form-control"/>
   <input type="submit" id="submit_payment_link" class="btn btn-success pull-right fa-credit-card " value="Submit Payment"  >

</form>-->

                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>





                        </div>



                        <!-- Invoice Form Div End -->


                        <!-- Submit Payments Details Div Start -->
                        <div class="submit_payments_div" id="submit_payments_div" style="display: none;">

                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Make Payments <small></small></h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li><a class="close_submit_payments_div"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <br />
                                            <button id="go_back_invoice" class="go_back_invoice btn btn-default"><i class="fa fa-backward"></i>View Invoice</button>
                                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left submit_payment_form">



                                                <table class="table table-responsive table-striped table-bordered dataTable">
                                                    <thead>
                                                    <th>Asset Name</th>
                                                    <th>Quantity</th>
                                                    <th>Amount Charged</th>
                                                    <th>Amount Paid</th>
                                                    <th>Balance</th>
                                                    <th>Payment Method</th>
                                                    <th>Payment Code</th>
                                                    </thead>
                                                    <tfoot>
                                                        <tr>
                                                            <td>Asset Name</td>
                                                            <td>Quantity</td>
                                                            <td>Amount Charged</td>
                                                            <td>Amount Paid</td>
                                                            <td>Balance</td>
                                                            <td>Payment Method</td>
                                                            <td>Payment Code</td>  
                                                        </tr>
                                                    </tfoot>
                                                    <tbody id="client_payment_div" class="client_payment_div">

                                                    </tbody>

                                                </table>


                                                <div class="ln_solid"></div>

                                                <div class="div_client_make_payment" id="div_client_make_payment" >

                                                    <div class="form-group">
                                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                            <button type="submit" class="btn btn-primary btn_cancel_payment" id="btn_cancel_payment">Cancel</button>
                                                            <button type="submit" class="btn btn-success"><i class="fa fa-credit-card"></i>Make Payment</button>
                                                        </div>
                                                    </div>  
                                                </div>


                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- Submit Payments details Div End -->

                        <!--Ajax Loader Div_1 Start -->
                        <div id="ajax_loader_div_1" class="ajax_loader_div_1" style="display: none;">

                            <img src="<?php echo base_url(); ?>assets/images/ajax_loader/ajax-loader.gif" alt="Ajax Loader" height="42" width="42"> 
                            <hr>
                            <div class="alert alert-info">Please Wait While Information is Loaded</div>
                        </div>

                        <!-- Ajax Loader Div_1 End -->



                        <!--Ajax Loader Div Start -->
                        <div id="ajax_loader_div" class="ajax_loader_div" style="display: none;">

                            <img src="<?php echo base_url(); ?>assets/images/ajax_loader/ajax-loader.gif" alt="Ajax Loader" height="42" width="42"> 
                            <hr>
                            <div class="alert alert-info">Please Wait While Information is Loaded</div>
                        </div>

                        <!-- Ajax Loader Div End -->






                    </div>
                    <!-- /page content -->

                    <!-- footer content -->
                    <footer>
                        <div class="">
                            <p class="pull-right">Unique Loo ! by <a>Harris Dindi</a>. |
                                <span class="lead"> <i class="fa fa-paw"></i> Unique Loo!</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </footer>
                    <!-- /footer content -->

                </div>

            </div>
        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>



        <!-- Sweet Alert Library -->
        <script src="<?php echo base_url(); ?>assets/dist/sweetalert-dev.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
        <!--.......................-->



        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- chart js -->
        <script src="<?php echo base_url(); ?>assets/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url(); ?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>
        <!-- tags -->
        <script src="<?php echo base_url(); ?>assets/js/tags/jquery.tagsinput.min.js"></script>
        <!-- switchery -->
        <script src="<?php echo base_url(); ?>assets/js/switchery/switchery.min.js"></script>
        <!-- daterangepicker -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assts/js/datepicker/daterangepicker.js"></script>
        <!-- richtext editor -->
        <script src="<?php echo base_url(); ?>assets/js/editor/bootstrap-wysiwyg.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/editor/external/jquery.hotkeys.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/editor/external/google-code-prettify/prettify.js"></script>
        <!-- select2 -->
        <script src="<?php echo base_url(); ?>assets/js/select/select2.full.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/parsley/parsley.min.js"></script>
        <!-- textarea resize -->
        <script src="<?php echo base_url(); ?>assets/js/textarea/autosize.min.js"></script>
        <script>
                                                            autosize($('.resizable_textarea'));
        </script>
        <!-- Autocomplete -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/autocomplete/countries.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/autocomplete/jquery.autocomplete.js"></script>
        <script type="text/javascript">
                                                            $(function () {
                                                                'use strict';
                                                                var countriesArray = $.map(countries, function (value, key) {
                                                                    return {
                                                                        value: value,
                                                                        data: key
                                                                    };
                                                                });
                                                                // Initialize autocomplete with custom appendTo:
                                                                $('#autocomplete-custom-append').autocomplete({
                                                                    lookup: countriesArray,
                                                                    appendTo: '#autocomplete-container'
                                                                });
                                                            });
        </script>
        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>

        <script>
                                                            $(document).ready(function () {



                                                                $('.btn_view_client_payment_list').click(function () {
                                                                    $(".client_payment_list").show("slow");
                                                                    $(".btn_view_client_payment_list_div").hide();


                                                                });
                                                                $('.btn_view_open_jobs').click(function () {
                                                                    $(".job_status_list").show("slow");
                                                                    $(".btn_view_open_jobs_div").hide();


                                                                });
                                                                $('.close_client_payments_div').click(function () {
                                                                    $(".client_payment_list").hide();
                                                                    $(".btn_view_client_payment_list_div").show("slow");


                                                                });
                                                                $('.close_open_jobs_div').click(function () {
                                                                    $(".job_status_list").hide();
                                                                    $(".btn_view_open_jobs_div").show("slow");


                                                                });


                                                                $('.go_back_invoice').click(function () {
                                                                    $(".submit_payments_div").hide();
                                                                    $(".invoice_form_details_div").show();


                                                                });


                                                                $('.btn_cancel_payment').click(function () {
                                                                    $(".submit_payments_div").hide();
                                                                    $(".invoice_form_details_div").show();


                                                                });

                                                                $('.close_submit_payments_div').click(function () {
                                                                    $(".submit_payments_div").hide();
                                                                    $(".invoice_form_details_div").show();


                                                                });





                                                                $('.submit_payment_form').submit(function (event) {
                                                                    dataString = $(".submit_payment_form").serialize();
                                                                    $(".ajax_loader_div").show();

                                                                    $.ajax({
                                                                        type: "POST",
                                                                        url: "<?php echo base_url() ?>index.php/operations/submit_payment_form",
                                                                        data: dataString,
                                                                        success: function (data) {
                                                                            sweetAlert("Great!...", "Paymnet made Successfully!", "success");
                                                                            $(".client_payment_list").show();
                                                                            $(".job_status_list").show();
                                                                            $(".submit_payments_div").hide();
                                                                            $(".ajax_loader_div").hide();

                                                                        }, error: function (data) {
                                                                            sweetAlert("Oops...", "Something went wrong!", "error");

                                                                        }

                                                                    });
                                                                    event.preventDefault();
                                                                    return false;
                                                                });






                                                                $('.submit_payment_link').click(function () {
                                                                    $(".ajax_loader_div").show();

                                                                    var job_card_id = $(".job_card_id_payment").val();
                                                                    html1 = '';
                                                                    htmlhead1 = '';



                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: "<?php echo base_url(); ?>operations/get_payment_details/" + job_card_id,
                                                                        dataType: "JSON",
                                                                        success: function (data) {
                                                                            for (i = 0; i < data.length; i++) {


                                                                                html1 += '<tr>\n\
                        <td class = "form-group">\n\
            \n\
                        <input type = "text" required=""  class = "form-control col-md-6 col-sm-6 col-xs-12 "  readonly="" name="asset_name[]" id = "asset_name" value="' + data[i].asset_name + '" placeholder = "Asset Name">\n\
            </td>\n\
            <td class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control quantity col-md-6 col-sm-6 col-xs-12 "  readonly="" name="quantity[]" id = "quantity" value="' + data[i].qty + '" placeholder = "Quantity">\n\
            </td>\n\
            <td class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control col-md-6 col-sm-6 col-xs-12  amount_charged' + data[i].amount_charged + '" name="amount_charged[]" readonly=""  id = "amount_charged' + data[i].amount_charged + '" value="' + data[i].amount_charged + '" placeholder = "Amount Charged">\n\
            </td>\n\
            <td class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control col-md-6 col-sm-6 col-xs-12  amount_paid' + data[i].statement_id + '" name="amount_paid[]"   id = "amount_paid' + data[i].statement_id + '"  placeholder = "Amount Paid">\n\
            </td>\n\
             <td class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control col-md-6 col-sm-6 col-xs-12  balance' + data[i].statement_id + '"   name="balance[]" id = "balance' + data[i].statement_id + '" value="' + data[i].balance + '" placeholder = "Balance Remaining">\n\
            <input type="hidden" required="" readonly="" class="form-control hidden hidden_balance' + data[i].statement_id + ' " name="hidden_balance[]" id="hidden_balance' + data[i].statement_id + '" value="' + data[i].balance + '">\n\
            <input type="hidden" required="" readonly="" class="form-control" name="statement_id[]" id="statement_id" value="' + data[i].statement_id + '">\n\
            <input type="hidden" required="" readonly="" class="form-control" name="client_id[]" id="client_id" value="' + data[i].client_id + '">\n\
            <input type="hidden" required="" readonly="" class="form-control" name="job_card_id[]" id="job_card_id" value="' + data[i].job_card_id + '">\n\
            <input type="hidden" required="" readonly="" class="form-control" name="invoice_no[]" id="invoice_no" value="' + data[i].invoice_no + '">\n\
            <input type="hidden" required="" readonly="" class="form-control" name="amount_paid_td[]" id="amount_paid_td" value="' + data[i].amount_paid + '">\n\
            \n\
            </td>\n\
            <td class = "form-group">\n\
            \n\
            \n\
            <select required="" name="payment_method[]" class="form-control col-md-6 col-sm-6 col-xs-12 " id="payment_method">\n\
            <option value="' + data[i].payment_method + '">' + data[i].payment_method + '<option>\n\
            <option value="Cash">Cash</option>\n\
            <option value="M-Pesa">M-Pesa</option>\n\
            <option value="Airtel Money">Airtel Money</option>\n\
            <option value="Orange Money">Orange Money</option>\n\
            <option value="PDQ">PDQ </option></select>\n\
            </td>\n\
            <td class = "form-group">\n\
            \n\
            <textarea type = "text"  required="" class = "form-control col-md-6 col-sm-6 col-xs-12 " name="payment_code[]" id = "payment_code" value="' + data[i].payment_code + '" placeholder = "Payment Code">' + data[i].payment_code + '</textarea>\n\
            </td>\n\
                        </tr>';
                                                                                $("#client_payment_div").on("keyup", ".amount_paid" + data[i].statement_id, function () {

                                                                                    var amount_paid = this.value;
                                                                                    var amount_paid = parseFloat(amount_paid);
                                                                                    var amount_owed = $("#" + this.id.replace("amount_paid", "hidden_balance")).val();
                                                                                    var amount_owed = parseFloat(amount_owed);

                                                                                    if (amount_paid > amount_owed) {
                                                                                        sweetAlert("Oops...", "You cannot Pay more than you owe!", "error");

                                                                                        $(".div_client_make_payment").hide("slow");
                                                                                    } else {
                                                                                        var new_balance = amount_owed - amount_paid;

                                                                                        $("#" + this.id.replace("amount_paid", "balance")).val(new_balance);
                                                                                        $(".div_client_make_payment").show("slow");
                                                                                    }
                                                                                });
                                                                            }

                                                                            htmlhead1 += '<div>\n\
                        <div class = "form-group">\n\
            <label class = " col-md-6 col-sm-6 col-xs-12 form-control " for = "asset_name">Asset Name</label>\n\
            </div>\n\
            <div class = "form-group">\n\
             <label class = " col-md-6 col-sm-6 col-xs-12 form-control " for = "quantity">Quantity</label>\n\
            </div>\n\
            \n\
            <div class = "form-group">\n\
            <label class = " col-md-6 col-sm-6 col-xs-12 form-control " for = "amount_charged">Amount Charged</label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = " col-md-6 col-sm-6 col-xs-12 form-control " for = "amount_paid">Amount Paid</label>\n\
            </div>\n\
             <div class = "form-group">\n\
            <label class = " col-md-6 col-sm-6 col-xs-12 form-control " for = "balance">Balance</label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = " col-md-6 col-sm-6 col-xs-12 form-control " for = "payment_method">Payment Method </label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = " col-md-6 col-sm-6 col-xs-12 form-control " for = "payment_code">Payment Code </label>\n\
            </div>\n\
                        </div>';
                                                                            $('#client_payment_div').empty();
                                                                            // $('#client_payment_div').append(htmlhead1);
                                                                            $('#client_payment_div').append(html1);


                                                                            $(".invoice_form_details_div").hide();
                                                                            $(".submit_payments_div").show();
                                                                            $(".ajax_loader_div").hide();


                                                                        },
                                                                        error: function (data) {

                                                                        }
                                                                    });




                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: "<?php echo base_url(); ?>operations/get_blld_clnt_info/" + job_card_id,
                                                                        dataType: "JSON",
                                                                        success: function (data) {
                                                                            var client_id = data[0].client_id;
                                                                            var client_name = data[0].client_name;
                                                                            var address = data[0].address;
                                                                            var website = data[0].website;
                                                                            var phone_no = data[0].phone_no;
                                                                            var email = data[0].email;
                                                                            var job_card_no = data[0].job_card_no;
                                                                            var invoice_no = data[0].invoice_no;
                                                                            $(".billed_client").empty();
                                                                            $(".billed_client").append(" To <address> <strong>" + client_name + "</strong> <br> " + address + " <br>Phone: " + phone_no + " <br>Email: " + email + "  </address>");

                                                                            $(".billed_client_invoice_details").empty();
                                                                            $(".billed_client_invoice_details").append(" <b>Invoice # " + invoice_no + "</b> <br>  <b>Order ID:</b> " + job_card_no + " <br> <b>Payment Due:</b> 2/22/2014 <br>  ");
                                                                        }, error: function (data) {
                                                                            sweetAlert("Oops...", "Something went wrong!", "error");

                                                                        }
                                                                    });




                                                                });




                                                                $.ajax({
                                                                    type: "GET",
                                                                    url: "<?php echo base_url(); ?>operations/get_user_configurations",
                                                                    dataType: 'JSON',
                                                                    success: function (data) {
                                                                        var company_name = data[0].name;
                                                                        var address = data[0].address;
                                                                        var website = data[0].website;
                                                                        var pin = data[0].pin;
                                                                        var phone_no = data[0].phone_no;
                                                                        var email = data[0].email;
                                                                        $(".client_conf").append("From<address><strong>" + company_name + "</strong><br> " + address + " <br> <br>Phone: " + phone_no + " <br>Email: " + email + " <br>Website: " + website + " </address>");
                                                                    }, error: function (data) {
                                                                        sweetAlert("Oops...", "Something went wrong!", "error");

                                                                    }
                                                                });

                                                                $(".ajax_loader_div_1").show();






                                                                setInterval(function () {


                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: "<?php echo base_url(); ?>operations/my_future_booked_jobs/",
                                                                        dataType: "JSON",
                                                                        success: function (cleint_lists) {
                                                                            cleint_list_list = $('#my_future_job_list_ul').empty();

                                                                            $.each(cleint_lists, function (i, cleint_list) {
                                                                                if(cleint_list.response === "No Data!"){
                                                                                    cleint_list_list.append("No My Future Job Lists!");
                                                                                }else{
                                                                                    
                                                                                
                                                                                cleint_list_list.append(
                                                                                        '<tr><td>' + cleint_list.client_name + '</td> <td>' + cleint_list.job_card_no + '</td>  <td>' + cleint_list.invoice_no + '</td> <td>' + cleint_list.amount_owed + '</td><td>' + cleint_list.job_status + '</td></tr>'

//                '<li class="my_future_job_list_li flat table table-bordered" id="my_future_job_list_li"> <span class="fa fa-arrow-right"></span> <p>' + cleint_list.client_name + ' :  &nbsp; ' + cleint_list.job_card_no + ' : &nbsp; ' + cleint_list.invoice_no + ' :  &nbsp; ' + cleint_list.amount_owed + ' :  &nbsp; \n\
//                       <br/>\n\
// &nbsp; <input type="hidden" id="my_future_list_job_id" class="my_future_list_job_id hidden " name="my_future_list_job_id" value="' + cleint_list.invoice_no + '"/> \n\
// <input type="hidden" id="my_future_list_job_card_id" class="my_future_list_job_card_id hidden " name="my_future_list_job_card_id" value="' + cleint_list.job_card_id + '"/> &nbsp; </p> </li>'
//             

                                                                                        );
                                                                                $(".ajax_loader_div_1").hide();
                                                                            }

                                                                            });



                                                                        }, error: function (data) {
                                                                            sweetAlert("Oops...", "Something went wrong!", "error");

                                                                        }
                                                                    });
                                                                }, 3000);



                                                                setInterval(function () {


                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: "<?php echo base_url(); ?>operations/other_future_booked_jobs/",
                                                                        dataType: "JSON",
                                                                        success: function (cleint_lists) {
                                                                            cleint_list_list = $('#all_future_job_list_ul').empty();

                                                                            $.each(cleint_lists, function (i, cleint_list) {
                                                                                 if(cleint_list.response === "No Data!"){
                                                                                    cleint_list_list.append("No Other Future Job Lists!");
                                                                                }else{
                                                                                cleint_list_list.append(
                                                                                        '<tr><td>' + cleint_list.client_name + '</td> <td>' + cleint_list.job_card_no + '</td>  <td>' + cleint_list.invoice_no + '</td> <td>' + cleint_list.amount_owed + '</td><td>' + cleint_list.job_status + '</td></tr>'

//                
//                '<li class="all_future_job_list_li flat table table-bordered" id="all_future_job_list_li"> <span class="fa fa-arrow-right"></span> <p>' + cleint_list.client_name + ' :  &nbsp; ' + cleint_list.job_card_no + ' : &nbsp; ' + cleint_list.invoice_no + ' :  &nbsp; ' + cleint_list.amount_owed + ' :  &nbsp; \n\
//                       <br/>\n\
// &nbsp; <input type="hidden" id="all_future_list_job_id" class="all_future_list_job_id hidden " name="all_future_list_job_id" value="' + cleint_list.invoice_no + '"/> \n\
// <input type="hidden" id="all_future_list_job_card_id" class="all_future_list_job_card_id hidden " name="all_future_list_job_card_id" value="' + cleint_list.job_card_id + '"/> &nbsp; </p> </li>'
//             


                                                                                        );
                                                                                $(".ajax_loader_div_1").hide();
                                                                            }

                                                                            });



                                                                        }, error: function (data) {
                                                                            sweetAlert("Oops...", "Something went wrong!", "error");

                                                                        }
                                                                    });
                                                                }, 3000);


                                                                setInterval(function () {


                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: "<?php echo base_url(); ?>operations/my_booked_jobs/",
                                                                        dataType: "JSON",
                                                                        success: function (cleint_lists) {
                                                                            cleint_list_list = $('#my_job_list_ul').empty();

                                                                            $.each(cleint_lists, function (i, cleint_list) {
                                                                                 if(cleint_list.response === "No Data!"){
                                                                                    cleint_list_list.append("No My Booked Job Lists!");
                                                                                }else{
                                                                                cleint_list_list.append(
                                                                                        '<tr><td>' + cleint_list.client_name + '</td> <td>' + cleint_list.job_card_no + '</td>  <td>' + cleint_list.invoice_no + '</td> <td>' + cleint_list.amount_owed + '</td><td>' + cleint_list.job_status + '</td></tr>'


                                                                                        );
                                                                                $(".ajax_loader_div_1").hide();
                                                                            }

                                                                            });



                                                                        }, error: function (data) {
                                                                            sweetAlert("Oops...", "Something went wrong!", "error");

                                                                        }
                                                                    });
                                                                }, 3000);



                                                                setInterval(function () {


                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: "<?php echo base_url(); ?>operations/other_booked_jobs/",
                                                                        dataType: "JSON",
                                                                        success: function (cleint_lists) {
                                                                            cleint_list_list = $('#all_job_list_ul').empty();

                                                                            $.each(cleint_lists, function (i, cleint_list) {
                                                                                 if(cleint_list.response === "No Data!"){
                                                                                    cleint_list_list.append("No other Booked Job Lists!");
                                                                                }else{
                                                                                cleint_list_list.append(
                                                                                        '<tr><td>' + cleint_list.client_name + '</td> <td>' + cleint_list.job_card_no + '</td>  <td>' + cleint_list.invoice_no + '</td> <td>' + cleint_list.amount_owed + '</td><td>' + cleint_list.job_status + '</td></tr>'


//                '<li class="all_job_list_li flat table table-bordered" id="all_job_list_li"> <span class="fa fa-arrow-right"></span> <p>' + cleint_list.client_name + ' :  &nbsp; ' + cleint_list.job_card_no + ' : &nbsp; ' + cleint_list.invoice_no + ' :  &nbsp; ' + cleint_list.amount_owed + ' :  &nbsp; \n\
//                       <br/>\n\
// &nbsp; <input type="hidden" id="all_list_job_id" class="all_list_job_id hidden " name="all_list_job_id" value="' + cleint_list.invoice_no + '"/> \n\
// <input type="hidden" id="all_list_job_card_id" class="all_list_job_card_id hidden " name="all_list_job_card_id" value="' + cleint_list.job_card_id + '"/> &nbsp; </p> </li>'
//             

                                                                                        );
                                                                                $(".ajax_loader_div_1").hide();
                                                                            }

                                                                            });



                                                                        }, error: function (data) {
                                                                            sweetAlert("Oops...", "Something went wrong!", "error");

                                                                        }
                                                                    });
                                                                }, 3000);




                                                                setInterval(function () {


                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: "<?php echo base_url(); ?>operations/my_booked_clients/",
                                                                        dataType: "JSON",
                                                                        success: function (cleint_lists) {
                                                                            cleint_list_list = $('#my_client_list_ul').empty();

                                                                            $.each(cleint_lists, function (i, cleint_list) {
                                                                                cleint_list_list.append(
                                                                                        '<tr><td>' + cleint_list.client_name + '</td> <td>' + cleint_list.job_card_no + '</td>  <td>' + cleint_list.invoice_no + '</td> <td>' + cleint_list.amount_owed + '</td><td>' + cleint_list.job_status + '</td></tr>'
//                    
//                '<li class="my_client_list_li flat table table-bordered" id="my_client_list_li"> <span class="fa fa-arrow-right"></span> <p>' + cleint_list.client_name + ' :  &nbsp; ' + cleint_list.job_card_no + ' : &nbsp; ' + cleint_list.invoice_no + ' :  &nbsp; ' + cleint_list.amount_due + ' :  &nbsp; \n\
//                        <button type="button" id="my_bill_patient_link" class="my_bill_patient_link btn btn-primary">View Invoice</button><br/>\n\
// &nbsp; <input type="hidden" id="my_list_client_id" class="my_list_client_id hidden " name="my_list_client_id" value="' + cleint_list.invoice_no + '"/> \n\
// <input type="hidden" id="my_list_job_card_id" class="my_list_job_card_id hidden " name="my_list_job_card_id" value="' + cleint_list.job_card_id + '"/> &nbsp; </p> </li>'


                                                                                        );
                                                                                $(".ajax_loader_div_1").hide();

                                                                            });



                                                                        }, error: function (data) {
                                                                            sweetAlert("Oops...", "Something went wrong!", "error");

                                                                        }
                                                                    });
                                                                }, 3000);



                                                                setInterval(function () {


                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: "<?php echo base_url(); ?>operations/other_booked_clients/",
                                                                        dataType: "JSON",
                                                                        success: function (cleint_lists) {
                                                                            cleint_list_list = $('#all_client_list_ul').empty();

                                                                            $.each(cleint_lists, function (i, cleint_list) {
                                                                                cleint_list_list.append(
                                                                                        '<tr><td>' + cleint_list.client_name + '</td> <td>' + cleint_list.job_card_no + '</td>  <td>' + cleint_list.invoice_no + '</td> <td>' + cleint_list.amount_owed + '</td><td>' + cleint_list.job_status + '</td></tr>'





                                                                                        );
                                                                                $(".ajax_loader_div_1").hide();

                                                                            });



                                                                        }, error: function (data) {
                                                                            sweetAlert("Oops...", "Something went wrong!", "error");

                                                                        }
                                                                    });
                                                                }, 3000);





                                                                $('.close_invoice_link').click(function () {
                                                                    $('.client_payment_list').show();
                                                                    $(".job_status_list").show();

                                                                    $('.invoice_form_details_div').hide();

                                                                });






                                                                function animate() {
                                                                    $('#client_payment_list').hide();
                                                                    $(".job_status_list").hide("slow");

                                                                    $('#invoice_form_details_div').show();
                                                                }

                                                                $('.all_client_list_ul').on('click', '.all_client_list_li', function () {

                                                                    //get
                                                                    var job_card_id = $(this).closest('li').find('input[name="all_list_job_card_id"]').val();
                                                                    client_transaction_details = '';
                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: "<?php echo base_url(); ?>operations/get_invoice_job_details/" + job_card_id,
                                                                        dataType: "JSON",
                                                                        success: function (data) {


                                                                            for (i = 0; i < data.length; i++) {

                                                                                client_transaction_details += '<tr><td>' + data[i].qty + '</td><td>' + data[i].asset_name + '</td>  <td>' + data[i].total_price + '</td> </tr>';


                                                                            }

                                                                            $('.client_ivoice_stmnt').empty();
                                                                            $('.client_ivoice_stmnt').append(client_transaction_details);

                                                                        }, error: function (data) {
                                                                            sweetAlert("Oops...", "Something went wrong!", "error");

                                                                        }
                                                                    });




                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: "<?php echo base_url(); ?>operations/client_info/" + job_card_id,
                                                                        dataType: 'JSON',
                                                                        success: function (data) {
                                                                            var client_id = data[0].client_id;
                                                                            var client_name = data[0].client_name;
                                                                            var address = data[0].address;
                                                                            var website = data[0].website;
                                                                            var phone_no = data[0].phone_no;
                                                                            var email = data[0].email;
                                                                            var job_card_no = data[0].job_card_no;
                                                                            var invoice_no = data[0].invoice_no;
                                                                            $(".invoiced_client").empty();
                                                                            $(".invoiced_client").append(" To <address> <strong>" + client_name + "</strong> <br> " + address + " <br>Phone: " + phone_no + " <br>Email: " + email + "  </address>");

                                                                            $(".invoice_no_details").empty();
                                                                            $(".invoice_no_details").append(" <b>Invoice # " + invoice_no + "</b> <br>  <b>Order ID:</b> " + job_card_no + " <br> <b>Payment Due:</b> 2/22/2014 <br>  ");

                                                                        }, error: function (data) {
                                                                            sweetAlert("Oops...", "Something went wrong!", "error");

                                                                        }
                                                                    });



                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: "<?php echo base_url(); ?>operations/get_job_sub_total/" + job_card_id,
                                                                        dataType: 'JSON',
                                                                        success: function (data) {
                                                                            var job_card_id = data[0].job_card_id;
                                                                            var statement_id = data[0].statement_id;
                                                                            var total_job_price = data[0].total_job_price;
                                                                            var tax = total_job_price * 0.16;
                                                                            var sub_total = total_job_price * 0.84;
                                                                            $('.job_card_id_payment').val(job_card_id);
                                                                            $(".subtotal_tr").empty();
                                                                            $(".subtotal_tr").append("  <th>Subtotal:</th> <td>" + sub_total + "</td>");
                                                                            $(".tax_tr").empty();
                                                                            $(".tax_tr").append(" <th>Tax (16.0%)</th><td>" + tax + "</td>");
                                                                            $(".total_tr").empty();
                                                                            $(".total_tr").append("<th>Total:</th> <td>" + total_job_price + "</td>");


                                                                        }, error: function (data) {
                                                                            sweetAlert("Oops...", "Something went wrong!", "error");

                                                                        }
                                                                    });
                                                                    $('.invoice_form_details_div').show('slow');
                                                                    $(".job_status_list").hide("slow");

                                                                    $('.client_payment_list').hide("slow");


                                                                });






                                                                $('.my_client_list_ul').on('click', '.my_client_list_li', function () {

                                                                    //get
                                                                    var job_card_id = $(this).closest('li').find('input[name="my_list_job_card_id"]').val();
                                                                    client_transaction_details = '';
                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: "<?php echo base_url(); ?>operations/get_invoice_job_details/" + job_card_id,
                                                                        dataType: "JSON",
                                                                        success: function (data) {


                                                                            for (i = 0; i < data.length; i++) {

                                                                                client_transaction_details += '<tr><td>' + data[i].qty + '</td><td>' + data[i].asset_name + '</td>  <td>' + data[i].total_price + '</td> </tr>';


                                                                            }

                                                                            $('.client_ivoice_stmnt').empty();
                                                                            $('.client_ivoice_stmnt').append(client_transaction_details);

                                                                        }, error: function (data) {
                                                                            sweetAlert("Oops...", "Something went wrong!", "error");

                                                                        }
                                                                    });




                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: "<?php echo base_url(); ?>operations/client_info/" + job_card_id,
                                                                        dataType: 'JSON',
                                                                        success: function (data) {
                                                                            var client_id = data[0].client_id;
                                                                            var client_name = data[0].client_name;
                                                                            var address = data[0].address;
                                                                            var website = data[0].website;
                                                                            var phone_no = data[0].phone_no;
                                                                            var email = data[0].email;
                                                                            var job_card_no = data[0].job_card_no;
                                                                            var invoice_no = data[0].invoice_no;
                                                                            $(".invoiced_client").empty();
                                                                            $(".invoiced_client").append(" To <address> <strong>" + client_name + "</strong> <br> " + address + " <br>Phone: " + phone_no + " <br>Email: " + email + "  </address>");

                                                                            $(".invoice_no_details").empty();
                                                                            $(".invoice_no_details").append(" <b>Invoice # " + invoice_no + "</b> <br>  <b>Order ID:</b> " + job_card_no + " <br> <b>Payment Due:</b> 2/22/2014 <br>  ");

                                                                        }, error: function (data) {
                                                                            sweetAlert("Oops...", "Something went wrong!", "error");

                                                                        }
                                                                    });



                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: "<?php echo base_url(); ?>operations/get_job_sub_total/" + job_card_id,
                                                                        dataType: 'JSON',
                                                                        success: function (data) {
                                                                            var job_card_id = data[0].job_card_id;
                                                                            var statement_id = data[0].statement_id;
                                                                            var total_job_price = data[0].total_job_price;
                                                                            var tax = total_job_price * 0.16;
                                                                            var sub_total = total_job_price * 0.84;
                                                                            $('.job_card_id_payment').val(job_card_id);
                                                                            $(".subtotal_tr").empty();
                                                                            $(".subtotal_tr").append("  <th>Subtotal:</th> <td>" + sub_total + "</td>");
                                                                            $(".tax_tr").empty();
                                                                            $(".tax_tr").append(" <th>Tax (16.0%)</th><td>" + tax + "</td>");
                                                                            $(".total_tr").empty();
                                                                            $(".total_tr").append("<th>Total:</th> <td>" + total_job_price + "</td>");


                                                                        }, error: function (data) {
                                                                            sweetAlert("Oops...", "Something went wrong!", "error");

                                                                        }
                                                                    });
                                                                    $('.invoice_form_details_div').show('slow');
                                                                    $(".job_status_list").hide("slow");

                                                                    $('.client_payment_list').hide("slow");


                                                                });


                                                            });
        </script>




    </body>

</html>