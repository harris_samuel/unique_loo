<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Unique Loo! | </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">
        <!-- editor -->
        <link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/editor/index.css" rel="stylesheet">
        <!-- select2 -->
        <link href="<?php echo base_url(); ?>assets/css/select/select2.min.css" rel="stylesheet">
        <!-- switchery -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/switchery/switchery.min.css" />

        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    </head>


    <body class="nav-md">

        <div class="container body">


            <div class="main_container">

                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">

                        <div class="navbar nav_title" style="border: 0;">


                            <a href="<?php echo base_url(); ?>"> <img  style="margin-left: 60px; margin-right: 0px; margin-bottom: 0px; margin-top: 0px;" src="<?php echo base_url(); ?>images/logo.png" alt="logo"  /></a>

                        </div>

                        <div class="clearfix"></div>



                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">
                                    <li><a href="<?php echo base_url(); ?>" ><i class="fa fa-home"></i> HOMR <span class="fa fa-chevron-right"></span></a> </li>




                                    <li><a><i class="fa fa-edit"></i> ADMINISTRATION <span class="fa fa-chevron-down"></span></a>

                                        <ul class="nav child_menu" style="display: none">

                                            <li>


                                                <?php
                                                $base_url = base_url();
                                                foreach ($admin_functions as $value) {
                                                    $controller_name = $value['controller_name'];
                                                    $function_name = $value['functions_name'];
                                                    $i_tag = $value['i_tag'];
                                                    $description = $value['description'];
                                                    $span_tag = $value['span_tag'];
                                                    ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>



                                        </ul> 

                                    </li>

                                    <li><a><i class="fa fa-edit"></i> DAILY OPERATIONS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($daily_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>






                                        </ul>
                                    </li>



                                    <li><a><i class="fa fa-edit"></i> REPORTS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($reports_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>







                                        </ul>
                                    </li>



                                </ul>
                            </div>

                        </div>
                        <!-- /sidebar menu -->



                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" href="<?php echo base_url(); ?>home/do_logout" title="Logout">
                                <span class="glyphicon glyphicon-off"  aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">

                    <div class="nav_menu">
                        <nav class="" user="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="<?php echo base_url(); ?>assets/images/img.jpg" alt=""><?php
                                        $username = $this->session->userdata('user_name');
                                        echo $username;
                                        ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <li><a href="<?php echo base_url(); ?>admin/profile">  Profile</a>
                                        </li>


                                        <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                        </li>
                                    </ul>
                                </li>


                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">

                        <div class="page-title">
                            <div class="title_left">
                                <h3>Unique Loo</h3>
                            </div>
                        </div>
                        <div class="clearfix"></div>



                        <div class="row">


                            <!-- Booked client Job Start -->

                            <div id="booked_client_div" class="booked_client_div" >


                                <div class="col-md-6 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Unique Loo <small>Booked Client Jobs </small></h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <br />


                                           

                                            <table class="table table-responsive table-bordered table-hover ">
                                                <thead>
                                                <th>Date</th>
                                                <th>Client Name </th>
                                                <th>Job Card No</th>
                                                <th>Action</th>
                                                </thead>

                                                <tbody id="client_list_ul" class="client_list_ul to_do">

                                                </tbody>
                                                <th>Date</th>
                                                <th>Client Name </th>
                                                <th>Job Card No</th>
                                                <th>Action</th>
                                                <tfoot>

                                                </tfoot>

                                            </table>



                                        </div>
                                    </div>

                                </div>



                            </div>


                            <!-- Book Client Job Card End -->


                            <!-- Completed Jobs List Start -->


                            <div id="completed_jobs_div" class="completed_jobs_div" >


                                <div class="col-md-6 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Unique Loo <small>Booked Client Jobs </small></h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <br />


                                            <!--<ul class="completed_jobs_list to_do" id="completed_jobs_list"></ul>-->


                                            <table class="table table-responsive table-bordered table-hover">
                                                <thead>
                                                <th>Date</th>
                                                <th>Client Name </th>
                                                <th>Job Card No</th>
                                                <th>Action</th>
                                                </thead>

                                                <tbody id="completed_jobs_list" class="completed_jobs_list to_do">

                                                </tbody>
                                                
                                                 <tfoot>
                                                <th>Date</th>
                                                <th>Client Name </th>
                                                <th>Job Card No</th>
                                                <th>Action</th>
                                                </tfoot>

                                            </table>



                                        </div>
                                    </div>

                                </div>



                            </div>


                            <!-- Completed Jobs List End -->



                            <!-- Assign resources to  Job List Start -->

                            <div class="col-md-6 col-sm-12 col-xs-12 cleint_job_list_div" id="cleint_job_list_div" style="display: none;">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2> Assign Assets  <small></small></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close_assign_asset"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">



                                        <div class="form-group">

                                            <div class="col-sm-12">





                                                <div id="booking_info_div" class="booking_info_div">

                                                    <table class="table table-bordered table-hover jambo_table">
                                                        <thead>
                                                        <th></th>
                                                        <th></th>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <input type="hidden" name="client_id_v" id="client_id_v" class="client_id_v form-control" readonly="readonly" placeholder="Cleint Id : "/>
                                                                    <!--<input type="hidden" name="job_card_id_v" class="job_card_id_v" id="job_card_id_v" />-->
                                                                    <input type="hidden" name="asset_tracker_id_v" class="asset_tracker_id_v" id="asset_tracker_id_v"/>
                                                                    <br>
                                                                    <label class="control-label col-md-6 col-sm-6 col-xs-12">Client Name : </label>

                                                                    <input type="text" name="client_name_v" id="client_name_v" class="client_name_v form-control" readonly="readonly" placeholder="Client Name : "/>

                                                                </td>
                                                                <td>
                                                                    <label class="control-label col-md-6 col-sm-6 col-xs-12">Job Card No : </label>

                                                                    <input type="text" name="job_card_no_v" id="job_card_no_v" class="job_card_no_v form-control" readonly="readonly" placeholder="Job Card No : "/>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label col-md-6 col-sm-6 col-xs-12">Total Price : </label>

                                                                    <input type="text" name="total_price_v" id="total_price_v" class="total_price_v form-control " readonly="readonly" placeholder="Total Price : "/>

                                                                </td>

                                                                <td>
                                                                    <label class="control-label col-md-6 col-sm-6 col-xs-12">Invoice No : </label>

                                                                    <input type="text" name="invoice_no_v" id="invoice_no_v" class="invoice_no_v form-control" readonly="readonly" placeholder="Invoice No : "/>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label col-md-6 col-sm-6 col-xs-12">Estimated Location Distance : </label>

                                                                    <input type="text" name="estmtd_lctn_dstnce_v" id="estmtd_lctn_dstnce_v" class="estmtd_lctn_dstnce_v form-control" readonly="" placeholder="Estimated Location Distance : "/>

                                                                </td>
                                                                <td>
                                                                    <label class="control-label col-md-6 col-sm-6 col-xs-12">Confirmed Location Distance : </label>

                                                                    <input type="text" name="cnfrmd_lctn_dstnce_v" id="cnfrmd_lctn_dstnce_v" readonly="" class="cnfrmd_lctn_dstnce_v form-control" placeholder="Confirmed Location Distance : "/>

                                                                </td>

                                                            </tr>
                                                            <tr>

                                                                <td>
                                                                    <label class="control-label col-md-6 col-sm-6 col-xs-12">Delivery Date : </label>

                                                                    <input readonly="readonly" required="required" name="delivery_date_v" id="delivery_date_v" class="delivery_date_v form-control" type="text" placeholder="Delivery Date : "/>

                                                                </td>
                                                                <td>
                                                                    <label class="control-label col-md-6 col-sm-6 col-xs-12">Event Date : </label>

                                                                    <input readonly="readonly" required="required" name="event_date_v" id="event_date_v" class="event_date_v form-control" type="text" placeholder="Event Date : "/>

                                                                </td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                    <table class="table table-bordered table-hover jambo_table">
                                                        <thead>
                                                        <th>Asset Name </th>
                                                        <th>No of Assets</th>
                                                        </thead>
                                                        <tbody id="no_of_assets_v_body" class="no_of_assets_v_body">

                                                        </tbody>
                                                    </table>

                                                </div>

                                                <div class="dashed-h">
                                                    <hr>
                                                </div>



                                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Assets</a>
                                                        </li>
                                                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false">Support Service</a>
                                                        </li>

                                                    </ul>
                                                    <div id="myTabContent" class="tab-content">


                                                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                                            <form class="assign_asset_form" id="assign_asset_form" >
                                                                <input type="hidden" name="job_card_id_v" class="job_card_id_v" id="job_card_id_v" />

                                                                <div id="assign_asset_div" class="assign_asset_div">
                                                                    <select name="asset_type" id="asset_type" class="asset_type  form-control">
                                                                        <option>Please Select Asset Type : </option>
                                                                        <?php
                                                                        foreach ($asset_types as $value) {
                                                                            ?>
                                                                            <option value="<?php echo $value['name']; ?>" ><?php echo $value['name']; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>    
                                                                    </select> 
                                                                    <br/>

                                                                    <div>



                                                                        <input type="text" name="asset_name" class="asset_name form-control" id="asset_name" placeholder="No of Assets Assigned" />

                                                                    </div>
                                                                    <br/>
                                                                    <div>
                                                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                                            <button type="button" class="btn btn-primary cancel_assign_asset_btn_1" id="cancel_assign_asset_btn_1">Cancel</button>
                                                                            <button type="submit" class="btn btn-sm btn-default assign_asset_btn" id="assign_asset_btn" name="assign_asset_btn">Assign Asset : </button>    

                                                                        </div>

                                                                    </div>
                                                                    <br/>
                                                                    <div>
                                                                    </div>
                                                                    <br/>

                                                                </div>

                                                            </form>

                                                        </div>
<!--                                                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                                            <form id="assign_asset_form_1" class="assign_asset_form_1 form">

                                                                <input type="hidden" name="asset_type_1" id="asset_type_1" class="asset_type_1" value="Support Service"/>
                                                                <input type="hidden" name="job_card_id_v_1" class="job_card_id_v_1" id="job_card_id_v_1" />
                                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                                    <input type="text" name="asset_name_1" class="form-control asset_name_1"/>

                                                                </div>
                                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                                    <input type="text"  name="asset_value" class="asset_value form-control" id="asset_value" placeholder="Value "/>
                                                                </div>

                                                                <div class="ln_solid"></div>
                                                                <div class="form-group">
                                                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                                        <button type="button" class="btn btn-primary cancel_assign_asset_btn" id="cancel_assign_asset_btn">Cancel</button>
                                                                        <button type="submit" class="btn btn-success book_job_btn" id="book_job_btn">Assign </button>
                                                                    </div>
                                                                </div>

                                                            </form>

                                                        </div>-->

                                                    </div>
                                                </div>









                                            </div>
                                        </div>





                                    </div>
                                </div>
                            </div>



                            <!-- Assign resources to  Job List  End -->




                            <!-- Show assigned asset list div start-->


                            <div class="col-md-6 col-sm-12 col-xs-12 assigned_assets_list_div" id="assigned_assets_list_div" style="display: none;">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2> Assign Assets Lists <small></small></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="assign_asset_close_link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">



                                        <div class="form-group">

                                            <div class="col-sm-12">




                                                <div class="divider-dashed"></div>


                                                <div id="assigned_assets_div" class="assigned_assets_div">

                                                    <table class="table table-bordered assigned_assets_table"  id="assigned_assets_table">
                                                        <thead>
                                                            <tr>

                                                                <th> Name</th>
                                                                <th>Value</th>
                                                                <th>Type</th>

                                                            </tr>
                                                        </thead>
                                                        <tfoot>
                                                            <tr>

                                                                <th> Name</th>
                                                                <th>Value</th>
                                                                <th>Type</th>

                                                            </tr>
                                                        </tfoot>
                                                        <tbody id="assigned_assets_table_body" class="assigned_assets_table_body">

                                                        </tbody>
                                                    </table>

                                                    <div class="ln_solid"></div>


                                                </div>





                                                <div class="dashed-h">
                                                    <hr>
                                                </div>












                                            </div>
                                        </div>





                                    </div>
                                </div>
                            </div>



                            <!-- Show assigned asset list div end-->






                            <!-- Add New Client Form start -->


                            <div class="row completed_job_details_div" id="completed_job_details_div" style="display: none;">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Unique Loo  Add Client </h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                                </li>

                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">

                                            <form id="release_assets_form" class="release_assets_form">


                                                <p>Release assets from Job card No : </p> <input type="text" name="completed_job_card_no" id="completed_job_card_no" class="completed_job_card_no form-control"/>
                                                <input type="hidden" name="release_job_card_id" id="release_job_card_id" class="release_job_card_id"/>

                                                <div class="ln_solid"></div>
                                                <div class="form-group">
                                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                        <button type="button" class="btn btn-primary cancel_release_asset_btn" id="cancel_release_asset_btn">Cancel</button>
                                                        <button type="submit" class="btn btn-success book_job_btn" id="book_job_btn">Close Job </button>
                                                    </div>
                                                </div>

                                            </form>

                                        </div>





                                    </div>
                                </div>
                            </div>

                            <!-- Add New Client Form End --> 


                            <!--Ajax Loader Div Start -->



                            <div class="col-md-6 col-sm-12 col-xs-12 ajax_loader_div" id="ajax_loader_div" style="display: none;">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2> Information Updater  <small></small></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <div id="" class="" >

                                            <img src="<?php echo base_url(); ?>assets/images/ajax_loader/ajax-loader.gif" alt="Ajax Loader" height="42" width="42"> 
                                            <hr>
                                            <div class="alert alert-info">Please Wait While Information is Loaded</div>
                                        </div>


                                    </div>
                                </div>
                            </div>



                            <!-- Ajax Loader Div End -->










                        </div>















                    </div>
                    <!-- /page content -->

                    <!-- footer content -->
                    <footer>
                        <div class="">
                            <p class="pull-right">Unique Loo ! by <a>Harris Dindi</a>. |
                                <span class="lead"> <i class="fa fa-paw"></i> Unique Loo!</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </footer>
                    <!-- /footer content -->

                </div>

            </div>
        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>


        <!-- Sweet Alert Library -->
        <script src="<?php echo base_url(); ?>assets/dist/sweetalert-dev.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
        <!--.......................-->


        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- chart js -->
        <script src="<?php echo base_url(); ?>assets/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url(); ?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>
        <!-- tags -->
        <script src="<?php echo base_url(); ?>assets/js/tags/jquery.tagsinput.min.js"></script>
        <!-- switchery -->
        <script src="<?php echo base_url(); ?>assets/js/switchery/switchery.min.js"></script>
        <!-- daterangepicker -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js"></script>
        <!-- richtext editor -->
        <script src="<?php echo base_url(); ?>assets/js/editor/bootstrap-wysiwyg.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/editor/external/jquery.hotkeys.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/editor/external/google-code-prettify/prettify.js"></script>
        <!-- select2 -->
        <script src="<?php echo base_url(); ?>assets/js/select/select2.full.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/parsley/parsley.min.js"></script>
        <!-- textarea resize -->
        <script src="<?php echo base_url(); ?>assets/js/textarea/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));
        </script>
        <!-- Autocomplete -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/autocomplete/countries.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/autocomplete/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>


        <!-- editor -->
        <script>
            $(document).ready(function () {
                $(".assigned_assets_list_div").hide("slow");

                setInterval(function () {
                    var job_card_id = $('.job_card_id_v').val();
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_assigned_assets/" + job_card_id,
                        dataType: "JSON",
                        success: function (booked_assets) {
                            booked_assets_list = $('#assigned_assets_table_body').empty();

                            $.each(booked_assets, function (i, booked_asset) {
                                var name = booked_asset.name;
                                if (name === "No Assets Assigned") {
                                    $(".assigned_assets_list_div").hide("slow");

                                } else {
                                    booked_assets_list.append("<tr><td>" + booked_asset.name + "</td><td>" + booked_asset.value + "</td><td>" + booked_asset.type + "</td></tr>");
                                }


                            });

                        },
                        error: function (data) {

                            // alert('An error occured, kindly try later');
                        }
                    });
                }, 3000);






                $(".close_assign_asset").click(function () {

                    $(".ajax_loader_div").show('slow');
                    $(".booked_client_div").show('slow');
                    $(".completed_jobs_div").show("slow");
                    $(".ajax_loader_div").hide('slow');
                    $(".cleint_job_list_div").hide('slow');
                    $(".assigned_assets_list_div").hide('slow');

                });



                $(".cancel_assign_asset_btn_1").click(function () {

                    $(".ajax_loader_div").show('slow');
                    $(".booked_client_div").show('slow');
                    $(".completed_jobs_div").show("slow");
                    $(".ajax_loader_div").hide('slow');
                    $(".cleint_job_list_div").hide('slow');
                    $(".assigned_assets_list_div").hide('slow');

                });

                $(".cancel_assign_asset_btn").click(function () {

                    $(".ajax_loader_div").show('slow');
                    $(".booked_client_div").show('slow');
                    $(".completed_jobs_div").show("slow");
                    $(".ajax_loader_div").hide('slow');
                    $(".cleint_job_list_div").hide('slow');
                    $(".assigned_assets_list_div").hide('slow');

                });


                $(".cancel_release_asset_btn").click(function () {

                    $(".ajax_loader_div").show('slow');
                    $(".booked_client_div").show('slow');
                    $(".completed_jobs_div").show("slow");
                    $(".ajax_loader_div").hide('slow');
                    $(".cleint_job_list_div").hide('slow');
                    $(".assigned_assets_list_div").hide('slow');
                    $(".release_assets_form").hide("slow");

                });





                $('#release_assets_form').submit(function (event) {
                    dataString = $("#release_assets_form").serialize();
                    $(".ajax_loader_div").show('slow');
                    $(".book_job_div").hide('slow');
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "<?php echo base_url() ?>operations/release_job_assets",
                        data: dataString,
                        success: function (data) {
                            sweetAlert("Success", "Assets Released Succesfully", "success");

                            $(".ajax_loader_div").show('slow');
                            $(".booked_client_div").show('slow');
                            $(".completed_jobs_div").show("slow");
                            $(".ajax_loader_div").hide('slow');
                            $(".completed_job_details_div").hide('slow');
                            $(".assigned_assets_list_div").hide('slow');


                            $("#release_assets_form").hide('slow');
                            $(".ajax_loader_div").hide('slow');

                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");
                            $(".book_job_div").hide();
                        }

                    });
                    event.preventDefault();
                    return false;
                });











            });


            $(document).ready(function () {



                setInterval(function () {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/view_completed_jobs/",
                        dataType: "JSON",
                        success: function (cleint_lists) {
                            cleint_list_list = $('#completed_jobs_list').empty();

                            $.each(cleint_lists, function (i, cleint_list) {
                                if (cleint_list.response === 'No Data!') {
                                    $(".ajax_loader_div_1").hide();
                                    cleint_list_list.append("No Completed Jobs found....");
                                } else {


                                    cleint_list_list.append(
                                            '<tr class="completed_jobs_list flat" id="completed_jobs_list" >    <td>' + cleint_list.event_date + '</td>     <td>' + cleint_list.client_name + '</td>     <td>' + cleint_list.job_card_no + '</td>        <td>\n\
<button type="button" id="completed_jobs_link" class="completed_jobs_link btn btn-primary">Close Job</button>\n\ \n\
 &nbsp; <input type="text" id="list_client_id" class="list_client_id hidden " name="list_client_id" value="' + cleint_list.client_id + '"/> \n\
<input type="text" id="completed_job_card_id" class="completed_job_card_id hidden " name="completed_job_card_id" value="' + cleint_list.job_card_id + '"/>  <input type="text" id="completed_job_card_no" class="completed_job_card_no hidden " name="completed_job_card_no" value="' + cleint_list.job_card_no + '"/> &nbsp;</td>     </tr>'

                                            );
                                }


                            });

                        },
                        error: function (data) {

                            // alert('An error occured, kindly try later');
                        }
                    });
                }, 3000);




                $('.completed_jobs_list').on('click', '.completed_jobs_list', function () {

                    //get
                    //  var job_card_id = $(this).closest('li').find('input[name="my_list_job_card_id"]').val();
                    var job_card_no = $(this).closest('tr').find('input[name="completed_job_card_no"]').val();

                    var job_card_id = $(this).closest('tr').find('input[name="completed_job_card_id"]').val();



                    swal({
                        title: "Do you want to Close  Job Card No : " + job_card_no + " ?",
                        text: "You will not be able to re-do this action!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "Red",
                        confirmButtonText: "Yes, Close it!",
                        closeOnConfirm: false
                    },
                    function () {


                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>operations/release_job_assets/" + job_card_id,
                            dataType: "JSON",
                            success: function (data) {
                                var response = data[0].response;
                                if (response === "Job Closed successfully!") {
                                    swal("Success!", "The Job Card has been Closed.", "success");

                                } else if (response === "Job  was not Closed!") {
                                    swal("Oops...", "Something went wrong!", "error");

                                }
                            }, error: function (data) {
                                var response = data[0].response;
                                if (response === "Job Closed successfully!") {
                                    swal("Success!", "The Job Card has been Closed.", "success");

                                } else if (response === "Job  was not Closed!") {
                                    swal("Oops...", "Something went wrong!", "error");

                                }
                            }
                        });


                    });


                });












                setInterval(function () {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/view_booked_clients/",
                        dataType: "JSON",
                        success: function (cleint_lists) {
                            cleint_list_list = $('#client_list_ul').empty();

                            $.each(cleint_lists, function (i, cleint_list) {

                                if (cleint_list.response === 'No Data!') {
                                    $(".ajax_loader_div_1").hide();
                                    cleint_list_list.append("No Booked Clients found....");
                                } else {

                                    cleint_list_list.append(
                                            '<tr class="client_list_li flat" id="client_list_li" >    <td>' + cleint_list.event_date + '</td>     <td>' + cleint_list.client_name + '</td>     <td>' + cleint_list.job_card_no + '</td>       <td><button type="button" id="bill_patient_link" class="bill_patient_link btn btn-primary">Assign Asset</button>  &nbsp; <input type="text" id="list_client_id" class="list_client_id hidden " name="list_client_id" value="' + cleint_list.client_id + '"/> <input type="text" id="list_job_card_id" class="list_job_card_id hidden " name="list_job_card_id" value="' + cleint_list.job_card_id + '"/></td>     </tr>'

//                '<li class="client_list_li flat" id="client_list_li"> <span class="fa fa-arrow-right"></span> <p>' + cleint_list.event_date + ' &nbsp; ' + cleint_list.client_name + '&nbsp; ' + cleint_list.job_card_no + ' &nbsp;' + cleint_list.invoice_no + ' &nbsp; \n\
//                        <button type="button" id="bill_patient_link" class="bill_patient_link btn btn-primary">Assign Asset</button>\n\
// &nbsp; <input type="text" id="list_client_id" class="list_client_id hidden " name="list_client_id" value="' + cleint_list.client_id + '"/> \n\
// <input type="text" id="list_job_card_id" class="list_job_card_id hidden " name="list_job_card_id" value="' + cleint_list.job_card_id + '"/> &nbsp; </p> </li>'
//             

                                            );

                                }


                            });

                        },
                        error: function (data) {

                            // alert('An error occured, kindly try later');
                        }
                    });
                }, 3000);



                // $('.client_list_ul').on('click', '.client_list_li', function () {

                $('.client_list_ul').on('click', '.client_list_li', function () {


                    //get
                    var job_card_id = $(this).closest('tr').find('input[name="list_job_card_id"]').val();
                    //alert(job_card_id);
                    $('.ajax_loader_div').show();
                    $('.booked_client_div').hide();
                    $('.completed_jobs_div').hide("slow");
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>operations/get_job_details/" + job_card_id,
                        dataType: "JSON",
                        success: function (data) {
                            $('.ajax_loader_div').hide();
                            $('.cleint_job_list_div').show();

                            $('.client_id_v').val(data[0].client_id);
                            $('.client_name_v').val(data[0].client_name);
                            $('.job_card_no_v').val(data[0].job_card_no);
                            $('.total_price_v').val(data[0].total_price);
                            $('.invoice_no_v').val(data[0].invoice_no);
                            $('.estmtd_lctn_dstnce_v').val(data[0].estmtd_lctn_dstnce);
                            $('.cnfrmd_lctn_dstnce_v').val(data[0].cnfrmd_lctn_dstnce);
                            $('.event_date_v').val(data[0].dte_evnt);
                            $('.delivery_date_v').val(data[0].dte_srcd);
                            $('.job_card_id_v').val(data[0].job_card_id);
                            $('.job_card_id_v_1').val(data[0].job_card_id);
                            $('.asset_tracker_id_v').val(data[0].asset_tracker_id);
                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }
                    });


                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>operations/get_assgnd_assets/" + job_card_id,
                        dataType: "JSON",
                        success: function (no_of_assets) {
                            no_of_assets_v_body = $('#no_of_assets_v_body').empty();

                            $.each(no_of_assets, function (i, asset_list) {
                                no_of_assets_v_body.append('<tr><td>' + asset_list.name + '</td><td>' + asset_list.no_of_assets + '</td></tr>');


                            });


                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }
                    });

                });



                $('.asset_type').change(function () {
                    var asset_type = $('.asset_type').val();
                    remaining_assets_list = '';
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_remaining_assets/" + asset_type,
                        dataType: "JSON",
                        success: function (response) {
                            remaining_assets_list = $('#asset_name').empty();
                            for (i = 0; i < response.length; i++) {

                                var value = response[i].name;

                                if (value === "No Assets Found") {



                                } else {
                                    remaining_assets_list = '<option value="' + response[i].id + '">' + response[i].number + '</option>';
                                    $('#asset_name').append(remaining_assets_list);
                                    $("#asset_name").show();


                                }


                            }


                        }, error: function (repsonse) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }
                    });

                });



                $(".assign_asset_close_link").click(function () {
                    $(".ajax_loader_div").show('slow');
                    $(".booked_client_div").show('slow');
                    $(".completed_jobs_div").show("slow");
                    $(".ajax_loader_div").hide('slow');
                    $(".cleint_job_list_div").hide('slow');
                    $(".assigned_assets_list_div").show("slow");


                });


                $('#assign_asset_form_1').submit(function (event) {
                    dataString = $("#assign_asset_form_1").serialize();
                    $(".ajax_loader_div").show('slow');

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>operations/assign_asset",
                        data: dataString,
                        success: function (data) {


                            $(".ajax_loader_div").hide('slow');
                            $(".cleint_job_list_div").show('slow');
                            $(".assigned_assets_list_div").show('slow');


                            $(".ajax_loader_div").hide('slow');
                            sweetAlert("Success", "Asset Assigned Successfully", "success")
                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }

                    });
                    event.preventDefault();
                    return false;
                });


                $('#assign_asset_form').submit(function (event) {
                    dataString = $("#assign_asset_form").serialize();
                    $(".ajax_loader_div").show('slow');

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>operations/assign_asset",
                        data: dataString,
                        success: function (data) {
                            data = JSON.parse(data);

                            var asset_check = data[0].asset_check;

                            if (asset_check === "Asset exists") {
                                sweetAlert("Sorry...", "The Asset has already been assigned to the Job and cannot be assigned twice.! :( ", "error");

                            } else if (asset_check === "Asset not assigned") {
                                $(".ajax_loader_div").hide('slow');
                                $(".cleint_job_list_div").show('slow');
                                $(".assigned_assets_list_div").show('slow');
                                $(".ajax_loader_div").hide('slow');

                                sweetAlert("Success", "Asset Assigned Successfully", "success");

                            }








                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }

                    });
                    event.preventDefault();
                    return false;
                });








            });
        </script>
        <!-- /editor -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

        <script>
            var j = jQuery.noConflict();
            j(document).ready(function () {
                j("#select_client_name").change(function () {
                    var client_id = this.value;

                    j.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>admin/get_client_details/" + client_id,
                        dataType: "JSON",
                        success: function (response) {
                            j(".client_e_mail").val(response[0].email);
                            j(".client_phone_no").val(response[0].phone_no);
                            j(".client_location").val(response[0].address);
                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }
                    });
                });


                j("#select_asset_name").change(function () {
                    var client_id = this.value;

                    j.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_asset_details/" + client_id,
                        dataType: "JSON",
                        success: function (response) {
                            j(".asset_id").val(response[0].id);
                            j(".asset_parameter").val(response[0].parameter);
                            j(".asset_value").val(response[0].value);
                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }
                    });
                });


                j('.asset_quantity').keyup(function () {
                    var quantity = j('.asset_quantity').val();
                    var value = j('.asset_value').val();
                    var total_price = quantity * value;

                    j('.asset_total').val(total_price);

                });







            });

        </script>        


        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script>
            var x = jQuery.noConflict();
            x(function () {
                x("#delivery_date").datepicker(
                        {dateFormat: 'yy-mm-dd'}
                );


                x("#event_date").datepicker(
                        {dateFormat: 'yy-mm-dd'}
                );

            });
        </script>


    </body>

</html>