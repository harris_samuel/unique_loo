<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Unique Loo! | </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">


        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    </head>

    <body style="background:#F7F7F7;">

        <div class="">
            <a class="hiddenanchor" id="toregister"></a>
            <a class="hiddenanchor" id="tologin"></a>

            <a href="<?php echo base_url(); ?>"> <img  style="margin-left: 630px; margin-right: 0px; margin-bottom: -93px; margin-top: 40px;" src="<?php echo base_url(); ?>images/logo.png" alt="logo"  /></a>

            <div id="wrapper">
                <?php if (!is_null($msg)) echo $msg; ?>
                <div id="login" class="animate form">
                    <section class="login_content">
                        <form class="form-signin" action="<?php echo base_url(); ?>login/process" method="post">
                            <h1>Login Form</h1>
                            <div>
                                <input type="text" class="form-control" placeholder="Username" name="username" required="" />
                            </div>
                            <div>
                                <input type="password" class="form-control" placeholder="Password" name="password" required="" />
                            </div>
                            <div>
                                <button class="btn btn-default submit" type="submit">Log In</button>


                            </div>
                            <div class="clearfix"></div>
                            <div class="separator">

                                <p class="change_link">Lost your Password?
                                    <a href="#toregister" class="to_register"> Reset Account </a>
                                </p>
                                <div class="clearfix"></div>
                                <br />
                                <div>
                                    <h1><i class="fa fa-users" style="font-size: 26px;"></i> Unique Loo!</h1>

                                    <p>©2015 All Rights Reserved. Unique Loo! </p>
                                </div>
                            </div>
                        </form>
                        <!-- form -->
                    </section>
                    <!-- content -->
                </div>
                <div id="register" class="animate form">
                    <section class="login_content">
                        <form id="reset_account_form1" class="reset_account_form1 form" method="post" action="<?php echo base_url(); ?>login/reset_password">
                            <h1>Reset  Account</h1>

                            <div>
                                <input type="email" name="email" class="form-control" placeholder="Email" required="" />


                            </div>

                            <div>


                                <button class="btn btn-default submit" type="submit">Reset Password</button>
                            </div>
                            <div class="clearfix"></div>
                            <div class="separator">

                                <p class="change_link">Remember Password ?
                                    <a href="#tologin" class="to_register"> Log in </a>
                                </p>
                                <div class="clearfix"></div>
                                <br />
                                <div>
                                    <h1><i class="fa fa-users" style="font-size: 26px;"></i> Unique Loo!</h1>

                                    <p>©2015 All Rights Reserved. Unique Loo!</p>
                                </div>
                            </div>
                        </form>
                        <!-- form -->
                    </section>
                    <!-- content -->
                </div>









            </div>
        </div>

    </body>


    <script type="text/javascript">
        $(document).ready(function () {

        $('#reset_account_form').submit(function (event) {
        dataString = $("#reset_account_form").serialize();
                $.ajax({
                type: "POST",
                        url: "<?php echo base_url() ?>index.php/login/reset_password",
                        data: dataString,
                        success: function (data) {

                        setInterval(function () {

                        }, 3000);
                        }
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("E-mail does not exist in the  system ");
                }

                });
                event.preventDefault();
                return false;
        });
        });
    </script>

</html>