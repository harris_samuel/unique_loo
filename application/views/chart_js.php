<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Unique Loo! | </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/maps/jquery-jvectormap-2.0.1.css" />
        <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/css/floatexamples.css" rel="stylesheet" type="text/css" />

        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/nprogress.js"></script>

        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->






    </head>


    <body class="nav-md">
        
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bar-chart-o fa-fw"></i> Account Registrations Past 7 days
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div id="acctregs" style="height: 300px;"></div>
            </div>
            <!-- /.panel-body -->
        </div>



        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>


        <script>
            $(document).ready(function () {


                $.getJSON("<?php echo site_url('operations/chart_js'); ?>", function (json) {
                    var acctregs = new Morris.Bar({
                        // ID of the element in which to draw the chart.
                        element: 'acctregs',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: json,
                        // The name of the data record attribute that contains x-values.
                        xkey: 'date_added',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['clnt_id','job_id'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Client Id','Job Id'],
                        dateFormat: function (x) {
                            return new Date(x).toString().split("00:00:00")[0];
                        }
                    });
                });
            });

        </script>


      

        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- gauge js -->
        <!-- chart js -->
        <script src="<?php echo base_url(); ?>assets/js/chartjs/chart.min.js"></script>

        <!-- bootstrap progress js -->
        <script src="<?php echo base_url(); ?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>
        <!-- daterangepicker -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js"></script>

        <script src="js/custom.js"></script>

        <!-- flot js -->
        <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.pie.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.orderBars.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.time.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/date.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.spline.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.stack.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/curvedLines.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.resize.js"></script>


        <!-- worldmap -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/maps/jquery-jvectormap-2.0.1.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/maps/gdp-data.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/maps/jquery-jvectormap-us-aea-en.js"></script>
        <script>
        </script>
        <!-- skycons -->
        <script src="<?php echo base_url(); ?>assets/js/skycons/skycons.js"></script>

        <!-- datepicker -->








        <!-- /datepicker -->
        <!-- /footer content -->
    </body>

</html>
