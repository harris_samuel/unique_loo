<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Unique Loo! | </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">
        <!-- editor -->
        <link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/editor/index.css" rel="stylesheet">
        <!-- select2 -->
        <link href="<?php echo base_url(); ?>assets/css/select/select2.min.css" rel="stylesheet">
        <!-- switchery -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/switchery/switchery.min.css" />

        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    </head>


    <body class="nav-md">

        <div class="container body">


            <div class="main_container">

                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">

                        <div class="navbar nav_title" style="border: 0;">


                            <a href="<?php echo base_url(); ?>"> <img  style="margin-left: 60px; margin-right: 0px; margin-bottom: 0px; margin-top: 0px;" src="<?php echo base_url(); ?>images/logo.png" alt="logo"  /></a>

                        </div>

                        <div class="clearfix"></div>



                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">
                                    <li><a href="<?php echo base_url(); ?>" ><i class="fa fa-home"></i> HOME <span class="fa fa-chevron-right"></span></a> </li>




                                    <li><a><i class="fa fa-edit"></i> ADMINISTRATION <span class="fa fa-chevron-down"></span></a>

                                        <ul class="nav child_menu" style="display: none">

                                            <li>


                                                <?php
                                                $base_url = base_url();
                                                foreach ($admin_functions as $value) {
                                                    $controller_name = $value['controller_name'];
                                                    $function_name = $value['functions_name'];
                                                    $i_tag = $value['i_tag'];
                                                    $description = $value['description'];
                                                    $span_tag = $value['span_tag'];
                                                    ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>



                                        </ul> 

                                    </li>

                                    <li><a><i class="fa fa-edit"></i> DAILY OPERATIONS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($daily_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>






                                        </ul>
                                    </li>



                                    <li><a><i class="fa fa-edit"></i> REPORTS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($reports_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>







                                        </ul>
                                    </li>



                                </ul>
                            </div>

                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" href="<?php echo base_url(); ?>home/do_logout" title="Logout">
                                <span class="glyphicon glyphicon-off"  aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">

                    <div class="nav_menu">
                        <nav class="" user="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="<?php echo base_url(); ?>assets/images/img.jpg" alt=""><?php
                                        $username = $this->session->userdata('user_name');
                                        echo $username;
                                        ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <li><a href="<?php echo base_url(); ?>admin/profile">  Profile</a>
                                        </li>


                                        <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                        </li>
                                    </ul>
                                </li>


                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">

                        <div class="page-title">
                            <div class="title_left">
                                <h3>Unique Loo</h3>
                            </div>
                        </div>
                        <div class="clearfix"></div>



                        <div class="row">




                            <div>

                                <!-- Asset Status List Start -->

                                <div id="book_job_div" class="book_job_div" >


                                    <div class="col-md-6 col-xs-12">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>Unique Loo <small>Asset Status List </small></h2>
                                                <ul class="nav navbar-right panel_toolbox">
                                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="class_one" id="class_one" style="display: inline;">
                                                <div class="x_content">
                                                    <br />
                                                    <button id="refresh_asset_table_data" class="refresh_asset_table_data btn btn-default btn-xs">Refresh Data</button>
                                                    <table id="asset_table_status" class="dataTable jambo_table table-bordered table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th>No of Jobs</th>
                                                                <th>Code</th>
                                                                <th>Type</th>
                                                                <th>Status</th>
                                                            </tr>
                                                        </thead>


                                                        <tfoot>
                                                            <tr>
                                                                <th>No of Jobs</th>
                                                                <th>Code</th>
                                                                <th>Type</th>
                                                                <th>Status</th>
                                                            </tr>
                                                        </tfoot>
                                                        <tbody id="asset_status_tr" class="asset_status_tr">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="class_two" id="class_two" style="display: none;">
                                                <div class="x_content">
                                                    <br />
                                                    <button type="button" id="go_back_btn" class="go_back_btn btn btn-default">Go Back </button>
                                                    <br>
                                                    <table id="asset_detailed_table" class="dataTable jambo_table table-bordered table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th>Job Card No</th>
                                                                <th>Client</th>
                                                                <th>Asset #</th>
                                                                <th>Event Date</th>
                                                                <th>Delivery Date</th>
                                                            </tr>
                                                        </thead>


                                                        <tfoot>
                                                            <tr>
                                                                <th>Job Card No</th>
                                                                <th>Client</th>
                                                                <th>Asset #</th>
                                                                <th>Event Date</th>
                                                                <th>Delivery Date</th>
                                                            </tr>
                                                        </tfoot>
                                                        <tbody id="asset_detailed_tr" class="asset_detailed_tr">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>


                                        </div>

                                    </div>



                                </div>


                                <!-- Asset Status List End -->
                            </div>


                            <!--Ajax Loader Div Start -->
                            <div id="ajax_loader_div" class="ajax_loader_div" style="display: none;">

                                <img src="<?php echo base_url(); ?>assets/images/ajax_loader/ajax-loader.gif" alt="Ajax Loader" height="42" width="42"> 
                                <hr>
                                <div class="alert alert-info">Please Wait While Information is Loaded</div>
                            </div>

                            <!-- Ajax Loader Div End -->


                        </div>















                    </div>
                    <!-- /page content -->

                    <!-- footer content -->
                    <footer>
                        <div class="">
                            <p class="pull-right">Unique Loo !  <a>Harris Dindi</a>. |
                                <span class="lead"> <i class="fa fa-paw"></i> Unique Loo!</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </footer>
                    <!-- /footer content -->

                </div>

            </div>
        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>



        <!-- Sweet Alert Library -->
        <script src="<?php echo base_url(); ?>assets/dist/sweetalert-dev.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
        <!--.......................-->



        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- chart js -->
        <script src="<?php echo base_url(); ?>assets/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url(); ?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>
        <!-- tags -->
        <script src="<?php echo base_url(); ?>assets/js/tags/jquery.tagsinput.min.js"></script>
        <!-- switchery -->
        <script src="<?php echo base_url(); ?>assets/js/switchery/switchery.min.js"></script>
        <!-- daterangepicker -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js"></script>
        <!-- richtext editor -->
        <script src="<?php echo base_url(); ?>assets/js/editor/bootstrap-wysiwyg.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/editor/external/jquery.hotkeys.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/editor/external/google-code-prettify/prettify.js"></script>
        <!-- select2 -->
        <script src="<?php echo base_url(); ?>assets/js/select/select2.full.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/parsley/parsley.min.js"></script>
        <!-- textarea resize -->
        <script src="<?php echo base_url(); ?>assets/js/textarea/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));
        </script>
        <!-- Autocomplete -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/autocomplete/countries.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/autocomplete/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>


        <script src="<?php echo base_url(); ?>assets/js/datatables/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>




        <!-- editor -->
        <script>
            $(document).ready(function () {

                $('.booked_by').change(function () {
                    // var booked_by = $(".booked_by").val();
                    var booked_by = this.value;

                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/check_user_existence/" + booked_by,
                        dataType: "json",
                        success: function (response) {
                            var user_check = response[0].user_check;

                            if (user_check === "User Exists in the System") {
                                $('.book_job_btn_div').show();

                            } else if (user_check === "User Does not exist in the System") {
                                $('.book_job_btn_div').hide();

                            }

                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }
                    });

                });





                setInterval(function () {
                    get_client_list();
                }, 3000);

                function get_client_list() {
                    client_list = '';
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_client_lists",
                        dataType: "JSON",
                        success: function (response) {
                            $('#select_client_name').empty();
                            client_list = '<option>Please select Client : </option>';
                            for (i = 0; i < response.length; i++) {
                                client_list += '<option value="' + response[i].id + '">' + response[i].name + '</option>';
                                $('#select_client_name').html('');
                                $('#select_client_name').append(client_list);
                            }


                        },
                        error: function (response) {

                        }
                    });

                }


                asset_status_list = '';
                $.ajax({
                    type: "GET",
                    url: "<?php echo base_url(); ?>operations/asset_status",
                    dataType: "JSON",
                    success: function (response) {
                        for (i = 0; i < response.length; i++) {
                            asset_status_list = '<tr><td >' + response[i].job_card_no + '</td><td >' + response[i].number + '</td><td>' + response[i].type + '</td><td ><input type="text" name="view_more_id" readonly class="view_more_id' + response[i].number + ' btn btn-default" id="view_more_id" value="' + response[i].number + '"/></td></tr>';
                            $('#asset_status_tr').append(asset_status_list);

                            $("#asset_status_tr").on("click", ".view_more_id" + response[i].asset_id, function () {
                                console.log(response);
                                var asset_id = this.value;
                                get_asset_details(asset_id);


                            });
                        }


                    },
                    error: function (response) {

                    }
                });

                function get_asset_details(asset_id) {
                    asset_detailed_list = '';
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_asset_status_details/" + asset_id + "",
                        dataType: "JSON",
                        success: function (response) {
                            for (i = 0; i < response.length; i++) {
                                asset_detailed_list = '<tr><td >' + response[i].job_card_no + '</td><td >' + response[i].client_name + '</td><td>' + response[i].asset_name + '</td><td >' + response[i].dte_evnt + '</td><td >' + response[i].dte_srcd + '</td></tr>';
                                $('#asset_detailed_tr').append(asset_detailed_list);



                            }
                            $("#class_two").show();
                            $("#class_one").hide();


                        },
                        error: function (response) {

                        }
                    });

                }



                $(".go_back_btn").click(function () {
                    $("#class_two").hide();
                    $("#class_one").show();
                });




                $(".refresh_asset_table_data").click(function () {

                    asset_status_list = '';
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/asset_status",
                        dataType: "JSON",
                        success: function (response) {
                            $('#asset_status_tr').empty();
                            for (i = 0; i < response.length; i++) {
                                asset_status_list = '<tr><td >' + response[i].job_card_no + '</td><td >' + response[i].number + '</td><td>' + response[i].type + '</td><td ><input type="text" name="view_more_id" readonly class="view_more_id' + response[i].number + ' btn btn-default" id="view_more_id" value="' + response[i].number + '"/></td></tr>';
                                $('#asset_status_tr').append(asset_status_list);

                                $("#asset_status_tr").on("click", ".view_more_id" + response[i].asset_id, function () {
                                    console.log(response);
                                    var asset_id = this.value;
                                    get_asset_details(asset_id);


                                });
                            }


                        },
                        error: function (response) {

                        }
                    });


                });

                user_email_list = '';
                $.ajax({
                    type: "GET",
                    url: "<?php echo base_url(); ?>admin/get_user_emails",
                    dataType: "JSON",
                    success: function (response) {
                        for (i = 0; i < response.length; i++) {
                            user_email_list = '<option value="' + response[i].email + '">' + response[i].email + '</option>';
                            $('#add_user_email').append(user_email_list);
                        }


                    },
                    error: function (response) {

                    }
                });






                $('#go_back_job_card').click(function () {
                    $(".add_new_client_div").hide('slow');
                    $(".ajax_loader_div").show('slow');
                    $(".book_job_div").show('slow');
                    $(".ajax_loader_div").hide('slow');
                });



                $('.cancel_job_btn').click(function () {

                });



                $('.cancel_job_btn_1').submit(function () {
                    var job_card_no = $('.job_card_id').val();
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>operations/delete_job_card/" + job_card_no,
                        dataType: 'JSON',
                        success: function (data) {
                            sweetAlert("...", "Job Deleted Successfully!", "error");
                        }
                    });
                });



                $('.invoice_client_button').click(function () {
                    var job_card_no = $('.job_card_id').val();
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>operations/invoice_client/" + job_card_no,
                        dataType: "JSON",
                        success: function (data) {
                            // data = JSON.parse(data);
                            var invoice_no = data[0].invoice_no;
                            $('.generated_inv_no').val(data[0].invoice_no);


                            sweetAlert("Success", "Invoice generated successfully, you can now proceed to payment", "success");

                        }
                    });
                });



                $('#back_add_client_btn').click(function () {
                    $(".asset_calculcator_div").hide('slow');
                    $(".ajax_loader_div").show('slow');

                    var job_card_no = $('.job_card_id').val();
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_booked_job_details/" + job_card_no,
                        dataType: "JSON",
                        success: function (response) {
                            $(".job_card_id").val(response[0].id);
                            $(".client_name").val(response[0].clnt_id);
                            $(".delivery_date").val(response[0].dte_srcd);
                            $(".event_date").val(response[0].dte_evnt);
                            $(".location").val(response[0].lctn);
                            $(".est_location_dstnce").val(response[0].estmtd_lctn_dstnce);
                            $(".cnfrmd_location_dstnce").val(response[0].cnfrmd_lctn_dstnce);
                            $(".time_from").val(response[0].time_from);
                            $(".time_to").val(response[0].time_to);
                            $(".edit_job_card").val(response[0].id);
                            $(".edit_job_card_no").val(response[0].job_card_no);
                            $(".book_job_div").show('slow');
                            $(".ajax_loader_div").hide('slow');
                            $(".asset_calculcator_div").hide('slow');


                        }
                    });


                });


                $(".add_new_client").click(function () {

                    $(".ajax_loader_div").show('slow');
                    $(".add_new_client_div").show('slow');
                    $(".ajax_loader_div").hide('slow');
                    $(".book_job_div").hide('slow');

                });


                $(".change_selected_client").click(function () {
                    $('.change_client_div').hide();
                    $('.select_client_name_div').show("slow");
                    $('.selected_client_name_div').hide("slow");

                });





                $(".reset_add_client_form").click(function () {
                    $(".ajax_loader_div").show('slow');
                    $(".add_new_client_div").hide('slow');
                    $(".ajax_loader_div").hide('slow');
                    $(".book_job_div").show('slow');

                });

                $('#add_new_client_form').submit(function (event) {
                    dataString = $("#add_new_client_form").serialize();
                    $(".ajax_loader_div").show('slow');

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/admin/add_new_client",
                        data: dataString,
                        success: function (data) {
                            sweetAlert("Success!", "New Client Added Successfully!", "success");

                            $(".add_new_client_div").hide('slow');
                            $(".ajax_loader_div").hide('slow');
                            $(".book_job_div").show('slow');
                        }

                    });
                    event.preventDefault();
                    return false;
                });



            


                $('#asset_calculcator_form').submit(function (event) {
                    dataString = $("#asset_calculcator_form").serialize();
                    $(".ajax_loader_div").show('slow');

                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "<?php echo base_url() ?>operations/save_asset_value",
                        data: dataString,
                        success: function (data) {
                            sweetAlert("Great!...", "Asset Saved Successfully!", "success");

                            $(".book_job_div").hide('slow');
                            $(".booked_assets_div").show('slow');
                            $(".ajax_loader_div").hide('slow');


                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");
                            $(".book_job_div").show('slow');

                        }

                    });
                    event.preventDefault();
                    return false;
                });







                $('.xcxc').click(function () {
                    $('#descr').val($('#editor').html());
                });
            });

            $(function () {
                function initToolbarBootstrapBindings() {
                    var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                        'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                        'Times New Roman', 'Verdana'],
                            fontTarget = $('[title=Font]').siblings('.dropdown-menu');
                    $.each(fonts, function (idx, fontName) {
                        fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
                    });
                    $('a[title]').tooltip({
                        container: 'body'
                    });
                    $('.dropdown-menu input').click(function () {
                        return false;
                    })
                            .change(function () {
                                $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                            })
                            .keydown('esc', function () {
                                this.value = '';
                                $(this).change();
                            });

                    $('[data-role=magic-overlay]').each(function () {
                        var overlay = $(this),
                                target = $(overlay.data('target'));
                        overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
                    });
                    if ("onwebkitspeechchange" in document.createElement("input")) {
                        var editorOffset = $('#editor').offset();
                        $('#voiceBtn').css('position', 'absolute').offset({
                            top: editorOffset.top,
                            left: editorOffset.left + $('#editor').innerWidth() - 35
                        });
                    } else {
                        $('#voiceBtn').hide();
                    }
                }
                ;

                function showErrorAlert(reason, detail) {
                    var msg = '';
                    if (reason === 'unsupported-file-type') {
                        msg = "Unsupported format " + detail;
                    } else {
                        console.log("error uploading file", reason, detail);
                    }
                    $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
                }
                ;
                initToolbarBootstrapBindings();
                $('#editor').wysiwyg({
                    fileUploadError: showErrorAlert
                });
                window.prettyPrint && prettyPrint();
            });

            $(document).ready(function () {






                setInterval(function () {
                    var get_client_id = $('.job_card_id').val();
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_booked_assets/" + get_client_id,
                        dataType: "JSON",
                        success: function (booked_assets) {
                            booked_assets_list = $('#booked_assets_table_body').empty();

                            $.each(booked_assets, function (i, booked_asset) {
                                booked_assets_list.append("<tr><td>" + booked_asset.name + "</td><td>" + booked_asset.price + "</td><td>" + booked_asset.qty + "</td><td>" + booked_asset.total_price + "</td></tr>");


                            });

                        },
                        error: function (data) {

                            // alert('An error occured, kindly try later');
                        }
                    });
                }, 3000);




            });




        </script>



        <!-- /editor -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

        <script>
            var j = jQuery.noConflict();
            j(document).ready(function () {



                j("#select_client_name").change(function () {
                    var client_id = this.value;

                    j.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>admin/get_client_details/" + client_id,
                        dataType: "JSON",
                        success: function (response) {
                            j(".client_e_mail").val(response[0].email);
                            j(".client_phone_no").val(response[0].phone_no);
                            j(".client_location").val(response[0].address);
                            j(".selected_client_name").val(response[0].name);
                            j(".client_id").val(client_id);

                            j('.select_client_name_div').hide("slow");
                            j('.selected_client_name_div').show("slow");
                            j('.change_client_div').show("slow");

                        }
                    });



                });


                j("#select_asset_name").change(function () {
                    j(".asset_quantity").val("");
                    j(".asset_total").val("");
                    var client_id = this.value;

                    j.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_asset_details/" + client_id,
                        dataType: "JSON",
                        success: function (response) {
                            j(".asset_id").val(response[0].id);
                            j(".asset_parameter").val(response[0].parameter);
                            j(".asset_value").val(response[0].value);
                        }
                    });
                });


                j('.asset_quantity').keyup(function () {
                    var quantity = j('.asset_quantity').val();
                    var value = j('.asset_value').val();
                    var total_price = quantity * value;

                    j('.asset_total').val(total_price);

                });





                j('.asset_quantity').click(function () {
                    var quantity = j('.asset_quantity').val();
                    var value = j('.asset_value').val();
                    var total_price = quantity * value;

                    j('.asset_total').val(total_price);

                });







            });

        </script>        


        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script>
            var x = jQuery.noConflict();
            x(function () {
                x("#delivery_date").datepicker(
                        {dateFormat: 'yy-mm-dd'}
                );


                x("#event_date").datepicker(
                        {dateFormat: 'yy-mm-dd'}
                );

            });
        </script>


    </body>

</html>