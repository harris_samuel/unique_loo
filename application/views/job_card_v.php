<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Unique Loo! | </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">
        <!-- editor -->
        <link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/editor/index.css" rel="stylesheet">
        <!-- select2 -->
        <link href="<?php echo base_url(); ?>assets/css/select/select2.min.css" rel="stylesheet">
        <!-- switchery -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/switchery/switchery.min.css" />

        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>



        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    </head>


    <body class="nav-md">

        <div class="container body">


            <div class="main_container">

                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">

                        <div class="navbar nav_title" style="border: 0;">


                            <a href="<?php echo base_url(); ?>"> <img  style="margin-left: 60px; margin-right: 0px; margin-bottom: 0px; margin-top: 0px;" src="<?php echo base_url(); ?>images/logo.png" alt="logo"  /></a>

                        </div>

                        <div class="clearfix"></div>



                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">
                                    <li><a href="<?php echo base_url(); ?>" ><i class="fa fa-home"></i> HOME <span class="fa fa-chevron-right"></span></a> </li>




                                    <li><a><i class="fa fa-edit"></i> ADMINISTRATION <span class="fa fa-chevron-down"></span></a>

                                        <ul class="nav child_menu" style="display: none">

                                            <li>


                                                <?php
                                                $base_url = base_url();
                                                foreach ($admin_functions as $value) {
                                                    $controller_name = $value['controller_name'];
                                                    $function_name = $value['functions_name'];
                                                    $i_tag = $value['i_tag'];
                                                    $description = $value['description'];
                                                    $span_tag = $value['span_tag'];
                                                    ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>



                                        </ul> 

                                    </li>

                                    <li><a><i class="fa fa-edit"></i> DAILY OPERATIONS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($daily_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>






                                        </ul>
                                    </li>



                                    <li><a><i class="fa fa-edit"></i> REPORTS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($reports_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>







                                        </ul>
                                    </li>



                                </ul>
                            </div>

                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" href="<?php echo base_url(); ?>home/do_logout" title="Logout">
                                <span class="glyphicon glyphicon-off"  aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">

                    <div class="nav_menu">
                        <nav class="" user="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="<?php echo base_url(); ?>assets/images/img.jpg" alt=""><?php
                                        $username = $this->session->userdata('user_name');
                                        echo $username;
                                        ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <li><a href="<?php echo base_url(); ?>admin/profile">  Profile</a>
                                        </li>


                                        <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                        </li>
                                    </ul>
                                </li>


                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">

                        <div class="page-title">
                            <div class="title_left">
                                <h3>Unique Loo</h3>
                            </div>
                        </div>
                        <div class="clearfix"></div>



                        <div class="row">


                            <!-- Book Job Start -->

                            <div id="book_job_div" class="book_job_div" >


                                <div class="col-md-6 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Unique Loo <small>Book Job </small></h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <br />
                                            <form  class="form-horizontal form-label-left input_mask book_client_form" id="book_client_form">
                                                <input type="hidden" class=" edit_job_card" id="edit_job_card" name="edit_job_card"/>
                                                <input type="hidden" class="edit_job_card_no" id="edit_job_card_no" name="edit_job_card_no"/>


                                                <div class="form-group">
                                                    <button type="button" id="add_new_client" class="add_new_client links">New Client ?:Add</button>    

                                                </div>







                                                <div id="other_job_card_info" class="other_job_card_info" style="display: none;">



                                                    <div class=" form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Client Name : </label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">


                                                            <input type="hidden" name="client_id" id="client_id" class="client_id form-control"/>
                                                            <input type="text" placeholder="Client Name : " name="client_name" id="client_name" class="client_name form-control"/>

                                                        </div>
                                                    </div>
                                                    <div class="ln_solid"></div>
                                                    <div class="form-group">
                                                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">

                                                            <button type="button" class="btn btn-primary btn-xs clnt_look_up" id="clnt_look_up">Client Lookup </button>
                                                        </div>
                                                    </div>


                                                    <div id="job_card_info" class="job_card_info well" style="display: none;">




                                                        <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Delivery Date : </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                <input type="text" class="form-control delivery_date" name="delivery_date" id="delivery_date" required="required"  placeholder="Delivery Date : ">
                                                            </div>
                                                        </div>








                                                        <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Event Time From : </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                <div id="datetimepicker_time_from" class="input-append  ">
                                                                    <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class=" time_from " name="time_from" id="time_from"   placeholder="Time From : "/>
                                                                    <span class="add-on">
                                                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                                                        </i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Event Time To : </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                <div id="datetimepicker_time_to" class="input-append ">
                                                                    <input data-format="yyyy-MM-dd hh:mm:ss"  type="text" class=" time_to " name="time_to" id="time_to"   placeholder="Time To : "/>
                                                                    <span class="add-on">
                                                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                                                        </i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>





                                                        <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Location : </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                <input type="text" class="form-control location" name="location" id="location" required="required"  placeholder=" Location : ">
                                                            </div>
                                                        </div>



                                                        <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Venue : </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                <input type="text" class="form-control venue" name="venue" id="venue" required="required"  placeholder=" Venue : ">
                                                            </div>
                                                        </div>




                                                        <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Estimated Distance to Location : </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                <input type="text" class="form-control est_location_dstnce" name="est_location_dstnce" id="est_location_dstnce" required="required"  placeholder="Estimate Distance to  Location : ">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirmed Distance to Location : </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                <input type="text" class="form-control cnfrmd_location_dstnce" name="cnfrmd_location_dstnce" id="cnfrmd_location_dstnce" required="required"  placeholder="Confirmed Distance to  Location : ">
                                                            </div>
                                                        </div>



                                                        <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Booked By :  : </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                <?php
                                                                $username = $this->session->userdata('user_name');
                                                                $user_id = $this->session->userdata('user_id');
                                                                ?>

                                                                <select class="form-control booked_by" name="booked_by"   id="booked_by" required="required"  placeholder="Booked by(Please Key In the  Employee No : ) : ">
                                                                    <option value="">Please select Employee No : </option>
                                                                    <?php foreach ($employees_list as $value) {
                                                                        ?><option value="<?php echo $value['id']; ?>"><?php echo $value['employee_no'] . ' : ' . $value['f_name'] . ' ' . $value['s_name'] ?></option> <?php }
                                                                    ?>
                                                                </select>

                                                                <!--<input type="hidden" class="booked_by_id form-control" id="booked_by_id" name="booked_by_id" value="<?php echo $user_id; ?>" readonly="" required=""/>-->

                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" >Notes : </label>
                                                            <textarea name="notes" id="notes" class="form-control notes" placeholder="Notes/Comments (Optional...)"></textarea>
                                                        </div>


                                                        <div style="display: none;" id="book_job_btn_div" class="book_job_btn_div">


                                                            <div class="ln_solid"></div>
                                                            <div class="form-group">
                                                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                                    <button type="reset" class="btn btn-primary cancel_job_btn" id="cancel_job_btn">Cancel</button>
                                                                    <button type="submit" class="btn btn-success book_job_btn" id="book_job_btn">Book Job  </button>
                                                                </div>
                                                            </div>

                                                        </div>



                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>



                            </div>


                            <!-- Book Job Card End -->
















                            <!-- Add New Client Form start -->


                            <div class="row add_new_client_div" id="add_new_client_div" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Unique Loo  Add Client </h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                                </li>

                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <form autocomplete="off" class="form-horizontal form-label-left add_new_client_form input_mask" id="add_new_client_form" method="post">

                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                    <input type="text" class="form-control has-feedback-left add_name"required="required" id="inputSuccess2" name="add_name" placeholder="Client Name">
                                                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                </div>

                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                    <input type="text" class="form-control has-feedback-left"  id="inputSuccess2" name="add_address" placeholder="Address">
                                                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                </div>

                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                    <input type="url" class="form-control has-feedback-left" id="inputSuccess3" name="add_website" placeholder="Website">
                                                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                </div>

                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                    <input type="text" class="form-control has-feedback-left"  id="inputSuccess2" name="add_industry" placeholder="Industry">
                                                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                </div>

                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                    <input type="text" class="form-control has-feedback-left" id="inputSuccess4" name="add_phone" placeholder="Phone No">
                                                    <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                                                </div>

                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                    <input type="email" class="form-control has-feedback-left" id="inputSuccess5" name="add_email" placeholder="Email">
                                                    <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                                                </div>




                                                <div class="ln_solid"></div>
                                                <div class="form-group">
                                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                        <button type="reset" id="reset_add_client_form" class="btn btn-primary reset_add_client_form">Cancel</button>
                                                        <button type="submit" id="add_client_form_btn" class=" add_client_form_btn btn btn-success">Submit</button>

                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                    <a id="go_back_job_card" class="btn btn-info btn-xs go_back_job_card"><i class="fa fa-backward"></i> View Job Card  <span class="fa fa-chevron-left"></span></a>

                                                </div>

                                            </form>
                                        </div>





                                    </div>
                                </div>
                            </div>

                            <!-- Add New Client Form End --> 


                            <!--Ajax Loader Div Start -->
                            <div id="ajax_loader_div" class="ajax_loader_div" style="display: none;">

                                <img src="<?php echo base_url(); ?>assets/images/ajax_loader/ajax-loader.gif" alt="Ajax Loader" height="42" width="42"> 
                                <hr>
                                <div class="alert alert-info">Please Wait While Information is Loaded</div>
                            </div>

                            <!-- Ajax Loader Div End -->







                            <!-- Add New Client Form start -->


                            <div class="row cancel_job_card_div" id="cancel_job_card_div" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Unique Loo  Cancel Client Job Card </h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                                </li>

                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <form id="cancel_job_card_form" class="cancel_job_card_form" >

                                                <p>
                                                    Are you sure you want to cancel Job Card No : 
                                                </p>
                                                <input type="text" class="form-control cancel_job_card_no" id="cancel_job_card_no" name="cancel_job_card_no" readonly=""/> <p>For Client : </p>
                                                <input type="text" class="form-control cancel_job_client_name" id="cancel_job_client_name" name="cancel_job_client_name"/>

                                            </form>    
                                        </div>





                                    </div>
                                </div>
                            </div>

                            <!-- Add New Client Form End --> 








                        </div>















                    </div>
                    <!-- /page content -->

                    <!-- footer content -->
                    <footer>
                        <div class="">
                            <p class="pull-right">Unique Loo ! by <a>Harris Dindi</a>. |
                                <span class="lead"> <i class="fa fa-paw"></i> Unique Loo!</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </footer>
                    <!-- /footer content -->

                </div>

            </div>
        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>



        <!-- Sweet Alert Library -->
        <script src="<?php echo base_url(); ?>assets/dist/sweetalert-dev.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
        <!--.......................-->



        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- chart js -->
        <script src="<?php echo base_url(); ?>assets/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url(); ?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>
        <!-- tags -->
        <script src="<?php echo base_url(); ?>assets/js/tags/jquery.tagsinput.min.js"></script>
        <!-- switchery -->
        <script src="<?php echo base_url(); ?>assets/js/switchery/switchery.min.js"></script>
        <!-- daterangepicker -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js"></script>
        <!-- richtext editor -->
        <script src="<?php echo base_url(); ?>assets/js/editor/bootstrap-wysiwyg.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/editor/external/jquery.hotkeys.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/editor/external/google-code-prettify/prettify.js"></script>
        <!-- select2 -->
        <script src="<?php echo base_url(); ?>assets/js/select/select2.full.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/parsley/parsley.min.js"></script>
        <!-- textarea resize -->
        <script src="<?php echo base_url(); ?>assets/js/textarea/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));
        </script>
        <!-- Autocomplete -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/autocomplete/countries.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/autocomplete/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>


        <script src="<?php echo base_url(); ?>assets/js/datatables/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>


        <script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.min.js"></script>

        <script type="text/javascript">
            $(function () {
                $('#datetimepicker_time_from').datetimepicker({
                    language: 'pt-BR'
                });

                $('#datetimepicker_time_to').datetimepicker({
                    language: 'pt-BR'
                });
            });
        </script>


        <!-- editor -->
        <script>
            $(document).ready(function () {





                $('.booked_by').change(function () {
                    // var booked_by = $(".booked_by").val();
                    var booked_by = this.value;

                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/check_user_existence/" + booked_by,
                        dataType: "json",
                        success: function (response) {
                            var user_check = response[0].user_check;

                            if (user_check === "User Exists in the System") {
                                $('.book_job_btn_div').show();

                            } else if (user_check === "User Does not exist in the System") {
                                $('.book_job_btn_div').hide();

                            }

                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }
                    });

                });





                setInterval(function () {
                   // get_client_list();
                }, 3000);

                function get_client_list() {
                    client_list = '';
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_client_lists",
                        dataType: "JSON",
                        success: function (response) {
                            $('#select_client_name').empty();
                            client_list = '<option>Please select Client : </option>';
                            for (i = 0; i < response.length; i++) {
                                client_list += '<option value="' + response[i].id + '">' + response[i].name + '</option>';
                                $('#select_client_name').html('');
                                $('#select_client_name').append(client_list);
                            }


                        },
                        error: function (response) {

                        }
                    });

                }






                $(".refresh_asset_table_data").click(function () {

                    asset_status_list = '';
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/asset_status",
                        dataType: "JSON",
                        success: function (response) {
                            $('#asset_status_tr').empty();
                            for (i = 0; i < response.length; i++) {
                                asset_status_list = '<tr><td >' + response[i].name + '</td><td >' + response[i].number + '</td><td>' + response[i].type + '</td><td >' + response[i].booked + '</td></tr>';
                                $('#asset_status_tr').append(asset_status_list);
                            }


                        },
                        error: function (response) {

                        }
                    });


                });


                $(".clnt_look_up").click(function () {

                    var client_name = $(".client_name").val();
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/check_client_existence/" + client_name,
                        dataType: "JSON",
                        success: function (response) {
                            $(".ajax_loader_div").show('slow');

                            var response = response[0].response;

                            if (response === "Client Already Exist in the System!") {

                                $(".job_card_info").show('slow');
                                $(".ajax_loader_div").hide('slow');
                            } else if (response === "Client Does Not Exist in the System!") {
                                $(".add_name").val(client_name);
                                $(".add_new_client_div").show('slow');
                                $(".ajax_loader_div").hide('slow');
                                $(".book_job_div").hide('slow');
                            }
                        },
                        error: function (response) {

                        }
                    });


                });













                $('#go_back_job_card').click(function () {
                    $(".add_new_client_div").hide('slow');
                    $(".ajax_loader_div").show('slow');
                    $(".book_job_div").show('slow');
                    $(".ajax_loader_div").hide('slow');
                });



                $('.cancel_job_btn').click(function () {

                });



                $('.cancel_job_btn_1').submit(function () {
                    var job_card_no = $('.job_card_id').val();

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>operations/delete_job_card/" + job_card_no,
                        dataType: 'JSON',
                        success: function (data) {
                            sweetAlert("...", "Job Deleted Successfully!", "error");
                        }
                    });
                });



                $('.invoice_client_button').click(function () {
                    var job_card_no = $('.job_card_id').val();
                    $(".invoice_client_button").hide("slow");
                    $(".ajax_loader_div").show('slow');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>operations/invoice_client/" + job_card_no,
                        dataType: "JSON",
                        success: function (data) {
                            // data = JSON.parse(data);
                            var invoice_no = data[0].invoice_no;
                            $('.generated_inv_no').val(data[0].invoice_no);

                            swal({
                                title: "Success!",
                                text: "Invoice No :  " + invoice_no + "  generated successfully, you can now proceed to payments!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Okay!",
                                closeOnConfirm: true
                            },
                            function () {
                                $(".asset_calculcator_div").hide("slow");
                                $(".book_job_div").show("slow");
                                $(".select_client_name_div").show("slow");
                                $(".delivery_date").val(" ");
                                $(".client_name").val(" ");
                                $(".venue").val(" ");
                                $(".event_date").val(" ");
                                $(".time_from").val(" ");
                                $(".time_to").val(" ");
                                $(".location").val(" ");
                                $(".est_location_dstnce").val(" ");
                                $(".cnfrmd_location_dstnce").val(" ");
                                $(".ajax_loader_div").hide('slow');

                            });


                        }
                    });
                });



                $('#back_add_client_btn').click(function () {
                    $(".asset_calculcator_div").hide('slow');
                    $(".ajax_loader_div").show('slow');

                    var job_card_no = $('.job_card_id').val();
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_booked_job_details/" + job_card_no,
                        dataType: "JSON",
                        success: function (response) {
                            $(".other_job_card_id").val(response[0].id);
                            $(".job_card_id").val(response[0].id);
                            $(".client_name").val(response[0].clnt_id);
                            $(".delivery_date").val(response[0].dte_srcd);
                            $(".event_date").val(response[0].dte_evnt);
                            $(".location").val(response[0].lctn);
                            $(".est_location_dstnce").val(response[0].estmtd_lctn_dstnce);
                            $(".cnfrmd_location_dstnce").val(response[0].cnfrmd_lctn_dstnce);
                            $(".time_from").val(response[0].time_from);
                            $(".time_to").val(response[0].time_to);
                            $(".edit_job_card").val(response[0].id);
                            $(".edit_job_card_no").val(response[0].job_card_no);
                            $(".book_job_div").show('slow');
                            $(".ajax_loader_div").hide('slow');
                            $(".asset_calculcator_div").hide('slow');


                        }
                    });


                });


                $(".add_new_client").click(function () {

                    $(".ajax_loader_div").show('slow');
                    $(".add_new_client_div").show('slow');
                    $(".ajax_loader_div").hide('slow');
                    $(".book_job_div").hide('slow');

                });


                $(".change_selected_client").click(function () {
                    $('.change_client_div').hide();
                    $('.select_client_name_div').show("slow");
                    $('.selected_client_name_div').hide("slow");

                });





                $(".reset_add_client_form").click(function () {
                    $(".ajax_loader_div").show('slow');
                    $(".add_new_client_div").hide('slow');
                    $(".ajax_loader_div").hide('slow');
                    $(".book_job_div").show('slow');

                });

                $('#add_new_client_form').submit(function (event) {
                    dataString = $("#add_new_client_form").serialize();
                    $(".ajax_loader_div").show('slow');

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/admin/add_new_client",
                        data: dataString,
                        success: function (data) {

                            var added_client_name = $(".add_name").val();
                            data = JSON.parse(data);
                            var response = data[0].response;

                            if (response === "Client Already Exist in the System!") {
                                sweetAlert("Success", " Client " + added_client_name + " Already Exist in the System! :( Please try again with a different Client Name", "error");
                            } else if (response === "Client Added Successfully!") {

                                sweetAlert("Success!", "New Client Added Successfully!", "success");

                                $(".add_new_client_div").hide('slow');
                                $(".ajax_loader_div").hide('slow');
                                $(".book_job_div").show('slow');
                            }








                        }

                    });
                    event.preventDefault();
                    return false;
                });



                $(".book_job_btn").click(function(){
                
                });


                $('#book_client_form').submit(function (event) {
                    dataString = $("#book_client_form").serialize();

                    $(".ajax_loader_div").show('slow');
                 
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>operations/book_client",
                        data: dataString,
                        success: function (data) {
                            $(".delivery_date").val("");
                            $(".client_name").val("");
                            $(".venue").val("");
                            $(".event_date").val("");
                            $(".time_from").val("");
                            $(".time_to").val("");
                            $(".location").val("");
                            $(".est_location_dstnce").val("");
                            $(".cnfrmd_location_dstnce").val("");
                           
                            data = JSON.parse(data);
                            var job_card_id = data[0].id;
                            
                            
                            var response = data[0].response;
                            if (response === "Client picked does not exist in the  system, Please add cllient before booking a job card under the Client.") {
                                sweetAlert("Oops!", "Client picked does not exist in the  system, Please add client before booking a job card under the Client.", "error");
                            }else if(response === "Job Details Exist in the  System."){
                               sweetAlert("Oops!", "The Job card you have entered already exists in the System please check the  data and try again, If it persists please consult with the  Help Desk.", "warning");
                             
                            }else{
                                   $(".book_job_div").hide();
                                 $(".job_card_info").hide();
                                created_job_details_popup(job_card_id); 
                            }

                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");
                            $(".book_job_div").show('slow');

                        }

                    });
                    event.preventDefault();
                    return false;
                });


                function created_job_details_popup(job_card_id) {

                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/created_job_card_no/" + job_card_id,
                        dataType: "JSON",
                        success: function (data) {
                            var created_job_card_id = data[0].job_card_no;
                            var client_name = data[0].client_name;
                            var delivery_date = data[0].delivery_date;
                            var location = data[0].lctn;
                            var venue = data[0].venue;
                            var estmd_lctn_distnce = data[0].estmtd_lctn_dstnce;
                            var cnfrmd_lctn_dstnce = data[0].cnfrmd_lctn_dstnce;
                            var event_date = data[0].event_date;
                            var notes = data[0].notes;
                            var employee_name = data[0].employee_name;
                            var employee_no = data[0].employee_no;
                            $('.job_card_id').val(created_job_card_id);
                            $('.other_job_card_id').val(created_job_card_id);

                            var response = data[0].response;
                            if (response === "Client picked does not exist in the  system, Please add cllient before booking a job card under the Client.") {
                                sweetAlert("Oops!", "Client picked does not exist in the  system, Please add client before booking a job card under the Client.", "error");
                            } else {
                                sweetAlert("Great!...", "Job No : " + created_job_card_id + " Created Successfully! Below are the Job Card details : clIENT", "success");
                                swal({
                                    title: "<small>Job No : " + created_job_card_id + " Created Successfully</small>!",
                                    text: "<span style='color:#000000'>Below are the Job Details : <span><ul><li>Client Name : " + client_name + "</li><li>Delivery Date : " + delivery_date + "</li><li>Event Date : " + event_date + "</li><li>Location : " + location + "</li><li>Venue : " + venue + "</li><li>Estimated Location Distance : " + estmd_lctn_distnce + "</li><li>Confirmed Location Distance : " + cnfrmd_lctn_dstnce + "</li><li>Booked By : " + employee_no + " : " + employee_name + "</li><li>Remarks : " + notes + "</li></ul>",
                                    html: true
                                });

                                $(".book_job_div").show('slow');
                                $(".ajax_loader_div").hide('slow');

                            }




                        },
                        error: function (data) {
                            sweetAlert("Ooopppsss....", "Something went wrong!!!Please try again....", "error");
                        }
                    });
                }


                $('#asset_calculcator_form').submit(function (event) {
                    dataString = $("#asset_calculcator_form").serialize();
                    $(".ajax_loader_div").show('slow');

                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "<?php echo base_url() ?>operations/save_asset_value",
                        data: dataString,
                        success: function (data) {
                            sweetAlert("Great!...", "Asset Saved Successfully!", "success");

                            $(".book_job_div").hide('slow');
                            $(".booked_assets_div").show('slow');
                            $(".ajax_loader_div").hide('slow');


                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");
                            $(".book_job_div").show('slow');

                        }

                    });
                    event.preventDefault();
                    return false;
                });




                $('#other_asset_calculcator_form').submit(function (event) {
                    dataString = $("#other_asset_calculcator_form").serialize();
                    $(".ajax_loader_div").show('slow');

                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "<?php echo base_url() ?>operations/save_asset_value",
                        data: dataString,
                        success: function (data) {
                            sweetAlert("Great!...", "Asset Saved Successfully!", "success");

                            $(".book_job_div").hide('slow');
                            $(".booked_assets_div").show('slow');
                            $(".ajax_loader_div").hide('slow');


                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");
                            $(".book_job_div").show('slow');

                        }

                    });
                    event.preventDefault();
                    return false;
                });







                $('.xcxc').click(function () {
                    $('#descr').val($('#editor').html());
                });
            });

            $(function () {
                function initToolbarBootstrapBindings() {
                    var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                        'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                        'Times New Roman', 'Verdana'],
                            fontTarget = $('[title=Font]').siblings('.dropdown-menu');
                    $.each(fonts, function (idx, fontName) {
                        fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
                    });
                    $('a[title]').tooltip({
                        container: 'body'
                    });
                    $('.dropdown-menu input').click(function () {
                        return false;
                    })
                            .change(function () {
                                $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                            })
                            .keydown('esc', function () {
                                this.value = '';
                                $(this).change();
                            });

                    $('[data-role=magic-overlay]').each(function () {
                        var overlay = $(this),
                                target = $(overlay.data('target'));
                        overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
                    });
                    if ("onwebkitspeechchange" in document.createElement("input")) {
                        var editorOffset = $('#editor').offset();
                        $('#voiceBtn').css('position', 'absolute').offset({
                            top: editorOffset.top,
                            left: editorOffset.left + $('#editor').innerWidth() - 35
                        });
                    } else {
                        $('#voiceBtn').hide();
                    }
                }
                ;

                function showErrorAlert(reason, detail) {
                    var msg = '';
                    if (reason === 'unsupported-file-type') {
                        msg = "Unsupported format " + detail;
                    } else {
                        console.log("error uploading file", reason, detail);
                    }
                    $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
                }
                ;
                initToolbarBootstrapBindings();
                $('#editor').wysiwyg({
                    fileUploadError: showErrorAlert
                });
                window.prettyPrint && prettyPrint();
            });

            $(document).ready(function () {






            });




        </script>



        <!-- /editor -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

        <script>
            var j = jQuery.noConflict();
            j(document).ready(function () {



                j("#select_client_name").change(function () {
                    var client_id = this.value;

                    j.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>admin/get_client_details/" + client_id,
                        dataType: "JSON",
                        success: function (response) {
                            j(".client_e_mail").val(response[0].email);
                            j(".client_phone_no").val(response[0].phone_no);
                            j(".client_location").val(response[0].address);
                            j(".selected_client_name").val(response[0].name);
                            j(".client_id").val(client_id);

                            j('.select_client_name_div').hide("slow");
                            j('.selected_client_name_div').show("slow");
                            j('.change_client_div').show("slow");

                        }
                    });



                });


                j("#select_asset_name").change(function () {
                    j(".asset_quantity").val("");
                    j(".asset_total").val("");
                    j(".discount").val();
                    var client_id = this.value;

                    j.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_asset_details/" + client_id,
                        dataType: "JSON",
                        success: function (response) {
                            j(".asset_id").val(response[0].id);
                            j(".asset_parameter").val(response[0].parameter);
                            j(".asset_value").val(response[0].value);
                        }
                    });
                });


                j('.asset_quantity').keyup(function () {
                    var quantity = j('.asset_quantity').val();
                    var value = j('.asset_value').val();
                    var discount = j('.discount').val();
                    var total_price = quantity * value - discount;

                    j('.asset_total').val(total_price);

                });



                j('.discount').keyup(function () {
                    var quantity = j('.asset_quantity').val();
                    var value = j('.asset_value').val();
                    var discount = j('.discount').val();
                    var total_price = quantity * value - discount;

                    j('.asset_total').val(total_price);

                });





                j('.asset_quantity').click(function () {
                    var quantity = j('.asset_quantity').val();
                    var value = j('.asset_value').val();
                    var total_price = quantity * value;

                    j('.asset_total').val(total_price);

                });







            });

        </script>        


        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script>
            var x = jQuery.noConflict();
            x(function () {
                x("#delivery_date").datepicker(
                        {dateFormat: 'yy-mm-dd'}
                );


                x("#event_date").datepicker(
                        {dateFormat: 'yy-mm-dd'}
                );

            });
        </script>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

        <script>
            var j = jQuery.noConflict();
            j(document).ready(function () {
                j(function () {
                    j(".client_name").autocomplete({
                        source: "<?php echo site_url(); ?>operations/get_active_clients", // path to the get_countries method

                    });
                    j(".other_job_card_info").show();


                });






            });
        </script>

    </body>

</html>