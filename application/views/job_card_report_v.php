<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Unique Loo! | </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">

        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    </head>


    <body class="nav-md">

        <div class="container body">


            <div class="main_container">


                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">

                        <div class="navbar nav_title" style="border: 0;">

                            <a href="<?php echo base_url(); ?>"> <img  style="margin-left: 60px; margin-right: 0px; margin-bottom: 0px; margin-top: 0px;" src="<?php echo base_url(); ?>images/logo.png" alt="logo"  /></a>

                        </div>
                        <div class="clearfix"></div>



                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">
                                    <li><a href="<?php echo base_url(); ?>" ><i class="fa fa-home"></i> Home <span class="fa fa-chevron-right"></span></a> </li>




                                    <li><a><i class="fa fa-edit"></i> ADMINISTRATION <span class="fa fa-chevron-down"></span></a>

                                        <ul class="nav child_menu" style="display: none">

                                            <li>


                                                <?php
                                                $base_url = base_url();
                                                foreach ($admin_functions as $value) {
                                                    $controller_name = $value['controller_name'];
                                                    $function_name = $value['functions_name'];
                                                    $i_tag = $value['i_tag'];
                                                    $description = $value['description'];
                                                    $span_tag = $value['span_tag'];
                                                    ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>



                                        </ul> 

                                    </li>

                                    <li><a><i class="fa fa-edit"></i> DAILY OPERATIONS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($daily_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>






                                        </ul>
                                    </li>



                                    <li><a><i class="fa fa-edit"></i> REPORTS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($reports_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>







                                        </ul>
                                    </li>



                                </ul>
                            </div>

                        </div>
                        <!-- /sidebar menu -->


                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" href="<?php echo base_url(); ?>home/do_logout" title="Logout">
                                <span class="glyphicon glyphicon-off"  aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->




                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">

                    <div class="nav_menu">
                        <nav class="" user="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="<?php echo base_url(); ?>assets/images/img.jpg" alt=""><?php
                                        $username = $this->session->userdata('user_name');
                                        echo $username;
                                        ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <li><a href="<?php echo base_url(); ?>admin/profile">  Profile</a>
                                        </li>


                                        <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                        </li>
                                    </ul>
                                </li>


                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->





                <!-- page content -->
                <div class="right_col" role="main">



                    <div id="job_card_report_div" class="job_card_report_div">

                        <div class="">
                            <div class="page-title">
                                <div class="title_left">
                                    <h3>
                                        Unique Loo
                                        <small>
                                            Daily Operations Report 
                                        </small>
                                    </h3>
                                </div>

                            </div>
                            <div class="clearfix"></div>

                            <div class="row">

                                <div id="job_card_report_div_1" class="job_card_report_div_1">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>Daily Operations <small>Revenues and Expense per Job Card per client YTD</small></h2>
                                                <ul class="nav navbar-right panel_toolbox">
                                                    <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li><a href="#"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content daily_report_div" id="daily_report_div">
                                                <button type="button" id="filter_report_btn" class="filter_report_btn btn btn-default btn-xs ">Filter Report</button>


                                                <table id="job_card_report_table" class=" job_card_report_table  table-bordered table table-striped responsive-utilities jambo_table">
                                                    <thead>
                                                        <tr >
                                                            <th>Client Name</th>
                                                            <th>Job Card No</th>
                                                            <th>Delivery Date</th>
                                                            <th>Event Date</th>
                                                            <th>Location</th>
                                                            <th>Venue</th>
                                                            <th>Estmtd</th>
                                                            <th>Cnfrmd</th>
                                                            <th>Booked By</th>
                                                            <th>Remarks</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>


                                                    </tfoot>

                                                    <tbody>
                                                        <?php foreach ($job_details as $value) {
                                                            ?>
                                                            <tr class="daily_report_tr" id="daily_report_tr" >
                                                                <td><?php echo $value['client_name']; ?></td>
                                                                <td><?php echo $value['job_card_no']; ?></td>
                                                                <td><?php echo $value['delivery_date']; ?></td>
                                                                <td><?php echo $value['event_date']; ?></td>
                                                                <td><?php echo $value['lctn']; ?></td> 
                                                                <td><?php echo $value['venue']; ?></td>
                                                                <td><?php echo $value['estmtd_lctn_dstnce']; ?></td> 
                                                                <td><?php echo $value['cnfrmd_lctn_dstnce']; ?></td>
                                                                <td><?php echo $value['employee_name']; ?></td>
                                                                <td><?php echo $value['notes']; ?></td>
                                                                <td class="center">
                                                                    <input type="hidden" name="hidden_job_card_id" class="hidden_job_card_id" id="hidden_job_card_id" value="<?php echo $value['job_card_id']; ?>"/>
                                                                    <input type="hidden" name="hidden_job_card_no" class="hidden_job_card_no" id="hidden_job_card_no" value="<?php echo $value['job_card_no']; ?>"/>


                                                                    <button id="edit_job_card_link" class="edit_job_card_link btn btn-xs">
                                                                        <i class="glyphicon glyphicon-edit icon-white"></i>Edit Job
                                                                    </button> 
                                                                    <button id="edit_quotation_link" class="edit_quotation_link btn btn-xs">
                                                                        <i class="glyphicon glyphicon-edit icon-white"></i>Edit Quote
                                                                    </button>

                                                                    <button id="delete_job_card_link" class="delete_job_card_link btn btn-xs">
                                                                        <i class="glyphicon glyphicon-trash icon-white"></i>Delete Job Card
                                                                    </button>



                                                                </td>  

                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>



                                            <div class="x_content filter_report_div" id="filter_report_div" style="display: none;">
                                                <button type="button" id="go_back_btn" class="go_back_btn btn btn-default btn-xs ">Go Back</button>
                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                    <input type="text" placeholder="First Name" id="inputSuccess2" class="form-control has-feedback-left">
                                                    <span aria-hidden="true" class="fa fa-user form-control-feedback left"></span>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                    <input type="text" placeholder="First Name" id="inputSuccess2" class="form-control has-feedback-left">
                                                    <span aria-hidden="true" class="fa fa-user form-control-feedback left"></span>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                    <input type="text" placeholder="First Name" id="inputSuccess2" class="form-control has-feedback-left">
                                                    <span aria-hidden="true" class="fa fa-user form-control-feedback left"></span>
                                                </div>
                                            </div>

                                        </div>
                                    </div> 
                                </div>



                                <br />
                                <br />
                                <br />






                            </div>
                        </div>


                    </div>





                    <!-- Edit Book Job Start -->

                    <div id="book_job_div" class="book_job_div" style="display: none;" >


                        <div class="col-md-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Unique Loo <small>Book Job </small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form  class="form-horizontal form-label-left input_mask book_client_form" id="book_client_form">
                                        <input type="hidden" class="job_card_id" id="job_card_id" name="edit_job_card"/>
                                        <input type="hidden" class="job_card_no" id="job_card_no" name="edit_job_card_no"/>









                                        <div id="other_job_card_info" class="other_job_card_info" style="display: inline;">



                                            <div class=" form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Client Name : </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">


                                                    <input type="hidden" name="client_id" id="client_id" class="client_id form-control"/>
                                                    <input type="text" placeholder="Client Name : " name="client_name" id="client_name" class="client_name form-control"/>

                                                </div>
                                            </div>
                                            <div class="ln_solid"></div>
                                            <div class="form-group">
                                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">

                                                    <button type="button" class="btn btn-primary btn-xs edt_clnt" id="edt_clnt">Edit Client </button>
                                                </div>
                                            </div>


                                            <div id="job_card_info" class="job_card_info" style="display: inline;">




                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Delivery Date : </label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <input type="text" class="form-control delivery_date" name="delivery_date" id="delivery_date" required="required"  placeholder="Delivery Date : ">
                                                    </div>
                                                </div>








                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Event Time From : </label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <div id="datetimepicker_time_from" class="input-append  ">
                                                            <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class=" time_from " name="time_from" id="time_from"   placeholder="Time From : "/>
                                                            <span class="add-on">
                                                                <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                                                </i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Event Time To : </label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <div id="datetimepicker_time_to" class="input-append ">
                                                            <input data-format="yyyy-MM-dd hh:mm:ss"  type="text" class=" time_to " name="time_to" id="time_to"   placeholder="Time To : "/>
                                                            <span class="add-on">
                                                                <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                                                </i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>





                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Location : </label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <input type="text" class="form-control location" name="location" id="location" required="required"  placeholder=" Location : ">
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Venue : </label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <input type="text" class="form-control venue" name="venue" id="venue" required="required"  placeholder=" Venue : ">
                                                    </div>
                                                </div>




                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Estimated Distance to Location : </label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <input type="text" class="form-control est_location_dstnce" name="est_location_dstnce" id="est_location_dstnce" required="required"  placeholder="Estimate Distance to  Location : ">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirmed Distance to Location : </label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <input type="text" class="form-control cnfrmd_location_dstnce" name="cnfrmd_location_dstnce" id="cnfrmd_location_dstnce" required="required"  placeholder="Confirmed Distance to  Location : ">
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Booked By :  : </label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <?php
                                                        $username = $this->session->userdata('user_name');
                                                        $user_id = $this->session->userdata('user_id');
                                                        ?>

                                                        <select class="form-control booked_by" name="booked_by"   id="booked_by" required="required"  placeholder="Booked by(Please Key In the  Employee No : ) : ">
                                                            <option value="">Please select Employee No : </option>
                                                            <?php foreach ($employees_list as $value) {
                                                                ?><option value="<?php echo $value['employee_no']; ?>"><?php echo $value['employee_no'] . ' : ' . $value['f_name'] . ' ' . $value['s_name'] ?></option> <?php }
                                                            ?>
                                                        </select>

                                                        <input type="hidden" class="booked_by_id form-control" id="booked_by_id" name="booked_by_id" value="<?php echo $user_id; ?>" readonly="" required=""/>

                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >Notes : </label>
                                                    <textarea name="notes" id="notes" class="form-control notes" placeholder="Notes/Comments (Optional...)"></textarea>
                                                </div>


                                                <div style="display: inline;" id="book_job_btn_div" class="book_job_btn_div">


                                                    <div class="ln_solid"></div>
                                                    <div class="form-group">
                                                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                            <button type="reset" class="btn btn-primary cancel_job_btn" id="cancel_job_btn">Cancel</button>
                                                            <button type="submit" class="btn btn-success book_job_btn" id="book_job_btn">Next </button>
                                                        </div>
                                                    </div>

                                                </div>



                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>



                    </div>


                    <!-- Edit Book Job Card End -->



                    <!-- Edit Quotation Start -->


                    <div id="edit_job_quote_div" class="edit_job_quote_div" style="display: none;" >


                        <div class="col-md-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Unique Loo <small>Book Job </small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form  class="form-horizontal form-label-left input_mask update_job_quotation_form" id="update_job_quotation_form">
                                        <input type="hidden" class="job_card_id" id="job_card_id" name="edit_job_card"/>
                                        <input type="hidden" class="job_card_no" id="job_card_no" name="edit_job_card_no"/>


                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <td>Description</td>
                                                    <td>Quantity</td>
                                                    <td>Days</td>
                                                    <td>Price</td>
                                                    <td>Discount</td>
                                                    <td>Sub Total</td>
                                                    <td>Status</td>
                                                </tr>
                                            </thead>
                                            <tbody id="edit_job_quote" class="edit_job_quote">

                                            </tbody>
                                        </table>
                                        <div class="ln_solid"></div>

                                        <div class="div_update_job_quote" id="div_update_job_quote" >

                                            <div class="form-group">
                                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                    <button type="button" class="btn btn-primary btn_cancel_payment" id="btn_cancel_payment">Cancel</button>
                                                    <button type="submit" class="btn btn-default"><i class="fa fa-download"></i>Update Quotation</button>
                                                </div>
                                            </div>  
                                        </div>

                                    </form>
                                </div>
                            </div>

                        </div>



                    </div>


                    <!---Edit Quotation End -->



                    <!-- footer content -->
                    <footer>
                        <div class="">
                            <p class="pull-right">Unique Loo ! by <a>Harris Dindi</a>. |
                                <span class="lead"> <i class="fa fa-paw"></i> Unique Loo!</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </footer>
                    <!-- /footer content -->


                </div>
                <!-- /page content -->








            </div>

        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>

        <!-- Sweet Alert Library -->
        <script src="<?php echo base_url(); ?>assets/dist/sweetalert-dev.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
        <!--.......................-->


        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- chart js -->
        <script src="<?php echo base_url(); ?>assets/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url(); ?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>


        <!-- Datatables -->
        <script src="<?php echo base_url(); ?>assets/js/datatables/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>

        <!-- Autocomplete -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/autocomplete/countries.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/autocomplete/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>


        <script>
            $(document).ready(function () {

                $(".btn_cancel_payment").click(function () {
                    $(".job_card_report_div").show();
                    $(".edit_job_quote_div").hide();
                });

                $(".edt_clnt").click(function () {
                    var client_name = $().val(".client_name");


                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_client_information/" + client_name,
                        dataType: "JSON",
                        success: function (data) {
                            $(".payment_statement_list").hide("slow");
                            var created_job_card_id = data[0].job_card_id;
                            var job_card_no = data[0].job_card_no;
                            var client_name = data[0].client_name;
                            var delivery_date = data[0].dte_srcd;
                            var location = data[0].lctn;
                            var venue = data[0].venue;
                            var estmd_lctn_distnce = data[0].estmtd_lctn_dstnce;
                            var cnfrmd_lctn_dstnce = data[0].cnfrmd_lctn_dstnce;
                            var event_date = data[0].dte_evnt;
                            var notes = data[0].notes;
                            var employee_name = data[0].employee_name;
                            var employee_no = data[0].employee_no;
                            var employee_id = data[0].employee_id;
                            var time_from = data[0].time_from;
                            var time_to = data[0].time_to;

                            $(".job_card_id").val(created_job_card_id);
                            $(".client_name").val(client_name);
                            $('.delivery_date').val(delivery_date);
                            $(".location").val(location);
                            $(".venue").val(venue);
                            $(".est_location_dstnce").val(estmd_lctn_distnce);
                            $(".cnfrmd_location_dstnce").val(cnfrmd_lctn_dstnce);
                            $(".time_from").val(time_from);
                            $(".time_to").val(time_to);
                            $(".notes").val(notes);
                            $('.booked_by option[value=' + employee_no + ']').prop('selected', true);
                            $('.job_card_no').val(job_card_no);
                            $(".book_job_div").show();
                            $(".job_card_report_div").hide();
                            $(".ajax_loader_div").hide();


                        },
                        error: function (data) {

                        }
                    });

                });







                $(document).on('click', ".delete_job_card_link", function () {
                    $(".ajax_loader_div").show();


                    var job_card_id = $(this).closest('tr').find('input[name="hidden_job_card_id"]').val();
                    var job_card_no = $(this).closest('tr').find('input[name="hidden_job_card_no"]').val();




                    swal({
                        title: "Generate Invoice",
                        text: "Are you sure you want to Delete  Job Card No : " + job_card_no + " ? You will not be able to revert this action. !",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete!",
                        closeOnConfirm: false
                    },
                    function () {
                        $.ajax({
                            type: "GET",
                            url: "<?php echo base_url(); ?>operations/cancel_job_card/" + job_card_id,
                            dataType: "JSON",
                            success: function (response) {
                                var response = response[0].response;
                                if (response === "Failed!") {
                                    sweetAlert("Oops...", "Something went wrong!", "error");
                                } else {
                                    swal("Success!", "Job Card No :" + job_card_no + " has been deleted successfully.", "success");

                                }



                            },
                            error: function (response) {
                                sweetAlert("Oops...", "Something went wrong!", "error");
                            }
                        });


                    });





                });


                $(document).on('click', ".edit_job_card_link", function () {

                    $(".ajax_loader_div").show();


                    var job_card_id = $(this).closest('tr').find('input[name="hidden_job_card_id"]').val();




                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_job_card_detail/" + job_card_id,
                        dataType: "JSON",
                        success: function (data) {
                            $(".payment_statement_list").hide("slow");
                            var created_job_card_id = data[0].job_card_id;
                            var job_card_no = data[0].job_card_no;
                            var client_name = data[0].client_name;
                            var delivery_date = data[0].dte_srcd;
                            var location = data[0].lctn;
                            var venue = data[0].venue;
                            var estmd_lctn_distnce = data[0].estmtd_lctn_dstnce;
                            var cnfrmd_lctn_dstnce = data[0].cnfrmd_lctn_dstnce;
                            var event_date = data[0].dte_evnt;
                            var notes = data[0].notes;
                            var employee_name = data[0].employee_name;
                            var employee_no = data[0].employee_no;
                            var employee_id = data[0].employee_id;
                            var time_from = data[0].time_from;
                            var time_to = data[0].time_to;

                            $(".job_card_id").val(created_job_card_id);
                            $(".client_name").val(client_name);
                            $('.delivery_date').val(delivery_date);
                            $(".location").val(location);
                            $(".venue").val(venue);
                            $(".est_location_dstnce").val(estmd_lctn_distnce);
                            $(".cnfrmd_location_dstnce").val(cnfrmd_lctn_dstnce);
                            $(".time_from").val(time_from);
                            $(".time_to").val(time_to);
                            $(".notes").val(notes);
                            $('.booked_by option[value=' + employee_no + ']').prop('selected', true);
                            $('.job_card_no').val(job_card_no);
                            $(".book_job_div").show();
                            $(".job_card_report_div").hide();
                            $(".ajax_loader_div").hide();


                        },
                        error: function (data) {

                        }
                    });





                });



                $(document).on('click', ".edit_quotation_link", function () {





                    $(".ajax_loader_div").show();


                    var job_card_id = $(this).closest('tr').find('input[name="hidden_job_card_id"]').val();



                    html1 = '';
                    htmlhead1 = '';



                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_job_quotation_details/" + job_card_id,
                        dataType: "JSON",
                        success: function (data) {
                            var response = data[0].response;
                            if (response === "No Data!") {
                                sweetAlert("Oops...", "No Job Card Quotation Data in the System!", "info");
                            } else {
                                for (i = 0; i < data.length; i++) {
                                    var quantity = data[i].asset_qty;
                                    var days = data[i].day;
                                    var price = data[i].asset_price;
                                    var discount = data[i].discount;
                                    var sub_total = quantity * days * price - discount;

                                    html1 += '<tr>\n\
                        <td class = "form-group">\n\
            \n\
                        <input type = "text" required=""  class = "form-control asset_name col-md-6 col-sm-6 col-xs-12 "   name="asset_name[]" id = "asset_name" value="' + data[i].asset_name + '" placeholder = "Asset Name">\n\
            </td>\n\
            <td class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control quantity' + data[i].asset_tracker_id + ' col-md-6 col-sm-6 col-xs-12 "  name="quantity[]" id = "quantity' + sub_total + '" value="' + data[i].asset_qty + '" placeholder = "Quantity">\n\
            </td>\n\\n\
            <td class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control days' + data[i].asset_tracker_id + ' col-md-6 col-sm-6 col-xs-12 "   name="days[]" id = "days' + sub_total + '" value="' + data[i].day + '" placeholder = "Days">\n\
            </td>\n\\n\
            <td class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control value col-md-6 col-sm-6 col-xs-12 " readonly=""   name="value_price[]" id = "value" value="' + data[i].asset_price + '" placeholder = "Value">\n\
            </td>\n\\n\
            <td class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control discount' + data[i].asset_tracker_id + ' col-md-6 col-sm-6 col-xs-12 "   name="discount[]" id = "discount' + sub_total + '" value="' + data[i].discount + '" placeholder = "Discount">\n\
            </td>\n\
            <td class = "form-group">\n\
            <input type = "text" required="" class = "form-control col-md-6 col-sm-6 col-xs-12  sub_total' + data[i].asset_tracker_id + '" name="sub_total[]" readonly=""  id = "sub_total' + data[i].asset_tracker_id + '" value="' + sub_total + '" placeholder = "Sub Total">\n\
            <input type="hidden" required="" readonly="" class="form-control  job_card_no' + data[i].asset_tracker_id + ' " name="job_card_no[]" id="job_card_no' + data[i].job_card_no + '" value="' + data[i].job_card_no + '">\n\
            <input type="hidden" required="" readonly="" class="form-control  job_card_id' + data[i].asset_tracker_id + ' " name="job_card_id[]" id="job_card_id' + data[i].job_card_id + '" value="' + data[i].job_card_id + '">\n\
                <input type="hidden" required="" readonly="" class="form-control  statement_id' + data[i].asset_tracker_id + ' " name="statement_id[]" id="statement_id' + data[i].asset_tracker_id + '" value="' + data[i].statement_id + '">\n\
            <input type="hidden" required="" readonly="" class="form-control  asset_tracker_id' + data[i].asset_tracker_id + ' " name="asset_tracker_id[]" id="asset_tracker_id' + data[i].asset_tracker_id + '" value="' + data[i].asset_tracker_id + '">\n\
</td>\n\<td class = "form-group">\n\
            <select required="" class="form-control status col-md-6 col-sm-6 col-xs-12" name="status[]" id="status" value="' + data[i].asset_status + '" placehlder="Status" ><option value="' + data[i].asset_status + '">' + data[i].asset_status + '</option><option value="Active">Active</option> <option value="In Active">In Active</option></select>\n\
            </td>\n\
                   </tr>';
                                    $("#edit_job_quote").on("keyup", ".quantity" + data[i].asset_tracker_id, function () {

                                        var quantity = this.value;
                                        var days = parseFloat(quantity);
                                        var days = $("#" + this.id.replace("quantity", "days")).val();
                                        var days = parseFloat(days);
                                        var price = $("#" + this.id.replace("quantity", "price")).val();
                                        var price = parseFloat(price);
                                        var discount = $("#" + this.id.replace("quantity", "discount")).val();
                                        var discount = parseFloat(discount);
                                        var sub_total = quantity * days * price - discount;
                                        var total_subtotal = $("#" + this.id.replace("sub_total", "discount")).val();

                                        $(total_subtotal).val(sub_total);
                                    });
                                }

                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>operations/get_total_job_value/" + job_card_id,
                                    dataType: 'JSON',
                                    success: function (data) {
                                        var job_card_id = data[0].job_card_id;
                                        var statement_id = data[0].id;
                                        var total_job_price = data[0].amount_cr;
                                        var discount = data[0].discount;
                                        var total_job_price = total_job_price - discount;
                                        var tax = total_job_price * 0.16;
                                        var sub_total = total_job_price * 0.84;
                                        var amount_cr = data[0].amount_cr;
                                        var amount_dr = data[0].amount_dr;
                                        var deposit_paid = amount_dr - discount;
                                        var balance_remaining = amount_cr - amount_dr - discount;
                                        var pymnt_code = data[0].pymnt_code;
                                        var pymnt_mthd = data[0].pymnt_mthd;
                                        var payment_date = data[0].payment_date;

                                        $(".submit_payments_job_card_id").val(job_card_id);
                                        $(".total_job_card_cost_id").val(statement_id);
                                        $('.job_card_id_payment').val(job_card_id);
                                        $(".submit_payment_subtotal_tr").empty();
                                        $(".submit_payment_subtotal_tr").append("  <th>Subtotal:</th> <td>" + sub_total + "</td>");
                                        $(".submit_payment_tax_tr").empty();
                                        $(".submit_payment_tax_tr").append(" <th>Tax (16.0%)</th><td>" + tax + "</td>");
                                        $(".submit_payment_total_tr").empty();
                                        $(".submit_payment_total_tr").append("<th>Total:</th> <td>" + total_job_price + "</td>");
                                        if (deposit_paid < 0) {
                                            var deposit_paid = 0;
                                            $(".deposit_paid").val(deposit_paid);
                                        } else {
                                            $(".deposit_paid").val(deposit_paid);
                                        }

                                        $(".balance_remaining").val(balance_remaining);
                                        $(".hidden_total_cost").val(amount_cr);
                                        $(".hidden_discount").val(discount);
                                        $('.payment_code').val(pymnt_code);
                                        $('.payment_date').val(payment_date);
                                        $('.payment_method option[value=' + pymnt_mthd + ']').attr("selected", "selected");

                                    }, error: function (data) {
                                        sweetAlert("Oops...", "Something went wrong!", "error");

                                    }
                                });


                                $('#edit_job_quote').empty();
                                $('#edit_job_quote').append(html1);


                                $(".job_card_report_div").hide();
                                $(".edit_job_quote_div").show();
                                $(".ajax_loader_div").hide();

                            }




                        },
                        error: function (data) {

                        }
                    });





                });




                $('.update_job_quotation_form').submit(function (event) {
                    dataString = $(".update_job_quotation_form").serialize();
                    $(".ajax_loader_div").show();

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/operations/update_job_quotation",
                        data: dataString,
                        success: function (data) {
                            sweetAlert("Great!...", "Quotation updated Successfully!", "success");
                            $(".client_payment_list").show();
                            $(".job_status_list").show();
                            $(".submit_payments_div").hide();
                            $(".ajax_loader_div").hide();

                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }

                    });
                    event.preventDefault();
                    return false;
                });







                $(".cancel_job_btn").click(function () {
                    $(".book_job_div").hide();
                    $(".job_card_report_div").show();
                    $(".ajax_loader_div").hide();
                });

                $(".filter_report_btn").click(function () {
                    $(".daily_report_div").hide();
                    $(".filter_report_div").show();
                });

                $(".go_back_btn").click(function () {
                    $(".daily_report_div").show();
                    $(".filter_report_div").hide();
                });





                $('#book_client_form').submit(function (event) {
                    dataString = $("#book_client_form").serialize();

                    $(".ajax_loader_div").show('slow');
                    $(".book_job_div").hide('slow');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>operations/book_client",
                        data: dataString,
                        success: function (data) {
                            data = JSON.parse(data);
                            var job_card_id = data[0].id;
                            created_job_details_popup(job_card_id);



                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");
                            $(".book_job_div").show('slow');

                        }

                    });
                    event.preventDefault();
                    return false;
                });


                function created_job_details_popup(job_card_id) {

                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/created_job_card_no/" + job_card_id,
                        dataType: "JSON",
                        success: function (data) {
                            var created_job_card_id = data[0].job_card_no;
                            var client_name = data[0].client_name;
                            var delivery_date = data[0].delivery_date;
                            var location = data[0].lctn;
                            var venue = data[0].venue;
                            var estmd_lctn_distnce = data[0].estmtd_lctn_dstnce;
                            var cnfrmd_lctn_dstnce = data[0].cnfrmd_lctn_dstnce;
                            var event_date = data[0].event_date;
                            var notes = data[0].notes;
                            var employee_name = data[0].employee_name;
                            var employee_no = data[0].employee_no;
                            $('.job_card_id').val(created_job_card_id);
                            $('.other_job_card_id').val(created_job_card_id);

                            var response = data[0].response;
                            if (response === "Client picked does not exist in the  system, Please add cllient before booking a job card under the Client.") {
                                sweetAlert("Oops!", "Client picked does not exist in the  system, Please add client before booking a job card under the Client.", "error");
                            } else {
                                sweetAlert("Great!...", "Job No : " + created_job_card_id + " Created Successfully! Below are the Job Card details : clIENT", "success");
                                swal({
                                    title: "<small>Job No : " + created_job_card_id + " Created Successfully</small>!",
                                    text: "<span style='color:#000000'>Below are the Job Details : <span><ul><li>Client Name : " + client_name + "</li><li>Delivery Date : " + delivery_date + "</li><li>Event Date : " + event_date + "</li><li>Location : " + location + "</li><li>Venue : " + venue + "</li><li>Estimated Location Distance : " + estmd_lctn_distnce + "</li><li>Confirmed Location Distance : " + cnfrmd_lctn_dstnce + "</li><li>Booked By : " + employee_no + " : " + employee_name + "</li><li>Remarks : " + notes + "</li></ul>",
                                    html: true
                                });

                                $(".book_job_div").hide('slow');
                                $(".ajax_loader_div").hide('slow');
                                $(".asset_calculcator_div").show('slow');
                            }




                        },
                        error: function (data) {
                            sweetAlert("Ooopppsss....", "Something went wrong!!!Please try again....", "error");
                        }
                    });
                }










                $('input.tableflat').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });
            });

            var asInitVals = new Array();
            $(document).ready(function () {
                var oTable = $('#example').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0]
                        } //disables sorting for column one
                    ],
                    'iDisplayLength': 12,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "<?php echo base_url('assets2/js/datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
                    }
                });
                $("tfoot input").keyup(function () {
                    /* Filter on the column based on the index of this element's parent <th> */
                    oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
                });
                $("tfoot input").each(function (i) {
                    asInitVals[i] = this.value;
                });
                $("tfoot input").focus(function () {
                    if (this.className == "search_init") {
                        this.className = "";
                        this.value = "";
                    }
                });
                $("tfoot input").blur(function (i) {
                    if (this.value == "") {
                        this.className = "search_init";
                        this.value = asInitVals[$("tfoot input").index(this)];
                    }
                });
            });
        </script>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

        <script>
            var j = jQuery.noConflict();
            j(document).ready(function () {
                j(function () {
                    j(".client_name").autocomplete({
                        source: "<?php echo site_url(); ?>operations/get_active_clients", // path to the get_countries method

                    });
                    j(".other_job_card_info").show();


                });






            });
        </script>




        <link href="<?php echo base_url(); ?>assets/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/datatables/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>

        <script>
            var table = jQuery.noConflict();


            var asInitVals = new Array();
            table(document).ready(function () {
                var oTable = table('.job_card_report_table').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0]
                        } //disables sorting for column one
                    ],
                    'iDisplayLength': 12,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "<?php echo base_url('assets/js/datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
                    }
                });
                table("tfoot input").keyup(function () {
                    /* Filter on the column based on the index of this element's parent <th> */
                    oTable.fnFilter(this.value, table("tfoot th").index(table(this).parent()));
                });
                table("tfoot input").each(function (i) {
                    asInitVals[i] = this.value;
                });
                table("tfoot input").focus(function () {
                    if (this.className == "search_init") {
                        this.className = "";
                        this.value = "";
                    }
                });
                table("tfoot input").blur(function (i) {
                    if (this.value == "") {
                        this.className = "search_init";
                        this.value = asInitVals[table("tfoot input").index(this)];
                    }
                });
            });
        </script>








    </body>

</html>