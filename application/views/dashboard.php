<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Unique Loo! | </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/maps/jquery-jvectormap-2.0.1.css" />
        <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/css/floatexamples.css" rel="stylesheet" type="text/css" />

        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/nprogress.js"></script>

        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    </head>


    <body class="nav-md">

        <div class="container body">


            <div class="main_container">








                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">

                        <div class="navbar nav_title" style="border: 0;">

                            <a href="<?php echo base_url(); ?>"> <img  style="margin-left: 60px; margin-right: 0px; margin-bottom: 0px; margin-top: 0px;" src="<?php echo base_url(); ?>images/logo.png" alt="logo"  /></a>

                        </div>
                        <div class="clearfix"></div>



                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">
                                    <li><a href="<?php echo base_url(); ?>" ><i class="fa fa-home"></i> HOME <span class="fa fa-chevron-right"></span></a> </li>




                                    <li><a><i class="fa fa-edit"></i> ADMINISTRATION <span class="fa fa-chevron-down"></span></a>

                                        <ul class="nav child_menu" style="display: none">

                                            <li>


                                                <?php
                                                $base_url = base_url();
                                                foreach ($admin_functions as $value) {
                                                    $controller_name = $value['controller_name'];
                                                    $function_name = $value['functions_name'];
                                                    $i_tag = $value['i_tag'];
                                                    $description = $value['description'];
                                                    $span_tag = $value['span_tag'];
                                                    ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>



                                        </ul> 

                                    </li>

                                    <li><a><i class="fa fa-edit"></i> DAILY OPERATIONS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($daily_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>






                                        </ul>
                                    </li>



                                    <li><a><i class="fa fa-edit"></i> REPORTS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($reports_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>







                                        </ul>
                                    </li>



                                </ul>
                            </div>

                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" href="<?php echo base_url(); ?>home/do_logout" title="Logout">
                                <span class="glyphicon glyphicon-off"  aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">

                    <div class="nav_menu">
                        <nav class="" user="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="<?php echo base_url(); ?>assets/images/img.jpg" alt=""><?php
                                        $username = $this->session->userdata('user_name');
                                        echo $username;
                                        ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <li><a href="<?php echo base_url(); ?>admin/profile">  Profile</a>
                                        </li>


                                        <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                        </li>
                                    </ul>
                                </li>


                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->














                <!-- page content -->
                <div class="right_col" role="main">

                    <!-- top tiles -->
                    <div class="row tile_count">

                        <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                            <div class="left"></div>
                            <div class="right">
                                <span class="count_top"><i class="fa fa-clock-o"></i>Jobs Active MTD</span>
                                <div class="count no_of_jobs_actve_mtd blue"></div>
                            </div>
                        </div>
                        <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                            <div class="left"></div>
                            <div class="right">
                                <span class="count_top"><i class="fa fa-clock-o"></i>Jobs Cleared MTD</span>
                                <div class="count no_of_jobs_clrd_mtd blue"></div>
                            </div>
                        </div>


                        <div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">
                            <div class="left"></div>
                            <div class="right" >
                                <span class="count_top"><i class="fa fa-money"></i> Expected Revenue MTD</span>
                                <div class="count green xpctd_rvne_mtd " ></div>
                            </div>
                        </div>

                        <div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">
                            <div class="left"></div>
                            <div class="right" >
                                <span class="count_top"><i class="fa fa-money"></i> Expected Revenue YTD</span>
                                <div class="count green xpctd_rvne_ytd " ></div>
                            </div>
                        </div>
                        <div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">
                            <div class="left"></div>
                            <div class="right">
                                <span class="count_top"><i class="fa fa-money"></i> Revenue Collected MTD </span>
                                <div class="count actl_rvne_mtd green"></div>
                            </div>
                        </div>
                        <div class="animated flipInY col-md-3 col-sm-4 col-xs-4 tile_stats_count">
                            <div class="left"></div>
                            <div class="right">
                                <span class="count_top"><i class="fa fa-money"></i> Revenue Collected YTD</span>
                                <div class="count green total_revenue "></div>
                            </div>
                        </div>


                    </div>
                    <!-- /top tiles -->






                    <div class="row">

                        <!-- bar chart -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2> Daily Revenue MTD <small>Sessions</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>

                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div id="monthly_bar" style="width:100%; height:280px;"></div>
                                    <button id="print_monthly_pdf">PDF</button>
                                </div>
                            </div>
                        </div>
                        <!-- /bar charts -->

                        <!-- bar charts group -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2> Monthly Revenue YTD <small></small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>

                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content1">
                                    <div id="daily_expense_bar" style="width:100%; height:280px;"></div>
                                    <button id="print_daily_pdf">PDF</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <!-- /bar charts group -->

                        <!-- bar charts group -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Top Ten Clients <small>Revenue Collection</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>

                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content2">
                                    <div id="jobs_ytd" style="width:100%; height:300px;"></div>

                                    <button id="print_pdf">PDF</button>
                                </div>
                            </div>
                        </div>
                        <!-- /bar charts group -->

                        <!-- pie chart -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2> No of Jobs MTD <small>No of Jobs per Day MTD </small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>

                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content2">
                                    <div id="NO_JOBS_MTD" style="width:100%; height:300px;"></div>

                                    <button id="print_pdf">PDF</button>
                                </div>
                            </div>
                        </div>
                        <!-- /Pie chart -->

                        <!-- graph area -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Jobs per Client YTD. <small>No of Jobs per Client YTD</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>

                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content2">
                                    <div id="job_status_pie" id="job_status_pie" style="width:100%; height:300px;"></div>



                                </div>
                            </div>
                        </div>
                        <!-- /graph area -->

                        <!-- line graph -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Jobs per Month YTD <small>No of Jobs per Month YTD</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content2">
                                    <div id="jobs_per_month" class="jobs_per_month" style="width:100%; height:300px;"></div>


                                </div>
                            </div>
                        </div>
                        <!-- /line graph -->

                    </div>















                    <div class="row">


                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel tile fixed_height_320">
                                <div class="x_title">
                                    <h2>Total Revenue per Client YTD</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table class="revenue_per_client_table" id="revenue_per_client_table">
                                        <thead>
                                            <tr>
                                                <th>Client Name</th>
                                                <th>Industry</th>
                                                <th>Total Revenue </th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <td>Client Name</td>
                                                <td>Industry</td>
                                                <td>Total Revenue</td>
                                            </tr>
                                        </tfoot>
                                        <tbody id="" class="">

                                            <?php
                                            foreach ($revenue_per_client as $value) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $value['client_name']; ?></td>
                                                    <td><?php echo $value['industry']; ?></td>
                                                    <td><?php echo $value['revenue_collected']; ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel tile fixed_height_320 overflow_hidden">
                                <div class="x_title">
                                    <h2>Total Jobs per Client YTD</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <table class="jobs_per_client_table " id="jobs_per_client_table">
                                        <thead>
                                            <tr>
                                                <th>Client Name</th>
                                                <th>Industry</th>
                                                <th>Total Jobs </th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <td>Client Name</td>
                                                <td>Industry</td>
                                                <td>Total Jobs</td>
                                            </tr>
                                        </tfoot>
                                        <tbody id="" class="">
                                            <?php
                                            foreach ($total_no_jobs as $value) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $value['client_name']; ?></td>
                                                    <td><?php echo $value['industry']; ?></td>
                                                    <td><?php echo $value['no_of_jobs']; ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div>



                    <!-- footer content -->

                    <footer>
                        <div class="">
                            <p class="pull-right">Unique Loo!  <a>Harris Dindi</a>. |
                                <span class="lead"> <i class="fa fa-paw"></i> Unique Loo!</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </footer>
                    <!-- /footer content -->
                </div>
                <!-- /page content -->

            </div>

        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>

        <!-- Sweet Alert Library -->
        <script src="<?php echo base_url(); ?>assets/dist/sweetalert-dev.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
        <!--.......................-->



        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- chart js -->
        <script src="<?php echo base_url(); ?>assets/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url(); ?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>
        <!-- tags -->
        <script src="<?php echo base_url(); ?>assets/js/tags/jquery.tagsinput.min.js"></script>
        <!-- switchery -->
        <script src="<?php echo base_url(); ?>assets/js/switchery/switchery.min.js"></script>
        <!-- daterangepicker -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js"></script>
        <!-- richtext editor -->
        <script src="<?php echo base_url(); ?>assets/js/editor/bootstrap-wysiwyg.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/editor/external/jquery.hotkeys.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/editor/external/google-code-prettify/prettify.js"></script>
        <!-- select2 -->
        <script src="<?php echo base_url(); ?>assets/js/select/select2.full.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/parsley/parsley.min.js"></script>
        <!-- textarea resize -->
        <script src="<?php echo base_url(); ?>assets/js/textarea/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));
        </script>
        <!-- Autocomplete -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/autocomplete/countries.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/autocomplete/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>


        <script src="<?php echo base_url(); ?>assets/js/datatables/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>




        <!-- datepicker -->







        <script>
            $(document).ready(function () {
                setInterval(function () {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/no_jobs_mtd/",
                        dataType: "JSON",
                        success: function (data) {
                            $('.no_of_jobs_mtd').empty();
                            var no_of_jobs_mtd = data[0].no_of_jobs_mtd;
                            $('.no_of_jobs_mtd').append('<p>' + no_of_jobs_mtd + '<p>');
                        },
                        error: function (data) {

                            // alert('An error occured, kindly try later');
                        }
                    });
                }, 3000);


                setInterval(function () {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/no_of_jobs_actve_mtd/",
                        dataType: "JSON",
                        success: function (data) {
                            $('.no_of_jobs_actve_mtd').empty();
                            var no_of_jobs_actve_mtd = data[0].no_of_jobs_actve_mtd;
                            $('.no_of_jobs_actve_mtd').append('<p>' + no_of_jobs_actve_mtd + '<p>');
                        },
                        error: function (data) {

                            // alert('An error occured, kindly try later');
                        }
                    });
                }, 3000);


                setInterval(function () {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/no_of_jobs_clrd_mtd/",
                        dataType: "JSON",
                        success: function (data) {
                            $('.no_of_jobs_clrd_mtd').empty();
                            var no_of_jobs_clrd_mtd = data[0].no_of_jobs_clrd_mtd;
                            $('.no_of_jobs_clrd_mtd').append('<p>' + no_of_jobs_clrd_mtd + '<p>');
                        },
                        error: function (data) {

                            // alert('An error occured, kindly try later');
                        }
                    });
                }, 3000);








                setInterval(function () {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/xpctd_rvne_ytd/",
                        dataType: "JSON",
                        success: function (data) {
                            $('.xpctd_rvne_ytd').empty();
                            var xpctd_rvne_ytd = data[0].expected_revenue;

                            if (!xpctd_rvne_ytd) {
                                var xpctd_rvne_ytd = "0";
                            } else if (xpctd_rvne_ytd) {

                            }


                            //Seperates the components of the number
                            var n = xpctd_rvne_ytd.toString().split(".");
                            //Comma-fies the first part
                            n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            //Combines the two sections
                            xpctd_rvne_ytd = n.join(".");


                            $('.xpctd_rvne_ytd').append('<p>' + xpctd_rvne_ytd + '<p>');
                        },
                        error: function (data) {

                            // alert('An error occured, kindly try later');
                        }
                    });
                }, 3000);




                setInterval(function () {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/xpctd_rvne_mtd/",
                        dataType: "JSON",
                        success: function (data) {
                            $('.xpctd_rvne_mtd').empty();
                            var xpctd_rvne_mtd = data[0].expected_revenue;


                            if (!xpctd_rvne_mtd) {
                                var xpctd_rvne_mtd = "0";
                            } else if (xpctd_rvne_mtd) {

                            }


                            //Seperates the components of the number
                            var n = xpctd_rvne_mtd.toString().split(".");
                            //Comma-fies the first part
                            n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            //Combines the two sections
                            xpctd_rvne_mtd = n.join(".");


                            $('.xpctd_rvne_mtd').append('<p>' + xpctd_rvne_mtd + '<p>');
                        },
                        error: function (data) {

                            // alert('An error occured, kindly try later');
                        }
                    });
                }, 3000);


                setInterval(function () {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/actl_rvne_mtd/",
                        dataType: "JSON",
                        success: function (data) {
                            $('.actl_rvne_mtd').empty();
                            var actl_rvne_mtd = data[0].actual_revenue;


                            if (!actl_rvne_mtd) {
                                var actl_rvne_mtd = "0";
                            } else if (actl_rvne_mtd) {

                            }



                            //Seperates the components of the number
                            var n = actl_rvne_mtd.toString().split(".");
                            //Comma-fies the first part
                            n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            //Combines the two sections
                            actl_rvne_mtd = n.join(".");

                            $('.actl_rvne_mtd').append('<p>' + actl_rvne_mtd + '<p>');
                        },
                        error: function (data) {

                            // alert('An error occured, kindly try later');
                        }
                    });
                }, 3000);




                setInterval(function () {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/total_revenue/",
                        dataType: "JSON",
                        success: function (data) {
                            $('.total_revenue').empty();
                            var total_revenue = data[0].total_revenue;


                            if (!total_revenue) {
                                var total_revenue = "0";
                            } else if (total_revenue) {

                            }

                            //Seperates the components of the number
                            var n = total_revenue.toString().split(".");
                            //Comma-fies the first part
                            n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                            //Combines the two sections
                            total_revenue = n.join(".");

                            $('.total_revenue').append('<p>' + total_revenue + '<p>');
                        },
                        error: function (data) {

                            // alert('An error occured, kindly try later');
                        }
                    });
                }, 3000);
                setInterval(function () {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/total_expenses/",
                        dataType: "JSON",
                        success: function (data) {
                            $('.total_expenses').empty();
                            var total_expenses = data[0].total_expenses;


                            if (!total_expenses) {
                                var total_expenses = "0";
                            } else if (total_expenses) {

                            }


                            $('.total_expenses').append('<p>' + total_expenses + '<p>');
                        },
                        error: function (data) {

                            // alert('An error occured, kindly try later');
                        }
                    });
                }, 3000);
                setInterval(function () {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/active_users/",
                        dataType: "JSON",
                        success: function (data) {
                            $('.active_users').empty();
                            var active_users = data[0].active_users;
                            $('.active_users').append('<p>' + active_users + '<p>');
                        },
                        error: function (data) {

                            // alert('An error occured, kindly try later');
                        }
                    });
                }, 3000);





                setInterval(function () {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/revenue_per_client/",
                        dataType: "JSON",
                        success: function (data) {





                            total_revenue_client = $('.total_revenue_client').empty();

                            $.each(data, function (i, revenue_client) {

                                var client_name = revenue_client.client_name;
                                var industry = revenue_client.industry;

                                var total_revenue = revenue_client.revenue_collected;

                                //Seperates the components of the number
                                var n = total_revenue.toString().split(".");
                                //Comma-fies the first part
                                n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                                //Combines the two sections
                                total_revenue = n.join(".");


                                $('.total_revenue_client').append('<tr><td>' + client_name + '</td><td>' + industry + '</td><td>' + total_revenue + '</td><tr>');


                            });




                        },
                        error: function (data) {

                            // alert('An error occured, kindly try later');
                        }
                    });
                }, 3000);


                setInterval(function () {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/job_per_client/",
                        dataType: "JSON",
                        success: function (data) {


                            total_expense_client = $('.total_expense_client').empty();

                            $.each(data, function (i, expense_client) {

                                var client_name = expense_client.client_name;
                                var industry = expense_client.industry;

                                var no_of_jobs = data[0].no_of_jobs;
                                $('.total_expense_client').append('<tr><td>' + client_name + '</td><td>' + industry + '</td><td>' + no_of_jobs + '</td><tr>');
                                // $('#total_expense_table_ytd').DataTable({});

                            });







                        },
                        error: function (data) {

                            // alert('An error occured, kindly try later');
                        }
                    });
                }, 3000);




            });
        </script>




        <script src="<?php echo base_url(); ?>assets/js/xepOnline.jqPlugin.js"></script>

        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>


        <script>

            var morris = jQuery.noConflict();
            morris(document).ready(function () {




                morris.getJSON("<?php echo site_url('operations/total_jobs_per_day'); ?>", function (json) {

                    new Morris.Bar({
                        // ID of the element in which to draw the chart.
                        element: 'NO_JOBS_MTD',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: json,
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['no_jobs'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['No of Jobs'],
                        barRatio: 0.4,
                        barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                        xLabelAngle: 35,
                        hideHover: 'auto'
                    });
                });


                morris.getJSON("<?php echo site_url('operations/top_clients_ytd'); ?>", function (json) {

                    new Morris.Bar({
                        // ID of the element in which to draw the chart.
                        element: 'jobs_ytd',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: json,
                        // The name of the data record attribute that contains x-values.
                        xkey: 'client_name',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['amount_dr'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Revenue(Kes)'],
                        barRatio: 0.4,
                        barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                        xLabelAngle: 35,
                        hideHover: 'auto'
                    });
                });

                morris('#print_pdf').click(function () {
                    printMe();
                });
                // This will render SVG only as PDF and download
                function printMe() {
                    xepOnline.Formatter.Format('jobs_ytd', {render: 'download', srctype: 'svg'});
                }


                morris('#print_daily_pdf').click(function () {
                    print_daily_pdf();
                });
                // This will render SVG only as PDF and download
                function print_daily_pdf() {
                    xepOnline.Formatter.Format('daily_expense_bar', {render: 'download', srctype: 'svg'});
                }



                morris('#print_monthly_pdf').click(function () {
                    print_monthly_pdf();
                });
                // This will render SVG only as PDF and download
                function print_monthly_pdf() {
                    xepOnline.Formatter.Format('monthly_bar', {render: 'download', srctype: 'svg'});
                }






                morris.getJSON("<?php echo site_url('operations/daily_expense_bar'); ?>", function (json) {
                    var acctregs = new Morris.Bar({
                        // ID of the element in which to draw the chart.
                        element: 'daily_expense_bar',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: json,
                        // The name of the data record attribute that contains x-values.
                        xkey: 'date',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['revenue'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Revenue(Kes)'],
                        barRatio: 0.4,
                        barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                        xLabelAngle: 35,
                        hideHover: 'auto'
                    });
                });


                morris.getJSON("<?php echo site_url('operations/chart_js'); ?>", function (json) {
                    var acctregs = new Morris.Bar({
                        // ID of the element in which to draw the chart.
                        element: 'monthly_bar',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: json,
                        // The name of the data record attribute that contains x-values.
                        xkey: 'date',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['revenue'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Revenue(Kes)'],
                        barRatio: 0.4,
                        barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                        xLabelAngle: 35,
                        hideHover: 'auto'
                    });
                });


                morris.getJSON("<?php echo site_url('operations/job_status_ytd'); ?>", function (json) {


                    var acctregss = new Morris.Donut({
                        // ID of the element in which to draw the chart.
                        element: 'job_status_pie',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: json,
                        // The name of the data record attribute that contains x-values.

                        colors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                        formatter: function (y) {
                            return y + " Jobs"
                        }
                    });







                });


                morris.getJSON("<?php echo site_url('operations/jobs_per_month'); ?>", function (json) {


                    var acctregss = new Morris.Donut({
                        // ID of the element in which to draw the chart.
                        element: 'jobs_per_month',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: json,
                        // The name of the data record attribute that contains x-values.
                        colors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                        formatter: function (y) {
                            return y + " Jobs"
                        }
                    });







                });


            });

        </script>

        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
        <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
        <script>
            var table = jQuery.noConflict();
            table(document).ready(function () {
                table('.revenue_per_client_table').DataTable({
                    "scrollY": 200, "scrollX": true, "paging": false
                });

                table('.jobs_per_client_table').DataTable({
                    "scrollY": 200, "scrollX": true, "paging": false
                });

            });



        </script>







        <!-- /datepicker -->
        <!-- /footer content -->
    </body>

</html>
