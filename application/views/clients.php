<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Unique Loo |Clients </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">

        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    </head>


    <body class="nav-md">

        <div class="container body">


            <div class="main_container">





                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">

                        <div class="navbar nav_title" style="border: 0;">

                            <a href="<?php echo base_url(); ?>"> <img  style="margin-left: 60px; margin-right: 0px; margin-bottom: 0px; margin-top: 0px;" src="<?php echo base_url(); ?>images/logo.png" alt="logo"  /></a>

                        </div>
                        <div class="clearfix"></div>



                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">
                                    <li><a href="<?php echo base_url(); ?>" ><i class="fa fa-home"></i> HOME <span class="fa fa-chevron-right"></span></a> </li>




                                    <li><a><i class="fa fa-edit"></i> ADMINISTRATION <span class="fa fa-chevron-down"></span></a>

                                        <ul class="nav child_menu" style="display: none">

                                            <li>


                                                <?php
                                                $base_url = base_url();
                                                foreach ($admin_functions as $value) {
                                                    $controller_name = $value['controller_name'];
                                                    $function_name = $value['functions_name'];
                                                    $i_tag = $value['i_tag'];
                                                    $description = $value['description'];
                                                    $span_tag = $value['span_tag'];
                                                    ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>



                                        </ul> 

                                    </li>

                                    <li><a><i class="fa fa-edit"></i> DAILY OPERATIONS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($daily_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>






                                        </ul>
                                    </li>



                                    <li><a><i class="fa fa-edit"></i> REPORTS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($reports_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>







                                        </ul>
                                    </li>



                                </ul>
                            </div>

                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" href="<?php echo base_url(); ?>home/do_logout" title="Logout">
                                <span class="glyphicon glyphicon-off"  aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->


                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">

                    <div class="nav_menu">
                        <nav class="" user="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="<?php echo base_url(); ?>assets/images/img.jpg" alt=""><?php
                                        $username = $this->session->userdata('user_name');
                                        echo $username;
                                        ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <li><a href="<?php echo base_url(); ?>admin/profile">  Profile</a>
                                        </li>


                                        <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                        </li>
                                    </ul>
                                </li>


                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->






                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="page-title">
                            <div class="title_left">
                                <h3>
                                    Clients

                                </h3>
                            </div>


                        </div>
                        <div class="clearfix"></div>


                        <!-- Add New Client Form start -->


                        <div class="row add_new_client_div" id="add_new_client_div" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Unique Loo  Add Client </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                            </li>

                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left add_new_client_form input_mask" id="add_new_client_form" method="post">

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left add_name"required="required" id="inputSuccess2" name="add_name" placeholder="Client Name">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left"  id="inputSuccess2" name="add_address" placeholder="Address">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left" id="inputSuccess3" name="add_website" placeholder="Website">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left"  id="inputSuccess2" name="add_industry" placeholder="Industry">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left" required="" id="inputSuccess4" name="add_phone" placeholder="Phone No">
                                                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="email" class="form-control has-feedback-left" required="" id="inputSuccess5" name="add_email" placeholder="Email">
                                                <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                                            </div>




                                            <div class="ln_solid"></div>
                                            <div class="form-group">
                                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                    <button type="reset" id="reset_add_client_form" class="btn btn-primary">Cancel</button>
                                                    <button type="submit" id="add_client_form_btn" class=" add_client_form_btn btn btn-success">Submit</button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>






                                </div>
                            </div>
                        </div>

                        <!-- Add New Client Form End --> 



                        <!-- View Client Form Start -->

                        <div class="row view_client_div" id="view_client_div" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Unique Loo  View Client </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                            </li>

                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left view_client_form input_mask" id="view_client_form" method="post">

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left view_name"required="required" id="view_name" readonly="" name="view_name" placeholder=" Name">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left view_address" required="required" readonly="" id="view_address" name="view_address" placeholder="Address">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left view_website" id="view_o_name" readonly="" name="view_website" placeholder="Website">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left view_industry" required="required" readonly="" id="view_industry" name="view_industry" placeholder="Industry">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left view_phone" id="view_phone" readonly="" name="view_phone" placeholder="Phone Number">
                                                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                                            </div>


                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="email" class="form-control has-feedback-left view_email" id="view_email" readonly="" name="view_email" placeholder=" Email">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>


                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left view_date_added"  required="required" id="view_date_added" name="view_date_added" readonly="" placeholder="Date Added">
                                                <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                                            </div>



                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <a id="go_back_view" class="btn btn-info btn-xs go_back_view"><i class="fa fa-backward"></i> Go Back  <span class="fa fa-chevron-left"></span></a>

                                            </div>






                                            <input type="hidden" name="view_client_id" id="view_client_id" class="view_client_id form-control hidden"/>



                                            <div class="ln_solid"></div>


                                        </form>
                                    </div>






                                </div>
                            </div>
                        </div>

                        <!-- View Client Form End -->



                        <!--  Edit Client Form Start-->




                        <div class="row edit_client_div" id="edit_client_div" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Unique Loo  Edit Client </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                            </li>

                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left edit_client_form input_mask" id="edit_client_form" method="post">

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left edit_name"required="required" id="edit_name" name="edit_name" placeholder=" Name">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left edit_address"  id="edit_address" name="edit_address" placeholder="Address">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left edit_website" id="edit_website" name="edit_website" placeholder="Website">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left edit_industry"  id="edit_industry" name="edit_industry" placeholder="Industry">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left edit_phone" id="edit_phone" name="edit_phone" placeholder="Phone Number">
                                                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                                            </div>


                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="email" class="form-control has-feedback-left edit_email" id="edit_email" name="edit_email" placeholder=" Email">
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                            </div>


                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left edit_date_added"  id="edit_date_added" name="edit_date_added" readonly="" placeholder="Date Added">
                                                <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                                            </div>









                                            <input type="hidden" name="edit_client_id" id="edit_client_id" class="edit_client_id form-control hidden"/>



                                            <div class="ln_solid"></div>
                                            <div class="form-group">
                                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                    <button type="reset" id="go_back_edit" class="btn btn-primary go_back_edit">Cancel</button>
                                                    <button type="submit" id="edit_client_form_btn" class=" edit_client_form_btn btn btn-success">Update </button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>


                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $('#edit_dob').daterangepicker({
                                                singleDatePicker: true,
                                                calender_style: "picker_4"
                                            }, function (start, end, label) {
                                                console.log(start.toISOString(), end.toISOString(), label);
                                            });
                                        });
                                    </script>



                                </div>
                            </div>
                        </div>





                        <!--  Edit Client Form End-->

                        <!--Delete Client Form Start -->

                        <div class="row delete_client_div" id="delete_client_div" style="display: none;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Unique Loo  Edit Client </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                            </li>

                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left delete_client_form input_mask" id="delete_client_form" method="post">




                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                <blockquote class="blockquote-reverse">
                                                    <h6>Are you sure you want to Delete Client : </h6> </blockquote> 
                                                <br>
                                                <input id="delete_client_name" readonly="readonly" class="delete_client_name form-control has-feedback-left"/>
                                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                <input type="hidden" name="delete_client_id" class="hidden form-control delete_client_id" id="delete_client_id"/>

                                                <div class="ln_solid"></div>
                                                <div class="form-group">
                                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                        <button type="reset" id="cancel_delete_button" class="btn btn-primary cancel_delete_button">Cancel</button>
                                                        <button type="submit" id="delete_client_form_btn" class=" delete_client_form_btn btn btn-success">Delete </button>
                                                    </div>
                                                </div>

                                            </div>



                                        </form>
                                    </div>






                                </div>
                            </div>
                        </div>

                        <!-- Delete Client Form End  -->
                        <!--Ajax Loader Div Start -->
                        <div id="ajax_loader_div" class="ajax_loader_div" style="display: none;">

                            <img src="<?php echo base_url(); ?>assets/images/ajax_loader/ajax-loader.gif" alt="Ajax Loader" height="42" width="42"> 
                            <hr>
                            <div class="alert alert-info">Please Wait While Information is Loaded</div>
                        </div>

                        <!-- Ajax Loader Div End -->





                        <div class="row clients_table_div" id="clients_table_div">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Unique Loo  Clients </h2>
                                        <br>
                                        <div id="info_box_reload" class=" info_box_reload" style="display: none; margin-left: 187px;" >
                                            <h5>Information has been Updated, Please reload the  page to view up to date information. </h5>
                                            <a class="btn btn-primary btn-xs reload_information " id="reload_information" href="<?php echo base_url(); ?>admin/clients" >
                                                <i class="fa fa-refresh"></i>
                                                Reload Client
                                            </a>   

                                        </div>

                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                            </li>

                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>


                                    <div class="x_content">

                                        <a class="btn btn-primary btn-xs add_new_client " id="add_new_client" >
                                            <i class="fa fa-plus"></i>
                                            Add Client
                                        </a>



                                        <table id="client_table" class="client_table table-bordered table-condensed table-responsive table-striped table-hover jambo_table">
                                            <thead>
                                                <tr>

                                                    <th>No </th>
                                                    <th>Name </th>
                                                    <th>Address </th>
                                                    <th>Web-site </th>
                                                    <th> Industry </th>
                                                    <th>Phone No </th>
                                                    <th>E Mail</th>
                                                    <th>Status</th>
                                                    <th>Date Added</th>
                                                    <th><span>Action</span>
                                                    </th>
                                                </tr>
                                            </thead>


                                            <tbody>

                                                <?php
                                                $i = 1;
                                                foreach ($clients as $value) {
                                                    ?>
                                                    <tr class="even pointer">
                                                        <td class="a-center"><?php echo $i; ?></td>
                                                        <td><?php echo $value['name']; ?></td>
                                                        <td><?php echo $value['address']; ?></td>
                                                        <td><?php echo $value['website']; ?></td>
                                                        <td><?php echo $value['industry']; ?></td>
                                                        <td><?php echo $value['phone_no']; ?></td>
                                                        <td><?php echo $value['email']; ?></td>
                                                        <td><?php echo $value['status']; ?></td>
                                                        <td><?php echo $value['date_added']; ?></td>
                                                        <td class="a-center">
                                                            <input class="hidden td_client_id" type="hidden" value="<?php echo $value['id']; ?>" name="td_client_id" id="td_client_id"/>


                                                            <a class="btn btn-primary btn-xs view_client_info" id="view_client_info" >
                                                                <i class="fa fa-folder"></i>
                                                                View
                                                            </a>
                                                            <a class="btn btn-info btn-xs edit_client_info" id="edit_client_info" >
                                                                <i class="fa fa-pencil"></i>
                                                                Edit
                                                            </a>
                                                            <a class="btn btn-danger btn-xs delete_client_info" id="delete_client_info" >
                                                                <i class="fa fa-trash-o"></i>
                                                                Delete
                                                            </a>


                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                                ?>



                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>

                            <br />
                            <br />
                            <br />

                        </div>
                    </div>


                    <!-- footer content -->
                    <footer>
                        <div class="">
                            <p class="pull-right">Unique Loo ! by <a>Harris Dindi</a>. |
                                <span class="lead"> <i class="fa fa-paw"></i> Unique Loo!</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </footer>
                    <!-- /footer content -->




                </div>
                <!-- /page content -->
            </div>

        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>


        <!-- Sweet Alert Library -->
        <script src="<?php echo base_url(); ?>assets/dist/sweetalert-dev.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
        <!--.......................-->


        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- chart js -->
        <script src="<?php echo base_url(); ?>assets/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url(); ?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>



        <!-- daterangepicker -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js"></script>



        <!-- Datatables -->
        <script src="<?php echo base_url(); ?>assets/js/datatables/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>







        <script>
                                        $(document).ready(function () {
                                            $('input.tableflat').iCheck({
                                                checkboxClass: 'icheckbox_flat-green',
                                                radioClass: 'iradio_flat-green'
                                            });
                                        });

                                        var asInitVals = new Array();
                                        $(document).ready(function () {
                                            var oTable = $('#client_table').dataTable({
                                                "oLanguage": {
                                                    "sSearch": "Search all columns:"
                                                },
                                                "aoColumnDefs": [
                                                    {
                                                        'bSortable': false,
                                                        'aTargets': [0]
                                                    } //disables sorting for column one
                                                ],
                                                'iDisplayLength': 12,
                                                "sPaginationType": "full_numbers",
                                                "dom": 'T<"clear">lfrtip',
                                                "tableTools": {
                                                    "sSwfPath": "<?php echo base_url('assets/js/datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
                                                }
                                            });
                                            $("tfoot input").keyup(function () {
                                                /* Filter on the column based on the index of this element's parent <th> */
                                                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
                                            });
                                            $("tfoot input").each(function (i) {
                                                asInitVals[i] = this.value;
                                            });
                                            $("tfoot input").focus(function () {
                                                if (this.className == "search_init") {
                                                    this.className = "";
                                                    this.value = "";
                                                }
                                            });
                                            $("tfoot input").blur(function (i) {
                                                if (this.value == "") {
                                                    this.className = "search_init";
                                                    this.value = asInitVals[$("tfoot input").index(this)];
                                                }
                                            });




                                            $(".add_new_client").click(function () {

                                                $(".add_new_client_div").show('slow');
                                                $(".clients_table_div").hide('slow');

                                            });





                                            $('#reset_add_client_form').click(function () {
                                                $(".add_new_client_div").hide('slow');
                                                $(".clients_table_div").show('slow');
                                            });





                                            $('#add_new_client_form').submit(function (event) {
                                                dataString = $("#add_new_client_form").serialize();

                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url() ?>index.php/admin/add_new_client",
                                                    data: dataString,
                                                    success: function (data) {

                                                        var added_client_name = $(".add_name").val();
                                                        data = JSON.parse(data);
                                                        var response = data[0].response;
                                                        if (response === "Client Already Exist in the System!") {
                                                            sweetAlert("Success", " Client " + added_client_name + " Already Exist in the System! :( Please try again with a different Client Name", "error");
                                                        } else if (response === "Client Added Successfully!") {
                                                            sweetAlert("Success!", "Client Added Successfully!", "success");

                                                            $(".add_new_client_div").show('slow');
                                                            $(".clients_table_div").hide('slow');
                                                            $(".info_box_reload").show('slow');
                                                        }


                                                    }, error: function (data) {
                                                        sweetAlert("Oops...", "Something went wrong!", "error");

                                                    }

                                                });
                                                event.preventDefault();
                                                return false;
                                            });





                                            $('#edit_client_form').submit(function (event) {
                                                dataString = $("#edit_client_form").serialize();
                                                $(".ajax_loader_div").show('slow');
                                                $(".edit_client_div").hide('slow');
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url() ?>index.php/admin/edit_client",
                                                    data: dataString,
                                                    success: function (data) {
                                                        sweetAlert("Success!", "Data updated successfully!", "success");


                                                        $(".clients_table_div").show('slow');
                                                        $(".ajax_loader_div").hide('slow');


                                                        $(".info_box_reload").show('slow');
                                                    }, error: function (data) {
                                                        sweetAlert("Oops...", "Something went wrong!", "error");

                                                    }

                                                });
                                                event.preventDefault();
                                                return false;
                                            });



                                            $('#delete_client_form').submit(function (event) {
                                                dataString = $("#delete_client_form").serialize();
                                                $(".ajax_loader_div").show('slow');
                                                $(".delete_client_div").hide('slow');
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url() ?>index.php/admin/delete_client",
                                                    data: dataString,
                                                    success: function (data) {

                                                        sweetAlert("Success!", "Data updated successfully!", "success");

                                                        $(".clients_table_div").show('slow');
                                                        $(".ajax_loader_div").hide('slow');
                                                        $(".info_box_reload").show('slow');
                                                    }, error: function (data) {
                                                        sweetAlert("Oops...", "Something went wrong!", "error");

                                                    }

                                                });
                                                event.preventDefault();
                                                return false;
                                            });


                                            $('#go_back_edit').click(function () {
                                                $(".edit_client_div").hide('slow');
                                                $(".ajax_loader_div").show('slow');
                                                $(".clients_table_div").show('slow');
                                                $(".ajax_loader_div").hide('slow');
                                            });




                                            $(document).on('click', ".edit_client_info", function () {



                                                //get data
                                                var client_id = $(this).closest('tr').find('input[name="td_client_id"]').val();


                                                $(".ajax_loader_div").show('slow');
                                                $(".clients_table_div").hide('slow');
                                                $.ajax({
                                                    type: "GET",
                                                    url: "<?php echo base_url(); ?>admin/get_client_details/" + client_id,
                                                    dataType: "json",
                                                    success: function (response) {

                                                        $('#edit_client_id').val(response[0].id);
                                                        $('#edit_name').val(response[0].name);
                                                        $('#edit_address').val(response[0].address);
                                                        $('#edit_website').val(response[0].website);
                                                        $('#edit_industry').val(response[0].industry);
                                                        $('#edit_phone').val(response[0].phone_no);
                                                        $('#edit_email').val(response[0].email);
                                                        $('#edit_date_added').val(response[0].date_added);
                                                        $('#edit_timestamp').val(response[0].timestamp);
                                                        $(".edit_client_div").show('slow');
                                                        $(".ajax_loader_div").hide('slow');

                                                    }, error: function (data) {
                                                        sweetAlert("Oops...", "Something went wrong!", "error");

                                                    }
                                                });
                                            });


                                            $('#go_back_view').click(function () {
                                                $(".view_client_div").hide('slow');
                                                $(".ajax_loader_div").show('slow');
                                                $(".clients_table_div").show('slow');
                                                $(".ajax_loader_div").hide('slow');
                                            });



                                            $(document).on('click', ".view_client_info", function () {



                                                //get data
                                                var client_id = $(this).closest('tr').find('input[name="td_client_id"]').val();


                                                $(".ajax_loader_div").show('slow');
                                                $(".clients_table_div").hide('slow');
                                                $.ajax({
                                                    type: "GET",
                                                    url: "<?php echo base_url(); ?>admin/get_client_details/" + client_id,
                                                    dataType: "json",
                                                    success: function (response) {


                                                        $('#view_client_id').val(response[0].id);
                                                        $('#view_name').val(response[0].name);
                                                        $('#view_address').val(response[0].address);
                                                        $('#view_website').val(response[0].website);
                                                        $('#view_industry').val(response[0].industry);
                                                        $('#view_phone').val(response[0].phone_no);
                                                        $('#view_email').val(response[0].email);
                                                        $('#view_date_added').val(response[0].date_added);
                                                        $('#view_timestamp').val(response[0].timestamp);
                                                        $(".view_client_div").show('slow');
                                                        $(".ajax_loader_div").hide('slow');

                                                    }, error: function (data) {
                                                        sweetAlert("Oops...", "Something went wrong!", "error");

                                                    }
                                                });
                                            });





                                            $('#cancel_delete_button').click(function () {
                                                $(".delete_client_div").hide('slow');
                                                $(".ajax_loader_div").show('slow');
                                                $(".clients_table_div").show('slow');
                                                $(".ajax_loader_div").hide('slow');
                                            });



                                            $(document).on('click', ".delete_client_info", function () {

                                                //get data
                                                var client_id = $(this).closest('tr').find('input[name="td_client_id"]').val();


                                                $(".ajax_loader_div").show('slow');
                                                $(".clients_table_div").hide('slow');
                                                $.ajax({
                                                    type: "GET",
                                                    url: "<?php echo base_url(); ?>admin/get_client_details/" + client_id,
                                                    dataType: "json",
                                                    success: function (response) {

                                                        $('#delete_client_id').val(response[0].id);
                                                        $('#delete_client_name').val(response[0].name);
                                                        $(".delete_client_div").show('slow');
                                                        $(".ajax_loader_div").hide('slow');

                                                    }, error: function (data) {
                                                        sweetAlert("Oops...", "Something went wrong!", "error");

                                                    }
                                                });
                                            });




























                                            function Draw() {

                                                oTable = $("#clients_tables").DataTable({
                                                    "dom": 'T<"clear">lfrtip',
                                                    "tableTools": {
                                                        // "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
                                                    },
                                                    stateSave: true,
                                                    "bautoWidth": false,
                                                    "aoColumns": [
                                                        {"sTitle": "User No", "mData": "client_name"},
                                                        {"sTitle": "User Name.", "mData": "id_no"},
                                                        {"sTitle": "Email", "mData": "dob"},
                                                        {"sTitle": "Status", "mData": "marital_status"},
                                                        {"sTitle": "User Name.", "mData": "nationality"},
                                                        {"sTitle": "Email", "mData": "department_name"},
                                                        {"sTitle": "Status", "mData": "roles_name"},
                                                        {"sTitle": "Salary", "mData": "id_no", "sDefaultContent": '<input type="button" value="Some Action">'}
                                                    ],
                                                    "bDeferRender": true,
                                                    "bProcessing": true,
                                                    "bDestroy": true,
                                                    "bLengthChange": true,
                                                    "iDisplayLength": 200,
                                                    "sA$axDataProp": "",
                                                    "sA$axSource": '<?php echo base_url() . "admin/client_data"; ?>',
                                                    "aaSorting": [[2, "desc"]]
                                                });

                                            }


                                        });
        </script>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

        <script>
                                        var j = jQuery.noConflict();
                                        j(document).ready(function () {
                                            j(function () {
                                                j(".nationality").autocomplete({
                                                    source: "<?php echo site_url(); ?>admin/get_countries" // path to the get_countries method
                                                });
                                            });

                                        });
        </script>






    </body>

</html>