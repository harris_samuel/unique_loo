<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Unique Loo! | Assign Resources </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">
        <!-- editor -->
        <link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/editor/index.css" rel="stylesheet">
        <!-- select2 -->
        <link href="<?php echo base_url(); ?>assets/css/select/select2.min.css" rel="stylesheet">
        <!-- switchery -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/switchery/switchery.min.css" />

        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    </head>


    <body class="nav-md">

        <div class="container body">


            <div class="main_container">





                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">

                        <div class="navbar nav_title" style="border: 0;">

                            <a href="<?php echo base_url(); ?>"> <img  style="margin-left: 60px; margin-right: 0px; margin-bottom: 0px; margin-top: 0px;" src="<?php echo base_url(); ?>images/logo.png" alt="logo"  /></a>

                        </div>
                        <div class="clearfix"></div>



                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">
                                    <li><a href="<?php echo base_url(); ?>" ><i class="fa fa-home"></i> HOME <span class="fa fa-chevron-right"></span></a> </li>




                                    <li><a><i class="fa fa-edit"></i> ADMINISTRATION <span class="fa fa-chevron-down"></span></a>

                                        <ul class="nav child_menu" style="display: none">

                                            <li>


                                                <?php
                                                $base_url = base_url();
                                                foreach ($admin_functions as $value) {
                                                    $controller_name = $value['controller_name'];
                                                    $function_name = $value['functions_name'];
                                                    $i_tag = $value['i_tag'];
                                                    $description = $value['description'];
                                                    $span_tag = $value['span_tag'];
                                                    ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>



                                        </ul> 

                                    </li>

                                    <li><a><i class="fa fa-edit"></i> DAILY OPERATIONS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($daily_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>






                                        </ul>
                                    </li>



                                    <li><a><i class="fa fa-edit"></i> REPORTS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($reports_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>







                                        </ul>
                                    </li>



                                </ul>
                            </div>

                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" href="<?php echo base_url(); ?>home/do_logout" title="Logout">
                                <span class="glyphicon glyphicon-off"  aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">

                    <div class="nav_menu">
                        <nav class="" user="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="<?php echo base_url(); ?>assets/images/img.jpg" alt=""><?php
                                        $username = $this->session->userdata('user_name');
                                        echo $username;
                                        ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <li><a href="<?php echo base_url(); ?>admin/profile">  Profile</a>
                                        </li>


                                        <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                        </li>
                                    </ul>
                                </li>


                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->










                <!-- page content -->
                <div class="right_col" role="main">

                    <!-- top tiles -->
                    <div class="row tile_count">
                        <!--Client Info Start -->
                        <div class="" id="">

                        </div>
                        <!-- Client Info End  -->
                    </div>
                    <!-- /top tiles -->



                    <div class="">

                        <div class="page-title">
                            <div class="title_left">
                                <h3>Unique Loo</h3>
                            </div>
                            <div class="title_right">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                                    <div class="input-group">



                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>






                        <div class="row">

                            <div  id="client_payment_list" class="client_payment_list">


                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2><i class="fa fa-bars"></i> Unique Loo <small>Assign Resources</small></h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">


                                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Assets </a>
                                                    </li>
                                                    <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false">Support Services</a>
                                                    </li>

                                                </ul>
                                                <div id="myTabContent" class="tab-content">

                                                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                                        <br/>
                                                        <form id="assign_asset" class="assign_asset">


                                                            <input type="hidden" name="job_card_id" class="job_card_id form-control" value="<?php echo $this->uri->segment(3); ?>"/>
                                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                                <select name="asset_type" id="asset_type" class="asset_type selector form-control">
                                                                    <option>Please select : </option>
                                                                    <option>Trailer</option>
                                                                    <option>Toilet</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">

                                                                <select name="asset_name" id="asset_name" class="asset_name selector form-control">

                                                                </select>
                                                            </div>

                                                            <div class="ln_solid"></div>
                                                            <div class="form-group">
                                                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                                    <button type="reset" class="btn btn-primary cancel_job_btn" id="cancel_job_btn">Cancel</button>
                                                                    <button type="submit" class="btn btn-success book_job_btn" id="book_job_btn">Assign </button>
                                                                </div>
                                                            </div>

                                                        </form>





                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                                                        <br/>

                                                        <form id="assign_asset_1" class="assign_asset_1 form">


                                                            <input type="hidden" name="job_card_id" class="job_card_id form-control" value="<?php echo $this->uri->segment(3); ?>"/>
                                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                                <input type="text" name="asset_name" class="form-control asset_name"/>

                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                                <input type="number" min="1"  name="asset_value" class="asset_value form-control" id="asset_value" placeholder="Value (KES : )"/>
                                                            </div>

                                                            <div class="ln_solid"></div>
                                                            <div class="form-group">
                                                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                                    <button type="reset" class="btn btn-primary cancel_job_btn" id="cancel_job_btn">Cancel</button>
                                                                    <button type="submit" class="btn btn-success book_job_btn" id="book_job_btn">Assign </button>
                                                                </div>
                                                            </div>

                                                        </form>

                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>




                                </div>

                            </div>





                        </div>






                        <!--Ajax Loader Div_1 Start -->
                        <div id="ajax_loader_div_1" class="ajax_loader_div_1" style="display: none;">

                            <img src="<?php echo base_url(); ?>assets/images/ajax_loader/ajax-loader.gif" alt="Ajax Loader" height="42" width="42"> 
                            <hr>
                            <div class="alert alert-info">Please Wait While Information is Loaded</div>
                        </div>

                        <!-- Ajax Loader Div_1 End -->



                        <!--Ajax Loader Div Start -->
                        <div id="ajax_loader_div" class="ajax_loader_div" style="display: none;">

                            <img src="<?php echo base_url(); ?>assets/images/ajax_loader/ajax-loader.gif" alt="Ajax Loader" height="42" width="42"> 
                            <hr>
                            <div class="alert alert-info">Please Wait While Information is Loaded</div>
                        </div>

                        <!-- Ajax Loader Div End -->






                    </div>
                    <!-- /page content -->

                    <!-- footer content -->
                    <footer>
                        <div class="">
                            <p class="pull-right">Unique Loo ! by <a>Harris Dindi</a>. |
                                <span class="lead"> <i class="fa fa-paw"></i> Unique Loo!</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </footer>
                    <!-- /footer content -->

                </div>

            </div>
        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>

        <!-- Sweet Alert Library -->
        <script src="<?php echo base_url(); ?>assets/dist/sweetalert-dev.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
        <!--.......................-->



        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- chart js -->
        <script src="<?php echo base_url(); ?>assets/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url(); ?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>
        <!-- tags -->
        <script src="<?php echo base_url(); ?>assets/js/tags/jquery.tagsinput.min.js"></script>
        <!-- switchery -->
        <script src="<?php echo base_url(); ?>assets/js/switchery/switchery.min.js"></script>
        <!-- daterangepicker -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assts/js/datepicker/daterangepicker.js"></script>
        <!-- richtext editor -->
        <script src="<?php echo base_url(); ?>assets/js/editor/bootstrap-wysiwyg.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/editor/external/jquery.hotkeys.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/editor/external/google-code-prettify/prettify.js"></script>
        <!-- select2 -->
        <script src="<?php echo base_url(); ?>assets/js/select/select2.full.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/parsley/parsley.min.js"></script>
        <!-- textarea resize -->
        <script src="<?php echo base_url(); ?>assets/js/textarea/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));
        </script>
        <!-- Autocomplete -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/autocomplete/countries.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/autocomplete/jquery.autocomplete.js"></script>
        <script type="text/javascript">
            $(function () {
                'use strict';
                var countriesArray = $.map(countries, function (value, key) {
                    return {
                        value: value,
                        data: key
                    };
                });
                // Initialize autocomplete with custom appendTo:
                $('#autocomplete-custom-append').autocomplete({
                    lookup: countriesArray,
                    appendTo: '#autocomplete-container'
                });
            });
        </script>
        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>

        <script>
            $(document).ready(function () {

                $("#asset_type").change(function () {
                    var asset_type = this.value;
                    available_assets = '';
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/available_assets/" + asset_type,
                        dataType: "JSON",
                        success: function (response) {
                            for (i = 0; i < response.length; i++) {
                                available_assets = '<option value="' + response[i].id + '">' + response[i].name + '</option>';
                                $('.asset_name').append(available_assets);
                            }


                        },
                        error: function (response) {

                        }
                    });
                });



                $('#assign_asset').submit(function (event) {
                    dataString = $("#assign_asset").serialize();

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/operations/assign",
                        data: dataString,
                        success: function (data) {
                            sweetAlert("Great!", "Asset Assigned Successfully!", "success");

                            $(".add_new_user_div").show('slow');
                            $(".users_table_div").hide('slow');
                            $(".info_box_reload").show('slow');
                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }

                    });
                    event.preventDefault();
                    return false;
                });



                $('#assign_asset_1').submit(function (event) {
                    dataString = $("#assign_asset_1").serialize();

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/operations/assign",
                        data: dataString,
                        success: function (data) {
                            sweetAlert("Great!", "Asset assigned Succesfully!", "success");

                            $(".add_new_user_div").show('slow');
                            $(".users_table_div").hide('slow');
                            $(".info_box_reload").show('slow');
                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");

                        }

                    });
                    event.preventDefault();
                    return false;
                });







            });
        </script>




    </body>

</html>