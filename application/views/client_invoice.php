<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Unique Loo | Client Invoice  </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">


        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    </head>


    <body class="nav-md">

        <div class="container body">


            <div class="main_container">


                <input type="hidden" name="random_key" id="random_key" class="random_key form-control" value="<?php echo $this->uri->segment(3); ?>"/>

                <!-- page content -->
                <div class="right_col" role="main">

                    <div class="">

                        <div class="clearfix"></div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Unique Loo  <small>
                                                <?php
                                                foreach ($invoice_types as $value) {
                                                    echo $value->invoice_type;
                                                }
                                                ?></small></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>

                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">

                                        <section class="content invoice">
                                            <!-- title row -->
                                            <div class="row">
                                                <div class="col-xs-12 invoice-header">
                                                    <a href="<?php echo base_url(); ?>"> <img  style="" src="<?php echo base_url(); ?>images/logo.png" alt="logo"  /></a>
                                                    <h1>

                                                        <small class="pull-right"><?php
                                                            $today = date("F j, Y, g:i a");
                                                            echo $today;
                                                            ?></small>
                                                    </h1>
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                            <!-- info row -->
                                            <div class="row invoice-info">
                                                <div class="col-sm-4 invoice-col client_conf">
                                                    From<address class="">
                                                        <strong class="clnt_name"></strong>
                                                        <br>
                                                        <span class="clnt_address"></span>
                                                        <br>
                                                        <span><?php foreach ($booked_by as $value) {
                                                               $booked_by = $value->booked_by;
                                                               $phone_no = $value->phone_no;
                                                               echo $booked_by.' Phone No :'.$phone_no;
                                                            }
                                                            ?></span>
                                                        <br>
                                                        <span class="clnt_website"></span>
                                                        <br>
                                                        <span class="clnt_email"></span>
                                                    </address>

                                                </div>
                                                <!-- /.col -->
                                                <div class="col-sm-4 invoice-col invoiced_client">

                                                </div>
                                                <!-- /.col -->
                                                <div class="col-sm-4 invoice-col invoice_no_details">

                                                </div>
                                                <!-- /.col -->
                                            </div>
                                            <!-- /.row -->

                                            <!-- Table row -->
                                            <div class="row">
                                                <div class="col-xs-12 table">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Qty</th>
                                                                <th>Product</th>
                                                                <th>@</th>
                                                                <th>Discount</th>
                                                                <th>Subtotal(KES)</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody id="client_ivoice_stmnt" class="client_ivoice_stmnt">
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                            <!-- /.row -->

                                            <div class="row">
                                                <!-- accepted payments column -->
                                                <div class="col-xs-6">
                                                    <p class="lead">Payment Methods:</p>
                                                    <img src="<?php echo base_url(); ?>assets/images/visa.png" alt="Visa">
                                                    <img src="<?php echo base_url(); ?>assets/images/mastercard.png" alt="Mastercard">
                                                    <img src="<?php echo base_url(); ?>assets/images/orange-money.png" alt="Orange Money">
                                                    <img src="<?php echo base_url(); ?>assets/images/airtel-money.jpg" alt="Airtel Money">
                                                    <img src="<?php echo base_url(); ?>assets/images/mpesa.jpg" alt="M-PESA">
                                                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                                        More Instructions will be provided by the  Cashier </p>
                                                </div>
                                                <!-- /.col -->
                                                <div class="col-xs-6">
                                                    <p class="lead">Amount Due : On or Before the  Service Day</p>
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <tbody>
                                                                <tr class="subtotal_tr">
                                                                    <th style="width:50%">Subtotal:</th>
                                                                    <td></td>
                                                                </tr>
                                                                <tr class="tax_tr">
                                                                </tr>

                                                                <tr class="total_tr">

                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                            <!-- /.row -->

                                            <!-- this row will not appear when printing -->
                                            <div class="row no-print">
                                                <div class="col-xs-12">
                                                    <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    <!-- footer content -->
                    <footer>
                        <div class="">
                            <p class="pull-right">Unique Loo ! by <a>Harris Dindi</a>. |
                                <span class="lead"> <i class="fa fa-paw"></i> Unique Loo!</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </footer>
                    <!-- /footer content -->

                </div>
                <!-- /page content -->
            </div>

        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>


        <!-- Sweet Alert Library -->
        <script src="<?php echo base_url(); ?>assets/dist/sweetalert-dev.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
        <!--.......................-->


        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- chart js -->
        <script src="<?php echo base_url(); ?>assets/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url(); ?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>


        <script>
                                                        $(document).ready(function () {
                                                            $.ajax({
                                                                type: "GET",
                                                                url: "<?php echo base_url(); ?>download/get_user_configurations",
                                                                dataType: 'JSON',
                                                                success: function (data) {
                                                                    var company_name = data[0].name;
                                                                    var address = data[0].address;
                                                                    var website = data[0].website;
                                                                    var pin = data[0].pin;
                                                                    var phone_no = data[0].phone_no;
                                                                    var email = data[0].email;
                                                                    $(".clnt_name").append(company_name);
                                                                    $(".clnt_address").append(address);
                                                                   // $(".clnt_phone").append(phone_no);
                                                                    $(".clnt_pin").append(pin);
                                                                    $(".clnt_email").append(email);
                                                                    $(".clnt_website").append(website);
//                                                                    $(".client_conf").append("<strong>" + company_name + "</strong><br> " + address + " <br> <br>Phone: " + phone_no + " <br>Email: " + email + " <br>Website: " + website + "");
                                                                }, error: function (data) {
                                                                    // sweetAlert("Oops...", "Something went wrong!", "error");
                                                                    
                                                                }
                                                            });
                                                            
                                                            var random_key = $(".random_key").val();
                                                            $.ajax({
                                                                type: "GET",
                                                                url: "<?php echo base_url(); ?>download/client_info/" + random_key,
                                                                dataType: 'JSON',
                                                                success: function (data) {
                                                                    var client_id = data[0].client_id;
                                                                    var client_name = data[0].client_name;
                                                                    var address = data[0].address;
                                                                    var website = data[0].website;
                                                                    var phone_no = data[0].phone_no;
                                                                    var email = data[0].email;
                                                                    var job_card_no = data[0].job_card_no;
                                                                    var invoice_no = data[0].invoice_no;
                                                                    $(".invoiced_client").empty();
                                                                    $(".invoiced_client").append(" To <address> <strong>" + client_name + "</strong> <br> " + address + " <br>Phone: " + phone_no + " <br>Email: " + email + "  </address>");
                                                                    
                                                                    $(".invoice_no_details").empty();
                                                                    $(".invoice_no_details").append(" <b>Invoice # " + invoice_no + "</b> <br>  <b>Order ID:</b> " + job_card_no + " <br> <b>Payment Due:</b> 2/22/2014 <br>  ");
                                                                    
                                                                }, error: function (data) {
                                                                    sweetAlert("Oops...", "Something went wrong!", "error");
                                                                    
                                                                }
                                                            });
                                                            
                                                            
                                                            client_transaction_details = '';
                                                            $.ajax({
                                                                type: "GET",
                                                                url: "<?php echo base_url(); ?>download/get_invoice_job_details/" + random_key,
                                                                dataType: "JSON",
                                                                success: function (data) {
                                                                    
                                                                    
                                                                    for (i = 0; i < data.length; i++) {
                                                                        var total_price = data[i].total_price;
                                                                        var discount = data[i].discount;
                                                                        var new_price = total_price;
                                                                        
                                                                        client_transaction_details += '<tr><td>' + data[i].qty + '</td><td>' + data[i].asset_name + '</td> <td>' + data[i].value + '</td> <td>(' + data[i].discount + ')</td> <td>' + new_price + '</td> </tr>';
                                                                        
                                                                        
                                                                    }
                                                                    
                                                                    $('.client_ivoice_stmnt').empty();
                                                                    $('.client_ivoice_stmnt').append(client_transaction_details);
                                                                    
                                                                }, error: function (data) {
                                                                    sweetAlert("Oops...", "Something went wrong!", "error");
                                                                    
                                                                }
                                                            });
                                                            
                                                            
                                                            $.ajax({
                                                                type: "GET",
                                                                url: "<?php echo base_url(); ?>download/get_job_sub_total/" + random_key,
                                                                dataType: 'JSON',
                                                                success: function (data) {
                                                                    var job_card_id = data[0].job_card_id;
                                                                    var statement_id = data[0].statement_id;
                                                                    var discount = data[0].discount;
                                                                    var total_job_price = data[0].total_job_price;
                                                                    var total_job_price = total_job_price - discount;
                                                                    
                                                                    var tax = total_job_price * 0.16;
                                                                    var sub_total = total_job_price * 0.84;
                                                                    $('.job_card_id_payment').val(job_card_id);
                                                                    $(".subtotal_tr").empty();
                                                                    $(".subtotal_tr").append("  <th>Subtotal:</th> <td>" + sub_total + "</td>");
                                                                    $(".tax_tr").empty();
                                                                    $(".tax_tr").append(" <th>Tax (16.0%)</th><td>" + tax + "</td>");
                                                                    $(".total_tr").empty();
                                                                    $(".total_tr").append("<th>Total:</th> <td>" + total_job_price + "</td>");
                                                                    
                                                                    
                                                                }, error: function (data) {
                                                                    sweetAlert("Oops...", "Something went wrong!", "error");
                                                                    
                                                                }
                                                            });
                                                            
                                                            
                                                            
                                                        });
        </script>

    </body>

</html>