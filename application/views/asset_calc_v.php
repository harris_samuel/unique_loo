<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Unique Loo! | </title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">
        <!-- editor -->
        <link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/editor/index.css" rel="stylesheet">
        <!-- select2 -->
        <link href="<?php echo base_url(); ?>assets/css/select/select2.min.css" rel="stylesheet">
        <!-- switchery -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/switchery/switchery.min.css" />

        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>



        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    </head>


    <body class="nav-md">

        <div class="container body">


            <div class="main_container">

                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">

                        <div class="navbar nav_title" style="border: 0;">


                            <a href="<?php echo base_url(); ?>"> <img  style="margin-left: 60px; margin-right: 0px; margin-bottom: 0px; margin-top: 0px;" src="<?php echo base_url(); ?>images/logo.png" alt="logo"  /></a>

                        </div>

                        <div class="clearfix"></div>



                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">
                                    <li><a href="<?php echo base_url(); ?>" ><i class="fa fa-home"></i> HOME <span class="fa fa-chevron-right"></span></a> </li>




                                    <li><a><i class="fa fa-edit"></i> ADMINISTRATION <span class="fa fa-chevron-down"></span></a>

                                        <ul class="nav child_menu" style="display: none">

                                            <li>


                                                <?php
                                                $base_url = base_url();
                                                foreach ($admin_functions as $value) {
                                                    $controller_name = $value['controller_name'];
                                                    $function_name = $value['functions_name'];
                                                    $i_tag = $value['i_tag'];
                                                    $description = $value['description'];
                                                    $span_tag = $value['span_tag'];
                                                    ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>



                                        </ul> 

                                    </li>

                                    <li><a><i class="fa fa-edit"></i> DAILY OPERATIONS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($daily_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>






                                        </ul>
                                    </li>



                                    <li><a><i class="fa fa-edit"></i> REPORTS <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">

                                            <?php
                                            $base_url = base_url();
                                            foreach ($reports_functions as $value) {
                                                $controller_name = $value['controller_name'];
                                                $function_name = $value['functions_name'];
                                                $i_tag = $value['i_tag'];
                                                $description = $value['description'];
                                                $span_tag = $value['span_tag'];
                                                ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $controller_name; ?>/<?php echo $function_name; ?>"><?php echo $i_tag; ?> <?php echo $description; ?> <?php echo $span_tag; ?> </a>

                                                    <?php
                                                }
                                                ?> </li>







                                        </ul>
                                    </li>



                                </ul>
                            </div>

                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" href="<?php echo base_url(); ?>home/do_logout" title="Logout">
                                <span class="glyphicon glyphicon-off"  aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">

                    <div class="nav_menu">
                        <nav class="" user="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="<?php echo base_url(); ?>assets/images/img.jpg" alt=""><?php
                                        $username = $this->session->userdata('user_name');
                                        echo $username;
                                        ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <li><a href="<?php echo base_url(); ?>admin/profile">  Profile</a>
                                        </li>


                                        <li><a href="<?php echo base_url(); ?>home/do_logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                        </li>
                                    </ul>
                                </li>


                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">

                        <div class="page-title">
                            <div class="title_left">
                                <h3>Unique Loo</h3>
                            </div>
                        </div>
                        <div class="clearfix"></div>



                        <div class="row">


                            <!-- Book Job Start -->

                            <div id="book_job_div_list" class="book_job_div_list" >


                                <div class="col-md-9 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Unique Loo <small>Booked Jobs List </small></h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li><a class=""><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <br />

                                            <button id="refresh_job_table_data" class="refresh_job_table_data btn btn-default btn-sm">Refresh List</button>
                                                
                                                 <table class="table table-responsive booked_jobs_list_table" id="booked_jobs_list_table">
                                                <thead>
                                                    <tr class="headings">
                                                        <th>Client name</th>
                                                        <th>Job Card No</th>
                                                        <th>Event Date</th>
                                                        <th>Action</th> 
                                                    </tr>  
                                                </thead>

                                                <tbody id="asset_calc_list" class="asset_calc_list">

                                                    <?php foreach ($booked_jobs as $value) {
                                                        ?>
                                                    <tr id="asset_calc_list_tr" class="asset_calc_list_tr">
                                                            <td><?php echo $value['client_name']; ?></td>
                                                            <td><?php echo $value['job_card_no']; ?></td>
                                                            <td><?php echo $value['event_date']; ?></td>

                                                            <td><button type="button" id="booked_jobs_btn" class="booked_jobs_btn btn btn-primary">Calculate Quotation</button>
                                                                <input type="text" id="hidden_job_card_id" name="hidden_job_card_id" value="<?php echo $value['job_card_id']; ?>" class="form-control hidden  hidden_job_card_id"/>
                                                                <input type="text" id="hidden_job_card_no" name="hidden_job_card_no" value="<?php echo $value['job_card_no']; ?>" class="form-control hidden  hidden_job_card_no"/>
                                                            </td>
                                                        </tr>
                                                    <?php }
                                                    ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr class="headings">
                                                        <th>Client name</th>
                                                        <th>Job Card No</th>
                                                        <th>Event Date</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </tfoot>

                                            </table>
                                        </div>
                                    </div>

                                </div>



                            </div>


                            <!-- Book Job Card End -->










                            <!-- Asset Calculator Start -->

                            <div class="col-md-6 col-sm-12 col-xs-12 asset_calculcator_div_1" id="asset_calculcator_div_1" style="display: none;">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2> Asset Calculator  <small></small></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class=" asset_calculcator_close_link_1" id="asset_calculcator_close_link_1"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">


                                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#fixed_cost_tab" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Fixed Costs</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#other_cost_tab" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false">Other Costs</a>
                                                </li>

                                            </ul>
                                            <div id="myTabContent" class="tab-content">
                                                <div role="tabpanel" class="tab-pane fade active in" id="fixed_cost_tab" aria-labelledby="home-tab">
                                                    <!--<ul class="my_job_list_ul to_do" id="my_job_list_ul"></ul>-->






                                                    <form autocomplete="off" class="form-horizontal form-label-left asset_calculcator_form" id="asset_calculcator_form">

                                                        <div class="col-sm-12">

                                                            <div class="well">



                                                                <input type="hidden" id="job_card_id" class="job_card_id" name="job_card_id"/>
                                                                <div class="form-group">
                                                                    <input type="text" name="job_card_no" class="form-control job_card_no" id="job_card_no" readonly=""/>
                                                                </div>

                                                                <div class=" form-group">
                                                                    <label class="control-label col-md-6 col-sm-6 col-xs-12">Asset Name : </label>
                                                                    <select name="asset_name" required="required" id="select_asset_name"  class="select_asset_name form-control has-feedback-left">
                                                                        <option value="">Please select : </option>                                                       
                                                                        <?php
                                                                        foreach ($parameters as $value) {
                                                                            ?>
                                                                            <option value="<?php echo $value['parameter']; ?>"><?php echo $value['parameter']; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>

                                                                    </select>

                                                                    <br>

                                                                </div>

                                                                <input type="hidden" name="asset_parameter" id="asset_parameter" required="required" class="asset_parameter form-control hidden " />
                                                                <div class=" form-group has-feedback">
                                                                    <input type="text" class="form-control asset_value" required="required"  id="inputSuccess3" readonly="readonly" placeholder="Value" name="asset_value">
                                                                    <span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
                                                                </div>

                                                                <div class=" form-group has-feedback">
                                                                    <label>Quantity : </label>
                                                                    <input type="number" value="1" class="form-control has-feedback-left asset_quantity" step="any"  min="1" required="required"  id="inputSuccess4" placeholder="Quantity" name="asset_quantity">
                                                                    <span class="fa fa-num form-control-feedback left" aria-hidden="true"></span>
                                                                </div>


                                                                <div class=" form-group has-feedback">
                                                                    <label>No of Days : </label>
                                                                    <input type="number" value="1" class="form-control has-feedback-left asset_no_days" step="any" min="1" required="required"  id="inputSuccess4" placeholder="No of Days" name="asset_no_days">
                                                                    <span class="fa fa-num form-control-feedback left" aria-hidden="true"></span>
                                                                </div>



                                                                <div class=" form-group has-feedback">
                                                                    <label>Discount ? : </label>
                                                                    <input type="number" value="0" class="form-control has-feedback-left discount" step="any" required=""   id="discount" min="0" placeholder="Discount" name="discount">
                                                                    <span class="fa fa-num form-control-feedback left" aria-hidden="true"></span>
                                                                </div>



                                                                <div class=" form-group has-feedback">
                                                                    <input type="text" class="form-control asset_total" readonly="readonly" id="inputSuccess5" step="any" placeholder=" Sub Total " required="required"  name="asset_total">
                                                                    <span class="fa fa-tot form-control-feedback right" aria-hidden="true"></span>
                                                                </div>



                                                                <div class="ln_solid"></div>
                                                                <div class="form-group">
                                                                    <div class=" col-md-offset-3">
                                                                        <button type="submit" class="btn  btn-default asset_calculcator_btn" id="asset_calculcator_btn" name="asset_calculcator_btn">Add Asset</button>    
                                                                        <button type="reset" class="btn btn-primary cancel_job_btn" id="cancel_job_btn">Cancel</button>
                                                                        <button type="button" class="btn btn-success done_listing_btn" id="done_listing_btn" style="display: none;">DONE LISTING </button>

                                                                    </div>
                                                                </div>


                                                            </div>
                                                        </div>
                                                        <div class="divider-dashed"></div>





                                                    </form>





                                                </div>




                                                <div role="tabpanel" class="tab-pane fade" id="other_cost_tab" aria-labelledby="profile-tab">

                                                    <form autocomplete="off" class="form-horizontal form-label-left other_asset_calculcator_form" id="other_asset_calculcator_form">

                                                        <div class="col-sm-12">

                                                            <input type="hidden" id="other_job_card_id" class="other_job_card_id" name="other_job_card_id"/>
                                                            <div class="well">

                                                                <div class="form-group">
                                                                    <input type="text" name="other_job_card_no" class="form-control other_job_card_no" id="other_job_card_no" readonly=""/>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Description</label>
                                                                    <input type="text" name="asset_description" class="form-control asset_description" id="asset_description" placeholder="Description"/>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Amount</label>
                                                                    <input type="text" value="0" name="other_amount" class="form-control other_amount" id="other_amount" placeholder="Amount"/>
                                                                    <input type="hidden" name="other_days" class="other_days form-control" id="other_days" placeholder="Days" value="1"/>
                                                                    <input type="hidden" name="other_qty" class="other_qty form-control" id="other_qty" placeholder="Qty" value="1"/>
                                                                    <input type="hidden" name="other_discount" class="other_discount form-control" id="other_discount" placeholder="Discount" value="1"/>
                                                                </div>

                                                                <div class="ln_solid"></div>
                                                                <div class="form-group">
                                                                    <div class=" col-md-offset-3">
                                                                        <button type="submit" class="btn  btn-default other_asset_calculcator_btn" id="other_asset_calculcator_btn" name="other_asset_calculcator_btn">Add </button>    
                                                                        <button type="reset" class="btn btn-primary cancel_job_btn" id="cancel_job_btn">Cancel</button>
                                                                        <button type="button" class="btn btn-success done_listing_btn" id="done_listing_btn" style="display: none;">DONE LISTING </button>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </form>






                                                </div>

                                            </div>
                                        </div>



                                        <div class="well">
                                            <div class="form-group">

                                                <div id="booked_assets_div" class="booked_assets_div" style="display: inline;">

                                                    <table class="table table-bordered booked_assets_table"  id="booked_assets_table">
                                                        <thead>
                                                            <tr>

                                                                <th> Name</th>
                                                                <th>@-Price</th>
                                                                <th>Quantity</th>
                                                                <th>Discount</th>
                                                                <th>Total</th>

                                                            </tr>
                                                        </thead>
                                                        <tfoot>
                                                            <tr>

                                                                <th> Name</th>
                                                                <th>@-Price</th>
                                                                <th>Quantity</th>
                                                                <th>Discount</th>
                                                                <th>Total</th>

                                                            </tr>
                                                        </tfoot>
                                                        <tbody id="booked_assets_table_body" class="booked_assets_table_body">

                                                        </tbody>
                                                    </table>

                                                    <div class="ln_solid"></div>



                                                </div> 
                                            </div>




                                            <div class="form-group">
                                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">

                                                    <div class="generate_pro_forma_div" id="generate_pro_forma_div" style="display: none;">



                                                        <button type="button" class="btn  btn-default invoice_client_button" id="invoice_client_button" name="invoice_client_button">
                                                            <i class="fa fa-download"></i>
                                                            Generate Pro-forma Invoice </button> 


                                                        <button type="button" class="btn  btn-default show_listing_tab" id="show_listing_tab" name="show_listing_tab">
                                                            <i class="fa fa-download"></i>
                                                            Show Charging Option </button> 





                                                    </div>


                                                </div>
                                            </div>




                                            <div class="form-group">
                                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                    <input type="hidden" name="generated_inv_no" id="generated_inv_no" class="generated_inv_no"/>

                                                </div>
                                            </div>

                                        </div>





                                    </div>
                                </div>
                            </div>



                            <!-- Asset Calculator End -->




                            <!--  Calculator Plug-in Start -->

                            <div class="col-md-6 col-sm-12 col-xs-12 asset_calculcator_div_2" id="asset_calculcator_div_2" style="display: none;">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>  Calculator  <small></small></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class=" asset_calculcator_close_link_2" id="asset_calculcator_close_link_2"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">


                                        <div class="well">
                                            <p><input type="text" id="sciCalculator" placeholder="Scientific Calculator" class="form-control sciCalculator "></p>
                                        </div>




                                    </div>
                                </div>
                            </div>



                            <!--  Calculator Plug-in End -->




                            <!--  Job Card Information Start -->

                            <div class="col-md-6 col-sm-12 col-xs-12 asset_calculcator_div_3" id="asset_calculcator_div_3" style="display: none;">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>  Calculator  <small></small></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class=" asset_calculcator_close_link_3" id="asset_calculcator_close_link_3"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">


                                        <div class="well">
                                            <table class="table-bordered table-responsive">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Job Card No</th>
                                                        <th>Event Date</th>
                                                        <th>Estmtd Distance</th>
                                                        <th>Cnfrmd Distance</th>
                                                        <th>Venue</th>
                                                        <th>Delivery Date</th>
                                                        <th>Notes</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="job_card_information" class="job_card_information"></tbody>

                                            </table>
                                        </div>




                                    </div>
                                </div>
                            </div>



                            <!--  Job Card Information End -->









                            <!--Ajax Loader Div Start -->
                            <div id="ajax_loader_div" class="ajax_loader_div" style="display: none;">

                                <img src="<?php echo base_url(); ?>assets/images/ajax_loader/ajax-loader.gif" alt="Ajax Loader" height="42" width="42"> 
                                <hr>
                                <div class="alert alert-info">Please Wait While Information is Loaded</div>
                            </div>

                            <!-- Ajax Loader Div End -->







                            <!-- Add New Client Form start -->


                            <div class="row cancel_job_card_div" id="cancel_job_card_div" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Unique Loo  Cancel Client Job Card </h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                                </li>

                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <form id="cancel_job_card_form" class="cancel_job_card_form" >

                                                <p>
                                                    Are you sure you want to cancel Job Card No : 
                                                </p>
                                                <input type="text" class="form-control cancel_job_card_no" id="cancel_job_card_no" name="cancel_job_card_no" readonly=""/> <p>For Client : </p>
                                                <input type="text" class="form-control cancel_job_client_name" id="cancel_job_client_name" name="cancel_job_client_name"/>

                                            </form>    
                                        </div>





                                    </div>
                                </div>
                            </div>

                            <!-- Add New Client Form End --> 








                        </div>















                    </div>
                    <!-- /page content -->

                    <!-- footer content -->
                    <footer>
                        <div class="">
                            <p class="pull-right">Unique Loo ! by <a>Harris Dindi</a>. |
                                <span class="lead"> <i class="fa fa-paw"></i> Unique Loo!</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </footer>
                    <!-- /footer content -->

                </div>

            </div>
        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>



        <!-- Sweet Alert Library -->
        <script src="<?php echo base_url(); ?>assets/dist/sweetalert-dev.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
        <!--.......................-->



        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- chart js -->
        <script src="<?php echo base_url(); ?>assets/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="<?php echo base_url(); ?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>
        <!-- tags -->
        <script src="<?php echo base_url(); ?>assets/js/tags/jquery.tagsinput.min.js"></script>
        <!-- switchery -->
        <script src="<?php echo base_url(); ?>assets/js/switchery/switchery.min.js"></script>
        <!-- daterangepicker -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min2.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datepicker/daterangepicker.js"></script>
        <!-- richtext editor -->
        <script src="<?php echo base_url(); ?>assets/js/editor/bootstrap-wysiwyg.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/editor/external/jquery.hotkeys.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/editor/external/google-code-prettify/prettify.js"></script>
        <!-- select2 -->
        <script src="<?php echo base_url(); ?>assets/js/select/select2.full.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/parsley/parsley.min.js"></script>
        <!-- textarea resize -->
        <script src="<?php echo base_url(); ?>assets/js/textarea/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));
        </script>
        <!-- Autocomplete -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/autocomplete/countries.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/autocomplete/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>


        <script src="<?php echo base_url(); ?>assets/js/datatables/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>





        <!-- editor -->
        <script>
            $(document).ready(function () {
                $(".done_listing_btn").click(function () {
                    $(".generate_pro_forma_div").show();
                    $("#fixed_cost_tab").hide();
                    $("#other_cost_tab").hide();
                });


                $(".show_listing_tab").click(function () {
                    $(".generate_pro_forma_div").hide();
                    $("#fixed_cost_tab").show();
                    $("#other_cost_tab").show();
                });

                $('.cancel_job_btn').click(function () {

                    $(".book").hide('slow');
                    $(".ajax_loader_div").show('slow');
                    $(".book_job_div_list").show('slow');
                    $(".ajax_loader_div").hide('slow');

                    $(".asset_calculcator_div_1").hide();
                    $(".asset_calculcator_div_2").hide();
                    $(".asset_calculcator_div_3").hide();

                });


                $('.asset_calculcator_close_link_1').click(function () {

                    $(".book").hide('slow');
                    $(".ajax_loader_div").show('slow');
                    $(".book_job_div_list").show('slow');
                    $(".ajax_loader_div").hide('slow');

                    $(".asset_calculcator_div_1").hide();
                    $(".asset_calculcator_div_2").hide();
                    $(".asset_calculcator_div_3").hide();

                });

                $('.asset_calculcator_close_link_2').click(function () {

                    $(".book").hide('slow');
                    $(".ajax_loader_div").show('slow');
                    $(".book_job_div_list").show('slow');
                    $(".ajax_loader_div").hide('slow');

                    $(".asset_calculcator_div_1").hide();
                    $(".asset_calculcator_div_2").hide();
                    $(".asset_calculcator_div_3").hide();

                });

                $('.asset_calculcator_close_link_3').click(function () {

                    $(".book").hide('slow');
                    $(".ajax_loader_div").show('slow');
                    $(".book_job_div_list").show('slow');
                    $(".ajax_loader_div").hide('slow');

                    $(".asset_calculcator_div_1").hide();
                    $(".asset_calculcator_div_2").hide();
                    $(".asset_calculcator_div_3").hide();

                });







                // get_booked_jobs();



                function get_booked_jobs() {
                    booked_jobs_list = '';
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_booked_jobs",
                        dataType: "JSON",
                        success: function (response) {
                            $('#asset_calc_list').empty();
                            if (booked_jobs_list.response === "No Data!") {
                                booked_jobs_list.append("No  Booked Job Lists!");
                            } else {
                                for (i = 0; i < response.length; i++) {
                                    if (response[i].quote_review === "No") {
                                        booked_jobs_list += '<tr id="asset_calc_tr" class="asset_calc_tr flat"><td>' + response[i].client_name + '</td><td>' + response[i].job_card_no + '</td><td>' + response[i].event_date + '</td><td><button type="button" id="booked_jobs_btn" class="booked_jobs_btn btn btn-primary">Calculate Quotation</button><input type="text" id="job_card_id" name="job_card_id" value="' + response[i].job_card_id + '" class="form-control hidden job_card_id"/><input type="text" id="job_card_no" name="job_card_no" value="' + response[i].job_card_no + '" class="form-control hidden job_card_no"/></td></tr>';

                                    } else if (response[i].quote_review === "Yes") {
                                        booked_jobs_list += '<tr id="asset_calc_tr" class="asset_calc_tr flat"><td>' + response[i].client_name + '</td><td>' + response[i].job_card_no + '</td><td>' + response[i].event_date + '</td><td>(<span style="color:red">Quote to be Reviewed</span>)<button type="button" id="booked_jobs_btn" class="booked_jobs_btn btn btn-primary">Calculate Quotation</button><input type="text" id="job_card_id" name="job_card_id" value="' + response[i].job_card_id + '" class="form-control hidden job_card_id"/><input type="text" id="job_card_no" name="job_card_no" value="' + response[i].job_card_no + '" class="form-control hidden job_card_no"/></td></tr>';

                                    }
                                    // booked_jobs_list += '<tr id="asset_calc_tr" class="asset_calc_tr flat"><td>' + response[i].client_name + '</td><td>' + response[i].job_card_no + '</td><td>' + response[i].event_date + '</td><td><button type="button" id="booked_jobs_btn" class="booked_jobs_btn btn btn-primary">Calculate Quotation</button><input type="text" id="job_card_id" name="job_card_id" value="' + response[i].job_card_id + '" class="form-control hidden job_card_id"/><input type="text" id="job_card_no" name="job_card_no" value="' + response[i].job_card_no + '" class="form-control hidden job_card_no"/></td></tr>';

                                    $('#asset_calc_list').append(booked_jobs_list);
                                }

                            }



                        },
                        error: function (response) {

                        }
                    });

                }



                function get_job_details(job_card_id) {

                    booked_jobs_list = '';
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_job_information/" + job_card_id,
                        dataType: "JSON",
                        success: function (response) {
                            $('#job_card_information').empty();
                            if (booked_jobs_list.response === "No Data!") {
                                booked_jobs_list.append("No Job Card Information!");
                            } else {

                                booked_jobs_list += '<tr id="asset_calc_tr" class="asset_calc_tr flat"><td>' + response[0].client_name + '</td><td>' + response[0].job_card_no + '</td><td>' + response[0].event_date + '</td><td>' + response[0].estmtd_lctn_dstnce + '</td><td>' + response[0].cnfrmd_lctn_dstnce + '</td><td>' + response[0].venue + '</td><td>' + response[0].dte_srcd + '</td><td>' + response[0].notes + '</td></tr>';


                            }
                            $('#job_card_information').append(booked_jobs_list);

                            $(".book_job_div_list").hide();
                            $(".asset_calculcator_div_1").show();
                            $(".asset_calculcator_div_2").show();
                            $(".asset_calculcator_div_3").show();

                        },
                        error: function (response) {

                        }
                    });

                }


                 $(document).on('click', ".booked_jobs_btn", function () {
               

                    //get
                    var job_card_no = $(this).closest('tr').find('input[name="hidden_job_card_no"]').val();

                    var job_card_id = $(this).closest('tr').find('input[name="hidden_job_card_id"]').val();
                    
                    console.log("Job Card No : "+job_card_no+"Job Card Id : "+job_card_id+"");

                    $(".job_card_no").val(job_card_no);
                    $(".job_card_id").val(job_card_id);
                    $(".other_job_card_no").val(job_card_no);
                    $(".other_job_card_id").val(job_card_id);


                    get_job_details(job_card_id);


                });








                $(".refresh_job_table_data").click(function () {

                    get_booked_jobs();


                });













                $('#go_back_job_card').click(function () {
                    $(".add_new_client_div").hide('slow');
                    $(".ajax_loader_div").show('slow');
                    $(".book_job_div").show('slow');
                    $(".ajax_loader_div").hide('slow');
                });






                $('.cancel_job_btn_1').submit(function () {
                    var job_card_no = $('.job_card_id').val();

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>operations/delete_job_card/" + job_card_no,
                        dataType: 'JSON',
                        success: function (data) {
                            sweetAlert("...", "Job Deleted Successfully!", "error");
                            $(".book_job_div_list").show();
                            $(".asset_calculcator_div_1").hide();
                            $(".asset_calculcator_div_2").hide();
                            $(".asset_calculcator_div_3").hide();

                        }
                    });
                });



                $('.invoice_client_button').click(function () {
                    var job_card_no = $('.job_card_id').val();
                    $(".invoice_client_button").hide("slow");
                    $(".ajax_loader_div").show('slow');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>operations/invoice_client/" + job_card_no,
                        dataType: "JSON",
                        success: function (data) {
                            // data = JSON.parse(data);
                            var invoice_no = data[0].invoice_no;
                            $('.generated_inv_no').val(data[0].invoice_no);

                            swal({
                                title: "Success!",
                                text: "Invoice No :  " + invoice_no + "  generated successfully, you can now proceed to payments!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Okay!",
                                closeOnConfirm: true
                            },
                            function () {
                                $(".asset_calculcator_div_1").hide("slow");
                                $(".asset_calculcator_div_2").hide("slow");
                                $(".asset_calculcator_div_3").hide("slow");
                                $(".book_job_div").show("slow");
                                $(".select_client_name_div").show("slow");

                                $(".ajax_loader_div").hide('slow');

                                window.location.reload();

                            });


                        }
                    });
                });





                $(".add_new_client").click(function () {

                    $(".ajax_loader_div").show('slow');
                    $(".add_new_client_div").show('slow');
                    $(".ajax_loader_div").hide('slow');
                    $(".book_job_div").hide('slow');

                });


                $(".change_selected_client").click(function () {
                    $('.change_client_div').hide();
                    $('.select_client_name_div').show("slow");
                    $('.selected_client_name_div').hide("slow");

                });





                $(".reset_add_client_form").click(function () {
                    $(".ajax_loader_div").show('slow');
                    $(".add_new_client_div").hide('slow');
                    $(".ajax_loader_div").hide('slow');
                    $(".book_job_div").show('slow');

                });






                $('#asset_calculcator_form').submit(function (event) {
                    dataString = $("#asset_calculcator_form").serialize();
                    $(".ajax_loader_div").show('slow');

                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "<?php echo base_url() ?>operations/save_asset_value",
                        data: dataString,
                        success: function (data) {
                            sweetAlert("Great!...", "Asset Saved Successfully!", "success");

                            $(".book_job_div").hide('slow');
                            $(".booked_assets_div").show('slow');
                            $(".ajax_loader_div").hide('slow');
                            $(".done_listing_btn").show();

                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");
                            $(".book_job_div").show('slow');

                        }

                    });
                    event.preventDefault();
                    return false;
                });




                $('#other_asset_calculcator_form').submit(function (event) {
                    dataString = $("#other_asset_calculcator_form").serialize();
                    $(".ajax_loader_div").show('slow');

                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "<?php echo base_url() ?>operations/save_asset_value",
                        data: dataString,
                        success: function (data) {
                            sweetAlert("Great!...", "Asset Saved Successfully!", "success");

                            $(".book_job_div").hide('slow');
                            $(".booked_assets_div").show('slow');
                            $(".ajax_loader_div").hide('slow');


                        }, error: function (data) {
                            sweetAlert("Oops...", "Something went wrong!", "error");
                            $(".book_job_div").show('slow');

                        }

                    });
                    event.preventDefault();
                    return false;
                });







                $('.xcxc').click(function () {
                    $('#descr').val($('#editor').html());
                });
            });

            $(function () {
                function initToolbarBootstrapBindings() {
                    var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                        'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                        'Times New Roman', 'Verdana'],
                            fontTarget = $('[title=Font]').siblings('.dropdown-menu');
                    $.each(fonts, function (idx, fontName) {
                        fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
                    });
                    $('a[title]').tooltip({
                        container: 'body'
                    });
                    $('.dropdown-menu input').click(function () {
                        return false;
                    })
                            .change(function () {
                                $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                            })
                            .keydown('esc', function () {
                                this.value = '';
                                $(this).change();
                            });

                    $('[data-role=magic-overlay]').each(function () {
                        var overlay = $(this),
                                target = $(overlay.data('target'));
                        overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
                    });
                    if ("onwebkitspeechchange" in document.createElement("input")) {
                        var editorOffset = $('#editor').offset();
                        $('#voiceBtn').css('position', 'absolute').offset({
                            top: editorOffset.top,
                            left: editorOffset.left + $('#editor').innerWidth() - 35
                        });
                    } else {
                        $('#voiceBtn').hide();
                    }
                }
                ;

                function showErrorAlert(reason, detail) {
                    var msg = '';
                    if (reason === 'unsupported-file-type') {
                        msg = "Unsupported format " + detail;
                    } else {
                        console.log("error uploading file", reason, detail);
                    }
                    $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
                }
                ;
                initToolbarBootstrapBindings();
                $('#editor').wysiwyg({
                    fileUploadError: showErrorAlert
                });
                window.prettyPrint && prettyPrint();
            });

            $(document).ready(function () {






                setInterval(function () {
                    var get_client_id = $('.job_card_id').val();
                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_booked_assets/" + get_client_id,
                        dataType: "JSON",
                        success: function (booked_assets) {
                            booked_assets_list = $('#booked_assets_table_body').empty();

                            $.each(booked_assets, function (i, booked_asset) {
                                booked_assets_list.append("<tr><td>" + booked_asset.name + "</td><td>" + booked_asset.price + "</td><td>" + booked_asset.qty + "</td><td>" + booked_asset.discount + "</td><td>" + booked_asset.total_price + "</td></tr>");


                            });

                        },
                        error: function (data) {

                            // alert('An error occured, kindly try later');
                        }
                    });
                }, 3000);




            });




        </script>






        <!-- /editor -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

        <script>
            var j = jQuery.noConflict();
            j(document).ready(function () {



                j("#select_client_name").change(function () {
                    var client_id = this.value;

                    j.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>admin/get_client_details/" + client_id,
                        dataType: "JSON",
                        success: function (response) {
                            j(".client_e_mail").val(response[0].email);
                            j(".client_phone_no").val(response[0].phone_no);
                            j(".client_location").val(response[0].address);
                            j(".selected_client_name").val(response[0].name);
                            j(".client_id").val(client_id);

                            j('.select_client_name_div').hide("slow");
                            j('.selected_client_name_div').show("slow");
                            j('.change_client_div').show("slow");

                        }
                    });



                });


                j("#select_asset_name").change(function () {
                    j(".asset_quantity").val("");
                    j(".asset_total").val("");
                    j(".discount").val();
                    var client_id = this.value;

                    j.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>operations/get_asset_details/" + client_id,
                        dataType: "JSON",
                        success: function (response) {
                            j(".asset_id").val(response[0].id);
                            j(".asset_parameter").val(response[0].parameter);
                            j(".asset_value").val(response[0].value);
                        }
                    });
                });


                j('.asset_quantity').keyup(function () {
                    var quantity = j('.asset_quantity').val();
                    var value = j('.asset_value').val();
                    var discount = j('.discount').val();
                    var no_of_days = j(".asset_no_days").val();
                    var total_price = quantity * value * no_of_days - discount;

                    j('.asset_total').val(total_price);

                });



                j('.discount').keyup(function () {
                    var quantity = j('.asset_quantity').val();
                    var value = j('.asset_value').val();
                    var discount = j('.discount').val();
                    var no_of_days = j(".asset_no_days").val();
                    var total_price = quantity * value * no_of_days - discount;

                    j('.asset_total').val(total_price);

                });

                j('.asset_no_days').keyup(function () {
                    var quantity = j('.asset_quantity').val();
                    var value = j('.asset_value').val();
                    var discount = j('.discount').val();
                    var no_of_days = j(".asset_no_days").val();
                    var total_price = quantity * value * no_of_days - discount;

                    j('.asset_total').val(total_price);

                });





                j('.asset_quantity').click(function () {
                    var quantity = j('.asset_quantity').val();
                    var value = j('.asset_value').val();
                    var total_price = quantity * value;

                    j('.asset_total').val(total_price);

                });







            });

        </script>        


        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script>
            var x = jQuery.noConflict();
            x(function () {
                x("#delivery_date").datepicker(
                        {dateFormat: 'yy-mm-dd'}
                );


                x("#event_date").datepicker(
                        {dateFormat: 'yy-mm-dd'}
                );

            });
        </script>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

        <script>
            var j = jQuery.noConflict();
            j(document).ready(function () {
                j(function () {
                    j(".client_name").autocomplete({
                        source: "<?php echo site_url(); ?>operations/get_active_clients", // path to the get_countries method

                    });
                    j(".other_job_card_info").show();


                });






            });
        </script>tb


        <!-- Calculator CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery.calculator/jquery.calculator.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.calculator/jquery.plugin.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.calculator/jquery.calculator.js"></script>
        <script>
            var calc = jQuery.noConflict();
            calc(function () {
                calc.calculator.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: 'calculator.png'});
                calc('#basicCalculator').calculator();
                calc('#sciCalculator').calculator({layout: calc.calculator.scientificLayout});
            });
        </script>



        
        
        
        
        
        
         
         <link href="<?php echo base_url(); ?>assets/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/datatables/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/tools/js/dataTables.tableTools.js"></script>

        <script>
            var table = jQuery.noConflict();
           

            var asInitVals = new Array();
            table(document).ready(function () {
                var oTable = table('.booked_jobs_list_table').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0]
                        } //disables sorting for column one
                    ],
                    'iDisplayLength': 12,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "<?php echo base_url('assets2/js/datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
                    }
                });
                table("tfoot input").keyup(function () {
                    /* Filter on the column based on the index of this element's parent <th> */
                    oTable.fnFilter(this.value, table("tfoot th").index(table(this).parent()));
                });
                table("tfoot input").each(function (i) {
                    asInitVals[i] = this.value;
                });
                table("tfoot input").focus(function () {
                    if (this.className == "search_init") {
                        this.className = "";
                        this.value = "";
                    }
                });
                table("tfoot input").blur(function (i) {
                    if (this.value == "") {
                        this.className = "search_init";
                        this.value = asInitVals[table("tfoot input").index(this)];
                    }
                });
            });
        </script>
        
        




    </body>

</html>