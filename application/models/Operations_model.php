<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/* Author: Harris Samuel Dind
 * Description: Operations  model class
 */

class Operations_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function update_job_card($client_id, $delivery_date, $event_date, $location, $est_location_dstnce, $cnfrmd_location_dstnce, $time_from, $time_to, $edit_job_card, $edit_job_card_no, $booked_by_id, $notes, $venue) {
        $today = date("Y-m-d H:i:s");
        $quoted = "No";

        $this->db->trans_start();





        $get_client_id = $this->db->query("Select id from client where name='$client_id'");
        foreach ($get_client_id->result() as $value) {
            $client_id = $value->id;

            if (empty($edit_job_card)) {

                // $job_card_no = $this->job_card_no_generator();

                $job_card_checker = $this->year_month_checker();

                if ($job_card_checker === "Empty") {
                    // echo 'Empty';
                    $month = date('m');
                    $year = date('y');
                    $number = "1";
                    $jc = "JC";
                    $hyphen = "-";
                    $new_job_card_no = $jc . $hyphen . $year . $month . $hyphen . $number;


                    $data_insert = array(
                        'clnt_id' => $client_id,
                        'dte_srcd' => $delivery_date,
                        'dte_evnt' => $event_date,
                        'lctn' => $location,
                        'date_added' => $today,
                        'estmtd_lctn_dstnce' => $est_location_dstnce,
                        'cnfrmd_lctn_dstnce' => $cnfrmd_location_dstnce,
                        'time_from' => $time_from,
                        'time_to' => $time_to,
                        'job_card_no' => $new_job_card_no,
                        'user_id' => $booked_by_id,
                        'notes' => $notes,
                        'venue' => $venue,
                        'quoted' => $quoted
                    );
                    $this->db->insert('job_card', $data_insert);
                    $last_insert = $this->db->insert_id();
                } else {
                    foreach ($job_card_checker as $value) {
                        $job_id = $value->id;
                        $sql = $this->db->query("select job_card_no, invoice_no from job_card where id='$job_id'");
                        foreach ($sql->result() as $value) {
                            $job_card_no = $value->job_card_no;
                            $break_job_card_no = explode("-", $job_card_no);
                            $jc = $break_job_card_no[0];
                            $yymm = $break_job_card_no[1];
                            $ai_no = $break_job_card_no[2];
                            $new_ai_no = $ai_no + 1;
                            $hyphen = "-";

                            $new_job_card_no = $jc . $hyphen . $yymm . $hyphen . $new_ai_no;



                            $data_insert = array(
                                'clnt_id' => $client_id,
                                'dte_srcd' => $delivery_date,
                                'dte_evnt' => $event_date,
                                'lctn' => $location,
                                'date_added' => $today,
                                'estmtd_lctn_dstnce' => $est_location_dstnce,
                                'cnfrmd_lctn_dstnce' => $cnfrmd_location_dstnce,
                                'time_from' => $time_from,
                                'time_to' => $time_to,
                                'job_card_no' => $new_job_card_no,
                                'user_id' => $booked_by_id,
                                'notes' => $notes,
                                'venue' => $venue
                            );
                            $this->db->insert('job_card', $data_insert);
                            $last_insert = $this->db->insert_id();
                        }
                    }
                }
            } else {

                $data_update = array(
                    'clnt_id' => $client_id,
                    'dte_srcd' => $delivery_date,
                    'dte_evnt' => $event_date,
                    'lctn' => $location,
                    'date_added' => $today,
                    'estmtd_lctn_dstnce' => $est_location_dstnce,
                    'cnfrmd_lctn_dstnce' => $cnfrmd_location_dstnce,
                    'time_from' => $time_from,
                    'time_to' => $time_to,
                    'job_card_no' => $edit_job_card_no,
                    'notes' => $notes,
                    'venue' => $venue
                );

                $this->db->where('id', $edit_job_card);
                $this->db->update('job_card', $data_update);
                $last_insert = $edit_job_card;
            }
        }


        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $last_insert;
        }
    }

    function book_client($client_id, $delivery_date, $event_date, $location, $est_location_dstnce, $cnfrmd_location_dstnce, $time_from, $time_to, $edit_job_card, $edit_job_card_no, $booked_by_id, $notes, $venue) {
        $today = date("Y-m-d H:i:s");
        $quoted = "No";

        $this->db->trans_start();
        $get_client_id = $this->db->query("Select id from client where name='$client_id'");
        foreach ($get_client_id->result() as $value) {
            $client_id = $value->id;

            if (empty($edit_job_card)) {

                // $job_card_no = $this->job_card_no_generator();

                $job_card_checker = $this->year_month_checker();

                if ($job_card_checker === "Empty") {
                    //echo 'Empty';
                    $month = date('m');
                    $year = date('y');
                    $number = "1";
                    $jc = "JC";
                    $hyphen = "-";
                    $new_job_card_no = $jc . $hyphen . $year . $month . $hyphen . $number;


                    $data_insert = array(
                        'clnt_id' => $client_id,
                        'dte_srcd' => $delivery_date,
                        'dte_evnt' => $event_date,
                        'lctn' => $location,
                        'date_added' => $today,
                        'estmtd_lctn_dstnce' => $est_location_dstnce,
                        'cnfrmd_lctn_dstnce' => $cnfrmd_location_dstnce,
                        'time_from' => $time_from,
                        'time_to' => $time_to,
                        'job_card_no' => $new_job_card_no,
                        'user_id' => $booked_by_id,
                        'notes' => $notes,
                        'venue' => $venue,
                        'quoted' => $quoted,
                        'quote_review' => "No"
                    );
//                    $this->db->insert('job_card', $data_insert);
//                    $last_insert = $this->db->insert_id();
                } else {
                    foreach ($job_card_checker as $value) {
                        $job_id = $value->id;
                        $sql = $this->db->query("select job_card_no, invoice_no from job_card where id='$job_id'");
                        foreach ($sql->result() as $value) {
                            $job_card_no = $value->job_card_no;
                            $break_job_card_no = explode("-", $job_card_no);
                            $jc = $break_job_card_no[0];
                            $yymm = $break_job_card_no[1];
                            $ai_no = $break_job_card_no[2];
                            $new_ai_no = $ai_no + 1;
                            $hyphen = "-";

                            $new_job_card_no = $jc . $hyphen . $yymm . $hyphen . $new_ai_no;



                            $data_insert = array(
                                'clnt_id' => $client_id,
                                'dte_srcd' => $delivery_date,
                                'dte_evnt' => $event_date,
                                'lctn' => $location,
                                'date_added' => $today,
                                'estmtd_lctn_dstnce' => $est_location_dstnce,
                                'cnfrmd_lctn_dstnce' => $cnfrmd_location_dstnce,
                                'time_from' => $time_from,
                                'time_to' => $time_to,
                                'job_card_no' => $new_job_card_no,
                                'user_id' => $booked_by_id,
                                'notes' => $notes,
                                'venue' => $venue,
                                'quoted' => $quoted,
                                'quote_review' => "No"
                            );
//                            $this->db->insert('job_card', $data_insert);
//                            $last_insert = $this->db->insert_id();
                        }
                    }
                }
                $this->db->insert('job_card', $data_insert);
                $last_insert = $this->db->insert_id();
            } else {

                $data_update = array(
                    'clnt_id' => $client_id,
                    'dte_srcd' => $delivery_date,
                    'dte_evnt' => $event_date,
                    'lctn' => $location,
                    'date_added' => $today,
                    'estmtd_lctn_dstnce' => $est_location_dstnce,
                    'cnfrmd_lctn_dstnce' => $cnfrmd_location_dstnce,
                    'time_from' => $time_from,
                    'time_to' => $time_to,
                    'job_card_no' => $edit_job_card_no,
                    'notes' => $notes,
                    'venue' => $venue,
                    'quoted' => $quoted,
                    'quote_review' => "Yes"
                );

                $this->db->where('id', $edit_job_card);
                $this->db->update('job_card', $data_update);
                $last_insert = $edit_job_card;
            }
        }


        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $last_insert;
        }
    }

    //CREATED JOB CARD NO
    function created_job_card_no($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("SELECT client.name as client_name,client.id as client_id, job_card.id as job_card_id, job_card.job_card_no,date_format(job_card.dte_srcd,'%d-%b-%Y') as delivery_date,lctn,venue,estmtd_lctn_dstnce,cnfrmd_lctn_dstnce ,CONCAT('FROM : ', date_format(job_card.time_from,'%h:%i %p, %d-%b-%Y') ,' TO :', date_format(job_card.time_to,'%h:%i %p, %d-%b-%Y')) as event_date, job_card.dte_srcd,job_card.lctn,job_card.estmtd_lctn_dstnce,job_card.cnfrmd_lctn_dstnce,job_card_no,notes, CONCAT(employee.f_name,' ',employee.s_name,' ',employee.o_name) as employee_name , employee.id as employee_id, employee_no FROM `job_card` inner join client on client.id = job_card.clnt_id inner join employee on employee.id = job_card.user_id where  job_card.status='Active'  and job_card.id='$job_card_id' order by job_card_no ASC");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    //GET JOB CARD DETAILS
    function get_job_card_detail($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("select client.id as client_id , client.name as client_name, job_card.id as job_card_id, job_card.cnfrmd_lctn_dstnce, job_card.date_added, job_card.dte_evnt,job_card.dte_srcd,job_card.estmtd_lctn_dstnce,job_card.cnfrmd_lctn_dstnce,job_card.invoiced,job_card.invoice_no, job_card.job_card_no,job_card.lctn,job_card.notes,job_card.quoted,job_card.status,job_card.time_from, job_card.time_to,job_card.time_stamp,job_card.user_id,job_card.venue ,employee.id as employee_id , concat(f_name,' ',s_name,' ',o_name) as employee_name, employee.employee_no from job_card inner join client on client.id = job_card.clnt_id inner join employee on employee.id = job_card.user_id where job_card.id='$job_card_id'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    //JOB CARD GENERATOR FUCNTION BEGIN
    public function last_job_card_no() {
        $this->db->trans_start();
        $query = $this->db->query("select max(id) as last_inserted_id from job_card");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            foreach ($query->result() as $value) {
                $last_inserted_id = $value->last_inserted_id;
                return $last_inserted_id;
            }
        }
    }

    public function job_card_no_generator() {
        $this->db->trans_start();
        $last_inserted_id = $this->last_job_card_no();
        if (empty($last_inserted_id)) {
            $new_job_card_no = "JC-1-1";
        } else {
            $query = $this->db->query("select job_card_no as last_job_card_no from job_card where id = '$last_inserted_id'");
            $num_rows = $query->num_rows();
            if ($num_rows >= 1) {
                foreach ($query->result() as $value) {
                    $get_last_job_card_no = $value->last_job_card_no;
                    if (empty($get_last_job_card_no)) {
                        $new_job_card_no = "JC-1-1";
                    } else {
                        $break_job_card_data = explode("-", $get_last_job_card_no);

                        $chunk_one = $break_job_card_data[0];
                        $chunk_two = $break_job_card_data[1];
                        $chunk_three = $break_job_card_data[2];
                        $high_value = 9999;
                        $low_value = 1;
                        $base_ltr = "JC";
                        $seperator = "-";
                        if (empty($get_last_job_card_no)) {
                            $new_job_card_no = "JC-1-1";
                        } else {
                            if ($chunk_three === "999") {
                                $new_chunk_two = $chunk_two + $low_value;
                                $new_job_card_no = $base_ltr . $seperator . $new_chunk_two . $seperator . $low_value;
                            } else {
                                $new_chunk_three = $chunk_three + $low_value;
                                $new_job_card_no = $base_ltr . $seperator . $chunk_two . $seperator . $new_chunk_three;
                            }
                        }
                    }
                }
            } else {

                $new_job_card_no = "JC-1-1";
            }
        }


        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $new_job_card_no;
        }
    }

    //JOB CARD GENERATOR FUNCTION END
    //JOB CARD NO  AND INVOICE NO YEAR AND MONTH CHECKER START

    function year_month_checker() {
        $this->db->trans_start();

        $month = date('m');
        $year = date('y');
        $sql = $this->db->query("Select max(id) as id from job_card where DATE_FORMAT(date_added,'%m')= '$month' and DATE_FORMAT(date_added,'%y')= '$year' HAVING MAX(id) is not null");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            if ($sql->num_rows() > 0) {
                return $sql->result();
            } else {
                return "Empty";
            }
        }
    }

    //JOB CARD NO  AND INVOICE NO YEAR AND MONTH CHECKER START




















    function get_booked_assets($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("SELECT id,name,qty,price,client_id,date_added,timestamp,job_card_id , (qty*price) as total_price,discount FROM `asset_tracker` where job_card_id='$job_card_id'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function save_asset_value($job_card_id, $asset_name, $asset_price, $asset_quantity, $discount, $asset_no_days, $other_job_card_id, $asset_description, $other_amount, $other_days, $other_qty, $other_discount) {
        $this->db->trans_start();
        $today = date("Y-m-d H:i:s");
        if (empty($asset_description)) {
            $data_insert = array(
                'job_card_id' => $job_card_id,
                'name' => $asset_name,
                'price' => $asset_price,
                'qty' => $asset_quantity,
                'date_added' => $today,
                'discount' => $discount,
                'days' => $asset_no_days
            );
            $this->db->insert('asset_tracker', $data_insert);
        } else {
            $data_insert = array(
                'job_card_id' => $other_job_card_id,
                'name' => $asset_description,
                'price' => $other_amount,
                'qty' => $other_qty,
                'date_added' => $today,
                'discount' => $other_discount,
                'days' => $other_days
            );
            $this->db->insert('asset_tracker', $data_insert);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function view_booked_clients() {
        $this->db->trans_start();

        $query = $this->db->query("Select client.id as client_id , CONCAT('FROM : ', date_format(job_card.time_from,'%T, %d-%b-%Y') ,' TO :', date_format(job_card.time_to,'%T, %d-%b-%Y')) as event_date,client.name as client_name , job_card.job_card_no,job_card.id as job_card_id,  job_card.status  from job_card "
                . " inner join client on client.id = job_card.clnt_id inner join asset_tracker on asset_tracker.job_card_id = job_card.id"
                . " where  DATE(job_card.date_added) >= DATE(NOW()) and job_card.status='Active' group by job_card.id");


        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function view_completed_jobs() {
        $this->db->trans_start();


        $query = $this->db->query("Select client.id as client_id ,CONCAT('FROM : ', date_format(job_card.time_from,'%T, %d-%b-%Y') ,' TO :', date_format(job_card.time_to,'%T, %d-%b-%Y')) as event_date, client.name as client_name ,job_card.job_card_no, job_card.invoice_no,job_card.id as job_card_id,  job_card.status as job_status from job_card "
                . " inner join client on client.id = job_card.clnt_id"
                . " where  DATE(job_card.date_added) <= DATE(NOW()) and job_card.status='Active' group by job_card.id");


        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function my_future_booked_jobs($id) {
        $this->db->trans_start();
        $query = $this->db->query("Select client.id as client_id , client.name as client_name , job_card.job_card_no, job_card.invoice_no,job_card.id as job_card_id, (amount_cr-discount-amount_dr) as amount_owed, job_card.status as job_status from job_card "
                . "inner join client on client.id = job_card.clnt_id "
                . "inner join total_job_card_cost on total_job_card_cost.job_card_id = job_card.id"
                . "  where user_id='$id' and DATE(job_card.date_added) >= DATE(NOW()) group by job_card.id");

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function other_future_booked_jobs($id) {
        $this->db->trans_start();
        $query = $this->db->query("Select client.id as client_id , client.name as client_name , job_card.job_card_no, job_card.invoice_no,job_card.id as job_card_id, (amount_cr-discount-amount_dr) as amount_owed, job_card.status as job_status from job_card "
                . " inner join client on client.id = job_card.clnt_id"
                . " inner join total_job_card_cost on total_job_card_cost.job_card_id = job_card.id "
                . " where user_id !='$id' and DATE(job_card.date_added) >= DATE(NOW()) and total_job_card_cost.status='Active' group by job_card.id");

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function my_booked_jobs($id) {
        $this->db->trans_start();
        $query = $this->db->query("Select client.id as client_id , client.name as client_name , job_card.job_card_no, job_card.invoice_no,job_card.id as job_card_id, (amount_cr-discount-amount_dr) as amount_owed, job_card.status as job_status from job_card"
                . " inner join client on client.id = job_card.clnt_id"
                . " inner join total_job_card_cost on total_job_card_cost.job_card_id = job_card.id "
                . " where user_id='$id' and amount_cr-discount - amount_dr >0 and total_job_card_cost.status='Active' group by job_card.id");

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function other_booked_jobs($id) {
        $this->db->trans_start();
        $query = $this->db->query("Select client.id as client_id , client.name as client_name , job_card.job_card_no, job_card.invoice_no,job_card.id as job_card_id, (amount_cr-discount-amount_dr) as amount_owed, job_card.status as job_status from job_card"
                . "  inner join client on client.id = job_card.clnt_id"
                . " inner join total_job_card_cost on total_job_card_cost.job_card_id = job_card.id "
                . " where user_id !='$id' and amount_cr -discount -discount - amount_dr >0 and total_job_card_cost.status='Active' group by job_card.id");


        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function my_booked_clients() {
        $this->db->trans_start();
        $query = $this->db->query("Select client.id as client_id, client.name as client_name , "
                . "job_card.id as job_card_id, job_card.job_card_no as job_card_no , "
                . "job_card.invoice_no as invoice_no , sum(amount_cr - amount_dr-discount) as amount_due , job_card.date_added"
                . "  from job_card  inner join client on client.id = job_card.clnt_id"
                . " inner join total_job_card_cost on total_job_card_cost.job_card_id = job_card.id "
                . " where   amount_cr-discount - amount_dr > 0 and total_job_card_cost.status='Active'  group by job_card.id");

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function other_booked_clients($id) {
        $this->db->trans_start();
        $query = $this->db->query("Select client.id as client_id, client.name as client_name , "
                . "job_card.id as job_card_id, job_card.job_card_no as job_card_no , "
                . "job_card.invoice_no as invoice_no , sum(amount_cr - amount_dr) as amount_due , job_card.date_added"
                . "  from job_card  inner join client on client.id = job_card.clnt_id"
                . " inner join total_job_card_cost on total_job_card_cost.job_card_id = job_card.id "
                . " where job_card.user_id !='$id' and amount_cr-discount - amount_dr > 0  and total_job_card_cost.status='Active' group by job_card.id");

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_job_details($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("select client.id as client_id , client.name as client_name,job_card.id as job_card_id "
                . ",job_card.invoice_no, job_card.job_card_no, job_card.estmtd_lctn_dstnce, job_card.cnfrmd_lctn_dstnce, "
                . "job_card.dte_evnt,job_card.dte_srcd,statement.id as statement_id ,  sum(statement.amnt_cr-discount) as total_price "
                . " from client inner join job_card on job_card.clnt_id = client.id "
                . "inner join statement on statement.job_card_id = job_card.id"
                . " where job_card.id='$job_card_id' and job_card.status='Active' and statement.status='Active' group by job_card.job_card_no  ");
        $query = $this->db->query("Select client.id as client_id , client.name as client_name,job_card.id as job_card_id,job_card.invoice_no, job_card.job_card_no, job_card.estmtd_lctn_dstnce, job_card.cnfrmd_lctn_dstnce,job_card.dte_evnt,job_card.dte_srcd from job_card inner join client on job_card.clnt_id = client.id where job_card.id='$job_card_id'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_assgnd_assets($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("SELECT name,sum(qty) as no_of_assets,job_card_id FROM `asset_tracker` where job_card_id='$job_card_id' group by job_card_id,name ");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_invoice_job_details($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("select qty , description as asset_name, (amnt_cr-discount) as total_price, discount, (amnt_cr/qty) as value from statement where job_card_id='$job_card_id' and statement.status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function more_job_details($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("SELECT client.id as client_id ,client.name as client_name"
                . " , (asset_tracker.price*asset_tracker.qty)-discount as total_price , asset_tracker.id as asset_tracker_id ,"
                . " job_card.id as job_card_id , job_card.dte_evnt as event_date , job_card.dte_srcd as delivery_date , job_card.date_added as date_booked, job_card.no_units , job_card.job_card_no"
                . ",job_card.estmtd_lctn_dstnce , job_card.cnfrmd_lctn_dstnce , job_card.job_card_no , asset_tracker.qty, asset_tracker.price, job_card.dte_srcd,job_card.dte_evnt FROM `client`"
                . " inner join `job_card` on job_card.clnt_id = client.id "
                . "inner join asset_tracker on asset_tracker.job_card_id = job_card.id"
                . " where job_card.id='$job_card_id' ");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_remaining_assets($asset_type) {
        $this->db->trans_start();
        $query = $this->db->query("Select * from asset where type='$asset_type'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function check_assgnd_same_job($asset_name, $job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("Select status from assgnd_rsces where name='$asset_name' and job_card_id='$job_card_id'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            foreach ($query->result() as $value) {
                $status = $value->status;
                return $status;
            }
        }
    }

    function assign_asset($job_card_id, $job_card_id_1, $asset_value, $asset_name_1, $asset_name, $asset_type, $asset_type_1) {
        $this->db->trans_start();
        $today = date("Y-m-d H:i:s");

        if (empty($asset_value)) {
            $asset_value = 0;
        } else {
            
        }
        if (empty($asset_name_1)) {
            $data_insert = array(
                'job_card_id' => $job_card_id,
                'name' => $asset_name,
                'value' => $asset_value,
                'type' => $asset_type,
                'date_added' => $today
            );
        } elseif (empty($asset_name)) {
            $data_insert = array(
                'job_card_id' => $job_card_id_1,
                'name' => $asset_name_1,
                'value' => $asset_value,
                'type' => $asset_type_1,
                'date_added' => $today
            );
        }
        $this->db->insert('assgnd_rsces', $data_insert);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function get_booked_job_details($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("Select * from job_card where id = '$job_card_id'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function generate_invoice($job_card_id) {
        $this->db->trans_start();
        $sql = $this->db->query("Select * from job_card where job_card.id='$job_card_id'");
        foreach ($sql->result() as $value) {
            $job_card_no = $value->job_card_no;
            $invoice_no = $value->invoice_no;
            $invoiced = "Yes";
            $status = "Active";
            $data_update = array(
                'invoiced' => $invoiced,
                'invoice_no' => $invoice_no,
                'status' => $status
            );
            $this->db->where('job_card_id', $job_card_id);
            $this->db->update('statement', $data_update);

            $data_update = array(
                'invoiced' => $invoiced,
                'invoice_no' => $invoice_no,
                'status' => $status
            );
            $this->db->where('id', $job_card_id);
            $this->db->update('job_card', $data_update);

            $invoice_type = " Invoice";
            $email_invoice = $this->generate_random_key($job_card_id, $invoice_type);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $failed = "Invoice Failed!";
            return $failed;
        } else {
            $success = "Invoice Sent!";
            return $success;
        }
    }

    function generate_random_key($job_card_id, $invoice_type) {
        $this->db->trans_start();




        $query = $this->db->query("Select * from job_card where id = '$job_card_id' LIMIT 1");
        foreach ($query->result() as $value) {
            $id = $value->id;
            $job_card_no = $value->job_card_no;
            $invoice_no = $value->invoice_no;
            $client_id = $value->clnt_id;


            $query_clnt_details = $this->db->query("Select * from client where id = '$client_id'");
            foreach ($query_clnt_details->result() as $value) {
                $client_name = $value->name;
                $client_email = $value->email;






                $random = '';

                $random_1 = chr(rand(ord('0'), ord('999')));
                $random_2 = chr(rand(ord('A'), ord('Z')));
                $stamp = date("Ymdhis");
                $randomised_stamp = rand($stamp, 0123456789);
                $randomised_stamp = str_replace("-", "", $randomised_stamp);
                $randomised_stamp = strrev(str_replace("!@#$%^&*()|:}{<>?|/+=", "_", $randomised_stamp));
                $random .= $random_1 . $randomised_stamp . $random_2;
                $today = date("Y-m-d H:i:s");

                $data_insert = array(
                    'job_card_id' => $id,
                    'job_card_no' => $job_card_no,
                    'invoice_no' => $invoice_no,
                    'random_key' => $random,
                    'date_added' => $today,
                    'invoice_type' => $invoice_type
                );
                $this->db->insert('clnt_invoice', $data_insert);

                $random_key = $random;
                $base_url = $this->config->item('base_url');
                //configure email settings

                $config['mailtype'] = 'html';
                $config['wordwrap'] = TRUE;
                $config['charset'] = 'utf-8';
                $config['priority'] = '1';
                $config['newline'] = "\r\n"; //use double quotes here
                $this->email->initialize($config);
                $from_email = "info@uniqueloo.com";
                $name = "Unique Loo Mobile Toilets";
                $subject = "Client Job Invoice ";

                $mail_body = '
                                                                           ---------------------
                                                                           
      

                                                                           <br>
                                                                            
                                                                                Good Day  :' . $client_name . '!
                                                                                    <fieldset>
                                                                                A ' . $invoice_type . ' was generated for your Job , Below are the  details of your Job : 
                                                                                <hr>
                                                                                <table>
                                                                                <tbody>
                                                                                <tr><td>Job Card No :  ' . $job_card_no . '</td><td></td></tr>
                                                                                <tr><td>Invoice No : ' . $invoice_no . '</td><td></td></tr>
                                                                                </tbody><table>
                                                                                <br>
                                                                                ------------------------
                                                                                Please follow link to view the  Invoice and print it :<a href="' . $base_url . 'download/client_invoice/' . $random_key . '">DownLoad Invoice</a>
                                                                                

                                                                                ------------------------ ';



                //send mail
                $this->email->from($from_email, $name);
                $this->email->reply_to('info@uniqueloo.com', 'Unique Loo Mobile Toilets');
                $this->email->to('info@uniqueloo.com', 'Unique Loo Mobile Toilets');
                $this->email->cc($client_email, $client_name);
                $this->email->subject($subject);
                $this->email->message($mail_body);
                $message_type = "Proforma Invoice";
                if ($this->email->send(FALSE)) {
                    // mail sent
                    $output = $this->email->print_debugger();
                    //echo 'Output: ' . $output . '<br>';
                    $data_insert = array(
                        'output' => $output,
                        'job_card_no' => $job_card_no,
                        'type' => $message_type
                    );
                    $this->db->insert('mail_debugger', $data_insert);
                } else {
                    //error
                    $output = $this->email->print_debugger();
                    //echo 'Output: ' . $output . '<br>';
                    $data_insert = array(
                        'output' => $output,
                        'job_card_no' => $job_card_no,
                        'type' => $message_type
                    );
                    $this->db->insert('mail_debugger', $data_insert);
                }
            }
        }





        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    function invoice_client($job_card_no) {
        $this->db->trans_start();
        $job_card_id = $job_card_no;
        $query = $this->db->query("Select * from job_card where id = '$job_card_no' LIMIT 1");
        foreach ($query->result() as $value) {
            $id = $value->id;
            $clnt_id = $value->clnt_id;
            $dte_srcd = $value->dte_srcd;
            $dte_evnt = $value->dte_evnt;
            $lctn = $value->lctn;
            $estmtd_lctn_dstnce = $value->estmtd_lctn_dstnce;
            $cnfrmd_lctn_dstnce = $value->cnfrmd_lctn_dstnce;
            $no_units = $value->no_units;
            $job_card_no = $value->job_card_no;
            $time_from = $value->time_from;
            $time_to = $value->time_to;
            $ivoiced = $value->invoiced;

            $query_2 = $this->db->query("Select * from asset_tracker where job_card_id = '$id'");
            foreach ($query_2->result() as $value) {
                $name = $value->name;
                $qty = $value->qty;
                $price = $value->price;
                $status = $value->status;
                $date_added = $value->date_added;
                $invoiced = $value->invoiced;
                $discount = $value->discount;
                $amnt_cr = $price * $qty;
                $today = date("Y-m-d H:i:s");
                $invoiced = 'No';
                $quoted = 'Yes';
                $status = 'Active';
                $client_insert = array(
                    'clnt_id' => $clnt_id,
                    'job_card_id' => $id,
                    'amnt_cr' => $amnt_cr,
                    'date_added' => $today,
                    'description' => $name,
                    'invoiced' => $invoiced,
                    'status' => $status,
                    'qty' => $qty,
                    'discount' => $discount,
                    'quoted' => $quoted
                );
                $this->db->insert('statement', $client_insert);
                $last_statement_id = $this->db->insert_id();
            }

            $sql_select = $this->db->query("Select sum(amnt_cr) as amount_credited, sum(discount) as total_discount from statement where job_card_id='$id'and status='Active' group by job_card_id");
            foreach ($sql_select->result() as $value) {
                $amount_credited = $value->amount_credited;
                $total_discount = $value->total_discount;
                $total_cost = array(
                    'amount_cr' => $amount_credited,
                    'date_added' => $today,
                    'job_card_id' => $id,
                    'discount' => $total_discount,
                    'status'=>$status
                );
                $this->db->insert('total_job_card_cost', $total_cost);
            }
        }
        //$new_invoice_no = $this->invoice_no_generator();

        $new_invoice_no = "";
        $invoice_no_checker = $this->invoice_checker();
        //echo 'Invoice No : ' . $new_invoice_no . '<br>';

        if ($invoice_no_checker === "Empty") {
            // echo 'Empty';
            $month = date('m');
            $year = date('y');
            $number = "1";
            $jc = "INV";
            $hyphen = "-";
            $new_invoice_no = $jc . $hyphen . $year . $month . $hyphen . $number;
            //echo 'New Invoice  No : ' . $new_invoice_no . '</br>';
            $quoted = "Yes";
            $invoiced = "No";
            $insert_invoice_no = array(
                'invoice_no' => $new_invoice_no,
                'invoiced' => $invoiced,
                'quoted' => $quoted
            );

            $this->db->where('id', $job_card_id);
            $this->db->update('job_card', $insert_invoice_no);
            $new_invoice_no .= $new_invoice_no;
            //echo 'Invoice No : ' . $new_invoice_no . '<br>';
        } else {
            foreach ($invoice_no_checker as $value) {
                $max_invoice_no = $value->max_invoice_no;
                // echo 'Max Invoice No ' . $max_invoice_no . '<br>';
                $month = date('m');
                $year = date('y');
                $number = "1";
                $inv = "INV";
                $hyphen = "-";
                $new_number = $number + $max_invoice_no;
                $new_invoice_no = $inv . $hyphen . $year . $month . $hyphen . $new_number;
                //echo 'New Invoice No : ' . $new_invoice_no . '<br>';
                $quoted = "Yes";
                $invoiced = "No";
                $insert_invoice_no = array(
                    'invoice_no' => $new_invoice_no,
                    'invoiced' => $invoiced,
                    'quoted' => $quoted
                );


                $this->db->where('id', $job_card_id);
                $this->db->update('job_card', $insert_invoice_no);
            }
        }


        $invoice_type = "Pro-Forma Invoice";

        $this->generate_random_key($job_card_id, $invoice_type);



        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            // echo 'Invoice No : ' . $new_invoice_no . '<br>';
            return

                    $new_invoice_no;
        }
    }

    //LAST INVOICE CHECKER START
    function invoice_checker() {
        $this->db->trans_start();
        $month = date('m');
        $year = date('y');
        $sql = $this->db->query("SELECT MAX(CONVERT(SUBSTRING_INDEX(invoice_no,'-',-1),UNSIGNED INTEGER)) AS max_invoice_no  FROM `job_card` where DATE_FORMAT(date_added,'%m')= '$month' and DATE_FORMAT(date_added,'%y')= '$year' HAVING MAX(invoice_no) is not null");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            if ($sql->num_rows() > 0) {
                return $sql->result();
            } else {
                return "Empty";
            }
        }
    }

    //LAST INVOICE CHECKER END
    //INVOICE NUMBER GENERATOR FUCNTION BEGIN
    public function last_invoice_no() {
        $this->db->trans_start();
        $query = $this->db->query("select max(id) as last_inserted_id from job_card");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            foreach ($query->result() as $value) {
                $last_inserted_id = $value->last_inserted_id;

                return $last_inserted_id;
            }
        }
    }

    public function invoice_no_generator() {



        $this->db->trans_start();



        $last_inserted_id = $this->last_invoice_no();
        if (empty($last_inserted_id)) {
            $new_invoice_no = "INV-1-1";
        } else {

            $last_inserted_id = $last_inserted_id - 1;
            $query = $this->db->query("select invoice_no as last_invoice_no from job_card where id = '$last_inserted_id'");
            $num_rows = $query->num_rows();
            if ($num_rows >= 1) {

                foreach ($query->result() as $value) {
                    $get_last_invoice_no = $value->last_invoice_no;
                    if (empty($get_last_invoice_no)) {
                        $new_invoice_no = "INV-1-1";
                    } else {

                        $break_invoice_no_data = explode("-", $get_last_invoice_no);

                        $chunk_two = $break_invoice_no_data[1];
                        $chunk_three = $break_invoice_no_data[2];
                        $low_value = 1;
                        $base_ltr = "INV";
                        $seperator = "-";
                        if (empty($get_last_invoice_no)) {
                            $new_invoice_no = "INV-1-1";
                        } else {
                            if ($chunk_three === "999") {
                                $new_chunk_two = $chunk_two + $low_value;
                                $new_invoice_no = $base_ltr . $seperator . $new_chunk_two . $seperator . $low_value;
                            } else {
                                $new_chunk_three = $chunk_three + $low_value;
                                $new_invoice_no = $base_ltr . $seperator . $chunk_two . $seperator . $new_chunk_three;
                            }
                        }
                    }
                }
            } else {
                $new_invoice_no = "INV-1-1";
            }
        }


        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $new_invoice_no;
        }
    }

//INVOICE NUMBER GENERATOR FUNCTION END
    function delete_job_card($delete_job_card) {
        $this->db->trans_start();
        $status = "In Active";
        $data_update = array('status' => $status
        );
        $this->db->where('id', $delete_job_card);
        $this->db->update('job_card', $data_update);



        $this->db->where('job_card_id', $delete_job_card);
        $this->db->update('asset_tracker', $data_update);


        $this->db->where('job_card_id', $delete_job_card);
        $this->db->update('statement', $data_update);
        $this->db->trans_complete();
        if ($this->db->trans_status() ===
                FALSE) {
            
        } else {
            
        }
    }

    function client_payment_list() {
        $this->db->trans_start();
        $query = $this->db->query("select statement.id as statement_id , job_card.id as job_card_id,job_card.job_card_no,job_card.invoice_no,(amnt_cr-discount-amnt_dr) as amount_due, client.name as client_name ,job_card.user_id  from statement inner join job_card on job_card.id = statement.job_card_id inner join client on client.id = job_card.clnt_id where statement.status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function my_client_payment_list($user_id) {
        $this->db->trans_start();
        $query = $this->db->query("select statement.id as statement_id , job_card.id as job_card_id,job_card.job_card_no,"
                . "job_card.invoice_no,(amnt_cr-discount-amnt_dr) as amount_due, client.name as client_name ,job_card.user_id "
                . " from statement inner join job_card on job_card.id = statement.job_card_id "
                . "inner join client on client.id = job_card.clnt_id where user_id='$user_id' and statement.status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_user_configurations() {
        $this->db->trans_start();
        $query = $this->db->query('Select * from conf limit 0,1');
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function client_info($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("Select client.id as client_id,client.name as client_name , client.address, client.website,client.phone_no,client.email,job_card.job_card_no,job_card.invoice_no from job_card inner join client on client.id = job_card.clnt_id where job_card.id='$job_card_id'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_job_sub_total($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("Select sum(amnt_cr) as total_job_price,sum(discount) as discount , statement.job_card_id , statement.id as statement_id from statement where job_card_id='$job_card_id' and statement.status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_payment_details($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("SELECT * FROM client_payment_list where job_card_id= '$job_card_id' ");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_blld_clnt_info($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("SELECT * FROM billed_client_info where job_card_id = '$job_card_id' ");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function check_balance_amnt($statement_id) {
        $this->db->trans_start();
        $query = $this->db->query("select amnt_dr as amount_dr from statement where id='$statement_id' and status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            foreach ($query->result() as $value) {
                $amount_paid = $value->amount_dr;

                return $amount_paid;
            }
        }
    }

    function submit_payment_form() {
        $this->db->trans_start();

        $asset_name = $this->input->post('asset_name');
        $quantity = $this->input->post('quantity');
        $amount_charged = $this->input->post('amount_charged');
        $amount_paid = $this->input->post('amount_paid');
        $job_card_no = $this->input->post('job_card_id');
        $invoice_no = $this->input->post('invoice_no');
        $statement_id = $this->input->post('statement_id');
        $temp = $this->input->post('job_card_id');
        $no_temp = count($temp);
        echo 'No temp : ' . $no_temp . '<br/>';

        $deposit_paid = $this->input->post('deposit_paid');
        $hidden_total_cost = $this->input->post('hidden_total_cost');
        $submit_amount_paid = $this->input->post("submit_amount_paid");
        $balance_remaining = $this->input->post("balance_remaining");
        $payment_method = $this->input->post("payment_method");
        $payment_code = $this->input->post("payment_code");
        $job_card_id = $this->input->post("submit_payments_job_card_id");
        $total_job_cost_id = $this->input->post("total_job_card_cost_id");
        $payment_date = $this->input->post('payment_date');
        $total_amount_paid = $deposit_paid + $submit_amount_paid;
        $status = "Active";
        $data_update = array(
            'pymnt_code' => $payment_code,
            'pymnt_mthd' => $payment_method,
            'amount_dr' => $total_amount_paid,
            'payment_date' => $payment_date,
            'status' => $status
        );
        $this->db->where('id', $total_job_cost_id);
        $this->db->update('total_job_card_cost', $data_update);


        for ($i = 0; $i < count($temp); $i++) {
            $status = 'Active';
            $data_update = array(
                'pymnt_code' => $payment_code,
                'pymnt_mthd' => $payment_method,
                'payment_date' => $payment_date,
                'status' => $status
            );
            $this->db->where('id', $job_card_id);
            $this->db->update('statement', $data_update);
        }
        $this->db->trans_complete();
        if (
                $this->db->trans_status() === FALSE) {
            
        }
    }

    function update_job_quotation() {
        $this->db->trans_start();

        $asset_name = $this->input->post('asset_name');
        $quantity = $this->input->post('quantity');
        $days = $this->input->post('days');
        $value_price = $this->input->post('value_price');
        $discount = $this->input->post('discount');
        $sub_total = $this->input->post('sub_total');
        $job_card_no = $this->input->post('job_card_no');
        $job_card_id = $this->input->post('job_card_id');
        $asset_tracker_id = $this->input->post('asset_tracker_id');
        $status = $this->input->post('status');
        $temp = $this->input->post('asset_tracker_id');
        $statement_id = $this->input->post('statement_id');
        $no_temp = count($temp);
        // echo 'No temp :  ' . $no_temp . '<br/>';




        for ($i = 0; $i < count($temp); $i++) {
            echo 'Asset Name : ' . $asset_name [$i] . '<br>Quantity : ' . $quantity [$i] . '<br>Days : ' . $days[$i] . '<br>Value:' . $value_price [$i] . '<br>Discount : ' . $discount [$i] . '<br>Sub Total : ' . $sub_total [$i] . '<br>Job Card No : ' . $job_card_no[$i] . '<br>Job Card Id : ' . $job_card_id[$i] . '<br>Asset Tracker Id : ' . $asset_tracker_id [$i] . '<br>Status : ' . $status[$i] . '<br>Statement Id:' . $statement_id[$i] . '<br>';

            $amount_cr = $value_price [$i] * $quantity[$i] * $days[$i];
            echo 'Amount Cr' . $amount_cr . '<br>';
            echo 'Stmnt id' . $statement_id [$i] . '<br>';
            echo 'Job card id' . $job_card_id[$i] . '<br>';
            $data_update = array(
                'name' => $asset_name[$i],
                'qty' => $quantity[$i],
                'days' => $days[$i],
                'price' => $amount_cr,
                'discount' => $discount[$i],
                'job_card_id' => $job_card_id[$i],
                'status' => $status[$i]
            );
            $this->db->where('id', $asset_tracker_id[$i]);
            $this->db->update('asset_tracker', $data_update);
            $statement_update = array(
                'description' => $asset_name[$i],
                'qty' => $quantity[$i],
                'days' => $days[$i],
                'amnt_cr' => $amount_cr,
                'discount' => $discount[$i],
                'job_card_id' => $job_card_id[$i],
                'status' => $status[$i]
            );
            $this->db->where('id', $statement_id[$i]);
            $this->db->update('statement', $statement_update);

            $sql_select = $this->db->query("Select sum(amnt_cr) as amount_credited, sum(discount) as total_discount from statement where job_card_id='$job_card_id[$i]' and statement.status='Active' group by job_card_id");
            foreach ($sql_select->result() as $value) {
                $amount_credited = $value->amount_credited;
                $total_discount = $value->total_discount;
                echo 'Amount credited' . $amount_credited . '<br>';
                $status = "Active";
                $total_job_card_cost_update = array(
                    'amount_cr' => $amount_credited,
                    'discount' => $total_discount,
                    'status' => $status
                );
                $this->db->where('job_card_id', $job_card_id[$i]);
                $this->db->update('total_job_card_cost', $total_job_card_cost_update);
                echo 'End of Transation No : ' . $statement_id[$i] . '<br> <hr>';
            }
        }





        $this->db->trans_complete();
        if ($this->db->trans_status() ===
                FALSE) {
            
        } else {
            
        }
    }

    function get_job($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("SELECT * FROM billed_client_info where job_card_id = '$job_card_id' ");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_available_assets($type) {
        $this->db->trans_start();
        $query = $this->db->query("Select * from asset where booked='Not Booked' and status='Active' and type='$type' ");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function assign($job_card_id, $asset_name, $asset_value) {
        $this->db->trans_start();
        $status = "Active";
        $today = date("Y-m-d H:i:s");

        $data_insert = array(
            'job_card_id' => $job_card_id,
            'name' => $asset_name,
            'value' => $asset_value,
            'status' => $status,
            'date_added' => $today
        );
        $this->db->insert('assgnd_rsces', $data_insert);
        $this->db->trans_complete();
        if ($this->db->trans_status() ===
                FALSE) {
            
        } else {
            
        }
    }

    public function job_cards_per_client() {
        $this->db->trans_start();
        $query = $this->bd->query("SELECT * FROM job_cards_client");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    public function revenue_collected() {
        $this->db->trans_start();
        $query = $this->bd->query("SELECT * FROM revenue_collected");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    public function expenses_incurred() {
        $this->db->trans_start();
        $query = $this->bd->query("SELECT * FROM expenses_incurred");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    public function revenue_per_client() {
        $this->db->trans_start();
        $query = $this->db->query("SELECT * FROM revenue_per_client");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    public function job_per_client() {
        $this->db->trans_start();
        $query = $this->db->query("SELECT * FROM job_per_client");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    public function expenses_per_job_card() {
        $this->db->trans_start();
        $query = $this->db->query("SELECT * FROM expenses_per_job_card");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    public function revenue_per_job_card() {
        $this->db->trans_start();
        $query = $this->db->query("SELECT * FROM revenue_per_job_card");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function total_clients() {
        $this->db->trans_start();
        $query = $this->db->query('Select * from total_clients ');
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function total_jobs() {
        $this->db->trans_start();
        $query = $this->db->query('Select * from total_jobs');
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function total_revenue() {
        $this->db->trans_start();
        $query = $this->db->query('Select * from total_revenue');
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function total_expenses() {
        $this->db->trans_start();
        $query = $this->db->query('Select * from total_expenses');
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function active_users() {
        $this->db->trans_start();
        $query = $this->db->query('Select * from active_users');
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function daily_revenue() {
        $this->db->trans_start();
        $query = $this->db->query(" select amnt_cr-discount  as expense,date_added as date , date(date_added) as day , month(date_added) as month ,year(date_added) as year   from statement where statement.status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function daily_expense() {
        $this->db->trans_start();
        $query = $this->db->query(" select amnt_cr  as expense,date_added as date , date(date_added) as day, month(date_added) as month ,year(date_added) as year    from statement where statement.status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_date() {
        $this->db->trans_start();
        $query = $this->db->query(" select amnt_dr  as revenue,amnt_cr as expense,date_added as date , date(date_added) as day, month(date_added) as month ,year(date_added) as year    from statement where statement.status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function getdstnct_date() {
        $this->db->trans_start();
        $query = $this->db->query(" select distinct date_added as date from statement where statement.status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function daily_report() {
        $this->db->trans_start();
        $query = $this->db->query("select client.name as client_name , job_card.job_card_no,job_card.invoice_no, date_format(job_card.date_added,'%T, %d-%b-%Y')as date_added , month(dte_evnt) as month , year(dte_evnt) as year,amount_dr as total_revenue  , date_format(dte_evnt,'%d-%b-%Y') as dte_evnt, CONCAT('FROM : ', date_format(job_card.time_from,'%T, %d-%b-%Y') ,' TO :', date_format(job_card.time_to,'%T, %d-%b-%Y')) as event_date  from client inner join job_card on job_card.clnt_id = client.id inner join total_job_card_cost on total_job_card_cost.job_card_id = job_card.id where total_job_card_cost.status='Active'  group by job_card.id");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function job_status() {
        $this->db->trans_start();
        $query = $this->db->query("select * from job_card_status");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function client_payment_statement() {
        $this->db->trans_start();
        $query = $this->db->query("select * from clnt_pymnt_stmnt_rprt order by job_card_id DESC");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_client_lists() {
        $this->db->trans_start();
        $query = $this->db->query("select id,name from client where status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function release_job_assets($release_job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("Select id from job_card where id ='$release_job_card_id'");
        foreach ($query->result() as $value) {
            $id = $value->id;
            $in_active = "In Active";
            $data_update = array(
                'status' => $in_active
            );
            $this->db->where('id', $id);
            $this->db->update('job_card', $data_update);

            $query = $this->db->query("Select name, job_card_id from assgnd_rsces where job_card_id ='$release_job_card_id'");
            foreach ($query->result() as $value) {
                $asset_name = $value->name;
                $job_card_id = $value->job_card_id;
                $inactive = "In Active";
                $data_update_job_card = array(
                    'status' => $inactive
                );
                $this->db->where('id', $job_card_id);
                $this->db->update('job_card', $data_update_job_card);

//                $query_2 = $this->db->query("Select id from asset where number ='$asset_name'");
//                foreach ($query_2->result() as $value) {
//                    $id = $value->id;
//                    $not_booked = "Not Booked";
//                    $data_update = array(
//                        'booked' => $not_booked
//                    );
//                    $this->db->where('id', $id);
//                    $this->db->update('asset', $data_update);
//                }
            }
        }


        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {

            return FALSE;
        } else {
            return TRUE;
        }
    }

    function check_user_existence($user) {
        $this->db->trans_start();
        $query = $this->db->query("select employee_no from employee where id='$user'");
        $row = $query->row();


        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        } else {
            if (isset($row)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function get_total_job_value($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("select * from total_job_card_cost where job_card_id='$job_card_id' and status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function un_approved_pymnts() {
        $this->db->trans_start();
        $query = $this->db->query("Select client.id as client_id, client.name as client_name , "
                . "job_card.id as job_card_id, job_card.job_card_no as job_card_no , "
                . "job_card.invoice_no as invoice_no , sum(amount_cr-discount - amount_dr) as amount_due , job_card.date_added"
                . "  from job_card  inner join client on client.id = job_card.clnt_id"
                . " inner join total_job_card_cost on total_job_card_cost.job_card_id = job_card.id "
                . " where  approved='Not Approved' and total_job_card_cost.status='Active' group by job_card.id");

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function approve_transaction($job_card_id) {
        $this->db->trans_start();
        $approved = "Approved";
        $query_1 = $this->db->query("Select id from statement where job_card_id='$job_card_id' and status='Active'");
        foreach ($query_1->result() as $value) {
            $id = $value->id;
            $data_update = array(
                'approved' => $approved
            );
            $this->db->where('id', $id);
            $this->db->update('statement', $data_update);
        }

        $query_2 = $this->db->query("Select id from total_job_card_cost where job_card_id='$job_card_id' and status='Active'");
        foreach ($query_2->result() as $value) {
            $id = $value->id;
            $status = "Active";
            $data_update = array(
                'approved' => $approved,
                'status'=>$status
            );
            $this->db->where('id', $id);
            $this->db->update('total_job_card_cost', $data_update);
        }
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {

            return FALSE;
        } else {
            return TRUE;
        }
    }

    function get_assigned_assets($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("SELECT * FROM `assgnd_rsces` where job_card_id='$job_card_id'");

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_job_card_details($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("Select * from client inner join job_card on  client.id = job_card.clnt_id inner join statement on statement.job_card_id = job_card.id inner join total_job_card_cost on total_job_card_cost.job_card_id = job_card.id where job_card.id='$job_card_id' and statement.status='Active' and total_job_card_cost.status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function future_jobs_report() {
        $this->db->trans_start();
        $query = $this->db->query("Select DISTINCT client.name as client_name , client.phone_no , job_card.job_card_no , job_card.invoice_no ,CONCAT('FROM : ', date_format(job_card.time_from,'%h:%i %p, %d-%b-%Y') ,' TO :', date_format(job_card.time_to,'%h:%i %p, %d-%b-%Y')) as dte_evnt from client inner join job_card on  client.id = job_card.clnt_id  where DATE(job_card.dte_evnt) >= DATE(NOW())  group by job_card_no order by dte_evnt DESC");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function summarised_future_jobs_data() {
        $this->db->trans_start();
        $query = $this->db->query("Select count(job_card.id) as no_job_cards , date_format(time_from,' %d-%b-%Y') as dte_evnt  from job_card  where date_format(job_card.time_from,'%Y-%m-%d') >= DATE(NOW()) group by job_card.dte_evnt");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function event_job_details() {
        $this->db->trans_start();
        $query = $this->db->query("select client.name as client_name , job_card_no, dte_evnt as event_date,notes as remarks , time_from , time_to,date_format(job_card.time_from,'%Y-%m-%d %H:%i:%s'),DATE(NOW()) from client inner join job_card on job_card.clnt_id = client.id where date_format(job_card.time_from,'%Y-%m-%d') >= DATE(NOW())  order by time_from DESC");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function job_card_report() {
        $this->db->trans_start();
        $query = $this->db->query("SELECT client.name as client_name,client.id as client_id, job_card.id as job_card_id, job_card.job_card_no,date_format(job_card.dte_srcd,'%d-%b-%Y') as delivery_date,lctn,venue,estmtd_lctn_dstnce,cnfrmd_lctn_dstnce ,CONCAT('FROM : ', date_format(job_card.time_from,'%h:%i %p, %d-%b-%Y') ,' TO :', date_format(job_card.time_to,'%h:%i %p, %d-%b-%Y')) as event_date, job_card.dte_srcd,job_card.lctn,job_card.estmtd_lctn_dstnce,job_card.cnfrmd_lctn_dstnce,job_card_no,notes, CONCAT(employee.f_name,' ',employee.s_name,' ',employee.o_name) as employee_name , employee.id as employee_id, employee_no FROM `job_card` inner join client on client.id = job_card.clnt_id inner join employee on employee.id = job_card.user_id where  job_card.status='Active' order by job_card_no ASC");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function payment_report() {
        $this->db->trans_start();
        $query = $this->db->query("Select client.id as client_id, client.name as client_name , client.phone_no as phone_no,job_card.id as job_card_id ,job_card.job_card_no , job_card.invoice_no,total_job_card_cost.amount_cr as amount_billed, total_job_card_cost.amount_dr as amount_paid,(amount_cr - amount_dr) as balance,time_from, date_format(total_job_card_cost.payment_date,'%d-%b-%Y')  as payment_date ,CONCAT('FROM : ', date_format(job_card.time_from,'%h:%i %p, %d-%b-%Y') ,' TO :', date_format(job_card.time_to,'%h:%i %p, %d-%b-%Y')) as event_date,date_format(job_card.dte_srcd,'%d-%b-%Y') as date_srcd from client inner join job_card on job_card.clnt_id = client.id inner join total_job_card_cost on total_job_card_cost.job_card_id = job_card.id where job_card.status='Active' and total_job_card_cost.status='Active' group by job_card.id order by date_format(total_job_card_cost.timestamp,'%h:%i %p, %d-%b-%Y') DESC ");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function asset_status() {
        $query = $this->db->query("SELECT count(job_card_no) as job_card_no,number,asset.type,asset.status , asset.id as asset_id FROM  asset inner join assgnd_rsces on assgnd_rsces.name = asset.id inner join job_card on job_card.id = assgnd_rsces.job_card_id");

        $result = $query->result_array();
        return $result;
    }

    function delete_job_card_details($job_card_id) {
        $this->db->trans_start();
        $in_active = "In Active";
        $data_update = array('status' => $in_active
        );
        $this->db->where('id', $job_card_id);
        $this->db->update('job_card', $data_update);
        $query = $this->db->query("select id from statement where job_card_id='$job_card_id' and statement.status='Active'");
        foreach ($query->result() as $value) {
            $id = $value->id;
            $data_update_stmnt = array(
                'status' => $in_active
            );
            $this->db->where('id', $id);
            $this->db->update('statement', $data_update_stmnt);
        }


        $query = $this->db->query("select id from total_job_card_cost where job_card_id='$job_card_id' and total_job_card_cost.status='Active'");
        foreach ($query->result() as $value) {
            $id = $value->id;
            $data_update_total_cost = array(
                'status' => $in_active
            );
            $this->db->where('id', $id);
            $this->db->update('total_job_card_cost', $data_update_total_cost);
        }



        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {

            return FALSE;
        } else {
            return TRUE;
        }
    }

    function get_active_clients($q) {


        $query = "Select * from client where name like '%$q%'";
        $query = $this->db->query($query);

        foreach ($query->result_array() as $value) {

            $new_row['label'] = htmlentities($value['name']);
            $new_row['value'] = htmlentities($value['name']);
            $row_set[] = $new_row; //build an array

            echo json_encode($row_set);
        }
    }

    function sales_report() {
        $this->db->trans_start();
        $sql = "Select * from sales_report order by date_added ASC ";
        $query = $this->db->query($sql);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }
    }

    function client_accnt_statement() {
        $this->db->trans_start();
        $sql = "Select * from client_stmnt_accnts order by client_name  ASC ";
        $query = $this->db->query($sql);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }
    }

    function check_client_existense($client_id) {
        $this->db->trans_start();
        $sql = "Select * from client where name = '$client_id' ";
        $query = $this->db->query($sql);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            foreach ($query->result() as $value) {
                $client_id = $value->id;
                return $client_id;
            }
        }
    }

    function check_job_card_existense($client_id, $delivery_date, $event_date, $location, $est_location_dstnce, $cnfrmd_location_dstnce, $time_from, $time_to, $edit_job_card, $edit_job_card_no, $booked_by_id, $notes, $venue) {
        //echo 'Data:'.$client_id.$delivery_date.$location.$est_location_dstnce.$cnfrmd_location_dstnce.$time_from.$time_to.$venue.'<br>';
        $this->db->trans_start();
        $get_client_id = $this->db->query("Select id from client where name='$client_id'");
        foreach ($get_client_id->result() as $value) {
            $client_id = $value->id;
             $sql = "Select job_card_no from job_card where clnt_id = '$client_id' and dte_srcd='$delivery_date' and lctn='$location' and estmtd_lctn_dstnce='$est_location_dstnce' and cnfrmd_lctn_dstnce='$cnfrmd_location_dstnce' and time_from='$time_from' and time_to='$time_to' and venue='$venue'";
        $query = $this->db->query($sql);
        }
        
       

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            foreach ($query->result() as $value) {
                $job_card_no = $value->job_card_no;
                return $job_card_no;
            }
        }
    }

    function get_asset_status_details($asset_id) {
        $this->db->trans_start();
        $query = $this->db->query("SELECT job_card_no, client.name as client_name, assgnd_rsces.name as asset_name, dte_evnt,dte_srcd FROM `assgnd_rsces` inner join job_card on job_card.id = assgnd_rsces.job_card_id inner join client on client.id = job_card.clnt_id where assgnd_rsces.name='$asset_id'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }
    }

    function get_client_information($client_name) {
        $this->db->trans_start();
        $query = $this->db->query("SELECT * from client where name='$client_name'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }
    }

    function get_job_information($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("SELECT client.name as client_name, job_card.id as job_card_id, job_card.quote_review,job_card.job_card_no,job_card.lctn,dte_srcd,estmtd_lctn_dstnce,cnfrmd_lctn_dstnce,notes,venue, CONCAT('FROM : ', date_format(job_card.time_from,'%T, %d-%b-%Y') ,' TO :', date_format(job_card.time_to,'%T, %d-%b-%Y')) as event_date FROM `job_card` inner join client on client.id = job_card.clnt_id where quoted='No' and job_card.status='Active' and job_card.id='$job_card_id'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }
    }

    function get_job_quotation_details($job_card_id) {
        $this->db->trans_start();
        $query = $this->db->query("SELECT DISTINCT statement.id as statement_id, asset_tracker.id as asset_tracker_id, asset_tracker.name as asset_name, asset_tracker.date_added , asset_tracker.price as asset_price, asset_tracker.qty as asset_quantity, asset_tracker.status as asset_status,asset_tracker.qty as asset_qty, asset_tracker.days as day, asset_tracker.discount as discount, job_card.id as job_card_id, job_card.job_card_no as job_card_no FROM `asset_tracker` inner join job_card on job_card.id = asset_tracker.job_card_id inner join statement on statement.job_card_id = job_card.id where job_card.id='$job_card_id' and job_card.status='Active' and statement.status='Active' group by statement.id");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result();
        }
    }

}

?>