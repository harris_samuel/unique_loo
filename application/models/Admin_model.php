<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Login model class
 */

class Admin_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    protected function get_roles() {
        $this->db->trans_start();
        $SQL = "Select * from roles";
        $query = $this->db->query($SQL);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result();
        }
    }

    function get_users() {
        $this->db->trans_start();
        $SQL = "Select * from users";
        $query = $this->db->query($SQL);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result();
        }
    }

    function get_assets() {
        $this->db->trans_start();
        $SQL = "Select * from assets";
        $query = $this->db->query($SQL);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result();
        }
    }

    function get_employees() {
        $this->db->trans_start();
        $SQL = "Select employee.id as employee_id,employee.employee_no ,employee.status ,concat(f_name,' ',s_name,' ',o_name) as employee_name , employee.id_no,employee.dob,employee.marital_status,employee.nationality,employee.gender,employee.time_stamp,employee.role_id,employee.department_id,department.name as department_name ,roles.name as roles_name from employee "
                . "inner join department on department.id = employee.department_id "
                . "inner join roles on roles.id = employee.role_id "
                . "";
        $query = $this->db->query($SQL);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_employee_details($employee_id) {
        $this->db->trans_start();
        $SQL = "Select employee.id as employee_id , employee.employee_no,concat(title,':',f_name,' ',s_name,' ',o_name) as employee_name "
                . ", employee.id_no,employee.phone_no,employee.gender,employee.dob,employee.marital_status,employee.nationality,employee.gender,"
                . "employee.time_stamp,employee.role_id,employee.department_id,department.name as department_name "
                . ",roles.name as roles_name from employee  inner join department on department.id = employee.department_id"
                . " inner join roles on roles.id = employee.role_id  where employee.id='1'";
        $query = $this->db->query($SQL);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_edit_employee_details($employee_id) {
        $this->db->trans_start();
        $SQL = "Select employee.id as employee_id , employee.employee_no,f_name , s_name,o_name, concat(f_name,' ',s_name,' ',o_name) as employee_name,employee.email ,employee.phone_no,employee.gender,roles.id as role_id,employee.status as employee_status, employee.id_no,employee.dob,employee.marital_status,employee.nationality,employee.time_stamp,employee.role_id,employee.department_id,department.name as department_name ,roles.name as roles_name, employee.nationality as nationality from employee "
                . "inner join department on department.id = employee.department_id "
                . "inner join roles on roles.id = employee.role_id "
                . " where employee.id='$employee_id'";
        $query = $this->db->query($SQL);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function add_new_employee() {
        $f_name = $this->input->post('f_name');
        $s_name = $this->input->post('s_name');
        $o_name = $this->input->post('o_name');
        $id_no = $this->input->post('id_no');
        $email = $this->input->post('email');
        $phone_no = $this->input->post('phone_no');
        $dob = $this->input->post('dob');
        $status = $this->input->post('employee_status');
        $marital_status = $this->input->post('marital_status');
        $nationality = $this->input->post('nationality');
        $employee_no = $this->input->post('employee_no');
        echo 'Nationality : ' . $nationality . '<br>';
        $role_id = $this->input->post('role_id');
        $department_id = $this->input->post('department_id');
        $gender = $this->input->post('gender');
        $today = date("Y-m-d H:i:s");
        $this->db->trans_start();

        $data_insert = array(
            'f_name' => $f_name,
            's_name' => $s_name,
            'o_name' => $o_name,
            'id_no' => $id_no,
            'email' => $email,
            'phone_no' => $phone_no,
            'dob' => $dob,
            'marital_status' => $marital_status,
            'nationality' => $nationality,
            'role_id' => $role_id,
            'department_id' => $department_id,
            'date_added' => $today,
            'gender' => $gender,
            'status' => $status,
            'employee_no' => $employee_no
        );
        $this->db->insert('employee', $data_insert);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function get_countries($q) {


        $query = "Select * from countries where countryName like '%$q%'";
        $query = $this->db->query($query);

        foreach ($query->result_array() as $value) {

            $new_row['label'] = htmlentities($value['countryName']);
            $new_row['value'] = htmlentities($value['countryName']);
            $row_set[] = $new_row; //build an array


            echo json_encode($row_set);
        }
    }

    function get_titles() {
        $this->db->trans_start();
        $SQL = "Select * from title";
        $query = $this->db->query($SQL);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result();
        }
    }

    function edit_employee() {
        $f_name = $this->input->post('edit_f_name');
        $s_name = $this->input->post('edit_s_name');
        $o_name = $this->input->post('edit_o_name');
        $id_no = $this->input->post('edit_id_no');
        $employe_id = $this->input->post('edit_employee_id');
        $dob = $this->input->post('edit_dob');
        $role_id = $this->input->post('edit_role_id');
        $department_id = $this->input->post('edit_department_id');
        $nationality = $this->input->post('edit_nationality');
        $phone_no = $this->input->post('edit_phone_no');
        $marital_status = $this->input->post('edit_marital_status');
        $email = $this->input->post('edit_email');
        $gender = $this->input->post('edit_gender');
        $status = $this->input->post('edit_status');
        $employee_no = $this->input->post('edit_employee_no');

        $this->db->trans_start();

        $select_employee = $this->db->query("Select email from employee where id='$employe_id'");
        foreach ($select_employee->result() as $value) {
            $employee_email = $value->email;
            echo $employee_email;
            if ($employee_email === $email) {
                $user_update = $this->db->query("Select * from users where email='$employee_email'");
                foreach ($user_update->result() as $value) {
                    $user_id = $value->id;
                    echo $user_id;
                    $user_data_update = array(
                        'email' => $email
                    );
                    $this->db->where('id', $user_id);
                    $this->db->update('users', $user_data_update);
                }
            } else {
                $user_update = $this->db->query("Select * from users where email='$employee_email'");
                foreach ($user_update->result() as $value) {
                    $user_id = $value->id;
                    echo $user_id;
                    $user_data_update = array(
                        'email' => $email
                    );
                    $this->db->where('id', $user_id);
                    $this->db->update('users', $user_data_update);
                }
            }
        }

        $data_update = array(
            'f_name' => $f_name,
            's_name' => $s_name,
            'o_name' => $o_name,
            'id_no' => $id_no,
            'dob' => $dob,
            'role_id' => $role_id,
            'department_id' => $department_id,
            'nationality' => $nationality,
            'phone_no' => $phone_no,
            'marital_status' => $marital_status,
            'email' => $email,
            'gender' => $gender,
            'employee_no' => $employee_no,
            'status' => $status
        );
        $this->db->where('id', $employe_id);
        $this->db->update('employee', $data_update);




        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    function delete_employee($employee_id) {
        $this->db->trans_start();
        $status = "In Active";
        $data_update = array(
            'status' => $status
        );
        $this->db->where('id', $employee_id);
        $this->db->update('employee', $data_update);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function get_role_details($role_id) {
        $this->db->trans_start();
        $SQL = "Select * from roles  where roles.id='$role_id'";
        $query = $this->db->query($SQL);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_edit_role_details($role_id) {
        $this->db->trans_start();
        $SQL = "Select * from roles where roles.id='$role_id'";
        $query = $this->db->query($SQL);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function edit_role() {
        $role_name = $this->input->post('edit_role_name');
        $role_status = $this->input->post('edit_role_status');
        $role_id = $this->input->post('edit_role_id');
        $this->db->trans_start();
        $data_update = array(
            'name' => $role_name,
            'status' => $role_status
        );
        $this->db->where('id', $role_id);
        $this->db->update('roles', $data_update);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    function delete_role($role_id) {
        $role_id = $this->input->post('delete_role_id');
        $role_status = "In Active";
        $this->db->trans_start();
        $data_delete = array(
            'status' => $role_status
        );
        $this->db->where('id', $role_id);
        $this->db->update('roles', $data_delete);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    public function add_new_role($role_name, $role_status) {
        $this->db->trans_start();
        $today = date("Y-m-d H:i:s");
        $data_insert = array(
            'name' => $role_name,
            'status' => $role_status,
            'date_added' => $today
        );
        $this->db->insert('roles', $data_insert);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function edit_department() {
        $department_name = $this->input->post('edit_department_name');
        $department_status = $this->input->post('edit_department_status');
        $department_id = $this->input->post('edit_department_id');
        $this->db->trans_start();
        $data_update = array(
            'name' => $department_name,
            'status' => $department_status
        );
        $this->db->where('id', $department_id);
        $this->db->update('department', $data_update);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    function delete_department($department_id) {
        $department_id = $this->input->post('delete_department_id');
        $department_status = "In Active";
        $this->db->trans_start();
        $data_delete = array(
            'status' => $department_status
        );
        $this->db->where('id', $department_id);
        $this->db->update('department', $data_delete);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    public function add_new_department($department_name, $department_status) {
        $this->db->trans_start();
        $data_insert = array(
            'name' => $department_name,
            'status' => $department_status
        );
        $this->db->insert('department', $data_insert);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function get_edit_department_details($department_id) {
        $this->db->trans_start();
        $SQL = "Select * from department where department.id='$department_id'";
        $query = $this->db->query($SQL);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function check_user_email($email) {
        $this->db->trans_start();
        $sql = "Select email from employee where email = '$email'";
        $query = $this->db->query($sql);
        foreach ($query->result() as $value) {
            $email = $value->email;
            if (empty($email)) {
                return "Empty";
            } else {
                return $email;
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    function get_department_details($department_id) {
        $this->db->trans_start();
        $SQL = "Select * from department  where department.id='$department_id'";
        $query = $this->db->query($SQL);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_user_emails() {
        $this->db->trans_start();
        $sql = $this->db->query("Select email from employee where status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $sql->result_array();
        }
    }

    function add_new_user($user_name, $user_status, $user_email, $get_user_national_id) {
        $this->db->trans_start();
        $password = md5($get_user_national_id);
        $data_insert = array(
            'email' => $user_email,
            'user_name' => $user_name,
            'status' => $user_status,
            'password' => $password
        );
        $this->db->insert('users', $data_insert);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    function get_edit_user_details($user_id) {
        $this->db->trans_start();
        $query = $this->db->query("Select * from users where id='$user_id'");

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function edit_user_details() {
        $this->db->trans_start();
        $user_name = $this->input->post('edit_user_name');
        $email = $this->input->post('edit_user_email');
        $status = $this->input->post('edit_user_status');
        $password = $this->input->post('edit_user_password');
        $user_id = $this->input->post('edit_user_id');
        $enc_password = md5($password);
        if (empty($password)) {
            $data_update = array(
                'user_name' => $user_name,
                'email' => $email,
                'status' => $status
            );
        } else {
            $data_update = array(
                'user_name' => $user_name,
                'email' => $email,
                'password' => $enc_password,
                'status' => $status
            );
        }
        $this->db->where('id', $user_id);
        $this->db->update('users', $data_update);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    function delete_user() {
        $this->db->trans_start();
        $in_active = "In Active";
        $id = $this->input->post('delete_user_id');
        $data_update = array(
            'status' => $in_active
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data_update);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function add_asset($asset_type, $asset_price, $purchase_date, $asset_uuid) {
        $this->db->trans_start();
        $data_insert = array(
            'type' => $asset_type,
            'price' => $asset_price,
            'purchase_date' => $purchase_date,
            'number' => $asset_uuid
        );
        $this->db->insert('asset', $data_insert);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function edit_asset($id, $asset_name, $asset_type, $asset_price, $purchase_date, $asset_uuid) {
        $this->db->trans_start();
        $data_insert = array(
            'type' => $asset_type,
            'price' => $asset_price,
            'purchase_date' => $purchase_date,
            'number' => $asset_uuid
        );
        $this->db->where('id', $id);
        $this->db->update('asset', $data_insert);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function get_asset($id) {
        $this->db->trans_start();
        $query = $this->db->query("select * from asset where id='$id'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function delete_asset($id) {
        $this->db->trans_start();
        $in_active = "In Active";
        $data_update = array(
            'status' => $in_active
        );
        $this->db->where('id', $id);
        $this->db->update('asset', $data_update);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    function add_new_client($name, $address, $website, $industry, $phone_no, $email) {
        $this->db->trans_start();
        $today = date("Y-m-d H:i:s");
        $data_insert = array(
            'name' => $name,
            'address' => $address,
            'website' => $website,
            'industry' => $industry,
            'phone_no' => $phone_no,
            'email' => $email,
            'date_added' => $today
        );
        $this->db->insert('client', $data_insert);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function get_client_details($client_id) {
        $this->db->trans_start();
        $query = $this->db->query("Select * from client where id='$client_id'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function edit_client($name, $address, $website, $industry, $phone, $email, $id) {
        $this->db->trans_start();
        $data_update = array(
            'name' => $name,
            'address' => $address,
            'website' => $website,
            'industry' => $industry,
            'phone_no' => $phone,
            'email' => $email
        );
        $this->db->where('id', $id);
        $this->db->update('client', $data_update);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    function delete_cleint($id) {
        $this->db->trans_start();
        $status = "In Active";
        $data_update = array(
            'status' => $status
        );
        $this->db->where('id', $id);
        $this->db->update('client', $data_update);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    function add_new_standard($standard_name, $standard_value, $status) {
        $this->db->trans_start();
        $today = date("Y-m-d H:i:s");
        $data_insert = array(
            'parameter' => $standard_name,
            'value' => $standard_value,
            'status' => $status,
            'date_added' => $today
        );
        $this->db->insert('standards', $data_insert);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    function edit_standards($standard_name, $standard_value, $status, $id) {
        $this->db->trans_start();
        $data_update = array(
            'parameter' => $standard_name,
            'value' => $standard_value,
            'status' => $status
        );
        $this->db->where('id', $id);
        $this->db->update('standards', $data_update);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function delete_standards($id) {
        $this->db->trans_start();
        $status = "In Active";
        $data_update = array(
            'status' => $status
        );
        $this->db->where('id', $id);
        $this->db->update('standards', $data_update);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            
        }
    }

    function get_edit_standard_details($id) {
        $this->db->trans_start();
        $query = $this->db->query("Select * from standards where id='$id'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function admin_functions() {
        $this->db->trans_start();
        $query = $this->db->query("Select * from functions where level='1' ");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function daily_functions() {
        $this->db->trans_start();
        $query = $this->db->query("Select * from functions where level='2' ");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function reports_functions() {
        $this->db->trans_start();
        $query = $this->db->query("Select * from functions where level ='3' ");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function system_users() {
        $this->db->trans_start();
        $query = $this->db->query("Select * from users where status ='Active' ");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_user_permissions($user_id) {
        $this->db->trans_start();
        $query = $this->db->query("Select * from permissions where status ='Active' and user_id='$user_id' ");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function check_email_existence($email) {
        $this->db->trans_start();
        $query = $this->db->query("select email from employee where email='$email'");
        $row = $query->row();


        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        } else {
            if (isset($row)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function check_employee_no_existence($employee_no) {
        $this->db->trans_start();
        $query = $this->db->query("select employee_no from employee where employee_no='$employee_no'");
        $row = $query->row();


        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        } else {
            if (isset($row)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function check_email_id($email) {
        $this->db->trans_start();
        $query = $this->db->query("select email from users where email='$email'");
        $row = $query->row();


        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        } else {
            if (isset($row)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function add_new_asset_type($name, $status) {
        $this->db->trans_start();
        $today = date("Y-m-d H:i:s");

        $data_insert = array(
            'name' => $name,
            'status' => $status,
            'date_added' => $today
        );
        $this->db->insert('asset_type', $data_insert);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function get_asset_type_details($asset_type_id) {
        $this->db->trans_start();
        $query = $this->db->query("Select * from asset_type where id='$asset_type_id'");

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function edit_asset_type($name, $status, $id) {
        $this->db->trans_start();
        $data_update = array(
            'name' => $name,
            'status' => $status
        );
        $this->db->where('id', $id);
        $this->db->update('asset_type', $data_update);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function delete_asset_type($id) {
        $this->db->trans_start();
        $in_active = "In Active";
        $data_update = array(
            'status' => $in_active,
        );
        $this->db->where('id', $id);
        $this->db->update('asset_type', $data_update);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        }
    }

    function check_uuid($asset_uuid) {
        $this->db->trans_start();
        $query = $this->db->query("Select number from asset where number = '$asset_uuid'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            foreach ($query->result() as $value) {
                $number = $value->number;
                return $number;
            }
        }
    }

    function check_client($name) {
        $this->db->trans_start();
        $query = $this->db->query("Select name from client where name = '$name'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            foreach ($query->result() as $value) {
                $name = $value->name;
                return $name;
            }
        }
    }

}

?>