<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        // $this->output->enable_profiler(TRUE);
        // $this->output->cache(30);
        $this->check_isvalidated();
    }

    private function check_isvalidated() {
        if (!$this->session->userdata('validated')) {
            redirect('login');
        }
    }

    function get_roles() {
        $this->db->trans_start();
        $sql = "Select * from roles  order by name ASC";
        $query = $this->db->query($sql);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_active_user_roles() {
        $this->db->trans_start();
        $sql = "Select * from roles where status='Active'  order by name ASC";
        $query = $this->db->query($sql);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_users() {
        $this->db->trans_start();
        $sql = "Select * from users order by user_name ASC";
        $query = $this->db->query($sql);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_employees() {
        $this->db->trans_start();
        $sql = "Select * from employee order by id ASC";
        $query = $this->db->query($sql);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_standards() {
        $this->db->trans_start();
        $sql = "Select * from standards order by parameter ASC";
        $query = $this->db->query($sql);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_clients() {
        $this->db->trans_start();
        $sql = "Select * from client order by name ASC";
        $query = $this->db->query($sql);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_department() {
        $this->db->trans_start();
        $sql = "Select * from department order by name ASC";
        $query = $this->db->query($sql);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_titles() {
        $this->db->trans_start();
        $SQL = "Select * from title";
        $query = $this->db->query($SQL);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result();
        }
    }

    function get_assets() {
        $this->db->trans_start();
        $sql = "Select * from asset order by date_added ASC";
        $query = $this->db->query($sql);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_job_cards() {
        $this->db->trans_start();
        $sql = "select job_card.id as job_card_id, job_card.job_card_no as job_card_no,job_card.invoice_no as invoice_no, dte_evnt,dte_srcd,job_card.status as status,client.id as client_id,client.name as client_name, total_job_card_cost.amount_cr, total_job_card_cost.id as total_job_card_cost_id from client inner join job_card on job_card.clnt_id = client.id inner join total_job_card_cost on total_job_card_cost.job_card_id = job_card.id where  total_job_card_cost.status='Active' order by job_card.id";
        $query = $this->db->query($sql);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function user_admin_functions() {
        $user_id = $this->session->userdata('user_id');
        $this->db->trans_start();
        $query = $this->db->query("Select * from user_permissions_list where level='1' and user_id='$user_id' and function_status='Active' order by description ASC ");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function user_daily_functions() {
        $user_id = $this->session->userdata('user_id');
        $this->db->trans_start();
        $query = $this->db->query("Select * from user_permissions_list where level='2' and user_id='$user_id' and function_status='Active' order by description ASC ");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function user_reports_functions() {
        $user_id = $this->session->userdata('user_id');
        $this->db->trans_start();
        $query = $this->db->query("Select * from user_permissions_list where level='3' and user_id='$user_id' and function_status='Active' order by description ASC");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function get_asset_type() {
        $this->db->trans_start();
        $query = $this->db->query("Select * from asset_type ");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function unqouted_booked_jobs() {
        $this->db->trans_start();
        $query = $this->db->query("SELECT client.name as client_name, job_card.id as job_card_id, job_card.quote_review,job_card.job_card_no,job_card.invoice_no, CONCAT('FROM : ', date_format(job_card.time_from,'%T, %d-%b-%Y') ,' TO :', date_format(job_card.time_to,'%T, %d-%b-%Y')) as event_date FROM `job_card` inner join client on client.id = job_card.clnt_id where quoted='No' and job_card.status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }
    }

    function client_job_invoice_list() {
        $this->db->trans_start();
        $query = $this->db->query("SELECT DISTINCT client.name as client_name, job_card.id as job_card_id, job_card.quote_review,job_card.job_card_no,job_card.invoice_no, CONCAT('FROM : ', date_format(job_card.time_from,'%T, %d-%b-%Y') ,' TO :', date_format(job_card.time_to,'%T, %d-%b-%Y')) as event_date FROM `job_card` inner join client on client.id = job_card.clnt_id inner join statement on statement.job_card_id = job_card.id where job_card.quoted='Yes' and job_card.invoiced='No' and job_card.status='Active' group by job_card.id");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return $query->result_array();
        } else {
            return $query->result_array();
        }
    }

    function no_jobs_mtd_qry() {
        $this->db->trans_start();
        $todays_month = date('F');
        $query = $this->db->query("Select count(job_card.id) as no_of_jobs_mtd,date_format(job_card.time_from,'%b') as month, date_format(job_card.time_from,'%Y') as year from job_card where date_format(job_card.time_from,'%M') ='$todays_month' and job_card.status !='Cancelled'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function no_of_jobs_actve_mtd_qry() {
        $this->db->trans_start();
        $todays_month = date('F');
        $query = $this->db->query("Select count(job_card.id) as no_of_jobs_actve_mtd,date_format(job_card.time_from,'%b') as month, date_format(job_card.time_from,'%Y') as year from job_card where date_format(job_card.time_from,'%M') ='$todays_month' and job_card.status ='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function no_of_jobs_clrd_mtd_qry() {
        $this->db->trans_start();
        $todays_month = date('F');
        $query = $this->db->query("Select count(job_card.id) as no_of_jobs_clrd_mtd,date_format(job_card.time_from,'%b') as month, date_format(job_card.time_from,'%Y') as year from job_card where date_format(job_card.time_from,'%M') ='$todays_month' and job_card.status ='In Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function no_jobs_per_day() {
        $this->db->trans_start();
        $query = $this->db->query("SELECT count(id) as no_jobs , date_format(job_card.time_from,'%d-%b-%Y') as event_date FROM `job_card` group by date_format(job_card.time_from,'%d-%b-%Y')");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function xpctd_rvne_ytd_qry() {
        $todays_year = date('Y');
        $this->db->trans_start();
        $query = $this->db->query("Select sum(amount_cr) as expected_revenue, date_format(job_card.time_from,'%b') as month, date_format(job_card.time_from,'%Y') as year from job_card inner join total_job_card_cost on total_job_card_cost.job_card_id = job_card.id where date_format(job_card.time_from,'%Y') ='$todays_year' and job_card.status !='Cancelled' and total_job_card_cost.status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function xpctd_rvne_mtd_qry() {
        $todays_month = date('F');
        $todays_year = date('Y');
        $this->db->trans_start();
        $query = $this->db->query("Select sum(amount_cr) as expected_revenue, date_format(job_card.time_from,'%b') as month, date_format(job_card.time_from,'%Y') as year from job_card inner join total_job_card_cost on total_job_card_cost.job_card_id = job_card.id where date_format(job_card.time_from,'%M') ='$todays_month' and date_format(job_card.time_from,'%Y') ='$todays_year'  and job_card.status !='Cancelled' and total_job_card_cost.status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function actl_rvne_mtd_qry() {
        $todays_month = date('F');
        $this->db->trans_start();
        $query = $this->db->query("Select sum(amount_dr) as actual_revenue, date_format(job_card.time_from,'%b') as month, date_format(job_card.time_from,'%Y') as year from job_card inner join total_job_card_cost on total_job_card_cost.job_card_id = job_card.id where date_format(job_card.time_from,'%M') ='$todays_month' and job_card.status !='Cancelled' and total_job_card_cost.status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function total_revenue_qry() {
        $todays_year = date('Y');
        $this->db->trans_start();
        $query = $this->db->query("Select sum(amount_dr) as total_revenue, date_format(job_card.time_from,'%Y') as year from job_card inner join total_job_card_cost on total_job_card_cost.job_card_id = job_card.id where date_format(job_card.time_from,'%Y') ='2016' and job_card.status !='Cancelled' and total_job_card_cost.status='Active'");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

    function total_jobs_per_day_qry() {
        $todays_month = date('F');
        $this->db->trans_start();
        $query = $this->db->query("Select count(job_card.id) as no_jobs , date_format(job_card.time_from,'%d-%b-%Y') as year from job_card  where date_format(job_card.time_from,'%M') ='$todays_month' and job_card.status !='Cancelled' group by date_format(job_card.time_from,'%d-%b-%Y')");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            
        } else {
            return $query->result_array();
        }
    }

}
