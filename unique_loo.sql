-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Dec 24, 2015 at 05:28 AM
-- Server version: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `unique_loo`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `active_job_cards`
--
CREATE TABLE `active_job_cards` (
`event_month` int(2)
,`event_year` int(4)
,`event_status_count` bigint(21)
,`id` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `active_users`
--
CREATE TABLE `active_users` (
`active_users` bigint(21)
);
-- --------------------------------------------------------

--
-- Table structure for table `asset`
--

CREATE TABLE `asset` (
`id` int(11) NOT NULL,
  `asset_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `price` decimal(13,2) NOT NULL,
  `purchase_date` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','In Active') NOT NULL,
  `booked` enum('Booked','Not Booked') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset`
--

INSERT INTO `asset` (`id`, `asset_id`, `name`, `type`, `number`, `price`, `purchase_date`, `date_added`, `timestamp`, `status`, `booked`) VALUES
(1, '', '', 'Toilet', '1', 12300.00, '2015-12-01 00:00:00', '0000-00-00 00:00:00', '2015-12-22 14:45:48', 'Active', 'Booked');

-- --------------------------------------------------------

--
-- Table structure for table `asset_tracker`
--

CREATE TABLE `asset_tracker` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(13,2) NOT NULL,
  `status` enum('Active','In Active') NOT NULL,
  `client_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `job_card_id` int(11) NOT NULL,
  `invoiced` enum('No','Yes') NOT NULL,
  `discount` decimal(13,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_tracker`
--

INSERT INTO `asset_tracker` (`id`, `name`, `qty`, `price`, `status`, `client_id`, `date_added`, `timestamp`, `job_card_id`, `invoiced`, `discount`) VALUES
(1, 'Toilet', 4, 5800.00, 'Active', 0, '2015-12-22 21:59:34', '2015-12-22 18:59:34', 3, 'No', 5000.00),
(2, 'Toilet', 4, 5800.00, 'Active', 0, '2015-12-22 22:03:42', '2015-12-22 19:03:42', 4, 'No', 5000.00),
(3, 'Trailers', 3, 12300.00, 'Active', 0, '2015-12-22 22:03:53', '2015-12-22 19:03:53', 4, 'No', 5000.00),
(4, 'Toilet', 10, 5800.00, 'Active', 0, '2015-12-23 08:30:10', '2015-12-23 05:30:10', 5, 'No', 8000.00),
(5, 'Trailers', 4, 12300.00, 'Active', 0, '2015-12-23 08:34:29', '2015-12-23 05:34:29', 5, 'No', 9200.00),
(6, 'Toilet', 25, 5800.00, 'Active', 0, '2015-12-23 08:36:10', '2015-12-23 05:36:10', 6, 'No', 5000.00),
(7, 'Trailers', 10, 12300.00, 'Active', 0, '2015-12-23 08:36:26', '2015-12-23 05:36:26', 6, 'No', 8000.00),
(8, 'Single Toilet', 4, 5800.00, 'Active', 0, '2015-12-24 05:09:53', '2015-12-24 02:09:53', 8, 'No', 200.00),
(9, 'Trailers', 2, 12300.00, 'Active', 0, '2015-12-24 05:10:19', '2015-12-24 02:10:19', 8, 'No', 600.00),
(10, 'Single Toilet', 9, 5800.00, 'Active', 0, '2015-12-24 05:23:06', '2015-12-24 02:23:06', 9, 'No', 200.00),
(11, 'Trailers', 10, 12300.00, 'Active', 0, '2015-12-24 05:23:18', '2015-12-24 02:23:18', 9, 'No', 800.00);

-- --------------------------------------------------------

--
-- Table structure for table `asset_type`
--

CREATE TABLE `asset_type` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','In Active') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_type`
--

INSERT INTO `asset_type` (`id`, `name`, `date_added`, `timestamp`, `status`) VALUES
(1, 'Toilet', '2015-12-22 17:44:35', '2015-12-22 14:44:35', 'Active'),
(2, 'Trailer', '2015-12-22 17:44:50', '2015-12-22 14:44:50', 'Active'),
(3, 'Vehicle', '2015-12-22 17:45:02', '2015-12-22 14:45:02', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `assgnd_rsces`
--

CREATE TABLE `assgnd_rsces` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` decimal(13,2) DEFAULT '0.00',
  `job_card_id` int(11) NOT NULL,
  `status` enum('Active','In Active') NOT NULL,
  `date_added` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `audit_trail`
--

CREATE TABLE `audit_trail` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `billed_client_info`
--
CREATE TABLE `billed_client_info` (
`client_id` int(11)
,`client_name` varchar(255)
,`client_address` varchar(255)
,`client_website` varchar(255)
,`client_industry` varchar(255)
,`client_phone_no` varchar(255)
,`client_email` varchar(255)
,`client_status` enum('Active','In Active')
,`job_card_no` varchar(255)
,`invoice_no` varchar(255)
,`job_card_id` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `cancelled_job_cards`
--
CREATE TABLE `cancelled_job_cards` (
`event_month` int(2)
,`event_year` int(4)
,`event_status_count` bigint(21)
);
-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('74eaeeef67cde3911bcd6b05cf34f545d282379c', '::1', 1450922775, ''),
('92e9b8000310c9132853f898488ee97dfb7de3d1', '::1', 1450923695, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435303932333639353b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2241646d696e223b6c6f67696e5f6c6f67735f69647c693a32303b76616c6964617465647c623a313b),
('99a03eeeaddfe7faf4fd8ed063a900ed4f074131', '::1', 1450923736, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435303932333733333b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2241646d696e223b6c6f67696e5f6c6f67735f69647c693a32313b76616c6964617465647c623a313b),
('e28e5670e5c8196572fa8a5f32ccc5916bfa0844', '::1', 1450922774, ''),
('f577021d72fc17b4c0b9d27608a00317f63ad766', '::1', 1450922775, '');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `industry` varchar(255) NOT NULL,
  `phone_no` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','In Active') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `name`, `address`, `website`, `industry`, `phone_no`, `email`, `date_added`, `timestamp`, `status`) VALUES
(1, 'Safaricom', '', '', '', '', '', '2015-12-22 17:47:30', '2015-12-22 14:47:30', 'Active'),
(2, 'Strathmore UNiversity', '', '', '', '0712345678', 'info@strathmore.edu', '2015-12-22 17:49:00', '2015-12-22 14:49:00', 'Active'),
(3, 'Ministry of Health', '12434', '', '', '', '', '2015-12-23 08:29:26', '2015-12-23 05:29:26', 'Active');

-- --------------------------------------------------------

--
-- Stand-in structure for view `client_payment_list`
--
CREATE TABLE `client_payment_list` (
`asset_name` varchar(255)
,`qty` int(11)
,`amount_charged` int(11)
,`amount_paid` int(11)
,`discount` decimal(13,2)
,`balance` bigint(12)
,`payment_method` varchar(255)
,`payment_code` varchar(255)
,`job_card_no` varchar(255)
,`job_card_id` int(11)
,`invoice_no` varchar(255)
,`statement_id` int(11)
,`client_id` int(11)
,`status` enum('Active','In Active')
,`parameter` varchar(255)
,`value` varchar(255)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `client_stmnt_accnts`
--
CREATE TABLE `client_stmnt_accnts` (
`client_name` varchar(255)
,`amount_paid` decimal(35,2)
,`amount_billed` decimal(36,2)
,`last_payment_date` varchar(50)
,`discount` decimal(35,2)
,`no_of_jobs` bigint(21)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `clnt_pymnt_stmnt_rprt`
--
CREATE TABLE `clnt_pymnt_stmnt_rprt` (
`client_id` int(11)
,`client_name` varchar(255)
,`phone_no` varchar(255)
,`job_card_id` int(11)
,`job_card_no` varchar(255)
,`invoice_no` varchar(255)
,`date_added` varchar(50)
,`month` int(2)
,`year` int(4)
,`amount_charged` decimal(35,2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `cln_pymnt_stmnt_rprt`
--
CREATE TABLE `cln_pymnt_stmnt_rprt` (
`client_id` int(11)
,`client_name` varchar(255)
,`phone_no` varchar(255)
,`job_card_id` int(11)
,`job_card_no` varchar(255)
,`invoice_no` varchar(255)
,`date_added` datetime
,`amount_charged` decimal(32,0)
);
-- --------------------------------------------------------

--
-- Table structure for table `conf`
--

CREATE TABLE `conf` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `website` varchar(255) NOT NULL,
  `pin` varchar(255) NOT NULL,
  `phone_no` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conf`
--

INSERT INTO `conf` (`id`, `name`, `address`, `website`, `pin`, `phone_no`, `email`, `date_added`, `timestamp`) VALUES
(1, 'Unique Loo Ltd', 'Address : Corner House 13th Floor, Kimathi Street Opposite Hilton Hotel\r\nPostal code/ZIP :  P.O. BOX 42278-00100, Nairobi, Kenya.', 'http://uniqueloo.co.ke', 'P1257687543', 'Phone : 0722 852 180\r\nPhone : 0724 616 134', 'Email : info@uniqueloo.com', '2015-10-14 09:14:22', '2015-10-14 06:12:42');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
`id` int(5) NOT NULL,
  `countryCode` char(2) NOT NULL DEFAULT '',
  `countryName` varchar(45) NOT NULL DEFAULT '',
  `continent` char(2) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=251 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `countryCode`, `countryName`, `continent`, `timestamp`) VALUES
(1, 'AD', 'Andorra', 'EU', '2015-11-02 04:16:35'),
(2, 'AE', 'United Arab Emirates', 'AS', '2015-11-02 04:16:35'),
(3, 'AF', 'Afghanistan', 'AS', '2015-11-02 04:16:35'),
(4, 'AG', 'Antigua and Barbuda', 'NA', '2015-11-02 04:16:35'),
(5, 'AI', 'Anguilla', 'NA', '2015-11-02 04:16:35'),
(6, 'AL', 'Albania', 'EU', '2015-11-02 04:16:35'),
(7, 'AM', 'Armenia', 'AS', '2015-11-02 04:16:35'),
(8, 'AO', 'Angola', 'AF', '2015-11-02 04:16:35'),
(9, 'AQ', 'Antarctica', 'AN', '2015-11-02 04:16:35'),
(10, 'AR', 'Argentina', 'SA', '2015-11-02 04:16:35'),
(11, 'AS', 'American Samoa', 'OC', '2015-11-02 04:16:35'),
(12, 'AT', 'Austria', 'EU', '2015-11-02 04:16:35'),
(13, 'AU', 'Australia', 'OC', '2015-11-02 04:16:35'),
(14, 'AW', 'Aruba', 'NA', '2015-11-02 04:16:35'),
(15, 'AX', 'Åland', 'EU', '2015-11-02 04:16:35'),
(16, 'AZ', 'Azerbaijan', 'AS', '2015-11-02 04:16:35'),
(17, 'BA', 'Bosnia and Herzegovina', 'EU', '2015-11-02 04:16:35'),
(18, 'BB', 'Barbados', 'NA', '2015-11-02 04:16:35'),
(19, 'BD', 'Bangladesh', 'AS', '2015-11-02 04:16:35'),
(20, 'BE', 'Belgium', 'EU', '2015-11-02 04:16:35'),
(21, 'BF', 'Burkina Faso', 'AF', '2015-11-02 04:16:35'),
(22, 'BG', 'Bulgaria', 'EU', '2015-11-02 04:16:35'),
(23, 'BH', 'Bahrain', 'AS', '2015-11-02 04:16:35'),
(24, 'BI', 'Burundi', 'AF', '2015-11-02 04:16:35'),
(25, 'BJ', 'Benin', 'AF', '2015-11-02 04:16:35'),
(26, 'BL', 'Saint Barthélemy', 'NA', '2015-11-02 04:16:35'),
(27, 'BM', 'Bermuda', 'NA', '2015-11-02 04:16:35'),
(28, 'BN', 'Brunei', 'AS', '2015-11-02 04:16:35'),
(29, 'BO', 'Bolivia', 'SA', '2015-11-02 04:16:35'),
(30, 'BQ', 'Bonaire', 'NA', '2015-11-02 04:16:35'),
(31, 'BR', 'Brazil', 'SA', '2015-01-02 04:16:35'),
(32, 'BS', 'Bahamas', 'NA', '2015-01-02 04:16:35'),
(33, 'BT', 'Bhutan', 'AS', '2015-01-02 04:16:35'),
(34, 'BV', 'Bouvet Island', 'AN', '2015-01-02 04:16:35'),
(35, 'BW', 'Botswana', 'AF', '2015-01-02 04:16:35'),
(36, 'BY', 'Belarus', 'EU', '2015-01-02 04:16:35'),
(37, 'BZ', 'Belize', 'NA', '2015-01-02 04:16:35'),
(38, 'CA', 'Canada', 'NA', '2015-01-02 04:16:35'),
(39, 'CC', 'Cocos [Keeling] Islands', 'AS', '2015-01-02 04:16:35'),
(40, 'CD', 'Democratic Republic of the Congo', 'AF', '2015-01-02 04:16:35'),
(41, 'CF', 'Central African Republic', 'AF', '2015-01-02 04:16:35'),
(42, 'CG', 'Republic of the Congo', 'AF', '2015-01-02 04:16:35'),
(43, 'CH', 'Switzerland', 'EU', '2015-01-02 04:16:35'),
(44, 'CI', 'Ivory Coast', 'AF', '2015-01-02 04:16:35'),
(45, 'CK', 'Cook Islands', 'OC', '2015-01-02 04:16:35'),
(46, 'CL', 'Chile', 'SA', '2015-01-02 04:16:35'),
(47, 'CM', 'Cameroon', 'AF', '2015-01-02 04:16:35'),
(48, 'CN', 'China', 'AS', '2015-01-02 04:16:35'),
(49, 'CO', 'Colombia', 'SA', '2015-01-02 04:16:35'),
(50, 'CR', 'Costa Rica', 'NA', '2015-01-02 04:16:35'),
(51, 'CU', 'Cuba', 'NA', '2015-01-02 04:16:35'),
(52, 'CV', 'Cape Verde', 'AF', '2015-01-02 04:16:35'),
(53, 'CW', 'Curacao', 'NA', '2015-01-02 04:16:35'),
(54, 'CX', 'Christmas Island', 'AS', '2015-01-02 04:16:35'),
(55, 'CY', 'Cyprus', 'EU', '2015-01-02 04:16:35'),
(56, 'CZ', 'Czech Republic', 'EU', '2015-01-02 04:16:35'),
(57, 'DE', 'Germany', 'EU', '2015-01-02 04:16:35'),
(58, 'DJ', 'Djibouti', 'AF', '2015-01-02 04:16:35'),
(59, 'DK', 'Denmark', 'EU', '2015-01-02 04:16:35'),
(60, 'DM', 'Dominica', 'NA', '2015-01-02 04:16:35'),
(61, 'DO', 'Dominican Republic', 'NA', '2015-02-02 04:16:35'),
(62, 'DZ', 'Algeria', 'AF', '2015-02-02 04:16:35'),
(63, 'EC', 'Ecuador', 'SA', '2015-02-02 04:16:35'),
(64, 'EE', 'Estonia', 'EU', '2015-02-02 04:16:35'),
(65, 'EG', 'Egypt', 'AF', '2015-02-02 04:16:35'),
(66, 'EH', 'Western Sahara', 'AF', '2015-02-02 04:16:35'),
(67, 'ER', 'Eritrea', 'AF', '2015-02-02 04:16:35'),
(68, 'ES', 'Spain', 'EU', '2015-02-02 04:16:35'),
(69, 'ET', 'Ethiopia', 'AF', '2015-02-02 04:16:35'),
(70, 'FI', 'Finland', 'EU', '2015-02-02 04:16:35'),
(71, 'FJ', 'Fiji', 'OC', '2015-02-02 04:16:35'),
(72, 'FK', 'Falkland Islands', 'SA', '2015-02-02 04:16:35'),
(73, 'FM', 'Micronesia', 'OC', '2015-02-02 04:16:35'),
(74, 'FO', 'Faroe Islands', 'EU', '2015-02-02 04:16:35'),
(75, 'FR', 'France', 'EU', '2015-02-02 04:16:35'),
(76, 'GA', 'Gabon', 'AF', '2015-02-02 04:16:35'),
(77, 'GB', 'United Kingdom', 'EU', '2015-02-02 04:16:35'),
(78, 'GD', 'Grenada', 'NA', '2015-02-02 04:16:35'),
(79, 'GE', 'Georgia', 'AS', '2015-02-02 04:16:35'),
(80, 'GF', 'French Guiana', 'SA', '2015-02-02 04:16:35'),
(81, 'GG', 'Guernsey', 'EU', '2015-02-02 04:16:35'),
(82, 'GH', 'Ghana', 'AF', '2015-02-02 04:16:35'),
(83, 'GI', 'Gibraltar', 'EU', '2015-02-02 04:16:35'),
(84, 'GL', 'Greenland', 'NA', '2015-02-02 04:16:35'),
(85, 'GM', 'Gambia', 'AF', '2015-02-02 04:16:35'),
(86, 'GN', 'Guinea', 'AF', '2015-02-02 04:16:35'),
(87, 'GP', 'Guadeloupe', 'NA', '2015-02-02 04:16:35'),
(88, 'GQ', 'Equatorial Guinea', 'AF', '2015-02-02 04:16:35'),
(89, 'GR', 'Greece', 'EU', '2015-02-02 04:16:35'),
(90, 'GS', 'South Georgia and the South Sandwich Islands', 'AN', '2015-02-02 04:16:35'),
(91, 'GT', 'Guatemala', 'NA', '2015-06-02 04:16:35'),
(92, 'GU', 'Guam', 'OC', '2015-06-02 04:16:35'),
(93, 'GW', 'Guinea-Bissau', 'AF', '2015-06-02 04:16:35'),
(94, 'GY', 'Guyana', 'SA', '2015-06-02 04:16:35'),
(95, 'HK', 'Hong Kong', 'AS', '2015-06-02 04:16:35'),
(96, 'HM', 'Heard Island and McDonald Islands', 'AN', '2015-06-02 04:16:35'),
(97, 'HN', 'Honduras', 'NA', '2015-06-02 04:16:35'),
(98, 'HR', 'Croatia', 'EU', '2015-06-02 04:16:35'),
(99, 'HT', 'Haiti', 'NA', '2015-06-02 04:16:35'),
(100, 'HU', 'Hungary', 'EU', '2015-06-02 04:16:35'),
(101, 'ID', 'Indonesia', 'AS', '2015-08-02 04:16:35'),
(102, 'IE', 'Ireland', 'EU', '2015-08-02 04:16:35'),
(103, 'IL', 'Israel', 'AS', '2015-08-02 04:16:35'),
(104, 'IM', 'Isle of Man', 'EU', '2015-08-02 04:16:35'),
(105, 'IN', 'India', 'AS', '2015-08-02 04:16:35'),
(106, 'IO', 'British Indian Ocean Territory', 'AS', '2015-08-02 04:16:35'),
(107, 'IQ', 'Iraq', 'AS', '2015-08-02 04:16:35'),
(108, 'IR', 'Iran', 'AS', '2015-08-02 04:16:35'),
(109, 'IS', 'Iceland', 'EU', '2015-08-02 04:16:35'),
(110, 'IT', 'Italy', 'EU', '2015-08-02 04:16:35'),
(111, 'JE', 'Jersey', 'EU', '2015-08-02 04:16:35'),
(112, 'JM', 'Jamaica', 'NA', '2015-08-02 04:16:35'),
(113, 'JO', 'Jordan', 'AS', '2015-08-02 04:16:35'),
(114, 'JP', 'Japan', 'AS', '2015-08-02 04:16:35'),
(115, 'KE', 'Kenya', 'AF', '2015-08-02 04:16:35'),
(116, 'KG', 'Kyrgyzstan', 'AS', '2015-08-02 04:16:35'),
(117, 'KH', 'Cambodia', 'AS', '2015-08-02 04:16:35'),
(118, 'KI', 'Kiribati', 'OC', '2015-08-02 04:16:35'),
(119, 'KM', 'Comoros', 'AF', '2015-08-02 04:16:35'),
(120, 'KN', 'Saint Kitts and Nevis', 'NA', '2015-08-02 04:16:35'),
(121, 'KP', 'North Korea', 'AS', '2015-08-02 04:16:35'),
(122, 'KR', 'South Korea', 'AS', '2015-08-02 04:16:35'),
(123, 'KW', 'Kuwait', 'AS', '2015-08-02 04:16:35'),
(124, 'KY', 'Cayman Islands', 'NA', '2015-08-02 04:16:35'),
(125, 'KZ', 'Kazakhstan', 'AS', '2015-08-02 04:16:35'),
(126, 'LA', 'Laos', 'AS', '2015-08-02 04:16:35'),
(127, 'LB', 'Lebanon', 'AS', '2015-08-02 04:16:35'),
(128, 'LC', 'Saint Lucia', 'NA', '2015-08-02 04:16:35'),
(129, 'LI', 'Liechtenstein', 'EU', '2015-08-02 04:16:35'),
(130, 'LK', 'Sri Lanka', 'AS', '2015-08-02 04:16:35'),
(131, 'LR', 'Liberia', 'AF', '2015-08-02 04:16:35'),
(132, 'LS', 'Lesotho', 'AF', '2015-08-02 04:16:35'),
(133, 'LT', 'Lithuania', 'EU', '2015-08-02 04:16:35'),
(134, 'LU', 'Luxembourg', 'EU', '2015-08-02 04:16:35'),
(135, 'LV', 'Latvia', 'EU', '2015-08-02 04:16:35'),
(136, 'LY', 'Libya', 'AF', '2015-08-02 04:16:35'),
(137, 'MA', 'Morocco', 'AF', '2015-08-02 04:16:35'),
(138, 'MC', 'Monaco', 'EU', '2015-08-02 04:16:35'),
(139, 'MD', 'Moldova', 'EU', '2015-08-02 04:16:35'),
(140, 'ME', 'Montenegro', 'EU', '2015-08-02 04:16:35'),
(141, 'MF', 'Saint Martin', 'NA', '2015-08-02 04:16:35'),
(142, 'MG', 'Madagascar', 'AF', '2015-08-02 04:16:35'),
(143, 'MH', 'Marshall Islands', 'OC', '2015-08-02 04:16:35'),
(144, 'MK', 'Macedonia', 'EU', '2015-08-02 04:16:35'),
(145, 'ML', 'Mali', 'AF', '2015-08-02 04:16:35'),
(146, 'MM', 'Myanmar [Burma]', 'AS', '2015-08-02 04:16:35'),
(147, 'MN', 'Mongolia', 'AS', '2015-08-02 04:16:35'),
(148, 'MO', 'Macao', 'AS', '2015-08-02 04:16:35'),
(149, 'MP', 'Northern Mariana Islands', 'OC', '2015-08-02 04:16:35'),
(150, 'MQ', 'Martinique', 'NA', '2015-08-02 04:16:35'),
(151, 'MR', 'Mauritania', 'AF', '2015-07-02 04:16:35'),
(152, 'MS', 'Montserrat', 'NA', '2015-07-02 04:16:35'),
(153, 'MT', 'Malta', 'EU', '2015-07-02 04:16:35'),
(154, 'MU', 'Mauritius', 'AF', '2015-07-02 04:16:35'),
(155, 'MV', 'Maldives', 'AS', '2015-07-02 04:16:35'),
(156, 'MW', 'Malawi', 'AF', '2015-07-02 04:16:35'),
(157, 'MX', 'Mexico', 'NA', '2015-07-02 04:16:35'),
(158, 'MY', 'Malaysia', 'AS', '2015-07-02 04:16:35'),
(159, 'MZ', 'Mozambique', 'AF', '2015-07-02 04:16:35'),
(160, 'NA', 'Namibia', 'AF', '2015-07-02 04:16:35'),
(161, 'NC', 'New Caledonia', 'OC', '2015-07-02 04:16:35'),
(162, 'NE', 'Niger', 'AF', '2015-07-02 04:16:35'),
(163, 'NF', 'Norfolk Island', 'OC', '2015-07-02 04:16:35'),
(164, 'NG', 'Nigeria', 'AF', '2015-07-02 04:16:35'),
(165, 'NI', 'Nicaragua', 'NA', '2015-07-02 04:16:35'),
(166, 'NL', 'Netherlands', 'EU', '2015-07-02 04:16:35'),
(167, 'NO', 'Norway', 'EU', '2015-07-02 04:16:35'),
(168, 'NP', 'Nepal', 'AS', '2015-07-02 04:16:35'),
(169, 'NR', 'Nauru', 'OC', '2015-07-02 04:16:35'),
(170, 'NU', 'Niue', 'OC', '2015-07-02 04:16:35'),
(171, 'NZ', 'New Zealand', 'OC', '2015-07-02 04:16:35'),
(172, 'OM', 'Oman', 'AS', '2015-07-02 04:16:35'),
(173, 'PA', 'Panama', 'NA', '2015-07-02 04:16:35'),
(174, 'PE', 'Peru', 'SA', '2015-07-02 04:16:35'),
(175, 'PF', 'French Polynesia', 'OC', '2015-07-02 04:16:35'),
(176, 'PG', 'Papua New Guinea', 'OC', '2015-07-02 04:16:35'),
(177, 'PH', 'Philippines', 'AS', '2015-07-02 04:16:35'),
(178, 'PK', 'Pakistan', 'AS', '2015-07-02 04:16:35'),
(179, 'PL', 'Poland', 'EU', '2015-07-02 04:16:35'),
(180, 'PM', 'Saint Pierre and Miquelon', 'NA', '2015-07-02 04:16:35'),
(181, 'PN', 'Pitcairn Islands', 'OC', '2015-07-02 04:16:35'),
(182, 'PR', 'Puerto Rico', 'NA', '2015-07-02 04:16:35'),
(183, 'PS', 'Palestine', 'AS', '2015-07-02 04:16:35'),
(184, 'PT', 'Portugal', 'EU', '2015-07-02 04:16:35'),
(185, 'PW', 'Palau', 'OC', '2015-07-02 04:16:35'),
(186, 'PY', 'Paraguay', 'SA', '2015-07-02 04:16:35'),
(187, 'QA', 'Qatar', 'AS', '2015-07-02 04:16:35'),
(188, 'RE', 'Réunion', 'AF', '2015-07-02 04:16:35'),
(189, 'RO', 'Romania', 'EU', '2015-07-02 04:16:35'),
(190, 'RS', 'Serbia', 'EU', '2015-07-02 04:16:35'),
(191, 'RU', 'Russia', 'EU', '2015-07-02 04:16:35'),
(192, 'RW', 'Rwanda', 'AF', '2015-07-02 04:16:35'),
(193, 'SA', 'Saudi Arabia', 'AS', '2015-07-02 04:16:35'),
(194, 'SB', 'Solomon Islands', 'OC', '2015-07-02 04:16:35'),
(195, 'SC', 'Seychelles', 'AF', '2015-07-02 04:16:35'),
(196, 'SD', 'Sudan', 'AF', '2015-07-02 04:16:35'),
(197, 'SE', 'Sweden', 'EU', '2015-07-02 04:16:35'),
(198, 'SG', 'Singapore', 'AS', '2015-07-02 04:16:35'),
(199, 'SH', 'Saint Helena', 'AF', '2015-07-02 04:16:35'),
(200, 'SI', 'Slovenia', 'EU', '2015-07-02 04:16:35'),
(201, 'SJ', 'Svalbard and Jan Mayen', 'EU', '2015-10-02 04:16:35'),
(202, 'SK', 'Slovakia', 'EU', '2015-10-02 04:16:35'),
(203, 'SL', 'Sierra Leone', 'AF', '2015-10-02 04:16:35'),
(204, 'SM', 'San Marino', 'EU', '2015-10-02 04:16:35'),
(205, 'SN', 'Senegal', 'AF', '2015-10-02 04:16:35'),
(206, 'SO', 'Somalia', 'AF', '2015-10-02 04:16:35'),
(207, 'SR', 'Suriname', 'SA', '2015-10-02 04:16:35'),
(208, 'SS', 'South Sudan', 'AF', '2015-10-02 04:16:35'),
(209, 'ST', 'São Tomé and Príncipe', 'AF', '2015-10-02 04:16:35'),
(210, 'SV', 'El Salvador', 'NA', '2015-10-02 04:16:35'),
(211, 'SX', 'Sint Maarten', 'NA', '2015-10-02 04:16:35'),
(212, 'SY', 'Syria', 'AS', '2015-10-02 04:16:35'),
(213, 'SZ', 'Swaziland', 'AF', '2015-10-02 04:16:35'),
(214, 'TC', 'Turks and Caicos Islands', 'NA', '2015-10-02 04:16:35'),
(215, 'TD', 'Chad', 'AF', '2015-10-02 04:16:35'),
(216, 'TF', 'French Southern Territories', 'AN', '2015-10-02 04:16:35'),
(217, 'TG', 'Togo', 'AF', '2015-10-02 04:16:35'),
(218, 'TH', 'Thailand', 'AS', '2015-10-02 04:16:35'),
(219, 'TJ', 'Tajikistan', 'AS', '2015-10-02 04:16:35'),
(220, 'TK', 'Tokelau', 'OC', '2015-10-02 04:16:35'),
(221, 'TL', 'East Timor', 'OC', '2015-10-02 04:16:35'),
(222, 'TM', 'Turkmenistan', 'AS', '2015-10-02 04:16:35'),
(223, 'TN', 'Tunisia', 'AF', '2015-10-02 04:16:35'),
(224, 'TO', 'Tonga', 'OC', '2015-10-02 04:16:35'),
(225, 'TR', 'Turkey', 'AS', '2015-10-02 04:16:35'),
(226, 'TT', 'Trinidad and Tobago', 'NA', '2015-10-02 04:16:35'),
(227, 'TV', 'Tuvalu', 'OC', '2015-10-02 04:16:35'),
(228, 'TW', 'Taiwan', 'AS', '2015-10-02 04:16:35'),
(229, 'TZ', 'Tanzania', 'AF', '2015-10-02 04:16:35'),
(230, 'UA', 'Ukraine', 'EU', '2015-10-02 04:16:35'),
(231, 'UG', 'Uganda', 'AF', '0000-00-00 00:00:00'),
(232, 'UM', 'U.S. Minor Outlying Islands', 'OC', '0000-00-00 00:00:00'),
(233, 'US', 'United States', 'NA', '0000-00-00 00:00:00'),
(234, 'UY', 'Uruguay', 'SA', '0000-00-00 00:00:00'),
(235, 'UZ', 'Uzbekistan', 'AS', '0000-00-00 00:00:00'),
(236, 'VA', 'Vatican City', 'EU', '0000-00-00 00:00:00'),
(237, 'VC', 'Saint Vincent and the Grenadines', 'NA', '0000-00-00 00:00:00'),
(238, 'VE', 'Venezuela', 'SA', '0000-00-00 00:00:00'),
(239, 'VG', 'British Virgin Islands', 'NA', '0000-00-00 00:00:00'),
(240, 'VI', 'U.S. Virgin Islands', 'NA', '0000-00-00 00:00:00'),
(241, 'VN', 'Vietnam', 'AS', '0000-00-00 00:00:00'),
(242, 'VU', 'Vanuatu', 'OC', '0000-00-00 00:00:00'),
(243, 'WF', 'Wallis and Futuna', 'OC', '0000-00-00 00:00:00'),
(244, 'WS', 'Samoa', 'OC', '0000-00-00 00:00:00'),
(245, 'XK', 'Kosovo', 'EU', '0000-00-00 00:00:00'),
(246, 'YE', 'Yemen', 'AS', '0000-00-00 00:00:00'),
(247, 'YT', 'Mayotte', 'AF', '0000-00-00 00:00:00'),
(248, 'ZA', 'South Africa', 'AF', '0000-00-00 00:00:00'),
(249, 'ZM', 'Zambia', 'AF', '0000-00-00 00:00:00'),
(250, 'ZW', 'Zimbabwe', 'AF', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Stand-in structure for view `daily_expense_report`
--
CREATE TABLE `daily_expense_report` (
`job_card_id` int(11)
,`client_id` int(11)
,`client_name` varchar(255)
,`job_card_no` varchar(255)
,`invoice_no` varchar(255)
,`total_expense` decimal(35,2)
,`year` int(4)
,`month` int(2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `daily_revenue_report`
--
CREATE TABLE `daily_revenue_report` (
`job_card_id` int(11)
,`client_id` int(11)
,`client_name` varchar(255)
,`job_card_no` varchar(255)
,`invoice_no` varchar(255)
,`total_revenue` decimal(32,0)
,`year` int(4)
,`month` int(2)
);
-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `status` enum('Active','In Active') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`, `timestamp`, `user_id`, `status`) VALUES
(1, 'Operations', '2015-11-23 18:25:48', 0, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `s_name` varchar(255) NOT NULL,
  `o_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `id_no` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `marital_status` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `role_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `status` enum('Active','In Active') NOT NULL,
  `date_added` datetime NOT NULL,
  `phone_no` varchar(255) NOT NULL,
  `gender` enum('Female','Male') NOT NULL,
  `employee_no` varchar(255) NOT NULL,
  `random_key` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `title`, `f_name`, `s_name`, `o_name`, `email`, `id_no`, `dob`, `marital_status`, `nationality`, `time_stamp`, `role_id`, `department_id`, `status`, `date_added`, `phone_no`, `gender`, `employee_no`, `random_key`) VALUES
(1, '', 'Admin', 'Admin', 'Admin', 'harris.samuel@strathmore.edu', '28200148', '2015-11-13', 'Single', 'Kenya', '2015-11-23 18:26:48', 1, 1, 'Active', '2015-11-23 21:26:48', 'bvecd', 'Female', 'UL_001', ''),
(2, '', 'Paul', 'Majaga', 'Andole', 'paulmajaga@gmail.com', '25571738', '2015-12-28', 'Single', 'Kenya', '2015-12-23 09:48:34', 1, 1, 'Active', '2015-12-23 12:48:34', '0723991599', 'Male', 'UL_002', '');

-- --------------------------------------------------------

--
-- Stand-in structure for view `expenses_incurred`
--
CREATE TABLE `expenses_incurred` (
`total_expenses` decimal(35,2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `expenses_per_job_card`
--
CREATE TABLE `expenses_per_job_card` (
`client_id` int(11)
,`client_name` varchar(255)
,`job_card_id` int(11)
,`job_card_no` varchar(255)
,`invoice_no` varchar(255)
,`expense` decimal(35,2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `expense_per_client`
--
CREATE TABLE `expense_per_client` (
`client_id` int(11)
,`client_name` varchar(255)
,`industry` varchar(255)
,`total_expense` decimal(35,2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `expense_per_month`
--
CREATE TABLE `expense_per_month` (
`expense_month` int(2)
,`expense_year` int(4)
,`expense` decimal(35,2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `expense_ytd`
--
CREATE TABLE `expense_ytd` (
`expense` decimal(35,2)
,`year` int(4)
);
-- --------------------------------------------------------

--
-- Table structure for table `functions`
--

CREATE TABLE `functions` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` text NOT NULL,
  `status` enum('Active','In Active') NOT NULL,
  `level` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `controller_name` text NOT NULL,
  `function_name` text NOT NULL,
  `i_tag` text NOT NULL,
  `description` text NOT NULL,
  `span_tag` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `functions`
--

INSERT INTO `functions` (`id`, `name`, `code`, `status`, `level`, `date_added`, `timestamp`, `controller_name`, `function_name`, `i_tag`, `description`, `span_tag`) VALUES
(1, 'Employees', ' <li><a href="<?php echo base_url(); ?>admin/employees"><i class="fa fa-edit"></i> Employees <span class="fa fa-chevron-right"></span></a>                                           ', 'Active', 1, '2015-10-21 19:48:52', '2015-11-14 13:20:43', 'admin', 'employees', '<i class="fa fa-edit"></i>', 'Employees', '<span class="fa fa-chevron-right"></span>'),
(2, 'Users', '<li><a href="<?php echo base_url(); ?>admin/users"><i class="fa fa-edit"></i> Users <span class="fa fa-chevron-right"></span></a>', 'Active', 1, '2015-10-21 21:45:54', '2015-10-22 13:39:19', 'admin', 'users', '<i class="fa fa-edit"></i>', 'Users', '<span class="fa fa-chevron-right"></span>'),
(3, 'Roles', ' <li><a href="<?php echo base_url(); ?>admin/roles"><i class="fa fa-edit"></i> Roles <span class="fa fa-chevron-right"></span></a>\n                                           ', 'Active', 1, '2015-10-21 16:41:46', '2015-10-22 13:22:17', 'admin', 'roles', '<i class="fa fa-edit"></i>', 'Roles', '<span class="fa fa-chevron-right"></span>'),
(4, 'Department', ' <li><a href="<?php echo base_url(); ?>admin/department"><i class="fa fa-edit"></i> Department <span class="fa fa-chevron-right"></span></a>\n                                           ', 'Active', 1, '2015-10-21 19:49:48', '2015-10-22 13:22:18', 'admin', 'department', '<i class="fa fa-edit"></i>', 'Department', '<span class="fa fa-chevron-right"></span>'),
(5, 'Assets', ' <li><a href="<?php echo base_url(); ?>admin/assets"><i class="fa fa-edit"></i> Assets <span class="fa fa-chevron-right"></span></a>\n                                           ', 'Active', 1, '2015-10-21 19:51:00', '2015-10-22 13:22:20', 'admin', 'assets', '<i class="fa fa-edit"></i>', 'Assets', '<span class="fa fa-chevron-right"></span>'),
(6, 'Clients', ' <li><a href="<?php echo base_url(); ?>admin/clients"><i class="fa fa-edit"></i> Clients <span class="fa fa-chevron-right"></span></a>\n                                           ', 'Active', 1, '2015-10-21 18:48:00', '2015-10-22 13:22:22', 'admin', 'clients', '<i class="fa fa-edit"></i>', 'Clients', '<span class="fa fa-chevron-right"></span>'),
(7, 'Standards', ' <li><a href="<?php echo base_url(); ?>admin/standards"><i class="fa fa-edit"></i> System Standards <span class="fa fa-chevron-right"></span></a></li>\n                                       ', 'Active', 1, '2015-10-21 20:49:00', '2015-10-22 13:22:24', 'admin', 'standards', '<i class="fa fa-edit"></i>', 'Standards', '<span class="fa fa-chevron-right"></span>'),
(8, 'Book Job Card', ' <li><a href="<?php echo base_url(); ?>operations">Book Job Card</a>\n                                            </li>\n                                           ', 'Active', 2, '2015-10-21 16:47:00', '2015-10-30 08:15:00', 'operations', 'index', '<i class="fa fa-edit"></i>', 'Book Job Card', '<span class="fa fa-chevron-right"></span>'),
(9, 'Payments', ' <li><a href="<?php echo base_url(); ?>operations/payments">Cashier</a>\n                                            </li>\n                                           ', 'Active', 2, '2015-10-21 18:45:00', '2015-10-30 07:17:38', 'operations', 'payments', '<i class="fa fa-edit"></i>', 'Payments', '<span class="fa fa-chevron-right"></span>'),
(10, 'Operations', ' <li><a href="<?php echo base_url(); ?>operations/cashier">Operations</a>\n                                            </li>\n', 'Active', 2, '2015-10-21 16:44:00', '2015-10-22 13:22:30', 'operations', 'cashier', '<i class="fa fa-edit"></i>', 'Operations', '<span class="fa fa-chevron-right"></span>'),
(11, 'Dashboard', '<li><a href="<?php echo base_url(); ?>operations/dashboard">Dashboard</a>\n                                            </li>\n                                           ', 'Active', 3, '2015-10-21 16:45:00', '2015-10-22 13:22:31', 'operations', 'dashboard', '<i class="fa fa-edit"></i>', 'Dashboard', '<span class="fa fa-chevron-right"></span>'),
(12, 'Daily Operation Report', ' <li><a href="<?php echo base_url(); ?>operations/daily_report">Daily Report</a>\n                                            </li>\n', 'Active', 3, '2015-10-21 19:47:00', '2015-10-22 13:22:34', 'operations', 'daily_report', '<i class="fa fa-edit"></i>', 'Daily Report', '<span class="fa fa-chevron-right"></span>'),
(13, 'Client Job Statement', ' <li><a href="<?php echo base_url(); ?>operations/client_payment_statement">Client Job Statements</a>                                             </li>', 'Active', 3, '2015-10-21 16:46:52', '2015-10-22 13:22:35', 'operations', 'client_payment_statement', '<i class="fa fa-edit"></i>', 'Client Job Statement', '<span class="fa fa-chevron-right"></span>'),
(14, 'Access Control List', ' <li><a href="<?php echo base_url(); ?>admin/access_control"><i class="fa fa-edit"></i> Access Control List <span class="fa fa-chevron-right"></span></a>\r\n                                           ', 'Active', 1, '2015-10-22 15:00:00', '2015-10-22 13:41:09', 'admin', 'access_control', '<i class="fa fa-edit"></i>', 'Access Control List', '<span class="fa fa-chevron-right"></span>'),
(15, 'Asset Types', ' <li><a href="<?php echo base_url(); ?>admin/asset_type"><i class="fa fa-edit"></i> Asset Type <span class="fa fa-chevron-right"></span></a>                                           ', 'Active', 1, '0000-00-00 00:00:00', '2015-10-27 15:27:35', 'admin', 'asset_type', '<i class="fa fa-edit"></i>', 'Asset Type', '<span class="fa fa-chevron-right"></span>'),
(16, 'View Job Status', ' <li><a href="<?php echo base_url(); ?>admin/asset_type"><i class="fa fa-edit"></i> Asset Type <span class="fa fa-chevron-right"></span></a>                                           ', 'Active', 2, '0000-00-00 00:00:00', '2015-10-30 18:21:55', 'operations', 'view_job_status', '<i class="fa fa-edit"></i>', 'View Job Status', '<span class="fa fa-chevron-right"></span>'),
(17, 'Approve Payments', '', 'Active', 2, '0000-00-00 00:00:00', '2015-10-30 08:14:22', 'operations', 'ack_payments', '<i class="fa fa-edit"></i>', 'Approve Payments', '<span class="fa fa-chevron-right"></span>'),
(18, 'Job Event Report', ' <li><a href="<?php echo base_url(); ?>operations/event_job_details"><i class="fa fa-edit"></i> Job Event Report <span class="fa fa-chevron-right"></span></a>                                           ', 'Active', 3, '0000-00-00 00:00:00', '2015-11-07 15:52:13', 'operations', 'event_job_details', '<i class="fa fa-edit"></i>', 'Job Event Report', '<span class="fa fa-chevron-right"></span>'),
(19, 'Future Job Report', ' <li><a href="<?php echo base_url(); ?>operations/event_job_details"><i class="fa fa-edit"></i> Job Event Report <span class="fa fa-chevron-right"></span></a>                                           ', 'Active', 3, '0000-00-00 00:00:00', '2015-11-07 15:52:13', 'operations', 'future_jobs_report', '<i class="fa fa-edit"></i>', 'Future Job Report', '<span class="fa fa-chevron-right"></span>'),
(20, 'Payment Report', '<span class="fa fa-chevron-right"></span>', 'Active', 3, '0000-00-00 00:00:00', '2015-11-07 15:53:32', 'operations', 'payment_report', '<i class="fa fa-edit"></i>', 'Payment Report', '<span class="fa fa-chevron-right"></span>'),
(21, 'View Payments', ' <li><a href="<?php echo base_url(); ?>admin/employees"><i class="fa fa-edit"></i> Employees <span class="fa fa-chevron-right"></span></a>                                           ', 'Active', 2, '0000-00-00 00:00:00', '2015-11-11 16:41:52', 'operations', 'view_payments', '<i class="fa fa-edit"></i>', 'View Payments', '<span class="fa fa-chevron-right"></span>'),
(22, 'Asset Status List', '', 'Active', 2, '0000-00-00 00:00:00', '2015-11-15 06:41:18', 'operations', 'asset_status_list', '<i class="fa fa-edit"></i>', 'Asset Status List', '<span class="fa fa-chevron-right"></span>'),
(23, 'Sales Report', '', 'Active', 3, '0000-00-00 00:00:00', '2015-12-23 17:55:27', 'operations', 'sales_report', '<i class="fa fa-edit"></i>', 'Sales Report', '<span class="fa fa-chevron-right"></span>'),
(24, 'Client Statement of Accounts', '', 'Active', 3, '0000-00-00 00:00:00', '2015-12-23 18:35:26', 'operations', 'client_accnt_statement', '<i class="fa fa-edit"></i>', 'Client Statement of Accounts', '<span class="fa fa-chevron-right"></span>');

-- --------------------------------------------------------

--
-- Stand-in structure for view `in_active_job_cards`
--
CREATE TABLE `in_active_job_cards` (
`event_month` int(2)
,`event_year` int(4)
,`event_status_count` bigint(21)
);
-- --------------------------------------------------------

--
-- Table structure for table `job_card`
--

CREATE TABLE `job_card` (
`id` int(11) NOT NULL,
  `clnt_id` int(11) DEFAULT NULL,
  `dte_srcd` varchar(255) DEFAULT NULL,
  `dte_evnt` varchar(255) DEFAULT NULL,
  `lctn` varchar(255) DEFAULT NULL,
  `estmtd_lctn_dstnce` varchar(255) DEFAULT NULL,
  `cnfrmd_lctn_dstnce` varchar(255) DEFAULT NULL,
  `no_units` varchar(255) DEFAULT NULL,
  `dpsts` decimal(13,2) DEFAULT NULL,
  `ttl_costs` decimal(13,2) DEFAULT NULL,
  `rate` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `time_stamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `paid` enum('Not Paid','Paid') DEFAULT NULL,
  `job_card_no` varchar(255) DEFAULT NULL,
  `time_from` varchar(255) DEFAULT NULL,
  `time_to` varchar(255) DEFAULT NULL,
  `invoiced` enum('No','Yes') DEFAULT NULL,
  `invoice_no` varchar(255) DEFAULT NULL,
  `status` enum('Active','In Active','Cancelled') DEFAULT 'Active',
  `user_id` int(11) DEFAULT NULL,
  `rsn_fr_cncl` text,
  `notes` text,
  `venue` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_card`
--

INSERT INTO `job_card` (`id`, `clnt_id`, `dte_srcd`, `dte_evnt`, `lctn`, `estmtd_lctn_dstnce`, `cnfrmd_lctn_dstnce`, `no_units`, `dpsts`, `ttl_costs`, `rate`, `date_added`, `time_stamp`, `paid`, `job_card_no`, `time_from`, `time_to`, `invoiced`, `invoice_no`, `status`, `user_id`, `rsn_fr_cncl`, `notes`, `venue`) VALUES
(1, 2, '2015-12-22', '2015-12-23', 'Karen Nairobi Kenya', '12', '12', NULL, NULL, NULL, NULL, '2015-12-22 21:23:38', '2015-12-24 02:24:47', NULL, 'JC-1-1', '12:00 PM', '12:00 AM', NULL, NULL, 'Active', 1, NULL, NULL, 'Nyayo Stadium'),
(2, 2, '2015-12-23', '2015-12-24', 'Karen Nairobi Kenya', '12', '12', NULL, NULL, NULL, NULL, '2015-12-22 21:53:45', '2015-12-22 18:53:45', NULL, 'JC-1-2', '9:00 AM', '11:00 PM', NULL, NULL, NULL, 1, NULL, 'Nnovbodfbv ovboudbv dbvobdo lvneoiudbviu dviuob iub ioub', 'Nyayo Stadium'),
(3, 2, '2015-12-23', '2015-12-26', 'Karen Nairobi Kenya', '12', '12', NULL, NULL, NULL, NULL, '2015-12-22 21:57:04', '2015-12-22 18:57:04', NULL, 'JC-1-3', '9:00 AM', '11:00 PM', NULL, NULL, NULL, 1, NULL, 'Nnsovbnof novnfoivn dovoin odnovinfo', 'Nyayo Stadium'),
(4, 2, '2015-12-24', '2015-12-24', 'Karen Nairobi Kenya', '12', '12', NULL, NULL, NULL, NULL, '2015-12-22 22:03:29', '2015-12-22 19:04:00', NULL, 'JC-1-4', '9:00 AM', '11:00 PM', NULL, 'INV-1-1', NULL, 1, NULL, 'Nsifnboi  voinfoiv dofnvoif', 'Nyayo Stadium'),
(5, 3, '2015-12-24', '2015-12-25', 'Karen Nairobi Kenya', '12', '12', NULL, NULL, NULL, NULL, '2015-12-23 08:29:55', '2015-12-23 05:34:33', NULL, 'JC-1-5', '9:00 AM', '11:00 PM', NULL, 'INV-1-2', NULL, 1, NULL, 'Nncsidonvodf oinvoidfnc v od ib nfgb', 'Nyayo Stadium'),
(6, 1, '2015-12-30', '2015-12-31', 'Nairobi West', '12', '12', NULL, NULL, NULL, NULL, '2015-12-23 08:35:45', '2015-12-23 05:36:32', NULL, 'JC-1-6', '12:00 PM', '12:00 AM', NULL, 'INV-1-3', NULL, 1, NULL, 'Ncncosidv invoidfnbvocnxvio dnfoinv idnfoiv nfd', 'Nyayo Stadium'),
(7, 2, '2015-12-24', '2015-12-26', 'Nyayo National Stadium', '12', '13', NULL, NULL, NULL, NULL, '2015-12-23 21:43:30', '2015-12-23 18:43:30', NULL, 'JC-1-7', '12:00 PM', '12:00 AM', NULL, NULL, NULL, 1, NULL, 'Nnvfbuobd bvubdf dvbu bduv', 'Nyayo Stadium'),
(8, 1, '2015-12-26', '2015-12-28', 'Karen Nairobi Kenya', '12', '12', NULL, NULL, NULL, NULL, '2015-12-24 05:09:38', '2015-12-24 02:10:24', NULL, 'JC-1-8', '9:00 AM', '12:00 AM', NULL, 'INV-1-1', NULL, 1, NULL, 'Nnsuobvuifo duvnbui fbduub', 'Nyayo Stadium'),
(9, 1, '2015-12-25', '2015-12-25', 'Karen Nairobi Kenya', '12', '12', NULL, NULL, NULL, NULL, '2015-12-24 05:22:50', '2015-12-24 02:23:25', NULL, 'JC-1-9', '9:00 AM', '11:00 PM', NULL, 'INV-1-2', 'Active', 1, NULL, 'Nnobu uvbnuid dfviubiubviu g', 'Nyayo Stadium');

-- --------------------------------------------------------

--
-- Stand-in structure for view `job_cards_client`
--
CREATE TABLE `job_cards_client` (
`client_id` int(11)
,`client_name` varchar(255)
,`client_address` varchar(255)
,`client_industry` varchar(255)
,`client_phone` varchar(255)
,`client_email` varchar(255)
,`client_status` enum('Active','In Active')
,`id` int(11)
,`job_card_no` varchar(255)
,`invoice_no` varchar(255)
);
-- --------------------------------------------------------

--
-- Table structure for table `job_card_no`
--

CREATE TABLE `job_card_no` (
`id` int(11) NOT NULL,
  `no` varchar(255) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `job_card_status`
--
CREATE TABLE `job_card_status` (
`client_id` int(11)
,`client_name` varchar(255)
,`phone_no` varchar(255)
,`job_card_id` int(11)
,`user_id` int(11)
,`job_card_no` varchar(255)
,`invoice_no` varchar(255)
,`amount_paid` decimal(32,0)
,`amount_billed` decimal(32,0)
,`amount_owed` decimal(33,0)
,`job_card_status` enum('Active','In Active','Cancelled')
,`date_opened` datetime
,`event_date` varchar(255)
,`date_sourced` varchar(255)
);
-- --------------------------------------------------------

--
-- Table structure for table `login_logs`
--

CREATE TABLE `login_logs` (
`login_logs_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `login_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` enum('Active','In Active') NOT NULL,
  `login_ip_address` text NOT NULL,
  `month` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_logs`
--

INSERT INTO `login_logs` (`login_logs_id`, `user_id`, `user_name`, `login_date`, `is_active`, `login_ip_address`, `month`, `year`, `time_start`, `time_end`) VALUES
(1, 1, 'Admin', '2015-11-18 17:44:50', 'In Active', 'Platform : Mac OS X / Agent : Firefox 42.0 / Login IP address : ::1', 'November', '2015', '20:44:50', '12:55:41'),
(2, 1, 'Admin', '2015-11-20 09:55:41', 'In Active', 'Platform : Mac OS X / Agent : Firefox 42.0 / Login IP address : ::1', 'November', '2015', '12:55:41', '12:57:13'),
(3, 1, 'Admin', '2015-11-20 09:57:13', 'In Active', 'Platform : Mac OS X / Agent : Firefox 42.0 / Login IP address : ::1', 'November', '2015', '12:57:13', '12:58:10'),
(4, 1, 'Admin', '2015-11-20 09:58:10', 'In Active', 'Platform : Mac OS X / Agent : Firefox 42.0 / Login IP address : ::1', 'November', '2015', '12:58:10', '21:23:50'),
(5, 1, 'Admin', '2015-11-23 18:23:50', 'In Active', 'Platform : Mac OS X / Agent : Firefox 42.0 / Login IP address : ::1', 'November', '2015', '21:23:50', '12:01:28'),
(6, 1, 'Admin', '2015-12-01 09:01:28', 'In Active', 'Platform : Android / Agent : Chrome 46.0.2490.76 / Login IP address : 192.168.0.23', 'December', '2015', '12:01:28', '10:32:13'),
(7, 1, 'Admin', '2015-12-16 07:32:13', 'In Active', 'Platform : Mac OS X / Agent : Firefox 42.0 / Login IP address : ::1', 'December', '2015', '10:32:13', '14:45:32'),
(8, 1, 'Admin', '2015-12-18 11:45:31', 'In Active', 'Platform : Mac OS X / Agent : Firefox 42.0 / Login IP address : ::1', 'December', '2015', '14:45:31', '17:39:42'),
(9, 1, 'Admin', '2015-12-22 14:39:41', 'In Active', 'Platform : Mac OS X / Agent : Firefox 43.0 / Login IP address : ::1', 'December', '2015', '17:39:41', '18:30:10'),
(10, 1, 'Admin', '2015-12-22 15:30:10', 'In Active', 'Platform : Mac OS X / Agent : Firefox 43.0 / Login IP address : ::1', 'December', '2015', '18:30:10', '20:44:48'),
(11, 1, 'Admin', '2015-12-22 17:44:48', 'In Active', 'Platform : Mac OS X / Agent : Firefox 43.0 / Login IP address : ::1', 'December', '2015', '20:44:48', '21:12:59'),
(12, 1, 'Admin', '2015-12-22 18:12:59', 'In Active', 'Platform : Mac OS X / Agent : Firefox 43.0 / Login IP address : ::1', 'December', '2015', '21:12:59', '21:32:03'),
(13, 1, 'Admin', '2015-12-22 18:32:03', 'In Active', 'Platform : Mac OS X / Agent : Firefox 43.0 / Login IP address : ::1', 'December', '2015', '21:32:03', '04:15:35'),
(14, 1, 'Admin', '2015-12-23 01:15:35', 'In Active', 'Platform : Mac OS X / Agent : Firefox 43.0 / Login IP address : ::1', 'December', '2015', '04:15:35', '05:13:04'),
(15, 1, 'Admin', '2015-12-23 02:13:04', 'In Active', 'Platform : Mac OS X / Agent : Firefox 43.0 / Login IP address : ::1', 'December', '2015', '05:13:04', '06:11:13'),
(16, 1, 'Admin', '2015-12-23 03:11:13', 'In Active', 'Platform : Mac OS X / Agent : Firefox 43.0 / Login IP address : ::1', 'December', '2015', '06:11:13', '07:33:52'),
(17, 1, 'Admin', '2015-12-23 04:33:52', 'In Active', 'Platform : Mac OS X / Agent : Firefox 43.0 / Login IP address : ::1', 'December', '2015', '07:33:52', '15:59:42'),
(18, 1, 'Admin', '2015-12-23 12:59:42', 'In Active', 'Platform : Mac OS X / Agent : Firefox 43.0 / Login IP address : ::1', 'December', '2015', '15:59:42', '20:25:55'),
(19, 1, 'Admin', '2015-12-23 17:25:55', 'In Active', 'Platform : Mac OS X / Agent : Firefox 43.0 / Login IP address : ::1', 'December', '2015', '20:25:55', '05:06:36'),
(20, 1, 'Admin', '2015-12-24 02:06:36', 'In Active', 'Platform : Mac OS X / Agent : Firefox 43.0 / Login IP address : ::1', 'December', '2015', '05:06:36', '05:22:16'),
(21, 1, 'Admin', '2015-12-24 02:22:16', 'Active', 'Platform : Mac OS X / Agent : Firefox 43.0 / Login IP address : ::1', 'December', '2015', '05:22:16', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `monthly_expense`
--
CREATE TABLE `monthly_expense` (
`expense` decimal(35,2)
,`expense_period` varchar(40)
,`expense_year` varchar(4)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `monthly_revenue`
--
CREATE TABLE `monthly_revenue` (
`revenues` decimal(35,2)
,`revenue_period` varchar(40)
,`revenue_year` varchar(4)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `performance`
--
CREATE TABLE `performance` (
`performance_month` int(2)
,`performance_year` int(4)
,`performance_sales` decimal(13,2)
,`performance_expense` decimal(35,2)
);
-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `function_id` int(11) NOT NULL,
  `status` enum('Active','In Active') NOT NULL,
  `date_added` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=470 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `user_id`, `function_id`, `status`, `date_added`, `timestamp`) VALUES
(58, 0, 8, 'Active', '2015-10-22 13:07:41', '2015-10-22 10:07:41'),
(59, 0, 9, 'Active', '2015-10-22 13:07:41', '2015-10-22 10:07:41'),
(60, 0, 13, 'Active', '2015-10-22 13:07:41', '2015-10-22 10:07:41'),
(104, 2, 11, 'Active', '2015-10-22 15:13:49', '2015-10-22 12:13:49'),
(105, 2, 12, 'Active', '2015-10-22 15:13:49', '2015-10-22 12:13:49'),
(106, 2, 13, 'Active', '2015-10-22 15:13:49', '2015-10-22 12:13:49'),
(107, 3, 1, 'Active', '2015-10-22 15:14:03', '2015-10-22 12:14:03'),
(108, 3, 2, 'Active', '2015-10-22 15:14:03', '2015-10-22 12:14:03'),
(109, 3, 3, 'Active', '2015-10-22 15:14:03', '2015-10-22 12:14:03'),
(110, 3, 4, 'Active', '2015-10-22 15:14:03', '2015-10-22 12:14:03'),
(111, 3, 5, 'Active', '2015-10-22 15:14:03', '2015-10-22 12:14:03'),
(112, 3, 6, 'Active', '2015-10-22 15:14:03', '2015-10-22 12:14:03'),
(113, 3, 7, 'Active', '2015-10-22 15:14:03', '2015-10-22 12:14:03'),
(114, 3, 8, 'Active', '2015-10-22 15:14:03', '2015-10-22 12:14:03'),
(115, 3, 9, 'Active', '2015-10-22 15:14:03', '2015-10-22 12:14:03'),
(116, 3, 10, 'Active', '2015-10-22 15:14:03', '2015-10-22 12:14:03'),
(117, 3, 11, 'Active', '2015-10-22 15:14:03', '2015-10-22 12:14:03'),
(118, 3, 12, 'Active', '2015-10-22 15:14:03', '2015-10-22 12:14:03'),
(119, 3, 13, 'Active', '2015-10-22 15:14:03', '2015-10-22 12:14:03'),
(120, 4, 1, 'Active', '2015-10-22 15:14:22', '2015-10-22 12:14:22'),
(121, 4, 2, 'Active', '2015-10-22 15:14:22', '2015-10-22 12:14:22'),
(122, 4, 3, 'Active', '2015-10-22 15:14:22', '2015-10-22 12:14:22'),
(123, 4, 4, 'Active', '2015-10-22 15:14:22', '2015-10-22 12:14:22'),
(124, 4, 5, 'Active', '2015-10-22 15:14:22', '2015-10-22 12:14:22'),
(125, 4, 6, 'Active', '2015-10-22 15:14:22', '2015-10-22 12:14:22'),
(126, 4, 7, 'Active', '2015-10-22 15:14:22', '2015-10-22 12:14:22'),
(127, 4, 8, 'Active', '2015-10-22 15:14:22', '2015-10-22 12:14:22'),
(128, 4, 9, 'Active', '2015-10-22 15:14:22', '2015-10-22 12:14:22'),
(129, 4, 10, 'Active', '2015-10-22 15:14:22', '2015-10-22 12:14:22'),
(181, 5, 1, 'Active', '2015-10-24 11:43:18', '2015-10-24 08:43:18'),
(182, 5, 2, 'Active', '2015-10-24 11:43:18', '2015-10-24 08:43:18'),
(183, 5, 3, 'Active', '2015-10-24 11:43:18', '2015-10-24 08:43:18'),
(184, 5, 4, 'Active', '2015-10-24 11:43:18', '2015-10-24 08:43:18'),
(185, 5, 5, 'Active', '2015-10-24 11:43:18', '2015-10-24 08:43:18'),
(186, 5, 6, 'Active', '2015-10-24 11:43:18', '2015-10-24 08:43:18'),
(187, 5, 7, 'Active', '2015-10-24 11:43:18', '2015-10-24 08:43:18'),
(188, 5, 14, 'Active', '2015-10-24 11:43:18', '2015-10-24 08:43:18'),
(189, 5, 8, 'Active', '2015-10-24 11:43:18', '2015-10-24 08:43:18'),
(190, 5, 9, 'Active', '2015-10-24 11:43:18', '2015-10-24 08:43:18'),
(191, 5, 10, 'Active', '2015-10-24 11:43:18', '2015-10-24 08:43:18'),
(192, 5, 11, 'Active', '2015-10-24 11:43:18', '2015-10-24 08:43:18'),
(193, 5, 12, 'Active', '2015-10-24 11:43:18', '2015-10-24 08:43:18'),
(194, 5, 13, 'Active', '2015-10-24 11:43:18', '2015-10-24 08:43:18'),
(446, 1, 1, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(447, 1, 2, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(448, 1, 3, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(449, 1, 4, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(450, 1, 5, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(451, 1, 6, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(452, 1, 7, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(453, 1, 14, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(454, 1, 15, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(455, 1, 8, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(456, 1, 9, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(457, 1, 10, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(458, 1, 16, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(459, 1, 17, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(460, 1, 21, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(461, 1, 22, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(462, 1, 11, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(463, 1, 12, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(464, 1, 13, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(465, 1, 18, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(466, 1, 19, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(467, 1, 20, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(468, 1, 23, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45'),
(469, 1, 24, 'Active', '2015-12-23 21:35:45', '2015-12-23 18:35:45');

-- --------------------------------------------------------

--
-- Stand-in structure for view `revenue_collected`
--
CREATE TABLE `revenue_collected` (
`total_revenue` decimal(32,0)
,`invoice_no` varchar(255)
,`date_added` datetime
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `revenue_per_client`
--
CREATE TABLE `revenue_per_client` (
`client_id` int(11)
,`client_name` varchar(255)
,`phone_no` varchar(255)
,`industry` varchar(255)
,`revenue_collected` decimal(35,2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `revenue_per_job_card`
--
CREATE TABLE `revenue_per_job_card` (
`client_id` int(11)
,`client_name` varchar(255)
,`job_card_id` int(11)
,`job_card_no` varchar(255)
,`invoice_no` varchar(255)
,`revenue` decimal(32,0)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `revenue_per_month`
--
CREATE TABLE `revenue_per_month` (
`revenue_month` int(2)
,`revenue_year` int(4)
,`revenue` decimal(35,2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `revenue_ytd`
--
CREATE TABLE `revenue_ytd` (
`revenue` decimal(32,0)
,`year` int(4)
);
-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` enum('Active','In Active') NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `status`, `timestamp`, `date_added`) VALUES
(1, 'Operations', 'Active', '2015-11-23 18:25:31', '2015-11-23 21:25:31');

-- --------------------------------------------------------

--
-- Stand-in structure for view `sales_report`
--
CREATE TABLE `sales_report` (
`employee_name` text
,`employee_no` varchar(255)
,`job_card_id` int(11)
,`job_card_no` varchar(255)
,`invoice_no` varchar(255)
,`amount_paid` decimal(13,2)
,`amount_billed` decimal(14,2)
,`discount` decimal(13,2)
,`date_added` varchar(50)
,`event_date` varchar(50)
,`client_name` varchar(255)
,`client_id` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `standards`
--

CREATE TABLE `standards` (
`id` int(11) NOT NULL,
  `parameter` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','In Active') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `standards`
--

INSERT INTO `standards` (`id`, `parameter`, `value`, `date_added`, `timestamp`, `status`) VALUES
(1, 'Single Toilet', '5800', '2015-12-22 21:55:19', '2015-12-23 18:42:31', 'Active'),
(2, 'Trailers', '12300', '2015-12-22 21:55:26', '2015-12-22 18:55:50', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `statement`
--

CREATE TABLE `statement` (
`id` int(11) NOT NULL,
  `clnt_id` int(11) NOT NULL,
  `job_card_id` int(11) NOT NULL,
  `amnt_dr` int(11) NOT NULL,
  `amnt_cr` int(11) NOT NULL,
  `pymnt_mthd` varchar(255) NOT NULL,
  `pymnt_code` varchar(255) NOT NULL,
  `invoice_no` varchar(255) NOT NULL,
  `rcpt_no` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` varchar(255) NOT NULL,
  `invoiced` enum('No','Yes') NOT NULL,
  `status` enum('Active','In Active') NOT NULL,
  `qty` int(11) NOT NULL,
  `payment_status` enum('Pending','Cleared') NOT NULL,
  `approved` enum('Not Approved','Approved') NOT NULL,
  `payment_date` date NOT NULL,
  `discount` decimal(13,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statement`
--

INSERT INTO `statement` (`id`, `clnt_id`, `job_card_id`, `amnt_dr`, `amnt_cr`, `pymnt_mthd`, `pymnt_code`, `invoice_no`, `rcpt_no`, `date_added`, `timestamp`, `description`, `invoiced`, `status`, `qty`, `payment_status`, `approved`, `payment_date`, `discount`) VALUES
(1, 2, 4, 0, 23200, '', '', '', '', '2015-12-22 22:04:00', '2015-12-22 19:07:14', 'Toilet', 'Yes', 'Active', 4, 'Pending', 'Not Approved', '0000-00-00', 5000.00),
(2, 2, 4, 0, 36900, '', '', '', '', '2015-12-22 22:04:00', '2015-12-22 19:07:20', 'Trailers', 'Yes', 'Active', 3, 'Pending', 'Not Approved', '0000-00-00', 10000.00),
(3, 3, 5, 0, 58000, '', '', '', '', '2015-12-23 08:34:33', '2015-12-23 05:34:33', 'Toilet', 'Yes', 'Active', 10, 'Pending', 'Not Approved', '0000-00-00', 8000.00),
(4, 3, 5, 0, 49200, '', '', '', '', '2015-12-23 08:34:33', '2015-12-23 05:34:33', 'Trailers', 'Yes', 'Active', 4, 'Pending', 'Not Approved', '0000-00-00', 9200.00),
(5, 1, 6, 0, 145000, '', '', '', '', '2015-12-23 08:36:32', '2015-12-23 05:36:32', 'Toilet', 'Yes', 'Active', 25, 'Pending', 'Not Approved', '0000-00-00', 5000.00),
(6, 1, 6, 0, 123000, '', '', '', '', '2015-12-23 08:36:32', '2015-12-23 05:36:32', 'Trailers', 'Yes', 'Active', 10, 'Pending', 'Not Approved', '0000-00-00', 8000.00),
(7, 1, 8, 0, 23200, '', '', '', '', '2015-12-24 05:10:24', '2015-12-24 02:10:24', 'Single Toilet', 'Yes', 'Active', 4, 'Pending', 'Not Approved', '0000-00-00', 200.00),
(8, 1, 8, 0, 24600, '', '', '', '', '2015-12-24 05:10:24', '2015-12-24 02:10:24', 'Trailers', 'Yes', 'Active', 2, 'Pending', 'Not Approved', '0000-00-00', 600.00),
(9, 1, 9, 0, 52200, '', '', '', '', '2015-12-24 05:23:25', '2015-12-24 02:23:25', 'Single Toilet', 'Yes', 'Active', 9, 'Pending', 'Not Approved', '0000-00-00', 200.00),
(10, 1, 9, 0, 123000, '', '', '', '', '2015-12-24 05:23:25', '2015-12-24 02:23:25', 'Trailers', 'Yes', 'Active', 10, 'Pending', 'Not Approved', '0000-00-00', 800.00);

-- --------------------------------------------------------

--
-- Table structure for table `title`
--

CREATE TABLE `title` (
`title_id` int(11) NOT NULL,
  `title_name` varchar(45) DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `title`
--

INSERT INTO `title` (`title_id`, `title_name`, `date_added`) VALUES
(1, 'Brgd', '2015-03-11 07:53:30'),
(2, 'Cardinal', '2015-03-11 07:53:30'),
(3, 'Dr', '2015-03-11 07:53:30'),
(4, 'Fr', '2015-03-11 07:53:30'),
(5, 'Lt', '2015-03-11 07:53:30'),
(6, 'Lt  Col', '2015-03-11 07:53:30'),
(7, 'Mr', '2015-03-11 07:53:30'),
(8, 'Miss', '2015-03-11 07:53:30'),
(9, 'Mrs', '2015-03-11 07:53:30'),
(10, 'Ms', '2015-03-11 07:53:30'),
(11, 'Prof', '2015-03-11 07:53:30'),
(12, 'Rabbi', '2015-03-11 07:53:30'),
(13, 'Rev', '2015-03-11 07:53:30'),
(14, 'Sir', '2015-03-11 07:53:30'),
(15, 'Sister', '2015-03-11 07:53:30');

-- --------------------------------------------------------

--
-- Stand-in structure for view `total_clients`
--
CREATE TABLE `total_clients` (
`total_clients` bigint(21)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `total_expenses`
--
CREATE TABLE `total_expenses` (
`total_expenses` decimal(35,2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `total_jobs`
--
CREATE TABLE `total_jobs` (
`total_jobs` bigint(21)
);
-- --------------------------------------------------------

--
-- Table structure for table `total_job_card_cost`
--

CREATE TABLE `total_job_card_cost` (
`id` int(11) NOT NULL,
  `job_card_id` varchar(255) NOT NULL,
  `amount_cr` decimal(13,2) NOT NULL,
  `amount_dr` decimal(13,2) NOT NULL,
  `date_added` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','In Active') NOT NULL,
  `pymnt_code` text NOT NULL,
  `pymnt_mthd` varchar(255) NOT NULL,
  `approved` enum('Not Approved','Approved') NOT NULL,
  `payment_date` date NOT NULL,
  `discount` decimal(13,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `total_job_card_cost`
--

INSERT INTO `total_job_card_cost` (`id`, `job_card_id`, `amount_cr`, `amount_dr`, `date_added`, `timestamp`, `status`, `pymnt_code`, `pymnt_mthd`, `approved`, `payment_date`, `discount`) VALUES
(1, '4', 60100.00, 0.00, '2015-12-22 22:04:00', '2015-12-22 19:04:00', 'Active', '', '', 'Not Approved', '0000-00-00', 15000.00),
(2, '5', 107200.00, 0.00, '2015-12-23 08:34:33', '2015-12-23 05:34:33', 'Active', '', '', 'Not Approved', '0000-00-00', 17200.00),
(3, '6', 268000.00, 0.00, '2015-12-23 08:36:32', '2015-12-23 05:36:32', 'Active', '', '', 'Not Approved', '0000-00-00', 13000.00),
(4, '8', 47800.00, 0.00, '2015-12-24 05:10:24', '2015-12-24 02:10:24', 'Active', '', '', 'Not Approved', '0000-00-00', 800.00),
(5, '9', 175200.00, 0.00, '2015-12-24 05:23:25', '2015-12-24 02:23:25', 'Active', '', '', 'Not Approved', '0000-00-00', 1000.00);

--
-- Triggers `total_job_card_cost`
--
DELIMITER //
CREATE TRIGGER `trg_payment_report` AFTER INSERT ON `total_job_card_cost`
 FOR EACH ROW INSERT INTO trgr_total_job_card_cost
   ( id_2,
     job_card_id,
    amount_cr,
     amount_dr,
   date_added,
   timestamp,
   status,
   pymnt_code,
   pymnt_mthd,
   approved)
   VALUES
   ( NEW.id,NEW.job_card_id,NEW.amount_cr,NEW.amount_dr,NEW.date_added,NEW.timestamp,NEW.status,NEW.pymnt_code,NEW.pymnt_mthd,NEW.approved)
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `trg_payment_report_update` AFTER UPDATE ON `total_job_card_cost`
 FOR EACH ROW INSERT INTO trgr_total_job_card_cost
   ( id_2,
     job_card_id,
    amount_cr,
     amount_dr,
   date_added,
   timestamp,
   status,
   pymnt_code,
   pymnt_mthd,
   approved)
   VALUES
   ( NEW.id,NEW.job_card_id,NEW.amount_cr,NEW.amount_dr,NEW.date_added,NEW.timestamp,NEW.status,NEW.pymnt_code,NEW.pymnt_mthd,NEW.approved)
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `total_revenue`
--
CREATE TABLE `total_revenue` (
`total_revenue` decimal(35,2)
);
-- --------------------------------------------------------

--
-- Table structure for table `trgr_total_job_card_cost`
--

CREATE TABLE `trgr_total_job_card_cost` (
`id` int(11) NOT NULL,
  `id_2` int(11) NOT NULL,
  `job_card_id` varchar(255) NOT NULL,
  `amount_cr` decimal(13,2) NOT NULL,
  `amount_dr` decimal(13,2) NOT NULL,
  `date_added` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','In Active') NOT NULL,
  `pymnt_code` text NOT NULL,
  `pymnt_mthd` varchar(255) NOT NULL,
  `approved` enum('Not Approved','Approved') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trgr_total_job_card_cost`
--

INSERT INTO `trgr_total_job_card_cost` (`id`, `id_2`, `job_card_id`, `amount_cr`, `amount_dr`, `date_added`, `timestamp`, `status`, `pymnt_code`, `pymnt_mthd`, `approved`) VALUES
(1, 1, '4', 60100.00, 0.00, '2015-12-22 22:04:00', '2015-12-22 19:04:00', 'Active', '', '', 'Not Approved'),
(2, 1, '4', 60100.00, 0.00, '2015-12-22 22:04:00', '2015-12-22 19:04:00', 'Active', '', '', 'Not Approved'),
(3, 2, '5', 107200.00, 0.00, '2015-12-23 08:34:33', '2015-12-23 05:34:33', 'Active', '', '', 'Not Approved'),
(4, 3, '6', 268000.00, 0.00, '2015-12-23 08:36:32', '2015-12-23 05:36:32', 'Active', '', '', 'Not Approved'),
(5, 4, '8', 47800.00, 0.00, '2015-12-24 05:10:24', '2015-12-24 02:10:24', 'Active', '', '', 'Not Approved'),
(6, 5, '9', 175200.00, 0.00, '2015-12-24 05:23:25', '2015-12-24 02:23:25', 'Active', '', '', 'Not Approved');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
`id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` enum('Active','In Active') NOT NULL,
  `timestmap` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `email`, `password`, `status`, `timestmap`) VALUES
(1, 'Admin', 'info@uniqueloo.co.ke', 'f9277477fc6238b01456aa4084edb202', 'Active', '2015-11-16 18:13:30'),
(2, 'Paul', 'paulmajaga@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Active', '2015-12-23 09:49:58');

-- --------------------------------------------------------

--
-- Stand-in structure for view `user_permissions_list`
--
CREATE TABLE `user_permissions_list` (
`permission_id` int(11)
,`user_id` int(11)
,`function_id` int(11)
,`permission_status` enum('Active','In Active')
,`functions_id` int(11)
,`function_name` varchar(255)
,`code` text
,`function_status` enum('Active','In Active')
,`level` int(11)
,`controller_name` text
,`functions_name` text
,`i_tag` text
,`description` text
,`span_tag` text
);
-- --------------------------------------------------------

--
-- Structure for view `active_job_cards`
--
DROP TABLE IF EXISTS `active_job_cards`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `active_job_cards` AS select month(`job_card`.`dte_evnt`) AS `event_month`,year(`job_card`.`dte_evnt`) AS `event_year`,count(`job_card`.`status`) AS `event_status_count`,`job_card`.`id` AS `id` from `job_card` where (`job_card`.`status` = 'Active') group by month(`job_card`.`dte_evnt`),year(`job_card`.`dte_evnt`);

-- --------------------------------------------------------

--
-- Structure for view `active_users`
--
DROP TABLE IF EXISTS `active_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `active_users` AS select count(`login_logs`.`login_logs_id`) AS `active_users` from `login_logs` where (`login_logs`.`is_active` = 'Active');

-- --------------------------------------------------------

--
-- Structure for view `billed_client_info`
--
DROP TABLE IF EXISTS `billed_client_info`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `billed_client_info` AS select `client`.`id` AS `client_id`,`client`.`name` AS `client_name`,`client`.`address` AS `client_address`,`client`.`website` AS `client_website`,`client`.`industry` AS `client_industry`,`client`.`phone_no` AS `client_phone_no`,`client`.`email` AS `client_email`,`client`.`status` AS `client_status`,`job_card`.`job_card_no` AS `job_card_no`,`job_card`.`invoice_no` AS `invoice_no`,`job_card`.`id` AS `job_card_id` from ((`client` join `job_card` on((`job_card`.`clnt_id` = `client`.`id`))) join `statement` on((`statement`.`job_card_id` = `job_card`.`id`))) group by `client`.`id`;

-- --------------------------------------------------------

--
-- Structure for view `cancelled_job_cards`
--
DROP TABLE IF EXISTS `cancelled_job_cards`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cancelled_job_cards` AS select month(`job_card`.`dte_evnt`) AS `event_month`,year(`job_card`.`dte_evnt`) AS `event_year`,count(`job_card`.`status`) AS `event_status_count` from `job_card` where (`job_card`.`status` = 'Cancelled') group by month(`job_card`.`dte_evnt`),year(`job_card`.`dte_evnt`);

-- --------------------------------------------------------

--
-- Structure for view `client_payment_list`
--
DROP TABLE IF EXISTS `client_payment_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `client_payment_list` AS select `statement`.`description` AS `asset_name`,`statement`.`qty` AS `qty`,`statement`.`amnt_cr` AS `amount_charged`,`statement`.`amnt_dr` AS `amount_paid`,`statement`.`discount` AS `discount`,(`statement`.`amnt_cr` - `statement`.`amnt_dr`) AS `balance`,`statement`.`pymnt_mthd` AS `payment_method`,`statement`.`pymnt_code` AS `payment_code`,`job_card`.`job_card_no` AS `job_card_no`,`statement`.`job_card_id` AS `job_card_id`,`statement`.`invoice_no` AS `invoice_no`,`statement`.`id` AS `statement_id`,`job_card`.`clnt_id` AS `client_id`,`statement`.`status` AS `status`,`standards`.`parameter` AS `parameter`,`standards`.`value` AS `value` from ((`statement` join `job_card` on((`job_card`.`id` = `statement`.`job_card_id`))) join `standards` on((`standards`.`parameter` = `statement`.`description`)));

-- --------------------------------------------------------

--
-- Structure for view `client_stmnt_accnts`
--
DROP TABLE IF EXISTS `client_stmnt_accnts`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `client_stmnt_accnts` AS select `client`.`name` AS `client_name`,sum(`total_job_card_cost`.`amount_dr`) AS `amount_paid`,sum((`total_job_card_cost`.`amount_cr` - `total_job_card_cost`.`discount`)) AS `amount_billed`,date_format(`total_job_card_cost`.`timestamp`,'%T, %d-%b-%Y') AS `last_payment_date`,sum(`total_job_card_cost`.`discount`) AS `discount`,count(`job_card`.`id`) AS `no_of_jobs` from ((`client` join `job_card` on((`job_card`.`clnt_id` = `client`.`id`))) join `total_job_card_cost` on((`total_job_card_cost`.`job_card_id` = `job_card`.`id`))) group by `client`.`id`;

-- --------------------------------------------------------

--
-- Structure for view `clnt_pymnt_stmnt_rprt`
--
DROP TABLE IF EXISTS `clnt_pymnt_stmnt_rprt`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `clnt_pymnt_stmnt_rprt` AS select `client`.`id` AS `client_id`,`client`.`name` AS `client_name`,`client`.`phone_no` AS `phone_no`,`job_card`.`id` AS `job_card_id`,`job_card`.`job_card_no` AS `job_card_no`,`job_card`.`invoice_no` AS `invoice_no`,date_format(`job_card`.`date_added`,'%T, %d-%b-%Y') AS `date_added`,month(`job_card`.`date_added`) AS `month`,year(`job_card`.`date_added`) AS `year`,sum(`trgr_total_job_card_cost`.`amount_cr`) AS `amount_charged` from ((`client` join `job_card` on((`job_card`.`clnt_id` = `client`.`id`))) join `trgr_total_job_card_cost` on((`trgr_total_job_card_cost`.`job_card_id` = `job_card`.`id`))) group by `job_card`.`job_card_no` order by `job_card`.`id` desc;

-- --------------------------------------------------------

--
-- Structure for view `cln_pymnt_stmnt_rprt`
--
DROP TABLE IF EXISTS `cln_pymnt_stmnt_rprt`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cln_pymnt_stmnt_rprt` AS select `client`.`id` AS `client_id`,`client`.`name` AS `client_name`,`client`.`phone_no` AS `phone_no`,`job_card`.`id` AS `job_card_id`,`job_card`.`job_card_no` AS `job_card_no`,`job_card`.`invoice_no` AS `invoice_no`,`job_card`.`date_added` AS `date_added`,sum(`statement`.`amnt_cr`) AS `amount_charged` from ((`client` join `job_card` on((`job_card`.`clnt_id` = `client`.`id`))) join `statement` on((`statement`.`job_card_id` = `job_card`.`id`))) group by `job_card`.`job_card_no`;

-- --------------------------------------------------------

--
-- Structure for view `daily_expense_report`
--
DROP TABLE IF EXISTS `daily_expense_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `daily_expense_report` AS select `job_card`.`id` AS `job_card_id`,`client`.`id` AS `client_id`,`client`.`name` AS `client_name`,`job_card`.`job_card_no` AS `job_card_no`,`job_card`.`invoice_no` AS `invoice_no`,sum(`assgnd_rsces`.`value`) AS `total_expense`,year(`job_card`.`date_added`) AS `year`,month(`job_card`.`date_added`) AS `month` from ((`job_card` join `client` on((`client`.`id` = `job_card`.`clnt_id`))) join `assgnd_rsces` on((`assgnd_rsces`.`job_card_id` = `job_card`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `daily_revenue_report`
--
DROP TABLE IF EXISTS `daily_revenue_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `daily_revenue_report` AS select `job_card`.`id` AS `job_card_id`,`client`.`id` AS `client_id`,`client`.`name` AS `client_name`,`job_card`.`job_card_no` AS `job_card_no`,`job_card`.`invoice_no` AS `invoice_no`,sum(`statement`.`amnt_dr`) AS `total_revenue`,year(`job_card`.`date_added`) AS `year`,month(`job_card`.`date_added`) AS `month` from ((`job_card` join `client` on((`client`.`id` = `job_card`.`clnt_id`))) join `statement` on((`statement`.`job_card_id` = `job_card`.`id`))) group by `statement`.`job_card_id`;

-- --------------------------------------------------------

--
-- Structure for view `expenses_incurred`
--
DROP TABLE IF EXISTS `expenses_incurred`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `expenses_incurred` AS select sum(`assgnd_rsces`.`value`) AS `total_expenses` from `assgnd_rsces` group by `assgnd_rsces`.`job_card_id`;

-- --------------------------------------------------------

--
-- Structure for view `expenses_per_job_card`
--
DROP TABLE IF EXISTS `expenses_per_job_card`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `expenses_per_job_card` AS select `client`.`id` AS `client_id`,`client`.`name` AS `client_name`,`job_card`.`id` AS `job_card_id`,`job_card`.`job_card_no` AS `job_card_no`,`job_card`.`invoice_no` AS `invoice_no`,sum(`assgnd_rsces`.`value`) AS `expense` from ((`job_card` join `client` on((`client`.`id` = `job_card`.`clnt_id`))) join `assgnd_rsces` on((`assgnd_rsces`.`job_card_id` = `job_card`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `expense_per_client`
--
DROP TABLE IF EXISTS `expense_per_client`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `expense_per_client` AS select `client`.`id` AS `client_id`,`client`.`name` AS `client_name`,`client`.`industry` AS `industry`,sum(`assgnd_rsces`.`value`) AS `total_expense` from ((`assgnd_rsces` join `job_card` on((`job_card`.`id` = `assgnd_rsces`.`job_card_id`))) join `client` on((`client`.`id` = `job_card`.`clnt_id`)));

-- --------------------------------------------------------

--
-- Structure for view `expense_per_month`
--
DROP TABLE IF EXISTS `expense_per_month`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `expense_per_month` AS select month(`job_card`.`dte_evnt`) AS `expense_month`,year(`job_card`.`dte_evnt`) AS `expense_year`,sum(`assgnd_rsces`.`value`) AS `expense` from (`assgnd_rsces` left join `job_card` on((`job_card`.`id` = `assgnd_rsces`.`job_card_id`))) group by month(`job_card`.`dte_evnt`);

-- --------------------------------------------------------

--
-- Structure for view `expense_ytd`
--
DROP TABLE IF EXISTS `expense_ytd`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `expense_ytd` AS select sum(`assgnd_rsces`.`value`) AS `expense`,year(`assgnd_rsces`.`date_added`) AS `year` from (`assgnd_rsces` join `job_card` on((`job_card`.`id` = `assgnd_rsces`.`job_card_id`))) where (year(`assgnd_rsces`.`date_added`) = year(curdate()));

-- --------------------------------------------------------

--
-- Structure for view `in_active_job_cards`
--
DROP TABLE IF EXISTS `in_active_job_cards`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `in_active_job_cards` AS select month(`job_card`.`dte_evnt`) AS `event_month`,year(`job_card`.`dte_evnt`) AS `event_year`,count(`job_card`.`status`) AS `event_status_count` from `job_card` where (`job_card`.`status` = 'In Active') group by month(`job_card`.`dte_evnt`),year(`job_card`.`dte_evnt`);

-- --------------------------------------------------------

--
-- Structure for view `job_cards_client`
--
DROP TABLE IF EXISTS `job_cards_client`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `job_cards_client` AS select `client`.`id` AS `client_id`,`client`.`name` AS `client_name`,`client`.`address` AS `client_address`,`client`.`industry` AS `client_industry`,`client`.`phone_no` AS `client_phone`,`client`.`email` AS `client_email`,`client`.`status` AS `client_status`,`job_card`.`id` AS `id`,`job_card`.`job_card_no` AS `job_card_no`,`job_card`.`invoice_no` AS `invoice_no` from (`job_card` join `client` on((`client`.`id` = `job_card`.`clnt_id`)));

-- --------------------------------------------------------

--
-- Structure for view `job_card_status`
--
DROP TABLE IF EXISTS `job_card_status`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `job_card_status` AS select `client`.`id` AS `client_id`,`client`.`name` AS `client_name`,`client`.`phone_no` AS `phone_no`,`job_card`.`id` AS `job_card_id`,`job_card`.`user_id` AS `user_id`,`job_card`.`job_card_no` AS `job_card_no`,`job_card`.`invoice_no` AS `invoice_no`,sum(`statement`.`amnt_dr`) AS `amount_paid`,sum(`statement`.`amnt_cr`) AS `amount_billed`,sum((`statement`.`amnt_cr` - `statement`.`amnt_dr`)) AS `amount_owed`,`job_card`.`status` AS `job_card_status`,`job_card`.`date_added` AS `date_opened`,`job_card`.`dte_evnt` AS `event_date`,`job_card`.`dte_srcd` AS `date_sourced` from ((`client` join `job_card` on((`job_card`.`clnt_id` = `client`.`id`))) join `statement` on((`statement`.`job_card_id` = `job_card`.`id`))) group by `statement`.`job_card_id`;

-- --------------------------------------------------------

--
-- Structure for view `monthly_expense`
--
DROP TABLE IF EXISTS `monthly_expense`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `monthly_expense` AS select sum(`assgnd_rsces`.`value`) AS `expense`,date_format(`job_card`.`dte_evnt`,'%d-%b-%Y') AS `expense_period`,date_format(`job_card`.`dte_evnt`,'%Y') AS `expense_year` from (`assgnd_rsces` join `job_card` on((`job_card`.`id` = `assgnd_rsces`.`job_card_id`))) where (month(`assgnd_rsces`.`date_added`) >= month(now()));

-- --------------------------------------------------------

--
-- Structure for view `monthly_revenue`
--
DROP TABLE IF EXISTS `monthly_revenue`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `monthly_revenue` AS select sum(`total_job_card_cost`.`amount_dr`) AS `revenues`,date_format(`job_card`.`dte_evnt`,'%d-%b-%Y') AS `revenue_period`,date_format(`job_card`.`dte_evnt`,'%Y') AS `revenue_year` from (`total_job_card_cost` join `job_card` on((`job_card`.`id` = `total_job_card_cost`.`job_card_id`))) where (month(`total_job_card_cost`.`date_added`) >= month(now()));

-- --------------------------------------------------------

--
-- Structure for view `performance`
--
DROP TABLE IF EXISTS `performance`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `performance` AS select distinct month(`job_card`.`dte_evnt`) AS `performance_month`,year(`job_card`.`dte_evnt`) AS `performance_year`,`total_job_card_cost`.`amount_dr` AS `performance_sales`,sum(`assgnd_rsces`.`value`) AS `performance_expense` from ((`job_card` join `total_job_card_cost` on((`total_job_card_cost`.`job_card_id` = `job_card`.`id`))) join `assgnd_rsces` on((`assgnd_rsces`.`job_card_id` = `job_card`.`id`))) group by month(`job_card`.`dte_evnt`),year(`job_card`.`dte_evnt`);

-- --------------------------------------------------------

--
-- Structure for view `revenue_collected`
--
DROP TABLE IF EXISTS `revenue_collected`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `revenue_collected` AS select sum(`statement`.`amnt_dr`) AS `total_revenue`,`statement`.`invoice_no` AS `invoice_no`,`statement`.`date_added` AS `date_added` from `statement` group by `statement`.`invoice_no`;

-- --------------------------------------------------------

--
-- Structure for view `revenue_per_client`
--
DROP TABLE IF EXISTS `revenue_per_client`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `revenue_per_client` AS select `client`.`id` AS `client_id`,`client`.`name` AS `client_name`,`client`.`phone_no` AS `phone_no`,`client`.`industry` AS `industry`,sum(`total_job_card_cost`.`amount_dr`) AS `revenue_collected` from ((`job_card` join `total_job_card_cost` on((`total_job_card_cost`.`job_card_id` = `job_card`.`id`))) join `client` on((`client`.`id` = `job_card`.`clnt_id`))) group by `job_card`.`clnt_id`;

-- --------------------------------------------------------

--
-- Structure for view `revenue_per_job_card`
--
DROP TABLE IF EXISTS `revenue_per_job_card`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `revenue_per_job_card` AS select `client`.`id` AS `client_id`,`client`.`name` AS `client_name`,`job_card`.`id` AS `job_card_id`,`job_card`.`job_card_no` AS `job_card_no`,`job_card`.`invoice_no` AS `invoice_no`,sum(`statement`.`amnt_dr`) AS `revenue` from ((`job_card` join `client` on((`client`.`id` = `job_card`.`clnt_id`))) join `statement` on((`statement`.`job_card_id` = `job_card`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `revenue_per_month`
--
DROP TABLE IF EXISTS `revenue_per_month`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `revenue_per_month` AS select month(`job_card`.`dte_evnt`) AS `revenue_month`,year(`job_card`.`dte_evnt`) AS `revenue_year`,sum(`total_job_card_cost`.`amount_dr`) AS `revenue` from (`total_job_card_cost` left join `job_card` on((`total_job_card_cost`.`job_card_id` = `job_card`.`id`))) group by month(`job_card`.`dte_evnt`);

-- --------------------------------------------------------

--
-- Structure for view `revenue_ytd`
--
DROP TABLE IF EXISTS `revenue_ytd`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `revenue_ytd` AS select sum(`statement`.`amnt_dr`) AS `revenue`,year(`statement`.`date_added`) AS `year` from (`job_card` join `statement` on((`statement`.`job_card_id` = `job_card`.`id`))) where (year(`statement`.`date_added`) = year(curdate()));

-- --------------------------------------------------------

--
-- Structure for view `sales_report`
--
DROP TABLE IF EXISTS `sales_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sales_report` AS select concat(`employee`.`f_name`,' ',`employee`.`s_name`,' ',`employee`.`o_name`) AS `employee_name`,`employee`.`employee_no` AS `employee_no`,`job_card`.`id` AS `job_card_id`,`job_card`.`job_card_no` AS `job_card_no`,`job_card`.`invoice_no` AS `invoice_no`,`total_job_card_cost`.`amount_dr` AS `amount_paid`,(`total_job_card_cost`.`amount_cr` - `total_job_card_cost`.`discount`) AS `amount_billed`,`total_job_card_cost`.`discount` AS `discount`,date_format(`job_card`.`date_added`,'%T, %d-%b-%Y') AS `date_added`,date_format(`job_card`.`dte_evnt`,'%T, %d-%b-%Y') AS `event_date`,`client`.`name` AS `client_name`,`client`.`id` AS `client_id` from (((`job_card` join `employee` on((`employee`.`id` = `job_card`.`user_id`))) join `total_job_card_cost` on((`total_job_card_cost`.`job_card_id` = `job_card`.`id`))) join `client` on((`client`.`id` = `job_card`.`clnt_id`)));

-- --------------------------------------------------------

--
-- Structure for view `total_clients`
--
DROP TABLE IF EXISTS `total_clients`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `total_clients` AS select count(`client`.`id`) AS `total_clients` from `client` where (`client`.`status` = 'Active');

-- --------------------------------------------------------

--
-- Structure for view `total_expenses`
--
DROP TABLE IF EXISTS `total_expenses`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `total_expenses` AS select sum(`assgnd_rsces`.`value`) AS `total_expenses` from `assgnd_rsces` where ((`assgnd_rsces`.`status` = 'Active') and (year(`assgnd_rsces`.`date_added`) >= year(now())));

-- --------------------------------------------------------

--
-- Structure for view `total_jobs`
--
DROP TABLE IF EXISTS `total_jobs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `total_jobs` AS select count(`job_card`.`id`) AS `total_jobs` from `job_card` where ((`job_card`.`status` = 'Active') and (year(`job_card`.`dte_evnt`) >= year(now())));

-- --------------------------------------------------------

--
-- Structure for view `total_revenue`
--
DROP TABLE IF EXISTS `total_revenue`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `total_revenue` AS select sum(`total_job_card_cost`.`amount_dr`) AS `total_revenue` from `total_job_card_cost` where ((`total_job_card_cost`.`status` = 'Active') and (year(`total_job_card_cost`.`date_added`) >= year(now())));

-- --------------------------------------------------------

--
-- Structure for view `user_permissions_list`
--
DROP TABLE IF EXISTS `user_permissions_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `user_permissions_list` AS select `permissions`.`id` AS `permission_id`,`permissions`.`user_id` AS `user_id`,`permissions`.`function_id` AS `function_id`,`permissions`.`status` AS `permission_status`,`functions`.`id` AS `functions_id`,`functions`.`name` AS `function_name`,`functions`.`code` AS `code`,`functions`.`status` AS `function_status`,`functions`.`level` AS `level`,`functions`.`controller_name` AS `controller_name`,`functions`.`function_name` AS `functions_name`,`functions`.`i_tag` AS `i_tag`,`functions`.`description` AS `description`,`functions`.`span_tag` AS `span_tag` from (`functions` join `permissions` on((`permissions`.`function_id` = `functions`.`id`)));

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset`
--
ALTER TABLE `asset`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `number` (`number`);

--
-- Indexes for table `asset_tracker`
--
ALTER TABLE `asset_tracker`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_type`
--
ALTER TABLE `asset_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assgnd_rsces`
--
ALTER TABLE `assgnd_rsces`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audit_trail`
--
ALTER TABLE `audit_trail`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
 ADD PRIMARY KEY (`id`), ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `conf`
--
ALTER TABLE `conf`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `functions`
--
ALTER TABLE `functions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_card`
--
ALTER TABLE `job_card`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_card_no`
--
ALTER TABLE `job_card_no`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_logs`
--
ALTER TABLE `login_logs`
 ADD PRIMARY KEY (`login_logs_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `standards`
--
ALTER TABLE `standards`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statement`
--
ALTER TABLE `statement`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `title`
--
ALTER TABLE `title`
 ADD PRIMARY KEY (`title_id`);

--
-- Indexes for table `total_job_card_cost`
--
ALTER TABLE `total_job_card_cost`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trgr_total_job_card_cost`
--
ALTER TABLE `trgr_total_job_card_cost`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset`
--
ALTER TABLE `asset`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `asset_tracker`
--
ALTER TABLE `asset_tracker`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `asset_type`
--
ALTER TABLE `asset_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `assgnd_rsces`
--
ALTER TABLE `assgnd_rsces`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `audit_trail`
--
ALTER TABLE `audit_trail`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `conf`
--
ALTER TABLE `conf`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=251;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `functions`
--
ALTER TABLE `functions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `job_card`
--
ALTER TABLE `job_card`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `job_card_no`
--
ALTER TABLE `job_card_no`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `login_logs`
--
ALTER TABLE `login_logs`
MODIFY `login_logs_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=470;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `standards`
--
ALTER TABLE `standards`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `statement`
--
ALTER TABLE `statement`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `title`
--
ALTER TABLE `title`
MODIFY `title_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `total_job_card_cost`
--
ALTER TABLE `total_job_card_cost`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `trgr_total_job_card_cost`
--
ALTER TABLE `trgr_total_job_card_cost`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
